# Webgis

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.1.4.

## Development server

Run `npm start` for a dev server. The app will automatically reload if you change any of the source files.
(*) See proxy.config.json if any problem with CORS

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

Components must usr -c "onPush" to set changedetectionstrategy to "OnPush", anyway in angular.json is configured by default on "component"

## i18n
To unify all translation files we used `gulp` to generate a file for each language supported by the application. Run `gulp i18n`.

## documentation
Run `npm run compodoc` to see all documentation

## Bundle´s size
Run `npm run bundle-report` to see all bundles sizes.

## Build

Run `npm run build:prod` to build the project. The build artifacts will be stored in the `dist/` directory.

Run `npm run start:prod` to serve app on prod mode. ( Only debug/dev purpose )

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
