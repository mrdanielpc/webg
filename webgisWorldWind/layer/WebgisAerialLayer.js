/*
 * Copyright (C) 2014 United States Government as represented by the Administrator of the
 * National Aeronautics and Space Administration. All Rights Reserved.
 */
/**
 * @exports WebgisAerialLayer
 * @version $Id: WebgisAerialLayer.js 2883 2015-03-06 19:04:42Z tgaskins $
 */
define([
    '../geom/Location',
    '../geom/Sector',
    '../layer/WebgisTiledImageLayer',
    '../util/WebgisImageryUrlBuilder'
],
    function (Location,
        Sector,
        WebgisTiledImageLayer,
        WebgisImageryUrlBuilder) {
        "use strict";

        /**
         * Constructs a Webgis Aerial layer.
         * @alias WebgisAerialLayer
         * @constructor
         * @augments WebgisTiledImageLayer
         * @classdesc Displays the Webgis Aerial layer.
         * See also {@link WebgisAerialWithLabelsLayer} and {@link WebgisRoadsLayer}.
         *
         * @param {String} bingMapsKey The Webgis Maps key to use for the image requests. If null or undefined, the key at
         * WorldWind.WebgisMapsKey is used. If that is null or undefined, the default World Wind Webgis Maps key is used,
         * but this fallback is provided only for non-production use. If you are using Web World Wind in an app or a
         * web page, you must obtain your own key from the
         * [Webgis Maps Portal]{@link https://www.microsoft.com/maps/choose-your-bing-maps-API.aspx}
         * and either pass it as a parameter to this constructor or specify it as the property WorldWind.WebgisMapsKey.
         */
        var WebgisAerialLayer = function (bingMapsKey) {
            WebgisTiledImageLayer.call(this, "Aerial");

            this.urlBuilder = new WebgisImageryUrlBuilder("Aerial", bingMapsKey);
        };

        WebgisAerialLayer.prototype = Object.create(WebgisTiledImageLayer.prototype);

        return WebgisAerialLayer;
    });