/*
 * Copyright (C) 2014 United States Government as represented by the Administrator of the
 * National Aeronautics and Space Administration. All Rights Reserved.
 */
/**
 * @exports WebgisRoadsLayer
 * @version $Id: WebgisRoadsLayer.js 2883 2015-03-06 19:04:42Z tgaskins $
 */
define([
    '../geom/Location',
    '../geom/Sector',
    '../layer/WebgisTiledImageLayer',
    '../util/WebgisImageryUrlBuilder'
],
    function (Location,
        Sector,
        WebgisTiledImageLayer,
        WebgisImageryUrlBuilder) {
        "use strict";

        /**
         * Constructs a Webgis Roads layer.
         * @alias WebgisRoadsLayer
         * @constructor
         * @augments WebgisTiledImageLayer
         * @classdesc Displays a Webgis Roads layer.
         * See also {@link WebgisAerialLayer} and {@link WebgisAerialWithLabelsLayer}.
         *
         * @param {String} bingMapsKey The Webgis Maps key to use for the image requests. If null or undefined, the key at
         * WorldWind.WebgisMapsKey is used. If that is null or undefined, the default World Wind Webgis Maps key is used,
         * but this fallback is provided only for non-production use. If you are using Web World Wind in an app or a
         * web page, you must obtain your own key from the
         * [Webgis Maps Portal]{@link https://www.microsoft.com/maps/choose-your-bing-maps-API.aspx}
         * and either pass it as a parameter to this constructor or specify it as the property WorldWind.WebgisMapsKey.
         */
        var WebgisRoadsLayer = function (bingMapsKey) {
            WebgisTiledImageLayer.call(this, "Roads");

            this.urlBuilder = new WebgisImageryUrlBuilder("Road", bingMapsKey);

            // Disable blank-image detection.
            this.detectBlankImages = false;
        };

        WebgisRoadsLayer.prototype = Object.create(WebgisTiledImageLayer.prototype);

        return WebgisRoadsLayer;
    });