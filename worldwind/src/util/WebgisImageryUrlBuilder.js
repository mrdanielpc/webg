/*
 * Copyright (C) 2014 United States Government as represented by the Administrator of the
 * National Aeronautics and Space Administration. All Rights Reserved.
 */
/**
 * @exports WebgisImageryUrlBuilder
 * @version $Id: WebgisImageryUrlBuilder.js 3094 2015-05-14 23:02:03Z tgaskins $
 */
define([
    '../error/ArgumentError',
    '../util/Logger',
    '../util/WWUtil'
],
    function (ArgumentError,
        Logger,
        WWUtil) {
        "use strict";

        /**
         * Constructs a URL builder for Webgis imagery.
         * @alias WebgisImageryUrlBuilder
         * @constructor
         * @classdesc Provides a factory to create URLs for Webgis image requests.
         * @param {String} imagerySet The name of the imagery set to display.
         * @param {String} bingMapsKey The Webgis Maps key to use for the image requests. If null or undefined, the key at
         * [WorldWind.WebgisMapsKey]{@link WorldWind#WebgisMapsKey} is used. If that is null or undefined, the default
         * World Wind Webgis Maps key is used,
         * but this fallback is provided only for non-production use. If you are using Web World Wind in an app or a
         * web page, you must obtain your own key from the
         * [Webgis Maps Portal]{@link https://www.microsoft.com/maps/choose-your-bing-maps-API.aspx}
         * and either pass it as a parameter to this constructor or specify it as the property
         * [WorldWind.WebgisMapsKey]{@link WorldWind#WebgisMapsKey}.
         */
        var WebgisImageryUrlBuilder = function (imagerySet, bingMapsKey) {
            this.imagerySet = imagerySet;
        };

        WebgisImageryUrlBuilder.prototype.requestMetadata = function () {
            // Retrieve the metadata for the imagery set.

            if (!this.metadataRetrievalInProcess) {
                this.metadataRetrievalInProcess = true;

                var url = "https://dev.virtualearth.net/REST/V1/Imagery/Metadata/" + this.imagerySet + "/0,0?zl=1&key="
                    + this.bingMapsKey;

                // Use JSONP to request the metadata. Can't use XmlHTTPRequest because the virtual earth server doesn't
                // allow cross-origin requests for metadata retrieval.

                var thisObject = this;
                WWUtil.jsonp(url, "jsonp", function (jsonData) {
                    thisObject.imageUrl = jsonData.resourceSets[0].resources[0].imageUrl;

                    // Send an event to request a redraw.
                    var e = document.createEvent('Event');
                    e.initEvent(WorldWind.REDRAW_EVENT_TYPE, true, true);
                    window.dispatchEvent(e);

                    thisObject.metadataRetrievalInProcess = false;
                });

            }
        };

        /**
         * Creates the URL string for a Webgis Maps request.
         * @param {Tile} tile The tile for which to create the URL.
         * @param {String} imageFormat This argument is not used.
         * @return {String} The URL for the specified tile.
         * @throws {ArgumentError} If the specified tile is null or undefined.
         */
        WebgisImageryUrlBuilder.prototype.urlForTile = function (tile, imageFormat) {
            if (!tile) {
                throw new ArgumentError(
                    Logger.logMessage(Logger.LEVEL_SEVERE, "WebgisImageryUrlBuilder", "urlForTile", "missingTile"));
            }

            return WWUtil.getWebgisTileUrl() + '?imagery=' + this.imagerySet
                + '&level=' + tile.level.levelNumber
                + '&row=' + tile.row
                + '&column=' + tile.column;
        };

        // Intentionally not documented.
        WebgisImageryUrlBuilder.prototype.quadKeyFromLevelRowColumn = function (levelNumber, row, column) {
            var digit, mask, key = "";

            for (var i = levelNumber + 1; i > 0; i--) {
                digit = 0;
                mask = 1 << (i - 1);

                if ((column & mask) != 0) {
                    digit += 1;
                }

                if ((row & mask) != 0) {
                    digit += 2;
                }

                key += digit.toString();
            }

            return key;
        };

        return WebgisImageryUrlBuilder;
    });
