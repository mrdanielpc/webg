/**
 * @exports WebgisElevationCoverage
 */
define([
    '../geom/Sector',
    '../globe/TiledElevationCoverage',
    '../util/WmsUrlBuilder',
    '../util/WWUtil'
],
    function (
        Sector,
        TiledElevationCoverage,
        WmsUrlBuilder,
        WWUtil
    ) {
        "use strict";

        /**
         * Constructs an Earth elevation coverage using GEBCO data.
         * @alias WebgisElevationCoverage
         * @constructor
         * @augments TiledElevationCoverage
         * @classdesc Provides elevations for Earth. Elevations are drawn from the custom elevation service.
         */
        var WebgisElevationCoverage = function () {
            var url = WWUtil.getUrlElevation(),
                layer = WWUtil.getLayerElevation(),
                version = WWUtil.getVersion();

            TiledElevationCoverage.call(this, {
                coverageSector: Sector.FULL_SPHERE,
                resolution: 0.008333333333333,
                retrievalImageFormat: "application/bil16",
                minElevation: -11000,
                maxElevation: 8850,
                urlBuilder: new WmsUrlBuilder(url, layer, "", version)
            });

            this.displayName = "Webgis Earth Elevation Coverage";
        };

        WebgisElevationCoverage.prototype = Object.create(TiledElevationCoverage.prototype);

        return WebgisElevationCoverage;
    });