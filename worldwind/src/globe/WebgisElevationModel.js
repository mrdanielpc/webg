/**
 * @exports WebgisElevationModel
 */
define([
    './WebgisElevationCoverage',
    './ElevationModel',
    '../util/WWUtil'
],
    function (
        WebgisElevationCoverage,
        ElevationModel,
        WWUtil
    ) {
        "use strict";

        /**
         * Constructs an WebgisElevationModel consisting of two elevation coverages GEBCO, SRTM.
         * @alias WebgisElevationModel
         * @constructor
         */
        var WebgisElevationModel = function (urlElevation,layerElevation,version) {

            // WebGIS
            WWUtil.setUrlElevation(urlElevation);
            WWUtil.setLayerElevation(layerElevation);
            WWUtil.setVersion(version);  
                    
            ElevationModel.call(this);
            this.addCoverage(new WebgisElevationCoverage());
        };

        WebgisElevationModel.prototype = Object.create(ElevationModel.prototype);

        return WebgisElevationModel;
    });