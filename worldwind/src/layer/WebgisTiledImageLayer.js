/*
 * Copyright (C) 2014 United States Government as represented by the Administrator of the
 * National Aeronautics and Space Administration. All Rights Reserved.
 */
/**
 * @exports WebgisTiledImageLayer
 * @version $Id: WebgisTiledImageLayer.js 3120 2015-05-28 02:32:45Z tgaskins $
 */
define([
        '../geom/Angle',
        '../geom/Location',
        '../geom/Sector',
        '../layer/MercatorTiledImageLayer'
    ],
    function (Angle,
              Location,
              Sector,
              MercatorTiledImageLayer) {
        "use strict";

        /**
         * Constructs a base Webgis layer. This constructor is meant to be called only by subclasses.
         * @alias WebgisTiledImageLayer
         * @constructor
         * @augments MercatorTiledImageLayer
         * @classdesc Provides an abstract base layer for Webgis imagery. This class is not intended to be constructed
         * independently but as a base layer for subclasses.
         * See {@link WebgisAerialLayer}, {@link WebgisAerialWithLabelsLayer} and {@link WebgisRoadsLayer}.
         *
         * @param {String} displayName This layer's display name.
         */
        var WebgisTiledImageLayer = function (displayName) {
            this.imageSize = 256;

            MercatorTiledImageLayer.call(this,
                new Sector(-85.05, 85.05, -180, 180), new Location(85.05, 180), 23, "image/jpeg", displayName,
                this.imageSize, this.imageSize);

            this.displayName = displayName;
            this.pickEnabled = false;

            this.detectBlankImages = true;
        };

        WebgisTiledImageLayer.prototype = Object.create(MercatorTiledImageLayer.prototype);

        WebgisTiledImageLayer.prototype.doRender = function (dc) {
        	MercatorTiledImageLayer.prototype.doRender.call(this, dc);
        };

        // Overridden from TiledImageLayer.
        WebgisTiledImageLayer.prototype.createTopLevelTiles = function (dc) {
            this.topLevelTiles = [];

            this.topLevelTiles.push(this.createTile(null, this.levels.firstLevel(), 0, 0));
            this.topLevelTiles.push(this.createTile(null, this.levels.firstLevel(), 0, 1));
            this.topLevelTiles.push(this.createTile(null, this.levels.firstLevel(), 1, 0));
            this.topLevelTiles.push(this.createTile(null, this.levels.firstLevel(), 1, 1));
        };

        // Determines the Webgis map size for a specified level number.
        WebgisTiledImageLayer.prototype.mapSizeForLevel = function (levelNumber) {
            return 256 << (levelNumber + 1);
        };

        return WebgisTiledImageLayer;
    }
)
;