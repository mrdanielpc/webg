export const environment = {
    production: false,
    enableTracing: false,
    apiUrl: 'api/',
    apiLogin: 'login',
    spinner: 100,
    roleAccessAdminModule: 'ROLE_ADMIN',
    roleAccessMap: 'ROLE_USER',
    toast: {
        duration: 5000,
        verticalPosition: 'bottom',
        horizontalPosition: 'end'
    },
    mustRefreshToken: true,
    forceRefreshToken: 300000,
    defaultI18nLang: 'gl',
    debounceModel: 700,
    httpAvoidMessages: [
        'webgisApp.wgMapRole.deleted',
        'webgisApp.wgMapRole.created',
        'webgisApp.wgMapRole.updated',
        'webgisApp.wgMapGroup.deleted',
        'webgisApp.wgMapGroup.created',
        'webgisApp.wgMapGroup.updated',
        'webgisApp.wg-markertype-roles.deleted',
        'webgisApp.wg-markertype-roles.created',
        'webgisApp.wg-markertype-roles.updated',
        'webgisApp.wg-markertype-groups.deleted',
        'webgisApp.wg-markertype-groups.created',
        'webgisApp.wg-markertype-groups.updated',
        'webgisApp.wg-panel-roles.deleted',
        'webgisApp.wg-panel-roles.created',
        'webgisApp.wg-panel-roles.updated',
        'webgisApp.wg-panel-groups.deleted',
        'webgisApp.wg-panel-groups.created',
        'webgisApp.wg-panel-groups.updated',
        'webgisApp.wgPanel.updated',
    ],
    map: {
        zoomRange: [
            {
                zoom: 37217, // Vigo
                size: 1
            },
            {
                zoom: 524892, // Galicia
                size: 0.8
            },
            {
                zoom: 2317604, // España
                size: 0.6
            },
            {
                zoom: 6851126, // Europa
                size: 0.4
            }
        ],
        config: {
            baseUrl: 'assets/',
            gpuCacheSize: 500e6,
            layerRetrievalQueueSize: 20,
            coverageRetrievalQueueSize: 20,
            timeToRedraw: 200
        },
        location: {
            lat: 42.8166265,
            lng: -8.0841564
        },
        range: 508000,
        squareSize: 10,
        squareHighlight: 1.5,
        disabledOpacity: .3,
        pixelsToConsiderIsDragging: 10,
        markerAltitude: null,
        baseLayer: 'WebgisAerialWithLabelsLayer',
        transparencyTimeZoneLayer: 0
    },
    tools: {
        attributes: {
            color: '#06e6a3',
            width: 4,
            transparency: 1,
            interiorTransparency: .5
        },
        media: {
            daysInterval: 11,
            formatFile: 'jpeg',
            enableFalseLocation: true
        }
    },
    resource: {
        user: 'diego.noal',
        password: '9RHwQFNPqedcmB'
    },
    constants: {
        map: {
            LINE_STRING: 'LineString',
            FAKE_LINE: 'Fake LineString',
            ORIGINAL_POLYGON_POINT: 'Original polygon point',
            REGULAR_CENTER_POINT: 'Middle point regular polygon',
            TEMPORARY_POLYGON_LINE: 'Fake Line',
            PLACEMARK: 'Placemark',
            PLACEMARKSELECTED: 'PlacemarkSelected',
            TOOL_LAYER: 'Tool Layer',
            SHAPE_FROM_LINE: 'Shape from line',
            TOOL_POINT: 'Tool point',
            TOOL_MIDDLE_POINT: 'Tool middle point',
            FAKE_RECTANGLE: 'Fake Rectangle',
            LAYER_CIRCLE: 'Layer Circle',
            LAYER_SQUARE: 'Layer Square',
            LAYER_ELLIPSE: 'Layer Ellipse',
            LAYER_MEDIA: 'Layer Media',
            LAYER_TIME_ZONE: 'Layer Time Zone',
            projections: {
                '3D': '3D',
                EQUIRECTANGULAR: 'Equirectangular',
                MERCATOR: 'Mercator',
                NORTH_POLAR: 'North Polar',
                SOUTH_POLAR: 'South Polar',
                NORTH_UPS: 'North UPS',
                SOUTH_UPS: 'South UPS',
                NORTH_GNOMONIC: 'North Gnomonic',
                SOUTH_GNOMONIC: 'South Gnomonic',
                NORTH: 'North',
                SOUTH: 'South'
            }
        }
    }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
