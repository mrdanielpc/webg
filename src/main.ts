import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { environment } from '@environment/environment';
import 'hammerjs';
import { AppModule } from './app/app.module';
import '@app/extensions';

if (environment.production) {
    enableProdMode();
    window.console.log = () => { };
}

platformBrowserDynamic().bootstrapModule(AppModule)
    .catch(err => console.error(err));
