import { Component } from '@angular/core';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { SwUpdate, UpdateAvailableEvent } from '@angular/service-worker';

import { environment } from '@environment/environment';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    constructor(
        private matIconRegistry: MatIconRegistry,
        private domSanitizer: DomSanitizer,
        private swUpdate: SwUpdate,
    ) {
        if (this.swUpdate.isEnabled) {
            swUpdate.checkForUpdate();
            this.swUpdate.available.subscribe((event: UpdateAvailableEvent) => {
                if (confirm(`Do you want to update?`)) {
                    window.location.reload();
                }
            });
        }
        sessionStorage.setItem('apiUrl', environment.apiUrl);
        sessionStorage.setItem('apiLogin', environment.apiLogin);

        this.matIconRegistry.addSvgIcon(
            `grouplayer`,
            this.domSanitizer.bypassSecurityTrustResourceUrl(`assets/baseline-folder_layers.svg`)
        );

        this.matIconRegistry.addSvgIcon(
            `ballot`,
            this.domSanitizer.bypassSecurityTrustResourceUrl(`assets/baseline-ballot-24px.svg`)
        );

        this.matIconRegistry.addSvgIcon(
            `ellipse`,
            this.domSanitizer.bypassSecurityTrustResourceUrl(`assets/elipse.svg`)
        );

        this.matIconRegistry.addSvgIcon(
            `touch_app_off`,
            this.domSanitizer.bypassSecurityTrustResourceUrl(`assets/touch_app_off.svg`)
        );

    }
}
