import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environment/environment';
import { Observable } from 'rxjs';
import { IApiEnvironment } from './api-environment.model';

@Injectable({
    providedIn: 'root'
})
export class ApiEnvironmentService {

    config: IApiEnvironment;

    constructor(
        private http: HttpClient,
    ) {

    }

    private getAPIEnvironment(): Observable<IApiEnvironment> {
        return this.http.get<IApiEnvironment>(`${environment.apiUrl}front/config`);
    }

    getConfig(): IApiEnvironment {
        return this.config;
    }

    searchAPIEnvironment(): Promise<IApiEnvironment> {
        return new Promise<IApiEnvironment>((resolve, reject) => {
            if (!!this.config) {
                resolve(this.config);
            } else {
                this.getAPIEnvironment().toPromise()
                    .then((data: IApiEnvironment) => {
                        this.config = data;
                        resolve(data);
                    }, reject);
            }
        });
    }



}
