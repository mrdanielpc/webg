import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@environment/environment';

import { IUser } from '@app/core/user.model';
import { WebgisLanguageService } from './language/language.service';
import { LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { WebGisState } from '@app/redux/global.reducer';
import { Store } from '@ngrx/store';
import { LoadUser } from '@app/redux/user/user.actions';

@Injectable({
    providedIn: 'root'
})

export class UserService {

    user: IUser;

    constructor(
        private http: HttpClient,
        private languageService: WebgisLanguageService,
        private localStorage: LocalStorageService,
        private router: Router,
        private store: Store<WebGisState>,
    ) {
    }

    logout(): Promise<any> {
        return new Promise((resolve, reject) => {
            this.http.post(environment.apiUrl + 'logout', {})
                .toPromise()
                .then(() => {
                    // this.localStorage.clear('userLanguage');
                    sessionStorage.clear();
                    resolve();
                })
                .catch((err) => {
                    console.error(err);
                    reject(err);
                });
        });
    }

    getIdentity(force?: boolean): Promise<IUser> {
        return new Promise<IUser>((resolve, reject) => {
            if (force === true || !this.user) {
                this.http.get(environment.apiUrl + 'account')
                    .toPromise()
                    .then((response: IUser) => {
                        this.user = {
                            ...response,
                            langKey: !!response.langKey ? response.langKey.toString().toLocaleLowerCase() : environment.defaultI18nLang
                        };
                        // Si al cargar el usuario no tiene langKey, y en localStorage tampoco, se pone la de por defecto
                        const actualLang = this.languageService.getCurrent();
                        if (!actualLang) {
                            if (!!this.user.langKey) {
                                this.languageService.changeLanguage(this.user.langKey);
                            }
                        } else {
                            if (actualLang !== this.user.langKey) {
                                this.languageService.changeLanguage(this.user.langKey);
                            }
                        }

                        if (this.user.authorities.length === 0 || !this.user.authorities.includes(environment.roleAccessMap)) {
                            this.router.navigate(['no-user']);
                        } else {
                            this.store.dispatch(new LoadUser(this.user));
                        }

                        resolve(response);
                    })
                    .catch((err) => {
                        if (err.status === 403) {
                            if (!this.languageService.getCurrent()) {
                                this.languageService.changeLanguage(environment.defaultI18nLang);
                            }
                            this.router.navigate(['no-user']);
                        }
                        reject(err);
                    });
            } else if (!!this.user) {
                resolve(this.user);
            }

        });
    }

    getLanguage(): string {
        if (!!this.user) {
            return this.user.langKey;
        } else {
            return null;
        }
    }

    isAuthenticated(): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            this.getIdentity()
                .then((result) => {
                    resolve(!!result);
                })
                .catch((err) => {
                    // console.error(err);
                    resolve(false);
                });
        });
    }

    getToken(): string {
        return !!this.user ? this.user.accessToken : null;
    }

    setToken(token: string): void {
        this.user = {
            ...this.user,
            accessToken: token
        };
    }

    getServerProfileInfo() {
        return this.http.get(environment.apiUrl + 'account')
            .toPromise();
    }

}
