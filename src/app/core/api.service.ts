import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { take, map } from 'rxjs/operators';
import { Store } from '@ngrx/store';

import { environment } from '@environment/environment';

import { WebGisState } from '@app/redux/global.reducer';
import { LoadPanels } from '@app/redux/panels/panel.actions';
import { ILayer } from '@app/pages/main/components/layers/layers.model';
import { IPanel } from '@app/pages/admin/panel/panel.model';
import { IMapMarker } from '@app/pages/main/components/markers/markers.model';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    constructor(
        private http: HttpClient,
        private store: Store<WebGisState>,
    ) { }

    public getPanels() {
        const url = environment.apiUrl + 'account/panels';
        return this.http.get(url)
            .pipe(take(1))
            .subscribe((panel: IPanel[]) => {
                if (!!panel) {
                    this.store.dispatch(new LoadPanels(panel));
                }
            });
    }

    public getLayersList() {
        const url = environment.apiUrl + 'account/geolayers';
        return this.http.get(url)
            .pipe(map((data) => {
                return data || {
                    layers: [],
                    workspaces: []
                };
            }))
            .pipe(map((layer: ILayer) => {
                layer.layers.map((lay) => {
                    return lay.queryable = (lay.queryable === '1');
                });
                layer.workspaces.map((ws) => {
                    ws.layers.map((layWs) => {
                        return layWs.queryable = (layWs.queryable === '1');
                    });
                });
                return layer;
            }));
    }

    public getMakers(): Observable<IMapMarker[]> {
        return this.http.get<IMapMarker[]>(environment.apiUrl + 'account/markers');
    }

    public getTool() {
        return this.http.get('./app/tools.json');
    }

    public getServerXml(url: string): Observable<string> {
        return this.http.get(url, { responseType: 'text' });
    }

}
