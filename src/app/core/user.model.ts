export interface IUser {
    accessToken: string;
    id: string;
    login: string;
    firstName: string;
    lastName: string;
    email: string;
    imageUrl: string;
    activated: boolean;
    langKey: string;
    createdBy: string;
    createdDate: string;
    lastModifiedBy: string;
    lastModifiedDate: string;
    authorities: string[];
}

// {
// 	"jti": "5a6c692e-c87d-473a-9bc0-f5e3ee3a95c6",
// 	"exp": 1537868648,
// 	"nbf": 0,
// 	"iat": 1537868348,
// 	"iss": "http://193.144.33.201:8080/auth/realms/jhipster",
// 	"aud": "web_app",
// 	"sub": "63f68ddd-29a6-40a2-82f8-3df9241ce1d3",
// 	"typ": "Bearer",
// 	"azp": "web_app",
// 	"auth_time": 1537868348,
// 	"session_state": "363818f6-8506-49f9-9496-c89e421c3bc0",
// 	"acr": "1",
// 	"allowed-origins": [
// "",
// "http://localhost:8080/*",
// "http://localhost:8100/*",
// "http://127.0.0.1:8761/*",
// "https://openidconnect.net/*",
// "https://demo.c2id.com/*",
// "http://localhost:8000/*",
// "http://localhost:9000/*"],
// 	"realm_access": {
// 		"roles": ["offline_access", "uma_authorization"]
// 	},
// 	"resource_access": {
// 		"web_app": {
// 			"roles": ["ROLE_USER", "ROLE_ADMIN"]
// 		},
// 		"account": {
// 			"roles": ["manage-account", "manage-account-links", "view-profile"]
// 		}
// 	},
// 	"scope": "openid profile email",
// 	"email_verified": true,
// 	"name": "Admin Administrator",
// 	"preferred_username": "admin",
// 	"given_name": "Admin",
// 	"family_name": "Administrator",
// 	"email": "admin@localhost"
// }

export interface IUserJWT {
    jti: string;
    exp: number;
    nbf: number;
    iat: number;
    iss: string;
    aud: string;
    sub: string;
    typ: string;
    azp: string;
    auth_time: number;
    session_state: string;
    acr: string;
    'allowed-origins': string[];
    'realm_access': string[];
    'resource_access': {
        'web_app': {
            roles: string[];
        },
        account: {
            roles: string[];
        }
    };
    scope: string;
    'email_verified': boolean;
    name: string;
    'preferred_username': string;
    'given_name': string;
    'family_name': string;
    email: string;
}
