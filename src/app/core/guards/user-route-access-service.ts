import { Injectable } from '@angular/core';
import {
    ActivatedRouteSnapshot,
    CanActivate,
    RouterStateSnapshot,
    CanLoad,
    Route
} from '@angular/router';
import { Observable } from 'rxjs';
import { environment } from '@environment/environment';
import { UserService } from '@app/core/user.service';
import { IUser } from '../user.model';

@Injectable({
    providedIn: 'root'
})
export class UserRouteAccessService implements CanLoad, CanActivate {

    constructor(
        private userService: UserService,
    ) {
    }

    private can(): Promise<boolean> {
        // console.log('user-route-access-service.ts');
        return new Promise((resolve, reject) => {
            this.userService.getIdentity()
                .then((user: IUser) => {
                    const isUser = user.authorities.includes(environment.roleAccessMap);
                    resolve(isUser);
                })
                .catch((err) => {
                    console.error(err);
                    reject(err);
                });
        });
    }

    canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {
        return this.can();
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        // return true;
        return this.can();
    }
}
