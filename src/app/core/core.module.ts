import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgModule, ErrorHandler } from '@angular/core';
import { NgxWebstorageModule } from 'ngx-webstorage';
import { MainInterceptor } from './handlers/interceptor.service';
import { ErrorsHandler } from './handlers/errors-handler';
// import { LayoutModule } from '../layout/layout.module';
// import { CustomPreloadingStrategy } from './guards/preload-custom-strategy';
import { BrowserModule } from '@angular/platform-browser';
import { DummyComponent } from './components/dummy/dummy.component';

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        BrowserAnimationsModule,
        NgxWebstorageModule.forRoot({ prefix: 'webgis-', separator: '-', caseSensitive: true }),
        // LayoutModule,
    ],
    declarations: [
        DummyComponent
    ],
    exports: [
        NgxWebstorageModule,
        BrowserModule,
    ],
    providers: [
        // CustomPreloadingStrategy,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: MainInterceptor,
            multi: true
        },
        {
            provide: ErrorHandler,
            useClass: ErrorsHandler,
        }
    ]
})
export class CoreModule { }
