export interface IApiEnvironment {
    mapquest: {
        consumerKey: string,
        consumerSecret: string,
        apiUrl: string
    };
    geoserver: {
        url: string;
    };
    profileUrl: string;
    elevationModel: {
        url: string;
        layer: string;
        version: string;
    };
}
