import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MomentModule } from 'ngx-moment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { QuicklinkModule } from 'ngx-quicklink';

import { GlobalReduceModule } from '@app/redux/global.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        GlobalReduceModule,
        MomentModule,
        RouterModule,
        QuicklinkModule,
    ],
    declarations: [
    ],
    entryComponents: [
    ],
    exports: [
        QuicklinkModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TranslateModule,
        GlobalReduceModule,
        MomentModule,
        RouterModule,
    ]
})
export class SharedModule { }
