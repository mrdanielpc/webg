interface Date {
    stdTimezoneOffsetCache: Object;
    stdTimezoneOffset: () => string;
    isDST: () => boolean;
}

Date.prototype.stdTimezoneOffset = function () {
    const fy = this.getFullYear();
    if (!Date.prototype.stdTimezoneOffsetCache.hasOwnProperty(fy)) {

        let maxOffset = new Date(fy, 0, 1).getTimezoneOffset();
        const monthsTestOrder = [6, 7, 5, 8, 4, 9, 3, 10, 2, 11, 1];

        for (let mi = 0; mi < 12; mi++) {
            const offset = new Date(fy, monthsTestOrder[mi], 1).getTimezoneOffset();
            if (offset !== maxOffset) {
                maxOffset = Math.max(maxOffset, offset);
                break;
            }
        }
        Date.prototype.stdTimezoneOffsetCache[fy] = maxOffset;
    }
    return Date.prototype.stdTimezoneOffsetCache[fy];
};

Date.prototype.stdTimezoneOffsetCache = {};

Date.prototype.isDST = function () {
    return this.getTimezoneOffset() < this.stdTimezoneOffset();
};
