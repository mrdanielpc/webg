import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { QuicklinkStrategy } from 'ngx-quicklink';

import { environment } from '@environment/environment';
import { AdminRouteAccessService } from './core/guards/admin-route-access-service';
import { UserRouteAccessService } from './core/guards/user-route-access-service';
import { DummyComponent } from './core/components/dummy/dummy.component';
import { HomeComponent } from './pages/layout/home/home.component';
import { LayoutModule } from './pages/layout/layout.module';

const publicRoutes: Routes = [
    {
        path: 'no-user',
        loadChildren: () => import('./pages/nouser/nouser.module').then(m => m.NoUserModule),
    },
    {
        path: 'dummy',
        component: DummyComponent,
    }
];

const privateRoutes: Routes = [
    {
        path: '',
        canActivate: [UserRouteAccessService],
        loadChildren: () => import('./pages/main/pages.module').then(m => m.PagesModule),
    },
];

const adminRoutes: Routes = [
    {
        path: 'admin',
        canActivate: [AdminRouteAccessService],
        loadChildren: () => import('./pages/admin/admin.module').then(m => m.AdminModule),
    },
];

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            ...publicRoutes,
            ...privateRoutes,
            ...adminRoutes,
        ]
    },
    {
        path: '**',
        redirectTo: ''
    }
];

@NgModule({
    imports: [
        LayoutModule,
        RouterModule.forRoot(routes, {
            onSameUrlNavigation: 'reload',
            preloadingStrategy: QuicklinkStrategy,
            useHash: true,
            enableTracing: environment.enableTracing
        }),
    ],
    exports: [
        RouterModule,
    ]
})
export class AppRoutingModule { }
