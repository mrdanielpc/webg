import { Action } from '@ngrx/store';
import { IRole } from '@app/pages/admin/role/role.model';

export enum RoleActionTypes {
    LoadRoles = '[ROLE] - LoadRoles',
    EditRole = '[ROLE] - EditRole',
    RemoveRole = '[ROLE] - RemoveRole',
    AddRole = '[ROLE] - AddRole',
}

export class LoadRoles implements Action {
    readonly type = RoleActionTypes.LoadRoles;

    constructor(public roles: IRole[]) { }
}

export class EditRole implements Action {
    readonly type = RoleActionTypes.EditRole;

    constructor(public role: IRole) { }
}

export class RemoveRole implements Action {
    readonly type = RoleActionTypes.RemoveRole;

    constructor(public role: IRole) { }
}

export class AddRole implements Action {
    readonly type = RoleActionTypes.AddRole;

    constructor(public role: IRole) { }
}

export type RolesActions = LoadRoles | EditRole | RemoveRole | AddRole;
