import { RolesActions, RoleActionTypes } from './role.actions';
import { IRole } from '@app/pages/admin/role/role.model';

export interface RoleState {
    roles: IRole[];
}

const initialStatus: RoleState = {
    roles: []
};

export function roleReducer(state = initialStatus, action: RolesActions): RoleState {

    switch (action.type) {
        case RoleActionTypes.LoadRoles:
            return {
                ...state,
                roles: [...action.roles]
            };
        case RoleActionTypes.AddRole:
            return {
                ...state,
                roles: [...state.roles, action.role]
            };
        case RoleActionTypes.EditRole:
            state.roles.forEach((role) => {
                if (role.id === action.role.id) {
                    role = action.role;
                }
            });
            return {
                ...state,
                roles: [...state.roles]
            };
        case RoleActionTypes.RemoveRole:
            const newRoles = state.roles.filter((group) => {
                return (group.id !== action.role.id);
            });
            return {
                ...state,
                roles: [...newRoles]
            };
        default:
            return state;
    }
}
