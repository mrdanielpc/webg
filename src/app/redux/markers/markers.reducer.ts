// import { IMapMarker, IMarker } from '@app/pages/main/markers/markers.model';
import { MarkersActions, MarkerActionTypes } from '@app/redux/markers/markers.actions';
import { IMarker, IMapMarker } from '@app/pages/main/components/markers/markers.model';

export interface MarkersState {
    markersTypes: IMarker[];
    markers: IMapMarker[];
}

const initialStatus: MarkersState = {
    markersTypes: null,
    markers: [],
};

export function markerReducer(state = initialStatus, action: MarkersActions): MarkersState {

    switch (action.type) {
        case MarkerActionTypes.LoadMarkers:
            if (!!action.markers) {
                action.markers.forEach((marker: IMapMarker) => {
                    marker.iconUrl = '/api' + marker.iconUrl;
                });
            } else {
                action.markers = [];
            }

            return {
                ...state,
                markersTypes: [...action.markers]
            };
        case MarkerActionTypes.AddMapMarker:
            let title = action.mapMarker.mapTitle;
            if (state.markers.length > 0) {
                title = `${title} (${state.markers.length})`;
            }
            action.mapMarker.mapTitle = title;
            const markersMapsList: IMapMarker[] = [...state.markers, action.mapMarker];
            return {
                ...state,
                markers: [...markersMapsList]
            };
        case MarkerActionTypes.AddMapMarkers:
            return {
                ...state,
                markers: [...action.mapMarkers]
            };
        case MarkerActionTypes.EditMapMarker:
            state.markers.map((markerMap) => {
                if (markerMap.idMapMarker === action.mapMarker.idMapMarker) {
                    return {
                        ...markerMap,
                        ...action.mapMarker
                    };
                } else {
                    return markerMap;
                }
            });
            return {
                ...state,
                markers: [...state.markers]
            };
        case MarkerActionTypes.RemoveMapMarker:
            const actualMarkers = [...state.markers];
            const idx = actualMarkers.findIndex((markerMapToRemove) => {
                return (markerMapToRemove.idMapMarker === action.mapMarker.idMapMarker);
            });
            actualMarkers.splice(idx, 1);
            return {
                ...state,
                markers: [...actualMarkers]
            };
        case MarkerActionTypes.CleanMarkers:
            const newState = {
                ...state,
                markers: new Array(),
            };
            if (action.fullClean) {
                debugger;
                newState.markersTypes = new Array();
            }
            return newState;
        default:
            return { ...state };
    }
}
