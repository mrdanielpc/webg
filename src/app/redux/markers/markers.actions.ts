import { Action } from '@ngrx/store';
import { IMarker, IMapMarker } from '@app/pages/main/components/markers/markers.model';


export enum MarkerActionTypes {
    LoadMarkers = '[MARKERS] - LoadMarkers',
    AddMapMarker = '[MARKERS] - AddMapMarker',
    AddMapMarkers = '[MARKERS] - AddMapMarkers',
    EditMapMarker = '[MARKERS] - EditMapMarker',
    RemoveMapMarker = '[MARKERS] - RemoveMapMarker',
    CleanMarkers = '[MARKERS] - CleanMarkers'
}

export class LoadMarkers implements Action {
    readonly type = MarkerActionTypes.LoadMarkers;

    constructor(public markers: IMarker[]) { }
}

export class AddMapMarker implements Action {
    readonly type = MarkerActionTypes.AddMapMarker;
    constructor(public mapMarker: IMapMarker) { }
}

export class AddMapMarkers implements Action {
    readonly type = MarkerActionTypes.AddMapMarkers;
    constructor(public mapMarkers: IMapMarker[]) { }
}

export class EditMapMarker implements Action {
    readonly type = MarkerActionTypes.EditMapMarker;

    constructor(public mapMarker: IMapMarker) { }
}

export class RemoveMapMarker implements Action {
    readonly type = MarkerActionTypes.RemoveMapMarker;

    constructor(public mapMarker: IMapMarker) { }
}

export class CleanMarkers implements Action {
    readonly type = MarkerActionTypes.CleanMarkers;

    constructor(public fullClean = false) { }
}

export type MarkersActions =
    AddMapMarker |
    AddMapMarkers |
    EditMapMarker |
    RemoveMapMarker |
    LoadMarkers |
    CleanMarkers;
