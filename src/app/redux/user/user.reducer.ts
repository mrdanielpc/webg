import { IUser } from '@app/core/user.model';
import { UserActions, UserActionTypes } from './user.actions';
import { IApiEnvironment } from '@app/core/api-environment.model';

export interface UserState {
    user: IUser;
    config: IApiEnvironment;
}

const initialStatus: UserState = {
    user: null,
    config: null
};

export function userReducer(state = initialStatus, action: UserActions): UserState {

    switch (action.type) {
        case UserActionTypes.LoadUser:
            return {
                ...state,
                user: { ...action.user }
            };
        case UserActionTypes.LoadAPIConfig:
            return {
                ...state,
                config: { ...action.config }
            };
        default:
            return state;
    }
}
