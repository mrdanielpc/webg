import { Action } from '@ngrx/store';
import { IUser } from '@app/core/user.model';
import { IApiEnvironment } from '@app/core/api-environment.model';

export enum UserActionTypes {
    LoadUser = '[USER] - LoadUser',
    LoadAPIConfig = '[USER] - LoadAPIConfig',
}

export class LoadUser implements Action {
    readonly type = UserActionTypes.LoadUser;

    constructor(public user: IUser) { }
}

export class LoadAPIConfig implements Action {
    readonly type = UserActionTypes.LoadAPIConfig;

    constructor(public config: IApiEnvironment) { }
}

export type UserActions = LoadUser | LoadAPIConfig;
