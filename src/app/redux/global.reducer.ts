import { ActionReducerMap } from '@ngrx/store';

// reducers:
import { layerReducer, LayersState } from '@app/redux/layers/layers.reducer';
import { toolReducer, ToolState } from '@app/redux/tools/tools.reducer';
// Modelos:
import { MarkersState, markerReducer } from '@app/redux/markers/markers.reducer';
import { mapReducer, MapState } from '@app/redux/map/map.reducer';
import { panelReducer, PanelState } from './panels/panel.reducer';
import { GroupState, groupReducer } from './groups/group.reducer';
import { RoleState, roleReducer } from './roles/role.reducer';
import { MarkerTypeState, markerTypeReducer } from './marker-types/marker-type.reducer';
import { MediaState, mediaReducer } from './media/media.reducer';
import { UserState, userReducer } from './user/user.reducer';
export interface WebGisState {
    layers: LayersState;
    markers: MarkersState;
    tools: ToolState;
    panels: PanelState;
    map: MapState;
    groups: GroupState;
    roles: RoleState;
    markerTypes: MarkerTypeState;
    media: MediaState;
    user: UserState;
}

export const compReducers: ActionReducerMap<WebGisState> = {
    layers: layerReducer,
    markers: markerReducer,
    tools: toolReducer,
    panels: panelReducer,
    map: mapReducer,
    groups: groupReducer,
    roles: roleReducer,
    markerTypes: markerTypeReducer,
    media: mediaReducer,
    user: userReducer,
};
