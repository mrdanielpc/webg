import { Injectable } from '@angular/core';
import {
    IMainLayer,
    LayerNode,
    IMainWorkspace,
    ILayer
} from '@app/pages/main/components/layers/layers.model';

@Injectable({
    providedIn: 'root'
})
export class LayerUtils {

    constructor(
    ) {
    }

    private static getLayerNodeBasic(objBasic: IMainLayer[], level: number): LayerNode[] {
        const layerNodes: LayerNode[] = [];
        if (!!objBasic) {
            objBasic.forEach((basic) => {
                const node = new LayerNode();
                node.idLayer = basic.idLayer;
                node.idWorkspace = basic.idWorkspace;
                const _item = { ...basic };
                // delete _item.queryable;
                node.item = _item;
                if (!!node.item) {
                    node.item.transparency = 0;
                }
                node.item.transparency = 0;
                node.denom = basic.name;
                node.expandable = (level !== 0);
                node.level = level;
                node.children = null;
                node.queryable = (basic.queryable === '1');
                layerNodes.push(node);
            });
        }
        return layerNodes;
    }

    private static getLayerNodeGroups(groups: IMainWorkspace[]): LayerNode[] {
        const groupNode: LayerNode[] = [];
        if (!!groups) {
            groups.forEach((group) => {
                const parentNode: LayerNode = new LayerNode();
                parentNode.denom = group.title;
                parentNode.expandable = true;
                parentNode.children = this.getLayerNodeBasic(group.layers, 1);
                parentNode.idWorkspace = group.idWorkspace;
                groupNode.push(parentNode);
            });
        }
        return groupNode;
    }

    private static buildFileTree(obj: ILayer): LayerNode[] {
        let layerNodes: LayerNode[] = [];
        layerNodes = [...this.getLayerNodeBasic(obj.layers, 0), ...this.getLayerNodeGroups(obj.workspaces)];
        // layerNodes = layerNodes.sort((a, b) => {
        //     return (a.zindex - b.zindex);
        // });

        return layerNodes;
    }

    static getLayerNodes(layerState: ILayer): LayerNode[] {
        let layerNodes: LayerNode[] = [];
        layerNodes = this.buildFileTree(layerState);
        return layerNodes;
    }

    // static addLayerToLayerNodes(layer: IMainLayer, layerList: LayerNode[]): LayerNode {
    static addLayerToLayerNodes(layer: IMainLayer): LayerNode {
        // const workSpace = layerList.find((ws) => {
        //     return (ws.idWorkspace === layer.idWorkspace);
        // });
        // workSpace.push()
        // const group: IMainWorkspace = {
        //     idWorkspace: layer.idWorkspace,
        //     title: workSpace,
        //     layers: [layer]
        // };

        // return this.getLayerNodeGroups([group])[0];
        // // return newLayer[0];
        const newLayer = this.getLayerNodeBasic([layer], 0);
        return newLayer[0];
    }
}

