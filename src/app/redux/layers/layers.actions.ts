import { Action } from '@ngrx/store';
import {
    ILayer,
    IServer,
    LayerNode,
    IMainLayer,
} from '@app/pages/main/components/layers/layers.model';

export enum LayerActionTypes {
    LoadLayerList = '[LAYER] - LoadLayerList',
    AddLayerList = '[LAYER] - AddLayerList',
    LoadLayersAdded = '[LAYER] - LoadLayersAdded',
    EditOrderLayerNodes = '[LAYER] - EditOrderLayerNodes',
    AddServer = '[LAYER] - AddServer',
    AddLayers = '[LAYER] - AddLAyer',
    ReorderLayers = '[LAYER] - ReorderLayers',
    RemoveLayer = '[LAYER] - RemoveLayer',
    HideLayer = '[LAYER] - HideLayer',
    ShowLayer = '[LAYER] - ShowLayer',
    ChangeTransparency = '[LAYER] - ChangeTransparency',
    CleanLayers = '[LAYER] - CleanLayers'
}

export class LoadLayerList implements Action {
    readonly type = LayerActionTypes.LoadLayerList;

    constructor(public layer: ILayer) { }
}

export class AddLayerList implements Action {
    readonly type = LayerActionTypes.AddLayerList;

    constructor(public params: { layer: IMainLayer, workSpace: string }) { }
}

export class LoadLayersAdded implements Action {
    readonly type = LayerActionTypes.LoadLayersAdded;

    constructor(public layers: LayerNode[]) { }
}

export class AddServer implements Action {
    readonly type = LayerActionTypes.AddServer;

    constructor(public server: IServer) { }
}

export class AddLayers implements Action {
    readonly type = LayerActionTypes.AddLayers;
    constructor(public layerNodes: LayerNode[]) { }
}

export class ReorderLayers implements Action {
    readonly type = LayerActionTypes.ReorderLayers;
    constructor(public layerNodes: LayerNode[]) { }
}

export class RemoveLayer implements Action {
    readonly type = LayerActionTypes.RemoveLayer;
    constructor(public layerNode: LayerNode) { }
}

export class HideLayer implements Action {
    readonly type = LayerActionTypes.HideLayer;
    constructor(public layerNode: LayerNode) { }
}

export class ShowLayer implements Action {
    readonly type = LayerActionTypes.ShowLayer;
    constructor(public layerNode: LayerNode) { }
}

export class ChangeTransparency implements Action {
    readonly type = LayerActionTypes.ChangeTransparency;
    constructor(public layerNode: LayerNode) { }
}

export class CleanLayers implements Action {
    readonly type = LayerActionTypes.CleanLayers;
    constructor(public fullClean = false) { }
}

export type LayersActions =
    LoadLayerList |
    AddLayerList |
    LoadLayersAdded |
    AddServer |
    AddLayers |
    RemoveLayer |
    HideLayer |
    ShowLayer |
    ChangeTransparency |
    ReorderLayers |
    CleanLayers;
