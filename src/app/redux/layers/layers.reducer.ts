import { LayerUtils } from './layers.utils';
import { LayersActions, LayerActionTypes } from './layers.actions';
import { LayerNode } from '@app/pages/main/components/layers/layers.model';

export interface LayersState {
    layersList: LayerNode[];
    layersAdded: LayerNode[];
}

const initialStatus: LayersState = {
    layersList: null,
    layersAdded: [],
};

export function layerReducer(state = initialStatus, action: LayersActions): LayersState {

    switch (action.type) {
        case LayerActionTypes.LoadLayerList:
            const layerNodes = LayerUtils.getLayerNodes(action.layer);
            return {
                ...state,
                layersList: [...layerNodes]
            };
        case LayerActionTypes.AddLayerList:
            if (!!state.layersList && state.layersList.length > 0) {
                const layer = state.layersList.find((layerL) => {
                    return (layerL.idWorkspace === action.params.layer.idWorkspace);
                });
                action.params.layer.queryable = (action.params.layer.queryable === '1');
                const child = LayerUtils.addLayerToLayerNodes(action.params.layer);
                if (!!layer) {
                    layer.children.push(child);
                } else {
                    const newLayer: LayerNode = new LayerNode();
                    newLayer.children = [child];
                    newLayer.denom = action.params.workSpace;
                    newLayer.expandable = true;
                    newLayer.idWorkspace = action.params.layer.idWorkspace;
                    state.layersList = [...state.layersList, newLayer];
                }
            }
            return {
                ...state,
                ...state.layersList && { layersList: [...state.layersList] }
            };
        case LayerActionTypes.LoadLayersAdded:
            return {
                ...state,
                layersAdded: [...action.layers]
            };
        case LayerActionTypes.AddLayers:
            const selected: LayerNode[] = [...state.layersAdded, ...action.layerNodes];
            return {
                ...state,
                layersAdded: [...selected]
            };
        case LayerActionTypes.ReorderLayers:
            const ordered: LayerNode[] = [...action.layerNodes];
            return {
                ...state,
                layersAdded: [...ordered]
            };
        case LayerActionTypes.RemoveLayer:
            const layerSel: LayerNode[] = state.layersAdded;
            layerSel.forEach((layerRem, idx) => {
                if (layerRem.idLayer === action.layerNode.idLayer) {
                    layerSel.splice(idx, 1);
                }
            });
            return {
                ...state,
                layersAdded: [...layerSel]
            };
        case LayerActionTypes.HideLayer:
            state.layersAdded.forEach(layerH => {
                if (layerH.idLayer === action.layerNode.idLayer) {
                    layerH.enabled = false;
                }
            });
            return {
                ...state,
                layersAdded: [...state.layersAdded]
            };
        case LayerActionTypes.ShowLayer:
            state.layersAdded.forEach(layerS => {
                if (layerS.idLayer === action.layerNode.idLayer) {
                    layerS.enabled = true;
                }
            });
            return {
                ...state,
                layersAdded: [...state.layersAdded]
            };
        case LayerActionTypes.ChangeTransparency:
            const layerSelTrans: LayerNode[] = state.layersAdded;
            layerSelTrans.forEach((layerCh) => {
                if (layerCh.idLayer === action.layerNode.idLayer) {
                    layerCh.item.transparency = action.layerNode.item.transparency;
                }
            });
            return {
                ...state,
                layersAdded: [...layerSelTrans]
            };
        case LayerActionTypes.CleanLayers:
            const newState = {
                ...state,
                layersAdded: new Array()
            };
            if (action.fullClean) {
                newState.layersList = new Array();
            }
            return newState;
        default:
            return state;
    }
}
