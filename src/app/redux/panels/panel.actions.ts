import { Action } from '@ngrx/store';
import { IPanel } from '@app/pages/admin/panel/panel.model';

export enum PanelActionTypes {
    LoadPanels = '[MAIN] - LoadPanels',
    EditPanel = '[MAIN] - EditPanel',
    ClosePanels = '[MAIN] - ClosePanels',
    TogglePanel = '[MAIN] - TogglePanel',
}

export class LoadPanels implements Action {
    readonly type = PanelActionTypes.LoadPanels;

    constructor(public panels: IPanel[]) { }
}

export class EditPanel implements Action {
    readonly type = PanelActionTypes.EditPanel;

    constructor(public panel: IPanel) { }
}

export class TogglePanel implements Action {
    readonly type = PanelActionTypes.TogglePanel;

    constructor(public panel: IPanel) { }
}

export class ClosePanels implements Action {
    readonly type = PanelActionTypes.ClosePanels;
    constructor() { }
}

export type PanelsActions = LoadPanels | EditPanel | ClosePanels | TogglePanel;
