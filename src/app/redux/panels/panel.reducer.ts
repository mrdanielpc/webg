import { PanelsActions, PanelActionTypes } from './panel.actions';
import { IPanel, PanelSide } from '@app/pages/admin/panel/panel.model';

export interface PanelState {
    panels: IPanel[];
}

const initialStatus: PanelState = {
    panels: []
};

export function panelReducer(state = initialStatus, action: PanelsActions): PanelState {

    switch (action.type) {
        case PanelActionTypes.LoadPanels:
            // El API no controla la calidad de los datos del campo "order",
            // es decir, que podemos tener para la columna izquierda
            // dos paneles con el mismo "order". Lo solucionamos en front
            // ordenándolos y dándoles un nuevo índice consecutivo.
            // Es importantísimo que sea consecutivo, ya que sino el algoritmo de
            // onDrop no funciona ya que para calcular el array con las nuevas posiciones
            // se utiliza dicho dato para calcular dicho array.
            const leftPanels = action.panels.filter((panel) => {
                return (panel.side === PanelSide.LEFT);
            });
            const orderedPanelsLeft = leftPanels.sort((a, b) => {
                return (a.order - b.order);
            });
            orderedPanelsLeft.forEach((panel, idx) => {
                panel.order = idx;
            });
            const rightPanels = action.panels.filter((panel) => {
                return (panel.side === PanelSide.RIGHT);
            });
            const orderedPanelsRight = rightPanels.sort((a, b) => {
                return (a.order - b.order);
            });
            orderedPanelsRight.forEach((panel, idx) => {
                panel.order = idx;
            });
            // const realPanels = [...orderedPanelsLeft, ...orderedPanelsRight].map((pan) => {
            //     pan.typeSelectedUrl = `'url(${pan.typeSelectedUrl})'`;
            //     return pan;
            // });
            return {
                ...state,
                panels: [...orderedPanelsLeft, ...orderedPanelsRight]
            };
        case PanelActionTypes.EditPanel:
            state.panels.forEach((panel) => {
                if (panel.id === action.panel.id) {
                    panel = action.panel;
                }
            });
            return {
                ...state,
                panels: [...state.panels]
            };
        case PanelActionTypes.TogglePanel:
            state.panels.forEach((panel) => {
                if (panel.side === action.panel.side) {
                    if (panel.id === action.panel.id) {
                        panel.isFixed = action.panel.isFixed;
                    } else {
                        panel.isFixed = false;
                    }
                }
            });
            return {
                ...state,
                panels: [...state.panels]
            };
        case PanelActionTypes.ClosePanels:
            state.panels.forEach((panel) => {
                panel.isFixed = false;
            });
            return {
                ...state,
                panels: [...state.panels]
            };
        default:
            return state;
    }
}
