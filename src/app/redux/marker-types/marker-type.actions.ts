import { Action } from '@ngrx/store';
import { IMarkerType } from '@app/pages/admin/marker-type/marker-type.model';

export enum MarkerTypeActionTypes {
    LoadMarkerTypes = '[GROUP] - LoadMarkerTypes',
    EditMarkerType = '[GROUP] - EditMarkerType',
    RemoveMarkerType = '[GROUP] - RemoveMarkerType',
    AddMarkerType = '[GROUP] - AddMarkerType',
}

export class LoadMarkerTypes implements Action {
    readonly type = MarkerTypeActionTypes.LoadMarkerTypes;

    constructor(public markerTypes: IMarkerType[]) { }
}

export class EditMarkerType implements Action {
    readonly type = MarkerTypeActionTypes.EditMarkerType;

    constructor(public markerType: IMarkerType) { }
}

export class RemoveMarkerType implements Action {
    readonly type = MarkerTypeActionTypes.RemoveMarkerType;

    constructor(public markerType: IMarkerType) { }
}

export class AddMarkerType implements Action {
    readonly type = MarkerTypeActionTypes.AddMarkerType;

    constructor(public markerType: IMarkerType) { }
}
export type MarkerTypeActions = LoadMarkerTypes | EditMarkerType | RemoveMarkerType | AddMarkerType;
