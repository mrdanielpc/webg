import { MarkerTypeActionTypes, MarkerTypeActions } from './marker-type.actions';
import { IMarkerType } from '@app/pages/admin/marker-type/marker-type.model';

export interface MarkerTypeState {
    markerTypes: IMarkerType[];
}

const initialStatus: MarkerTypeState = {
    markerTypes: []
};

export function markerTypeReducer(state = initialStatus, action: MarkerTypeActions): MarkerTypeState {

    switch (action.type) {
        case MarkerTypeActionTypes.LoadMarkerTypes:
            return {
                ...state,
                markerTypes: [...action.markerTypes]
            };
        case MarkerTypeActionTypes.AddMarkerType:
            return {
                ...state,
                markerTypes: [...state.markerTypes, action.markerType]
            };
        case MarkerTypeActionTypes.EditMarkerType:
            // const clonedMakers = [...state.markerTypes];
            state.markerTypes.forEach((markerType) => {
                if (markerType.id === action.markerType.id) {
                    markerType.icon = action.markerType.icon;
                    markerType.iconContentType = action.markerType.iconContentType;
                    markerType.wgGroups = action.markerType.wgGroups;
                    markerType.wgRoles = action.markerType.wgRoles;
                }
            });
            return {
                ...state,
                markerTypes: [...state.markerTypes]
            };
        case MarkerTypeActionTypes.RemoveMarkerType:
            const newGroups = state.markerTypes.filter((markerType) => {
                return (markerType.id !== action.markerType.id);
            });
            return {
                ...state,
                markerTypes: [...newGroups]
            };
        default:
            return state;
    }
}
