import { Action } from '@ngrx/store';
import { IMediaList, IMediaResource, IUserRequest } from '@app/components/media-list/media.model';

export enum MediaActionTypes {
    LoadUserMedia = '[MEDIA] - LoadUserMedia',
    ClearUser = '[MEDIA] - ClearUser',
    LoadMediaList = '[MEDIA] - LoadMediaList',
    AddMediaList = '[MEDIA] - AddMediaList',
    SetMediaListSelected = '[MEDIA] - SetMediaListSelected',
    AddMediaInfo = '[MEDIA] - AddMediaInfo',
    AddResource = '[MEDIA] - AddResource',
    EditResource = '[MEDIA] - EditResource',
    EditMediaList = '[MEDIA] - EditMediaList',
    ToggleMediaVisibility = '[MEDIA] - ToggleMediaVisibility',
    RemoveMediaList = '[MEDIA] - RemoveMediaList',
    ShowMediaResource = '[MEDIA] - ShowResource',
    CleanMedia = '[MEDIA] - CleanMedia',
}

export class ClearUser implements Action {
    readonly type = MediaActionTypes.ClearUser;
    constructor() { }
}

export class CleanMedia implements Action {
    readonly type = MediaActionTypes.CleanMedia;
    constructor() { }
}

export class LoadUserMedia implements Action {
    readonly type = MediaActionTypes.LoadUserMedia;
    constructor(public userReq: IUserRequest) { }
}

export class LoadMediaList implements Action {
    readonly type = MediaActionTypes.LoadMediaList;
    constructor(public mediaListTotal: IMediaList[]) { }
}

export class AddMediaList implements Action {
    readonly type = MediaActionTypes.AddMediaList;

    constructor(public mediaList: IMediaList) { }
}

export class SetMediaListSelected implements Action {
    readonly type = MediaActionTypes.SetMediaListSelected;

    constructor(public mediaList: IMediaList) { }
}

export class AddResource implements Action {
    readonly type = MediaActionTypes.AddResource;

    constructor(public mediaResources: IMediaResource[]) { }
}

export class EditResource implements Action {
    readonly type = MediaActionTypes.EditResource;

    constructor(public mediaResources: IMediaResource[], public idMediaList: number) { }
}

export class AddMediaInfo implements Action {
    readonly type = MediaActionTypes.AddMediaInfo;

    constructor(public mediaResource: IMediaResource) { }
}

export class EditMediaList implements Action {
    readonly type = MediaActionTypes.EditMediaList;

    constructor(public mediaList: IMediaList) { }
}

export class ToggleMediaVisibility implements Action {
    readonly type = MediaActionTypes.ToggleMediaVisibility;

    constructor(public mediaList: IMediaList) { }
}

export class RemoveMediaList implements Action {
    readonly type = MediaActionTypes.RemoveMediaList;

    constructor(public mediaList: IMediaList) { }
}

export class ShowMediaResource implements Action {
    readonly type = MediaActionTypes.ShowMediaResource;

    constructor(public mediaResource: IMediaResource) { }
}

export type MediaActions =
    LoadUserMedia |
    ClearUser |
    LoadMediaList |
    SetMediaListSelected |
    AddMediaList |
    AddMediaInfo |
    AddResource |
    EditResource |
    EditMediaList |
    ToggleMediaVisibility |
    RemoveMediaList |
    ShowMediaResource |
    CleanMedia;
