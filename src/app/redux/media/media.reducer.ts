import { IMediaList, IMediaResource, IUserRequest } from '@app/components/media-list/media.model';
import { MediaActions, MediaActionTypes } from './media.actions';

// import { IMapMarke, IMarker } from '@app/pages/main/markers/markers.model';

export interface MediaState {
    mediaList: IMediaList[];
    mediaResources: IMediaResource[];
    mediaResourceSelected: IMediaResource;
    mediaListSelected: IMediaList;
    userReq: IUserRequest;
}

const initialStatus: MediaState = {
    mediaList: null,
    mediaResources: [],
    mediaResourceSelected: null,
    mediaListSelected: null,
    userReq: null
};

export function mediaReducer(state = initialStatus, action: MediaActions): MediaState {
    switch (action.type) {
        case MediaActionTypes.ClearUser:
            return {
                ...state,
                userReq: null
            };
        case MediaActionTypes.LoadUserMedia:
            return {
                ...state,
                userReq: { ...action.userReq }
            };
        case MediaActionTypes.LoadMediaList:
            return {
                ...state,
                mediaList: [...action.mediaListTotal]
            };
        case MediaActionTypes.AddMediaList:
            const counter = !!state.mediaList ? state.mediaList.length + 1 : 1;
            const newMediaList: IMediaList = {
                ...action.mediaList,
                title: `${action.mediaList.title} (${counter})`,
            };
            const stateMediaL = !!state.mediaList ? [...state.mediaList] : [];
            return {
                ...state,
                mediaList: [
                    ...stateMediaL,
                    newMediaList
                ]
            };
        case MediaActionTypes.AddResource:
            return {
                ...state,
                mediaResources: [...state.mediaResources, ...action.mediaResources]
            };
        case MediaActionTypes.EditResource:
            const idMediaListToEdit = action.idMediaList;
            // const idMediaListToEdit = !!action.mediaResources[0] ? action.mediaResources[0].idMediaList : null;
            let newResources: IMediaResource[] = state.mediaResources;
            if (!!idMediaListToEdit) {
                newResources = state.mediaResources.filter((res) => {
                    return (res.idMediaList !== idMediaListToEdit);
                });
                newResources = [...newResources, ...action.mediaResources];
            }
            return {
                ...state,
                mediaResources: newResources
            };
        case MediaActionTypes.SetMediaListSelected:
            return {
                ...state,
                mediaListSelected: { ...action.mediaList }
            };
        case MediaActionTypes.AddMediaInfo:
            return {
                ...state,
                mediaResourceSelected: { ...action.mediaResource }
            };
        case MediaActionTypes.ToggleMediaVisibility:
            state.mediaResources.map((media) => {
                if (media.idMediaList === action.mediaList.id) {
                    media.visible = !media.visible;
                }
            });
            return {
                ...state,
                mediaResources: [...state.mediaResources]
            };
        case MediaActionTypes.EditMediaList:
            state.mediaList.forEach((list) => {
                if (list.id === action.mediaList.id) {
                    list.title = action.mediaList.title;
                    list.startDate = action.mediaList.startDate;
                    list.endDate = action.mediaList.endDate;
                    list.idInterval = action.mediaList.idInterval;
                    list.customerId = action.mediaList.customerId;
                    list.captureType = action.mediaList.captureType;
                    list.visible = action.mediaList.visible;
                }
            });
            return {
                ...state,
                mediaList: [...state.mediaList],
            };
        case MediaActionTypes.RemoveMediaList:
            const idx = state.mediaList.findIndex((media) => {
                return (media.id === action.mediaList.id);
            });
            state.mediaList.splice(idx, 1);
            const mediaRes = state.mediaResources.filter((media) => {
                return (media.idMediaList !== action.mediaList.id);
            });
            return {
                ...state,
                mediaList: [...state.mediaList],
                mediaResources: mediaRes,
                mediaListSelected: null
            };
        case MediaActionTypes.ShowMediaResource:
            return {
                ...state,
                mediaResourceSelected: action.mediaResource
            };
        case MediaActionTypes.CleanMedia:
            return {
                ...state,
                mediaList: null,
                mediaResources: [],
                mediaResourceSelected: null,
                mediaListSelected: null,
                userReq: null
            };
        default:
            return { ...state };
    }
}
