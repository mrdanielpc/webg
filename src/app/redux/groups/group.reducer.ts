import { GroupActionTypes, GroupsActions } from './group.actions';
import { IGroup } from '@app/pages/admin/group/group.model';

export interface GroupState {
    groups: IGroup[];
}

const initialStatus: GroupState = {
    groups: []
};

export function groupReducer(state = initialStatus, action: GroupsActions): GroupState {

    switch (action.type) {
        case GroupActionTypes.LoadGroups:
            return {
                ...state,
                groups: [...action.groups]
            };
        case GroupActionTypes.AddGroup:
            return {
                ...state,
                groups: [...state.groups, action.group]
            };
        case GroupActionTypes.EditGroup:
            state.groups.forEach((group) => {
                if (group.id === action.group.id) {
                    group = action.group;
                }
            });
            return {
                ...state,
                groups: [...state.groups]
            };
        case GroupActionTypes.RemoveGroup:
            const newGroups = state.groups.filter((group) => {
                return (group.id !== action.group.id);
            });
            return {
                ...state,
                groups: [...newGroups]
            };
        default:
            return state;
    }
}
