import { Action } from '@ngrx/store';
import { IGroup } from '@app/pages/admin/group/group.model';

export enum GroupActionTypes {
    LoadGroups = '[GROUP] - LoadGroups',
    EditGroup = '[GROUP] - EditGroup',
    RemoveGroup = '[GROUP] - RemoveGroup',
    AddGroup = '[GROUP] - AddGroup',
}

export class LoadGroups implements Action {
    readonly type = GroupActionTypes.LoadGroups;

    constructor(public groups: IGroup[]) { }
}

export class EditGroup implements Action {
    readonly type = GroupActionTypes.EditGroup;

    constructor(public group: IGroup) { }
}

export class RemoveGroup implements Action {
    readonly type = GroupActionTypes.RemoveGroup;

    constructor(public group: IGroup) { }
}

export class AddGroup implements Action {
    readonly type = GroupActionTypes.AddGroup;

    constructor(public group: IGroup) { }
}
export type GroupsActions = LoadGroups | EditGroup | RemoveGroup | AddGroup;
