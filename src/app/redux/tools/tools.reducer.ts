import { ToolsActions, ToolActionTypes } from './tools.action';
import { ToolUtils } from '@app/redux/tools/tools.utils';
import { ITool } from '@app/pages/main/components/tools/tools.model';


export interface ToolState {
    toolsType: ITool[];
    tools: ITool[];
}

const initialStatus: ToolState = {
    toolsType: [],
    tools: []
};

export function toolReducer(state = initialStatus, action: ToolsActions): ToolState {

    switch (action.type) {
        case ToolActionTypes.LoadTools:
            const newTools: ITool[] = ToolUtils.processTools(action.tools);
            return {
                ...state,
                toolsType: [...newTools]
            };
        case ToolActionTypes.AddTools:
            return {
                ...state,
                tools: [...action.tools]
            };
        case ToolActionTypes.AddTool:
            const newTool: ITool = action.tool;
            newTool.attributes.title = newTool.attributes.title +
                (
                    (state.tools.length > 0) ? ' (' + state.tools.length + ')' : ''
                );
            return {
                ...state,
                tools: [...state.tools, newTool]
            };
        case ToolActionTypes.RemoveTool:
            const newToolsAdded = [...state.tools];
            newToolsAdded.forEach((tool, idx) => {
                if (tool.attributes.idMapTool === action.tool.attributes.idMapTool) {
                    newToolsAdded.splice(idx, 1);
                }
            });
            return {
                ...state,
                tools: newToolsAdded
            };
        case ToolActionTypes.EditTool:
            state.tools.forEach((tool) => {
                if (tool.attributes.idMapTool === action.tool.attributes.idMapTool) {
                    tool.attributes = action.tool.attributes;
                }
            });
            return {
                ...state,
                tools: [...state.tools]
            };
        case ToolActionTypes.CleanTools:
            const newState = {
                ...state,
                tools: new Array(),
            };
            if (action.fullClean) {
                newState.toolsType = new Array();
            }
            return newState;
        default:
            return state;
    }
}
