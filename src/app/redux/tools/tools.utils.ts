import { Injectable } from '@angular/core';
import { environment } from '@environment/environment';
import { ITool, ToolsType } from '@app/pages/main/components/tools/tools.model';


@Injectable({
    providedIn: 'root'
})
export class ToolUtils {

    constructor(
    ) {
    }

    static processTools(tools: ITool[]): ITool[] {

        tools.forEach((tool) => {

            tool.attributes = {
                color: environment.tools.attributes.color,
                transparency: environment.tools.attributes.transparency,
                width: environment.tools.attributes.width,

            };

            switch (tool.type) {
                case ToolsType.LineString:
                case ToolsType.MultiLineString:
                    tool.icon = 'linear_scale';
                    break;
                case ToolsType.Polygon:
                case ToolsType.MultiPolygon:
                    tool.icon = 'format_shapes';
                    break;
                case ToolsType.Square:
                    tool.icon = 'crop_square';
                    break;
                case ToolsType.Rectangle:
                    tool.icon = 'crop_7_5';
                    break;
                case ToolsType.Circle:
                    tool.icon = 'panorama_fish_eye';
                    break;
                case ToolsType.Ellipse:
                    tool.svgIcon = 'ellipse';
                    break;
            }

        });

        return tools;
    }


}
