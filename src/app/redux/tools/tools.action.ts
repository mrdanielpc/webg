import { Action } from '@ngrx/store';
import { ITool } from '@app/pages/main/components/tools/tools.model';

export enum ToolActionTypes {
    LoadTools = '[Tool] - LoadTools',
    AddTool = '[TOOL] - AddTool',
    AddTools = '[TOOL] - AddTools',
    RemoveTool = '[TOOL] - RemoveTool',
    EditTool = '[TOOL] - EditTool',
    CleanTools = '[TOOL] - CleanTools',
}

export class LoadTools implements Action {
    readonly type = ToolActionTypes.LoadTools;

    constructor(public tools: ITool[]) { }
}

export class AddTool implements Action {
    readonly type = ToolActionTypes.AddTool;
    constructor(public tool: ITool) { }
}

export class AddTools implements Action {
    readonly type = ToolActionTypes.AddTools;
    constructor(public tools: ITool[]) { }
}

export class RemoveTool implements Action {
    readonly type = ToolActionTypes.RemoveTool;
    constructor(public tool: ITool) { }
}

export class EditTool implements Action {
    readonly type = ToolActionTypes.EditTool;
    constructor(public tool: ITool) { }
}

export class CleanTools implements Action {
    readonly type = ToolActionTypes.CleanTools;
    constructor(public fullClean = false) { }
}

export type ToolsActions =
    LoadTools |
    AddTool |
    AddTools |
    RemoveTool |
    EditTool |
    CleanTools;
