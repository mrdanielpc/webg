import { Action } from '@ngrx/store';
import { ILocation, EnumProjection, EnumBaseLayer, EnumMapState, EnumMapOwner } from '@app/pages/main/components/map/map.model';
import { IMap } from '@app/pages/main/components/map-manager/map-manager.model';
import { ITool } from '@app/pages/main/components/tools/tools.model';
import { IMarker, IMapMarker } from '@app/pages/main/components/markers/markers.model';
import { IMapQuestAPI } from '@app/pages/main/main/main.model';

export enum MapActionTypes {
    MoveToLocation = '[MAP] - MoveToLocation',
    SetDirty = '[MAP] - SetDirty',
    SetPristine = '[MAP] - SetPristine',
    SetProjection = '[MAP] - SetProjection',
    SetBaseLayer = '[MAP] - SetBaseLayer',
    CleanMap = '[MAP] - CleanMap',
    SetToolTypeSelected = '[MAP] - SetToolTypeSelected',
    SetToolSelected = '[MAP] - SetToolSelected',
    SetMarkerTypeSelected = '[MAP] - SetMarkerTypeSelected',
    SetMarkerSelected = '[MAP] - SetMarkerSelected',
    SetMapEditable = '[MAP] - SetMapEditable',
    SetMapOwnerState = '[MAP] - SetMapOwnerState',
    SearchCity = '[MAP] - SearchCity',
    ClearSelected = '[MAP] - ClearSelected',
    SetMapPublishedAsLayer = '[MAP] - SetMapPublishedAsLayer',
}

export class MoveToLocation implements Action {
    readonly type = MapActionTypes.MoveToLocation;
    constructor(public location: ILocation) { }
}

export class SetProjection implements Action {
    readonly type = MapActionTypes.SetProjection;
    constructor(public projection: EnumProjection) { }
}

export class SetBaseLayer implements Action {
    readonly type = MapActionTypes.SetBaseLayer;
    constructor(public baseLayer: EnumBaseLayer) { }
}

export class SetDirty implements Action {
    readonly type = MapActionTypes.SetDirty;
    constructor() { }
}

export class SetPristine implements Action {
    readonly type = MapActionTypes.SetPristine;
    constructor() { }
}

export class CleanMap implements Action {
    readonly type = MapActionTypes.CleanMap;
    constructor(public fullClean = false) { }
}

export class SetToolTypeSelected implements Action {
    readonly type = MapActionTypes.SetToolTypeSelected;
    constructor(public toolType: ITool) { }
}

export class SetToolSelected implements Action {
    readonly type = MapActionTypes.SetToolSelected;
    constructor(public tool: ITool) { }
}

export class SetMArkerTypeSelected implements Action {
    readonly type = MapActionTypes.SetMarkerTypeSelected;
    constructor(public markerType: IMarker) { }
}

export class SetMarkerSelected implements Action {
    readonly type = MapActionTypes.SetMarkerSelected;
    constructor(public marker: IMapMarker) { }
}

export class SetMapEditable implements Action {
    readonly type = MapActionTypes.SetMapEditable;
    constructor(public mode: EnumMapState) { }
}

export class SetMapOwnerState implements Action {
    readonly type = MapActionTypes.SetMapOwnerState;
    constructor(public mapOwner: EnumMapOwner) { }
}

export class SearchCity implements Action {
    readonly type = MapActionTypes.SearchCity;
    constructor(public city: IMapQuestAPI) { }
}

export class ClearSelected implements Action {
    readonly type = MapActionTypes.ClearSelected;
    constructor() { }
}

export class SetMapPublishedAsLayer implements Action {
    readonly type = MapActionTypes.SetMapPublishedAsLayer;
    constructor() { }
}

export type MapActions =
    SetDirty |
    SetPristine |
    CleanMap |
    MoveToLocation |
    SetToolTypeSelected |
    SetMArkerTypeSelected |
    SetMapEditable |
    SetToolSelected |
    SetMarkerSelected |
    SetMapOwnerState |
    SearchCity |
    ClearSelected |
    SetProjection |
    SetBaseLayer |
    SetMapPublishedAsLayer;
