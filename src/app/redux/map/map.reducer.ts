import { MapActions, MapActionTypes } from '@app/redux/map/map.actions';
import { IMap } from '@app/pages/main/components/map-manager/map-manager.model';
import { EnumMapState, EnumMapOwner, EnumProjection, ILocation, EnumBaseLayer } from '@app/pages/main/components/map/map.model';
import { ITool, ToolsType } from '@app/pages/main/components/tools/tools.model';
import { IMarker, IMapMarker } from '@app/pages/main/components/markers/markers.model';
import { IMapQuestAPI } from '@app/pages/main/main/main.model';
import { environment } from '@environment/environment';


export interface MapState {
    baseLayer: EnumBaseLayer;
    isDirty: boolean;
    editableMode: EnumMapState;
    location: ILocation;
    mapOwnerState: EnumMapOwner;
    markerSelected: IMapMarker;
    markerTypeSelected: IMarker;
    projection: EnumProjection;
    publishedAsLayer: boolean;
    searchCity: IMapQuestAPI;
    toolTypeSelected: ITool;
    toolSelected: ITool;
}

const initialStatus: MapState = {
    baseLayer: <EnumBaseLayer>environment.map.baseLayer,
    isDirty: false,
    editableMode: EnumMapState.NONE,
    location: null,
    mapOwnerState: null,
    markerSelected: null,
    markerTypeSelected: null,
    projection: EnumProjection['3D'],
    publishedAsLayer: false,
    searchCity: null,
    toolTypeSelected: null,
    toolSelected: null
};

export function mapReducer(state = initialStatus, action: MapActions): MapState {

    switch (action.type) {
        case MapActionTypes.SetMapPublishedAsLayer:
            return {
                ...state,
                publishedAsLayer: true
            };
        case MapActionTypes.MoveToLocation:
            return {
                ...state,
                location: { ...action.location }
            };

        case MapActionTypes.SetProjection:
            return {
                ...state,
                projection: action.projection
            };

        case MapActionTypes.SetBaseLayer:
            return {
                ...state,
                baseLayer: action.baseLayer
            };
        case MapActionTypes.SetMapOwnerState:
            return {
                ...state,
                mapOwnerState: action.mapOwner
            };
        case MapActionTypes.SetDirty:
            return {
                ...state,
                isDirty: true
            };
        case MapActionTypes.SetPristine:
            return {
                ...state,
                isDirty: false,
            };
        case MapActionTypes.CleanMap:
            return {
                ...state,
                isDirty: false,
                editableMode: EnumMapState.SELECT_TOOL_MARKER,
                markerTypeSelected: null,
                toolTypeSelected: null,
                toolSelected: null,
                markerSelected: null,
                mapOwnerState: null,
                publishedAsLayer: false,
                baseLayer: <EnumBaseLayer>environment.map.baseLayer,
                projection: EnumProjection['3D'],
            };
        case MapActionTypes.SetMapEditable:
            let stateReturn: MapState = {
                ...state,
                editableMode: action.mode
            };
            stateReturn = {
                ...stateReturn,
                markerTypeSelected: null,
                markerSelected: null,
                toolSelected: null,
                toolTypeSelected: null
            };
            return stateReturn;
        case MapActionTypes.SetMarkerTypeSelected:
            return {
                ...state,
                editableMode: !!action.markerType ? EnumMapState.ADD_TOOL_IRREGULAR_MARKER : EnumMapState.SELECT_TOOL_MARKER,
                markerTypeSelected: action.markerType,
                markerSelected: null,
                toolSelected: null,
                toolTypeSelected: null
            };
        case MapActionTypes.SetMarkerSelected:
            return {
                ...state,
                editableMode: EnumMapState.MOVE_TOOL_MARKER,
                toolTypeSelected: null,
                toolSelected: null,
                markerSelected: !!action.marker ? { ...action.marker } : null,
                markerTypeSelected: null
            };
        case MapActionTypes.SetToolTypeSelected:
            let newEditableMode: EnumMapState;
            if (!!action.toolType) {
                if (
                    (action.toolType.type === ToolsType.LineString)
                    ||
                    (action.toolType.type === ToolsType.Polygon)
                ) {
                    newEditableMode = EnumMapState.ADD_TOOL_IRREGULAR_MARKER;
                } else {
                    newEditableMode = EnumMapState.ADD_TOOL_REGULAR;
                }
            } else {
                newEditableMode = EnumMapState.SELECT_TOOL_MARKER;
            }

            return {
                ...state,
                editableMode: newEditableMode,
                toolTypeSelected: action.toolType,
                toolSelected: null,
                markerSelected: null,
                markerTypeSelected: null
            };
        case MapActionTypes.SetToolSelected:
            return {
                ...state,
                editableMode: EnumMapState.MOVE_TOOL_MARKER,
                toolTypeSelected: null,
                toolSelected: !!action.tool ? { ...action.tool } : null,
                markerSelected: null,
                markerTypeSelected: null
            };
        case MapActionTypes.SearchCity:
            return {
                ...state,
                searchCity: action.city
            };
        case MapActionTypes.ClearSelected:
            return {
                ...state,
                editableMode: EnumMapState.SELECT_TOOL_MARKER,
                markerSelected: null,
                markerTypeSelected: null,
                toolSelected: null,
                toolTypeSelected: null
            };
        default:
            return state;
    }
}
