export interface IField {
    value: string;
    viewValue: string;
    defaultFilter: boolean;
}

export interface IFilter {
    value: string;
    field: string;
}
