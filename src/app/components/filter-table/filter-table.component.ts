import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter
} from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { environment } from '@environment/environment';
import { IFilter, IField } from './filter-table.model';
// import { IField, IFilter } from './filter-table.model';

@Component({
    selector: 'app-filter-table',
    templateUrl: './filter-table.component.html',
    styleUrls: ['./filter-table.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush

})
export class FilterTableComponent implements OnInit {

    @Input() fields: IField[];
    @Output() readonly  applyFilter = new EventEmitter<IFilter>();

    attrTextChangeObserver: any;

    selectedValue: string;
    filterValue: string;


    constructor() { }

    ngOnInit() {
        const defaultItem = this.fields.find((field) => {
            return !!field.defaultFilter;
        });
        this.fields = [{
            defaultFilter: false,
            value: null,
            viewValue: '-----'
        }, ...this.fields];
        if (!!defaultItem) {
            this.selectedValue = defaultItem.value;
        } else {
            this.selectedValue = this.fields[0].value;
        }
    }

    trackById(index, item) {
        return item.value;
    }

    setFilter() {

        if (!this.attrTextChangeObserver) {
            new Observable((observer) => {
                this.attrTextChangeObserver = observer;
            }).pipe(debounceTime(environment.debounceModel))
                .pipe(distinctUntilChanged())
                .subscribe(() => {
                    // if (!!this.filterValue) {
                    //     console.log(
                    //         `Buscamos por el campo: ${this.selectedValue} con el valor: ${this.filterValue.trim().toLocaleLowerCase()}`
                    //     );
                    // } else {
                    //     console.log(
                    //         `Buscamos por el campo: ${this.selectedValue} sin texto a buscar`
                    //     );
                    // }
                    this.applyFilter.emit({
                        field: this.filterValue || '',
                        value: this.selectedValue
                    });
                });
        }
        this.attrTextChangeObserver.next(this.filterValue);

    }

}
