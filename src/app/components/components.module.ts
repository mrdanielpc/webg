import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatDialogModule } from '@angular/material/dialog';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatRadioModule } from '@angular/material/radio';
import { MatSelectModule } from '@angular/material/select';

import { FilterTableComponent } from './filter-table/filter-table.component';
import { SharedModule } from '@app/shared/shared.module';
import { ConfirmDialogComponent } from './confirm/confirm.component';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MediaListComponent } from './media-list/media-list.component';
import { MediaUserComponent } from './media-list/media-user/media-user.component';
import { OpenSeaDragonComponent } from './open-sea-dragon/open-sea-dragon.component';
import { MatTooltipModule } from '@angular/material/tooltip';

const MATERIAL_COMPONENTS_MODULES = [
    MatListModule,
    MatIconModule,
    MatDividerModule,
    MatFormFieldModule,
    MatDialogModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatRadioModule,
    MatTooltipModule,
];

@NgModule({
    imports: [
        MATERIAL_COMPONENTS_MODULES,
        SharedModule,
    ],
    declarations: [
        FilterTableComponent,
        ConfirmDialogComponent,
        MediaListComponent,
        MediaUserComponent,
        OpenSeaDragonComponent,
    ],
    entryComponents: [
        ConfirmDialogComponent,
        MediaUserComponent,
        OpenSeaDragonComponent,
    ],
    exports: [
        MATERIAL_COMPONENTS_MODULES,
        SharedModule,
        FilterTableComponent,
        ConfirmDialogComponent,
        MediaListComponent,
        MediaUserComponent,
        OpenSeaDragonComponent,
    ]
})
export class ComponentsModule { }
