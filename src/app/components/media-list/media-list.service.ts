import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import * as moment from 'moment';
import { Observable } from 'rxjs';
import { IUserRequest, IMediaList, IMediaResource } from './media.model';

@Injectable({
    providedIn: 'root'
})
export class MediaListService {

    constructor(
        private http: HttpClient,
    ) {
    }

    getFakeToken(userReq: IUserRequest): string {
        return btoa(userReq.user + ':' + userReq.password);
    }

    private addAuthHeaders(userReq: IUserRequest): HttpHeaders {
        const headers = new HttpHeaders({
            'x-auth-token': this.getFakeToken(userReq),
            'Content-Type': 'application/json',
        });
        return headers;
    }

    addMediaList(userReq: IUserRequest, resource: IMediaList): Observable<IMediaResource[]> {
        const url = `viewer/resources/find?`;
        const headers = this.addAuthHeaders(userReq);
        let params = new HttpParams();
        params = params.set('startDate', moment(resource.startDate).format('YYYYMMDDTHHmmss'))
            .set('endDate', moment(resource.endDate).format('YYYYMMDDTHHmmss'));
        if (!!resource.customerId) {
            params = params.set('customerId', resource.customerId);
        }
        if (!!resource.processType) {
            params = params.set('processType', resource.processType);
        }
        if (resource.captureType) {
            params = params.set('captureType', resource.captureType);
        }
        return this.http.get<IMediaResource[]>(url, { headers, params });
    }


}
