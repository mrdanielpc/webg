import { Component, OnInit, ChangeDetectionStrategy, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { WebGisState } from '@app/redux/global.reducer';
import { Store } from '@ngrx/store';
import { LoadUserMedia } from '@app/redux/media/media.actions';
import { IUserRequest } from '../../../components/media-list/media.model';
import { MatDialogRef } from '@angular/material/dialog';
import { environment } from '@environment/environment';

@Component({
    selector: 'app-media-user',
    templateUrl: './media-user.component.html',
    styleUrls: ['./media-user.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MediaUserComponent implements OnInit {

    authForm = new FormGroup({});
    userReq: IUserRequest;

    constructor(
        private formBuilder: FormBuilder,
        private store: Store<WebGisState>,
        public dialogRef: MatDialogRef<MediaUserComponent>,
    ) {
        this.authForm = this.formBuilder.group({
            user: [environment.resource.user, Validators.required],
            password: [environment.resource.password, Validators.required],
        });
    }

    ngOnInit() {
    }

    authenticate() {
        this.userReq = {
            user: this.authForm.controls['user'].value,
            password: this.authForm.controls['password'].value,
        };
        this.store.dispatch(new LoadUserMedia(this.userReq));
        this.dialogRef.close(this.userReq);
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
