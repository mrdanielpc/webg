import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    OnDestroy,
    ChangeDetectorRef,
} from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { DateAdapter } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { take, takeUntil, debounceTime, distinctUntilChanged } from 'rxjs/operators';
import * as moment from 'moment';
import { Subject, Observable } from 'rxjs';

import { UserService } from '@app/core/user.service';
import { WebGisState } from '@app/redux/global.reducer';
import { AddMediaList, EditMediaList, EditResource } from '@app/redux/media/media.actions';
import { MediaUserComponent } from './media-user/media-user.component';
import { environment } from '@environment/environment';
import { SetDirty } from '@app/redux/map/map.actions';
import {
    IMediaList,
    IUserRequest,
    EnumInterval,
    IMediaResource,
    IInterval
} from './media.model';
import { MediaService } from '@app/pages/main/components/media/media.service';

enum MediaMode {
    CREATE,
    EDIT
}

@Component({
    selector: 'app-media-list',
    templateUrl: './media-list.component.html',
    styleUrls: ['./media-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MediaListComponent implements OnInit, OnDestroy {

    @Input() mediaList: IMediaList;

    mode: MediaMode;
    userReq: IUserRequest;

    groupForm = new FormGroup({});
    intervals: IInterval[] = [];
    public enumInterval: typeof EnumInterval = EnumInterval;
    public enumMode: typeof MediaMode = MediaMode;

    // private readonly daysInMiliseconds = (environment.tools.media.daysInterval * 60 * 24 * 60 * 1000);
    private unsubscribe$ = new Subject<void>();

    attrTextChangeObserver: any;

    constructor(
        private formBuilder: FormBuilder,
        private translateService: TranslateService,
        private userService: UserService,
        private adapter: DateAdapter<any>,
        private store: Store<WebGisState>,
        private dialog: MatDialog,
        private cd: ChangeDetectorRef,
        private mediaService: MediaService,
    ) {
        this.groupForm = this.formBuilder.group({
            dateStart: ['', Validators.required],
            dateEnd: ['', Validators.required],
        });

        const userLangKey = this.userService.getLanguage();
        this.adapter.setLocale(userLangKey);

    }

    ngOnInit() {

        this.createIntervals();
        this.observeUserRequestMultimedia();

        if (!!this.mediaList) {
            this.mode = MediaMode.EDIT;
            this.setMediaList();

            this.store.select('webgis', 'media', 'mediaListSelected')
                .pipe(takeUntil(this.unsubscribe$))
                .subscribe((mediaL: IMediaList) => {
                    this.mediaList = mediaL;
                    if (!!mediaL) {
                        this.setMediaList();
                    }
                    this.cd.markForCheck();
                });

        } else {
            this.mode = MediaMode.CREATE;
            this.initMediaRequest();
        }
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        if (!!this.attrTextChangeObserver) {
            this.attrTextChangeObserver.unsubscribe();
        }
    }

    private setMediaList() {
        const actualInterval: IInterval = this.intervals.find((interval) => {
            return interval.value === this.mediaList.idInterval;
        });
        if (!!actualInterval && (!this.mediaList.startDate && !this.mediaList.endDate)) {
            actualInterval.minDays = moment(this.mediaList.endDate).diff(this.mediaList.startDate, 'd');
            this.selectInterval(actualInterval);
        } else if (!!this.mediaList.startDate && !!this.mediaList.endDate) {
            actualInterval.minDays = moment(this.mediaList.endDate).diff(this.mediaList.startDate, 'd');

            actualInterval.startDate = moment(this.mediaList.startDate);
            actualInterval.endDate = moment(this.mediaList.endDate);

            this.selectInterval(actualInterval);
        }
        this.cd.markForCheck();
    }

    private setAllIntervalUnchecked() {
        this.intervals.map((interval) => {
            interval.checked = false;
        });
    }

    private createIntervals() {
        this.intervals = [...this.mediaService.getIntervals()];
    }

    private search() {
        this.mediaList.id = new Date().getTime();
        this.store.dispatch(new AddMediaList(this.mediaList));
        this.store.dispatch(new SetDirty());
    }

    private getResourcesFromEditedMediaList(mediaList: IMediaList) {
        this.mediaService.getResourcesMediaList(this.userReq, mediaList)
            .pipe(take(1))
            .subscribe((resources: IMediaResource[]) => {
                this.store.dispatch(new EditResource(resources, mediaList.id));
            });
    }

    searchMediaList() {
        if (!!this.userReq) {
            this.search();
        } else {
            const dialogRef = this.dialog.open(MediaUserComponent, {
                width: '400px',
                maxWidth: '100vw'
            });
            dialogRef.afterClosed()
                .pipe(take(1))
                .subscribe((result) => {
                    this.userReq = result;
                    if (!!this.userReq) {
                        this.search();
                    }
                });
        }
    }

    private observeUserRequestMultimedia() {
        // Esto se hace porque si se introducen credenciales incorrectas
        // Se resetea, y hay que igualar el userReq a null
        this.store.select('webgis', 'media', 'userReq')
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user: IUserRequest) => {
                this.userReq = user;
            });
    }

    selectInterval(interval: IInterval, forceChangeModel: boolean = false) {
        let fromDate;
        let toDate;
        if (interval.startDate && interval.endDate) {
            fromDate = interval.startDate;
            toDate = interval.endDate;
        } else {
            fromDate = moment().add(interval.minDays * -1, 'days');
            toDate = moment();
        }
        this.groupForm.controls['dateStart'].setValue(fromDate);
        this.groupForm.controls['dateEnd'].setValue(toDate);

        this.mediaList.startDate = new Date(+fromDate.format('x'));
        this.mediaList.endDate = new Date(+toDate.format('x'));
        this.mediaList.idInterval = interval.value;
        this.setAllIntervalUnchecked();
        this.intervals.map((inter) => {
            if (interval.value === inter.value) {
                inter.checked = true;
            }
        });
        if (this.mode === MediaMode.EDIT && !!forceChangeModel) {
            this.store.dispatch(new EditMediaList(this.mediaList));
            this.getResourcesFromEditedMediaList(this.mediaList);
        }
    }

    initMediaRequest() {
        this.mediaList = {
            ...this.mediaList,
            title: `${this.translateService.instant('media.query')} `,
            startDate: null,
            endDate: null,
            idInterval: null
        };
        this.setAllIntervalUnchecked();
    }

    changeMediaListAttribute(attr: string, value: any) {
        if (this.userReq) {
            if (this.mode === MediaMode.EDIT) {
                this.mediaList[attr] = value;
                this.store.dispatch(new EditMediaList(this.mediaList));
                this.getResourcesFromEditedMediaList(this.mediaList);
            }
        }
    }

    changeMediaListAttributeText(attr: string, value: any) {
        if (!this.attrTextChangeObserver) {
            new Observable((observer) => {
                this.attrTextChangeObserver = observer;
            }).pipe(debounceTime(environment.debounceModel)) // wait 300ms after the last event before emitting last event
                .pipe(distinctUntilChanged()) // only emit if value is different from previous value
                .subscribe(() => {
                    this.changeMediaListAttribute(attr, this.mediaList[attr]);
                });
        }
        this.attrTextChangeObserver.next(value);
    }

}
