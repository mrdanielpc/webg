import { IMapCoordinates } from '@app/pages/main/components/markers/markers.model';
import * as moment from 'moment';

interface IMediaResourceAPI {
    captureTime: string;
    captureType: string;
    contentType: string;
    id: string;
    latitude: number;
    longitude: number;
    name: string;
    relatedResources: IMediaResource[];
    sensorType?: string;
    mediaInfo?: IMediaInfo[];
    mapCoordinates?: IMapCoordinates;
}

export interface IMediaResource extends IMediaResourceAPI {
    visible: boolean;
    idMediaList: number;
    isVideo: boolean;
    isImage: boolean;
    isGeo: boolean;
    resourceUrl: string;
    downloadUrl?: string;
    isGeolocated?: boolean;
    moveToLocation?: boolean;
}

export interface IUserRequest {
    user: string;
    password: string;
}

export enum EnumInterval {
    first = 0,
    second = 1,
    third = 2,
    free = 3
}

interface IMediaParams {
    startDate: Date;
    endDate: Date;
    title: string;
    idInterval?: EnumInterval;
    customerId?: string;
    processType?: string;
    captureType?: string;
}

export interface IMediaList extends IMediaParams {
    id: number;
    visible: boolean;
}

export interface IMediaInfo {
    name: string;
    type: string;
    value: string;
}


export interface IInterval {
    text: string;
    value: number;
    minDays: number;
    checked: boolean;
    startDate?: moment.Moment;
    endDate?: moment.Moment;
}
