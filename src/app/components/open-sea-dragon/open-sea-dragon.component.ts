import {
  Component,
  OnInit,
  AfterViewInit,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef,
  RendererFactory2,
  Input
} from '@angular/core';

// import * as OpenSeaDragon from 'openseadragon/build/openseadragon';
import * as OpenSeaDragon from 'openseadragon/build/openseadragon/openseadragon.js';

interface ITile {
  type: string;
  url: string;
}

@Component({
  selector: 'app-open-sea-dragon',
  templateUrl: './open-sea-dragon.component.html',
  styleUrls: ['./open-sea-dragon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OpenSeaDragonComponent implements OnInit, AfterViewInit {

  @ViewChild('dragon', { static: true }) dragon: ElementRef;

  private tileSources: ITile[] = [];

  @Input() images: string[] = [];
  @Input() selectedIndex: number;

  _openSeaDragon: OpenSeaDragon;

  constructor(
    private rootRenderer: RendererFactory2,
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.rootRenderer.whenRenderingDone()
      .then(() => {
        this.images.forEach((image) => {
          this.tileSources.push({
            type: 'image',
            url: image
          });
        });
        const ops = OpenSeaDragon;
        console.log(this.tileSources);
        const viewer = ops({
          id: 'dragon',
          type: 'image',
          tileSources: this.tileSources,
          sequenceMode: (this.tileSources.length > 1),
          buildPyramid: false,
          // visibilityRatio: 1.0,
          maxZoomPixelRatio: 5,
          constrainDuringPan: true,
          showNavigator: true,
          toolbar: 'toolbar',
          zoomInButton: 'zoom_in',
          zoomOutButton: 'zoom_out',
          homeButton: 'home',
          fullPageButton: 'fullscreen',
          nextButton: 'navigate_next',
          previousButton: 'navigate_before',
        });
        if (this.selectedIndex) {
          viewer.goToPage(this.selectedIndex);
        }

      });
  }
}


