import {
    Component,
    OnInit,
    OnDestroy,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    Output,
    ChangeDetectorRef
} from '@angular/core';
import { FlatTreeControl } from '@angular/cdk/tree';
import { MatTreeFlatDataSource, MatTreeFlattener } from '@angular/material/tree';
import { SelectionModel } from '@angular/cdk/collections';
import {
    MatSnackBar,
    MatSnackBarVerticalPosition,
    MatSnackBarHorizontalPosition
} from '@angular/material/snack-bar';
import { TranslateService } from '@ngx-translate/core';
import { Subject, Subscription, BehaviorSubject, Observable } from 'rxjs';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';

import { environment } from '@environment/environment';
import {
    LayerNode, EnumLayerType,
} from './layers.model';
import { LayersService } from './layers.service';
import { filter } from 'rxjs/operators';

@Component({
    selector: 'app-layers',
    templateUrl: './layers.component.html',
    styleUrls: ['./layers.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class LayersComponent implements OnInit, OnDestroy {

    @Input() layerTypes: Subject<LayerNode[]>;
    @Input() layersAdded: BehaviorSubject<LayerNode[]>;

    @Output() readonly layersAdd = new EventEmitter<LayerNode[]>();
    @Output() readonly layersOrder = new EventEmitter<LayerNode[]>();
    @Output() readonly layersRemove = new EventEmitter<LayerNode>();
    @Output() readonly layersToggleVisibility = new EventEmitter<LayerNode>();
    @Output() readonly changeAttr = new EventEmitter<LayerNode>();

    LAYERS_DRAG_TYPES = ['layers'];
    EnumLayerType: any = EnumLayerType;
    treeControl: FlatTreeControl<LayerNode>;
    treeFlattener: MatTreeFlattener<LayerNode, LayerNode>;
    dataSource: MatTreeFlatDataSource<LayerNode, LayerNode>;
    hasLayers: boolean;
    searchDone = false;
    /** The selection for checklist */
    checklistSelection = new SelectionModel<LayerNode>(true /* multiple */);

    layerEditable: LayerNode;
    layerEditableType: string;

    nodeAddLayerHovered: number;
    nodeRemoveLayerHovered: number;
    layersSelectedList: LayerNode[] = [];
    layersSelectedText = '';

    listOpened = true;
    selectedListOpened = false;

    LAYER_TYPE_SINGLE: string;
    LAYER_TYPE_GROUP: string;

    evtlayersSelected: Subscription;
    evtlayerTypes: Subscription;

    legend$: Observable<string>;

    constructor(
        private snackBar: MatSnackBar,
        private translateService: TranslateService,
        private cd: ChangeDetectorRef,
        private layersService: LayersService,
    ) {
        this.translateService.get([
            'layers.type.single',
            'layers.type.group'
        ])
            .toPromise()
            .then((response) => {
                this.LAYER_TYPE_SINGLE = response['layers.type.single'];
                this.LAYER_TYPE_GROUP = response['layers.type.group'];
            });

    }

    ngOnInit() {
        this.evtlayersSelected = this.layersAdded
            .subscribe((layersSelected) => {
                this.layersSelectedList = layersSelected || [];
                this.layersSelectedText = (this.layersSelectedList.length > 0) ? ` (${this.layersSelectedList.length})` : null;
                this.cd.markForCheck();
            });
        this.evtlayerTypes = this.layerTypes
            .pipe(filter((data) => {
                return !!data;
            }))
            .subscribe((pnl) => {
                this.hasLayers = (pnl.length > 0);
                this.searchDone = true;
                this.setNewDatasource(pnl);
            });
    }

    ngOnDestroy() {

    }

    trackById(index, item) {
        return item.idLayer;
    }

    setNewDatasource(data) {
        this.treeFlattener = new MatTreeFlattener(this.transformer, this.getLevel,
            this.isExpandable, this.getChildren);
        this.treeControl = new FlatTreeControl<LayerNode>(this.getLevel, this.isExpandable);
        this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);
        this.dataSource.data = data;
        this.cd.markForCheck();
    }

    getLevel = (node: LayerNode) => {
        return node.level;
    }

    isExpandable = (node: LayerNode) => {
        return node.expandable;
    }

    getChildren = (node: LayerNode): LayerNode[] => {
        return node.children;
    }

    hasChild = (_: number, _nodeData: LayerNode) => {
        return _nodeData.expandable;
    }

    transformer = (node: LayerNode, level: number) => {
        const flatNode = new LayerNode();
        flatNode.idLayer = node.idLayer;
        flatNode.idWorkspace = node.idWorkspace;
        flatNode.item = node.item;
        if (!!flatNode.item) {
            flatNode.item.transparency = 0;
        }
        flatNode.level = level;
        flatNode.denom = node.denom;
        flatNode.expandable = !!node.children;
        flatNode.selected = !!node.selected;
        return flatNode;
    }

    editLayer(node: LayerNode) {
        if (!!this.layerEditable && (node.idLayer === this.layerEditable.idLayer)) {
            this.layerEditable = null;
            this.layerEditableType = null;
        } else {
            this.layerEditable = node;
            this.layerEditableType = (node.item.type === EnumLayerType.SINGLE) ? this.LAYER_TYPE_SINGLE : this.LAYER_TYPE_GROUP;
        }
        if (this.layerEditable) {
            this.legend$ = this.layersService.getLegend(this.layerEditable.denom);
        }
    }

    changeTransparency(attrValue: number) {
        this.layerEditable.item.transparency = (attrValue * 100);
        this.changeAttr.emit(this.layerEditable);
    }

    showAddLayer(node: LayerNode) {
        this.nodeAddLayerHovered = node.idLayer || node.idWorkspace;
    }

    hideAddLayer() {
        this.nodeAddLayerHovered = null;
    }

    showActionLayer(node: LayerNode) {
        this.nodeRemoveLayerHovered = node.idLayer;
    }

    hideActionLayer() {
        this.nodeRemoveLayerHovered = null;
    }

    addLayer(node: LayerNode) {
        this.layerEditable = null;
        this.layerEditableType = null;
        const layerNodeFound = this.layersSelectedList.find((layer) => {
            return (layer.idLayer === node.idLayer);
        });
        if (!!layerNodeFound) {
            this.snackBar.open(this.translateService.instant('layers.addedprev'), null, {
                duration: environment.toast.duration,
                verticalPosition: <MatSnackBarVerticalPosition>environment.toast.verticalPosition,
                horizontalPosition: <MatSnackBarHorizontalPosition>environment.toast.horizontalPosition,
            });
        } else {
            this.layersAdd.emit([node]);
        }
    }

    unselectLayer() {
        this.layerEditable = null;
    }

    delLayer(node: LayerNode) {
        this.layerEditable = null;
        this.layerEditableType = null;
        this.layersRemove.emit(node);
    }

    toggleLayer(node: LayerNode) {
        this.layerEditable = null;
        this.layerEditableType = null;
        node.enabled = (node.enabled === false) ? true : false;
        this.layersToggleVisibility.emit(node);
    }

    expandNode(node: LayerNode) {
        if (this.treeControl.isExpanded(node)) {
            this.treeControl.collapseDescendants(node);
            return;
        }
        this.treeControl.expandDescendants(node);
    }

    addWorkspace(node: LayerNode) {
        this.layerEditable = null;
        this.layerEditableType = null;
        const descendants = this.treeControl.getDescendants(node);
        const newNodes: LayerNode[] = [];
        descendants.forEach((layerDesc) => {
            const layerNodeFound = this.layersSelectedList.find((layer) => {
                return (layer.idLayer === layerDesc.idLayer);
            });
            if (!layerNodeFound) {
                newNodes.push(layerDesc);
            }
        });
        if (newNodes.length > 0) {
            this.layersAdd.emit(newNodes);
        } else {
            this.snackBar.open(this.translateService.instant('layers.alladdedprev'), null, {
                duration: environment.toast.duration,
                verticalPosition: <MatSnackBarVerticalPosition>environment.toast.verticalPosition,
                horizontalPosition: <MatSnackBarHorizontalPosition>environment.toast.horizontalPosition,
            });
        }

    }

    drop(event: CdkDragDrop<string[]>) {
        moveItemInArray(this.layersSelectedList, event.previousIndex, event.currentIndex);
        if (event.previousIndex !== event.currentIndex) {
            this.layersOrder.emit(this.layersSelectedList);
        }

    }

    toggleList() {
        this.listOpened = !this.listOpened;
    }

    toggleSelected() {
        this.selectedListOpened = !this.selectedListOpened;
        // this.layerEditable = null;
    }

}
