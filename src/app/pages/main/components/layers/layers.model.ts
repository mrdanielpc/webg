export enum EnumLayerType {
    SINGLE = 'SINGLE',
    GROUP = 'GROUP'
}
export interface IMainLayer {
    idLayer: number;
    name: string;
    label: string;
    title: string;
    observations: string;
    type: EnumLayerType;
    transparency: number;
    gisServerUrl: string;
    idWorkspace?: number;
    queryable?: string | boolean;
}

export interface IMainWorkspace {
    idWorkspace: number;
    title: string;
    layers: IMainLayer[];
}

export interface ILayer {
    layers?: IMainLayer[];
    workspaces?: IMainWorkspace[];
}

export class LayerNode {
    idLayer: number;
    idWorkspace: number;
    selected: boolean;
    item: IMainLayer;
    level: number;
    expandable: boolean;
    denom: string;
    children: LayerNode[];
    type: any;
    enabled?: boolean;
    queryable?: boolean;
}

export interface IServer {
    xml: string;
    url: string;
}
