import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiEnvironmentService } from '@app/core/api-environment.service';
import { switchMap, map, tap, filter } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class LayersService {

    constructor(
        private http: HttpClient,
        private apiEnvironment: ApiEnvironmentService,
    ) {

    }
    /* tslint:disable */
    // https://docs.geoserver.org/stable/en/user/services/wms/get_legend_graphic/index.html

    // http://localhost:8090/geoserver/wms?request=GetLegendGraphic&version=1.0.0&layer=nurc:Arc_Sample&format=image/png&width=40&height=40&legend_options=forceRule:True;forceLabels:off;labelMargin:15;dpi:240;borderColor:ffffff;border:true;
    /* tslint:enable */

    private arrayToQueryString(paramsArray: []): string {
        let text = '';
        for (let idx = 0; idx < paramsArray.length; idx++) {
            const key = Object.keys(paramsArray[idx])[0];
            const value = Object.values(paramsArray[idx])[0];
            text += `${idx === 0 ? '?' : '&'}${key}=${value}`;
        }
        return text;
    }

    private checkPNGImage(base64: string) {

        const src = base64;
        const imageData = Uint8Array.from(atob(src.replace('data:image/png;base64,', '')), c => c.charCodeAt(0));
        const sequence = [0, 0, 0, 0, 73, 69, 78, 68, 174, 66, 96, 130]; // in hex:

        // check last 12 elements of array so they contains needed values
        for (let i = 12; i > 0; i--) {
            if (imageData[imageData.length - i] !== sequence[12 - i]) {
                return false;
            }
        }

        return true;
    }

    getLegend(layerName: string): Observable<string> {
        const URL_BASE = this.apiEnvironment.getConfig().geoserver.url;
        let URL = `${URL_BASE}/wms`;
        // let legendOptions = 'forceRule:True;forceLabels:off;labelMargin:15;dpi:240;';
        let legendOptions = 'forceRule:True;forceLabels:on;labelMargin:15;fontSize:16;fontName:Helvetica;';
        legendOptions = legendOptions.concat('dx:0.2;dy:0.2;mx:0.2;my:0.2;');
        // let legendOptions = 'forceRule:True;forceLabels:off;labelMargin:15;dpi:240;borderColor:ffffff;border:true;countMatched:true;';
        // legendOptions = legendOptions.concat('dx:0.2;dy:0.2;mx:0.2;my:0.2;');
        const paramsArray = [
            { request: 'GetLegendGraphic' },
            { version: '1.0.0' },
            { layer: layerName },
            { format: 'image/png' },
            { width: 40 },
            { height: 40 },
            { legend_options: legendOptions },
        ];

        const q = this.arrayToQueryString(<[]>paramsArray);
        URL += q;

        const headers = new HttpHeaders({
            'Content-Type': 'image/png'
        });

        return this.http.get(URL, {
            headers: headers,
            responseType: 'arraybuffer'
        })
            .pipe(filter((img) => {
                return (img.byteLength > 0);
            }))
            .pipe(map((img) => {
                const base64 = `data:image/png;base64,${btoa(String.fromCharCode(...new Uint8Array(img)))}`;
                if (this.checkPNGImage(base64)) {
                    return base64;
                } else {
                    // tslint:disable-next-line: max-line-length
                    // return `data:image/png;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBggIBwgLDwsQCBYKEAgYBxsIFRAWIB0iIiAdHx8kKDQsJCYxJx8fLTItMTU3Oi4uFx8zODMsNygtLisBCgoKBQUFDgUFDisZExkrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrKysrK//AABEIAKAA+AMBIgACEQEDEQH/xAAbAAEBAQADAQEAAAAAAAAAAAAABQQBAwYCB//EAEMQAAEDAgIFBwcJBwUAAAAAAAABAgMEEQUSEyEiMVEGFEFUYXGUFlOBgsHR4hUjMjNCRFKy8EORkqGjsfEkJTQ1Yv/EABQBAQAAAAAAAAAAAAAAAAAAAAD/xAAUEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwD9TAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAADJidWlHSPk+0uy1O0CPj9eq1DIIHqmRbq9HWu4+qDHnJZlYmZPOpv9J04FSLVVazy62MXMva4rV+EU1Zd6IkcvnEbsr3p7gNsM0c7EfC9HJxOw8jJDXYPKi60Rf2ibbHFagxyKazKlEY78X2VAsA4RUVLovpOQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACByp+6+v7CfTYRXVMLJoYmqxb2XnLWf3UD1xExfD8Qrqi8cbdEiWb/qmJft3mH5BxLzLPGM95x8g4j0ws8Yz3gejoqZtJSshZrsl1XNa69JiWHFJMQimkha2Fj/oc6Yur95KTAcS8y3xjPePkHErfUM8Yz3gepWytc11laupWrrRSNX4BHJd9C5Gr5lXbK9y/57yNW0FTQ5OcsRM17fPpJu7lLtBUtpMCjmeirbNs8doCRDV1uFTLDIjk4wOaXqHFKer1IuV/m19hipsTixN6UlbTNst8q5t3uJ2MUMdBOxsMqrdM2RfpNA9aCbgS1LqJJKl6qirsXbrsUgAAAAAAAAAAAAAAAAAAAAAAAAAAAgcqfunrewoYF/1UHrfmUn8qfunrew14XMkGBMlX7LHO/moHwuLpHij6eT6rUxH8FK3RdF9J4R6q9yuXeqqpYwzGUpoViqczsuti71A9IeRnxSsWqc/SubZypo9yIXMJxFa986OY1uW1kNb6Kkkm00lLGsn4938twEPlC5ZIMPe5tlWNXK3huN2GU0dXgUcMt8qo7aTem0ZeVOvmvr+woYEv+1U/rfmUDPT4VBhivq5qhX5GqqJkyWJEDJMVxK7l1OXO5fwt/VjZyjrcz0pI11N2n95RwSi5pSZnp84/bXsToQCg1qNajWJZqJZG8DkAAAAAAAAAAAAAAAAAAAAAAAAAAAAIHKn7r63sOKOqw9+FR0lXM9u9Vamr7V+ClLEsNZiGjzzqzLf9hpb39KGLybi68/wPxAZ9Dye6xU/xfANDyf6xU/xfAaPJuLrz/A/GPJuLrz/A/GBzRz4NRPc6nqZbqmWy3en5TX8s4f1j+k73GPybi68/wPxjyai68/wPxAY8frIKvQc3fmy5r7KpbdxEqVDMKw+en0mxnvInRd3SbPJuLrz/AAPxFShpkoqZkDXq5Evt5Ml79npA85gtItdWrNNrYxdI5d916EPUqfLIookVIo2NRXZ1RG2S59gAAAAAAAAAAAAAAAAAAAAAAAAAAABJgxKSSiqnqjUlYiuTZ3puubJa6CF2WR+uyKqoxVRveBqBmr53QUUs0droiKnSh8y4jTRSujket2qjXfNLZANYMqV9OsT5M62a7IqZFRb9xxz+myK7M76zR6PRLmvwsBrBlSvpliSVJFtpNEmwt1XhY7oJmTszxu1XVOFlQDsBgkrXtxFsKNTRXSNz8utHKl0O9auBGvdpPoypEuz0gaAZW11OsqRo9b5ljR+Rct+Fzqjq1WWBNIxzFje90mRWfRUDeDBJiMSwTOhcudIFlbeJWXTicPxJjqKZ8LvnGw6SyxKy/aBQBlkrqeJ+SR+vVmXJqbfiagAAAAAAAAAAAAAAAAAAAAACNLh1Q7DWJGxUnbnbk/E1zv0p3yw1TOesip3uSZiIj03JqttdxSOAMldTvdhj6eNMz9EjLcTqnpZ3JV5WLt1Ub0TiiWuUEOQJs8FVnqHRxvVHVTXqqWV6tRNeW/adDIKiGpbPo5b86VzYnTo97kVtunpLJ1zQxTMyzRo5L3tmVLfuAkQQzTN0yMlu3EJHOjZLkel9Wz3FCghdEyRXRyNzyq/K6XSOXvNLI44mJHCxGsTcxOg51AS1w+WSlllV0qVDpFmSHOmVFvs6rdidJ9LSVEmIxyKxdCqtmf2PRN39imAJCU9Y+WBZYZ8zavSPdnRkdv8Ay05ipKuOKG0e22GVNbb2VV1FYARlpKl+ddBUf8F0N3vRdrgidCH3NTVVQx6uppGZaBYEauq69nYhV6bgCVNSTI+qakNQ9JLKisn0bN1truKqJZEROjVxOQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAB//9k=`;
                    return 'assets/images/noimage.jpg';
                }
            }));

    }
}
