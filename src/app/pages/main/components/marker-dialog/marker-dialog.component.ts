import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IMarkerDialog, IMapMarker } from '../markers/markers.model';


@Component({
    selector: 'app-marker-dialog',
    templateUrl: './marker-dialog.component.html',
    styleUrls: ['./marker-dialog.component.scss']
})
export class MarkerDialogComponent {

    draggable = true;
    useHandle = true;
    isCollapsed = false;

    @Input() marker: IMarkerDialog;
    @Output() readonly editMarkerDialog = new EventEmitter<IMapMarker>();
    @Output() readonly closeMarkerDialog = new EventEmitter<IMapMarker>();

    constructor() {
    }

    save() {
        this.closeMarkerDialog.emit(this.marker.marker);
        this.editMarkerDialog.emit(this.marker.marker);
    }

    close() {
        this.closeMarkerDialog.emit(this.marker.marker);
    }

    toggleCollapse() {
        this.isCollapsed = !this.isCollapsed;
    }

}
