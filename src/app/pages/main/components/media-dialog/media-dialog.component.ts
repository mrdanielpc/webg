import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    EventEmitter,
    Output,
    ChangeDetectorRef,
    OnDestroy,
} from '@angular/core';
import * as moment from 'moment';
import { Subject, Subscription } from 'rxjs';
import { take } from 'rxjs/operators';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';

import { MediaService } from '../media/media.service';
import { WebGisState } from '@app/redux/global.reducer';
import { IMediaResource, IUserRequest } from '@app/components/media-list/media.model';
import { IAlbum, IAlbumFullData, IGallery } from '../../main/main.model';
import { WgLightboxComponent } from '../wg-lightbox/wg-lightbox.component';

@Component({
    selector: 'app-media-dialog',
    templateUrl: './media-dialog.component.html',
    styleUrls: ['./media-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MediaDialogComponent implements OnInit, OnDestroy {

    resource: IMediaResource = null;
    @Input() mediaResourceSelected$: Subject<IMediaResource>;
    @Output() readonly close = new EventEmitter<IMediaResource>();

    title: string;

    isCollapsed = false;
    private userReq: IUserRequest;

    resourceSelected: IMediaResource;
    private mainResource: IMediaResource;

    images: IAlbum[] = [];
    imagesFullData: IAlbumFullData[] = [];

    idResourceSelected: string;

    evtResourceSelected: Subscription;

    constructor(
        private mediaService: MediaService,
        private store: Store<WebGisState>,
        private cd: ChangeDetectorRef,
        private dialog: MatDialog,
    ) {

    }

    ngOnInit() {
        this.observeUserRequest();
        this.observeResources();
    }

    ngOnDestroy() {
        if (!!this.evtResourceSelected) {
            this.evtResourceSelected.unsubscribe();
        }
    }

    private observeUserRequest() {
        this.store.select('webgis', 'media', 'userReq')
            .pipe(take(1))
            .subscribe((userRequest: IUserRequest) => {
                this.userReq = userRequest;
            });
    }

    private observeResources() {
        this.evtResourceSelected = this.mediaResourceSelected$.asObservable()
            .subscribe((resource) => {
                if (!!resource) {
                    const _date = resource.mediaInfo.filter((info) => {
                        return info.name === 'exif:DateTimeOriginal';
                    })[0];

                    let date;
                    if (_date) {
                        date = moment(_date.value).format('L LTS');
                    }

                    const _contentType = resource.mediaInfo.filter((info) => {
                        return info.name === 'ContentType';
                    })[0];
                    const contentType = !!_contentType ? _contentType.value : '';
                    const _captureType = resource.mediaInfo.filter((info) => {
                        return info.name === 'CaptureType';
                    })[0];
                    const captureType = !!_captureType ? _captureType.value : '';
                    this.title = `${date} - ${contentType} - ${captureType}`;
                    this.mainResource = resource;
                    this.resource = resource;
                    this.setResourceSelected(resource);
                } else {
                    this.mainResource = null;
                    this.resource = null;
                }

                this.cd.markForCheck();
            });

    }

    closeButton() {
        this.close.emit(this.resource);
    }

    toggleCollapse() {
        this.isCollapsed = !this.isCollapsed;
    }

    setResourceSelected(resource: IMediaResource) {
        this.resourceSelected = { ...resource };

        this.idResourceSelected = resource.id;

        if (!!this.resourceSelected.relatedResources && this.resourceSelected.relatedResources.length > 0) {

            if (!this.resource.isVideo) {
                const main: IAlbum = {
                    caption: this.resource.name,
                    src: this.resource.resourceUrl,
                    thumb: this.resource.resourceUrl
                };
                this.images = [main];
                this.imagesFullData = [{
                    ...main,
                    id: this.resource.id
                }];
            }

            this.resourceSelected.relatedResources
                .forEach((relResource) => {
                    if (!relResource.isVideo) {
                        this.images.push({
                            caption: relResource.name,
                            src: relResource.resourceUrl,
                            thumb: relResource.resourceUrl
                        });
                        this.imagesFullData.push({
                            caption: relResource.name,
                            src: relResource.resourceUrl,
                            thumb: relResource.resourceUrl,
                            id: relResource.id
                        });
                    }
                });
        } else {
            if (!!this.resource && !this.resource.isVideo && (this.mainResource.id === resource.id)) {
                const main: IAlbum = {
                    caption: this.resource.name,
                    src: this.resource.resourceUrl,
                    thumb: this.resource.resourceUrl
                };
                this.images = [main];
                this.imagesFullData = [{
                    ...main,
                    id: this.resource.id
                }];
            }
        }

        if (!this.resourceSelected.mediaInfo) {
            this.mediaService.getResourceInfo(this.userReq, this.resourceSelected)
                .pipe(take(1))
                .subscribe((mediaInfo) => {
                    this.resourceSelected.mediaInfo = [...mediaInfo];
                    this.cd.markForCheck();
                });
        }

    }

    openLightbox(): void {
        // open lightbox
        let index = this.imagesFullData.findIndex((res) => {
            return (res.id === this.resourceSelected.id);
        });
        if (index === -1) {
            index = 0;
        }
        const dialogRef = this.dialog.open(WgLightboxComponent, {
            data: <IGallery>{
                index: index,
                images: this.imagesFullData,
                title: this.title
            },
            width: '100vw',
            height: '100vh'
        });

        dialogRef.afterClosed()
            .pipe(take(1))
            .subscribe((result) => {
                console.log(result);
            });
    }

    downloadResource(resource: IMediaResource) {
        const fileName = resource.name;
        const a = window.document.createElement('a');
        a.href = resource.downloadUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();
        document.body.removeChild(a);
    }
}
