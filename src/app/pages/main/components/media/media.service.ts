import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import {
    IMediaResource,
    IMediaList,
    IMediaInfo,
    IUserRequest,
    IInterval,
    EnumInterval
} from '@app/components/media-list/media.model';
import * as moment from 'moment';
import { environment } from '@environment/environment';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { MatSnackBar, MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';

@Injectable({
    providedIn: 'root'
})
export class MediaService {

    userReq: IUserRequest;

    constructor(
        private http: HttpClient,
        private translateService: TranslateService,
        private snackBar: MatSnackBar,
    ) {
    }

    getFakeToken(userReq: IUserRequest): string {
        if (!!userReq && !this.userReq) {
            this.userReq = userReq;
        } else if (!userReq) {
            userReq = this.userReq;
        }
        return btoa(userReq.user + ':' + userReq.password);
    }

    private addAuthHeaders(userReq: IUserRequest): HttpHeaders {
        const headers = new HttpHeaders({
            'x-auth-token': this.getFakeToken(userReq),
            'Content-Type': 'application/json',
        });
        return headers;
    }

    private overrideResourceUrl(resource: IMediaResource, token: string): string {
        const resourceUrl = `/viewer/resources/view/${resource.id}`;
        resource.resourceUrl = resourceUrl;

        switch (true) {
            case resource.isVideo:
                break;
            case resource.isImage:
                resource.downloadUrl = `${resourceUrl}?token=${token}`;
                if (resource.contentType === 'image/tiff') {
                    resource.resourceUrl = resource.resourceUrl + '/' + environment.tools.media.formatFile;
                }
                break;
            case resource.isGeo:
                resource.resourceUrl = 'assets/clip.svg';
                resource.downloadUrl = `${resourceUrl}?token=${token}`;
                break;
            default:
                break;
        }

        return `${resource.resourceUrl}?token=${token}`;
    }

    getResourceInfo(userReq: IUserRequest, mediaResource: IMediaResource): Observable<IMediaInfo[]> {
        const url = `viewer/resources/get/${mediaResource.id}`;
        const headers = this.addAuthHeaders(userReq);
        return this.http.get<IMediaInfo[]>(url, { headers });
    }

    private transformResource(resource: IMediaResource, userReq: IUserRequest, idMediaList: number): IMediaResource {
        resource.idMediaList = idMediaList;
        resource.isVideo = (resource.contentType.indexOf('video') !== -1);
        resource.isImage = (resource.contentType.indexOf('image') !== -1);
        resource.isGeo = (!resource.isVideo && !resource.isImage);
        resource.isGeolocated = (resource.latitude !== 0 && resource.longitude !== 0);
        resource.resourceUrl = this.overrideResourceUrl(resource, this.getFakeToken(userReq));
        return resource;
    }

    getResourcesMediaList(userReq: IUserRequest, resource: IMediaList): Observable<IMediaResource[]> {
        const url = `viewer/resources/find?`;
        const headers = this.addAuthHeaders(userReq);
        let params = new HttpParams();
        const startDate = moment(resource.startDate).format('YYYYMMDDT000000');
        const endDate = moment(resource.endDate).format('YYYYMMDDT235959');

        params = params
            .set('startDate', startDate)
            .set('endDate', endDate);

        if (!!resource.customerId) {
            params = params.set('customerId', resource.customerId);
        }
        if (!!resource.processType) {
            params = params.set('processType', resource.processType);
        }
        if (resource.captureType) {
            params = params.set('captureType', resource.captureType);
        }

        return this.http.get<IMediaResource[]>(url, { headers, params })
            .pipe(tap((mediaResrources: IMediaResource[]) => {
                if (mediaResrources.length === 0) {
                    this.snackBar.open(this.translateService.instant('webgis.http.204'), null, {
                        duration: environment.toast.duration,
                        verticalPosition: <MatSnackBarVerticalPosition>environment.toast.verticalPosition,
                        horizontalPosition: <MatSnackBarHorizontalPosition>environment.toast.horizontalPosition,
                    });
                }
            }))
            .pipe(map((mediaResrources: IMediaResource[]) => {
                mediaResrources.map((mediaResource) => {
                    mediaResource.visible = true;
                    mediaResource = this.transformResource(mediaResource, userReq, resource.id);

                    mediaResource.relatedResources.forEach((related: IMediaResource) => {
                        related = this.transformResource(related, userReq, resource.id);
                    });

                });
                return mediaResrources;
            }));
    }

    getIntervals(): IInterval[] {
        const intervals: IInterval[] = [];
        intervals.push({
            'text': this.translateService.instant('media.intervals.twodays'),
            'value': EnumInterval.first,
            'checked': false,
            'minDays': 2
        });
        intervals.push({
            'text': this.translateService.instant('media.intervals.fifteendays'),
            'value': EnumInterval.second,
            'checked': false,
            'minDays': 15
        });
        intervals.push({
            'text': this.translateService.instant('media.intervals.thirtydays'),
            'value': EnumInterval.third,
            'checked': false,
            'minDays': 30
        });
        intervals.push({
            'text': this.translateService.instant('media.intervals.free'),
            'value': EnumInterval.free,
            'checked': false,
            'minDays': null
        });
        return intervals;
    }

}
