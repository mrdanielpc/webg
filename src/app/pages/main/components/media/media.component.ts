import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    OnDestroy,
    ChangeDetectorRef,
    Output,
    EventEmitter
} from '@angular/core';
import { Subject, forkJoin, Observable, BehaviorSubject } from 'rxjs';
import { takeUntil, take, map, filter } from 'rxjs/operators';
import * as moment from 'moment';
import { TranslateService } from '@ngx-translate/core';
import { MAT_MOMENT_DATE_FORMATS, MomentDateAdapter } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDialog } from '@angular/material/dialog';

import { MediaService } from './media.service';
import { MediaUserComponent } from '@app/components/media-list/media-user/media-user.component';
import { MapService } from '../map/map.service';
import { ILocation } from '../map/map.model';
import {
    IMediaList,
    IMediaResource,
    IUserRequest,
    IInterval,
    EnumInterval
} from '@app/components/media-list/media.model';


interface IMediaListResources extends IMediaList {
    resources?: IMediaResource[];
}

@Component({
    selector: 'app-media',
    templateUrl: './media.component.html',
    styleUrls: ['./media.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush,
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS },
    ],
})
export class MediaComponent implements OnInit, OnDestroy {

    // MediaList
    @Input() mediaList$: BehaviorSubject<IMediaList[]>;
    @Input() mediaListSelected$: Subject<IMediaList>;
    @Input() mediaResources$: Subject<IMediaResource[]>;
    @Input() mediaResourceSelected$: Subject<IMediaResource>;

    // User
    @Input() userReq$: BehaviorSubject<IUserRequest>;
    // MediaList
    @Output() readonly setMediaListSelected = new EventEmitter<IMediaList>();
    @Output() readonly toggleMediaVisibility = new EventEmitter<IMediaList>();
    @Output() readonly removeMediaList = new EventEmitter<IMediaList>();
    // Recursos
    @Output() readonly addResources = new EventEmitter<IMediaResource[]>();
    @Output() readonly setResourceSelected = new EventEmitter<IMediaResource>();
    @Output() readonly moveToLocation = new EventEmitter<ILocation>();
    // User
    @Output() readonly clearUser = new EventEmitter<any>();

    private unsubscribe$ = new Subject<void>();

    mediaList: IMediaListResources[] = [];
    mediaListText = '';
    mediaListWithResources: IMediaListResources[] = [];
    mediaResources: IMediaResource[] = [];
    mediaResourceSelected: IMediaResource;
    mediaListExpandedId: number;

    newOpened = true;
    listOpened = false;

    mediaHovered: IMediaList;
    mediaEditable: IMediaList;
    userReq: IUserRequest;

    intervals: IInterval[] = [];

    private T_EXPAND: string;
    private T_COLLAPSE: string;

    constructor(
        private cd: ChangeDetectorRef,
        private mediaService: MediaService,
        private dialog: MatDialog,
        private mapService: MapService,
        private translateService: TranslateService,
    ) {
    }

    ngOnInit() {
        this.observeUserRequest();
        this.observeMediaSelected();
        this.observeResources();
        this.observeMediaResourceSelected();
        this.observeMedia();
        this.intervals = [...this.mediaService.getIntervals()];
        this.translateService.get(['entity.action.expand', 'entity.action.collapse'])
            .pipe(take(1))
            .subscribe((response) => {
                this.T_COLLAPSE = response['entity.action.collapse'];
                this.T_EXPAND = response['entity.action.expand'];
            });
    }

    trackById(index, item) {
        return item.id;
    }

    toggleList() {
        this.listOpened = !this.listOpened;
    }

    toggleNew() {
        this.newOpened = !this.newOpened;
    }

    showActionMedia(media: IMediaList) {
        this.mediaHovered = media;
    }

    delMedia(media: IMediaList) {
        this.removeMediaList.emit(media);
    }

    setCenter(mediaResource: IMediaResource) {
        if (mediaResource.isGeolocated) {
            this.moveToLocation.emit({
                latitude: mediaResource.latitude,
                longitude: mediaResource.longitude
            });
        }
    }

    hideActionMedia() {
        this.mediaHovered = null;
    }

    setVisibility(media: IMediaList) {
        media.visible = !media.visible;
        this.toggleMediaVisibility.emit(media);
    }

    expandMediaList(id: number) {
        if (this.mediaListExpandedId === id) {
            this.mediaListExpandedId = null;
        } else {
            this.mediaListExpandedId = id;
        }
    }

    setMediaSelected(media: IMediaList) {
        this.setMediaListSelected.emit(media);
    }

    selectResource(mediaResource: IMediaResource) {
        const realPosition = this.mapService.getHorizontalCoord(document.getElementById('scene'), 600);
        mediaResource.mapCoordinates = realPosition;
        this.setResourceSelected.next({ ...mediaResource });
    }

    getToolTip(id: number): string {
        if (id !== this.mediaListExpandedId) {
            return this.T_EXPAND;
        } else {
            return this.T_COLLAPSE;
        }
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    private observeMediaSelected() {
        this.mediaListSelected$.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((mediaListSelected: IMediaList) => {
                this.mediaEditable = mediaListSelected;
                this.cd.markForCheck();
            });
    }

    private addResourcesToMediaList(resources: IMediaResource[]) {
        const idMediaLists = resources.map((res) => {
            return res.idMediaList;
        });
        const ids = [...new Set(idMediaLists)];
        ids.forEach((id) => {
            const mediaList = this.mediaListWithResources.find((media) => {
                return (media.id === id);
            });
            if (!mediaList.resources) {
                mediaList.resources = [];
            }
            mediaList.resources = resources.filter((resource) => {
                return (resource.idMediaList === id);
            });
        });
    }

    private getMediaResources(mediaList: IMediaList[]) {
        const mediaListForSearch: Observable<IMediaResource[]>[] = [];
        mediaList.forEach((media) => {
            const exists = this.mediaList.find((mediaSearch) => {
                return (media.id === mediaSearch.id);
            });
            // Buscamos la búsqueda por si es nueva.
            if (!exists) {
                media.visible = true;
                if (media.idInterval !== EnumInterval.free) {
                    const intervalSelected = this.mediaService.getIntervals()
                        .find((interval) => {
                            return (interval.value === media.idInterval);
                        });
                    const fromDate = moment().add(intervalSelected.minDays * -1, 'days');
                    const toDate = moment();
                    media.startDate = new Date(+fromDate.format('x'));
                    media.endDate = new Date(+toDate.format('x'));
                }

                mediaListForSearch.push(this.mediaService.getResourcesMediaList(this.userReq, media));
            }
            forkJoin(mediaListForSearch)
                .pipe(take(1))
                .pipe(
                    map((mediaRes: IMediaResource[][]) => {
                        const allResources = [].concat.apply([], mediaRes);
                        allResources.map((mediaItem) => {
                            mediaItem.visible = true;
                        });
                        return allResources;
                    })
                )
                .subscribe((mediaResources: IMediaResource[]) => {
                    this.addResources.emit(mediaResources);
                }, () => {
                    this.clearUser.next();
                });
        });
        this.mediaList = [...mediaList];
        this.mediaListText = (this.mediaList.length > 0) ? ` (${this.mediaList.length})` : null;
        this.mediaListWithResources = [...mediaList];
        this.cd.markForCheck();
    }

    private observeMediaResourceSelected() {
        this.mediaResourceSelected$.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((mediaResource: IMediaResource) => {
                this.mediaResourceSelected = { ...mediaResource };
                this.cd.markForCheck();
            });
    }

    private observeResources() {
        this.mediaResources$.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((mediaResources: IMediaResource[]) => {
                this.mediaResources = [...mediaResources];
                // Antes de nada hay que vaciarlo, porque sin idMediaList
                // la función de addResourcesToMediaList no va a saber
                // dónde tiene que vaciar / añadir los recursos
                // y puede ser que no se vacíen si teníamos recursos, cambiamos
                // la búsqueda, y devuelve 0 registros
                this.mediaListWithResources.forEach((mediaList) => {
                    mediaList.resources = [];
                });
                this.addResourcesToMediaList(mediaResources);
                this.mediaListExpandedId = null;
                this.cd.markForCheck();
            });
    }

    private convertMeaListToResources(mediaL: IMediaList[]) {
        if (!!this.userReq) {
            this.getMediaResources(mediaL);
        } else {
            const dialogRef = this.dialog.open(MediaUserComponent, {
                width: '400px',
                maxWidth: '100vw'
            });
            dialogRef.afterClosed()
                .pipe(take(1))
                .subscribe((result) => {
                    this.userReq = result;
                    if (!!this.userReq) {
                        this.getMediaResources(mediaL);
                    }
                });
        }
    }

    private observeMedia() {
        this.mediaList$.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .pipe((filter((mediaL) => {
                // return (!!mediaL && mediaL.length > 0);
                return !!mediaL;
            })))
            .subscribe((mediaL: IMediaList[]) => {
                this.convertMeaListToResources(mediaL);
            });
    }

    private observeUserRequest() {
        this.userReq$
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((userReq: IUserRequest) => {
                this.userReq = userReq;
                this.cd.markForCheck();
            });
    }

}
