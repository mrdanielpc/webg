import { Component, OnInit, ChangeDetectionStrategy, Inject, ChangeDetectorRef } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IGallery } from '../../main/main.model';

@Component({
    selector: 'app-wg-lightbox',
    templateUrl: './wg-lightbox.component.html',
    styleUrls: ['./wg-lightbox.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class WgLightboxComponent implements OnInit {

    images: string[] = [];
    selectedIndex: number;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: IGallery,
        private cd: ChangeDetectorRef,
    ) {
        const urls: string[] = [];
        this.data.images.forEach((image) => {
            urls.push(image.src);
            this.images.push(image.src);
        });
        this.selectedIndex = this.data.index;
        this.cd.markForCheck();
    }

    ngOnInit() {
    }

}
