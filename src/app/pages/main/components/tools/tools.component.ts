import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    Input,
    Output,
    EventEmitter,
    ChangeDetectorRef,
    OnDestroy,
} from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { distinctUntilChanged, debounceTime, take, takeUntil } from 'rxjs/operators';
import { environment } from '@environment/environment';
import { ITool, ToolsType } from './tools.model';
import { WebGisState } from '@app/redux/global.reducer';
import { Store } from '@ngrx/store';

@Component({
    selector: 'app-tools',
    templateUrl: './tools.component.html',
    styleUrls: ['./tools.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class ToolsComponent implements OnInit, OnDestroy {

    @Input() toolsTypes: BehaviorSubject<ITool[]>;
    @Input() toolsTypeSelected: Subject<ITool>;
    @Input() tools: BehaviorSubject<ITool[]>;
    @Input() toolMapEditable: Subject<ITool>;

    @Output() readonly selectToolType = new EventEmitter<ITool>();
    @Output() readonly setToolEditable = new EventEmitter<ITool>();
    @Output() readonly setToolEdit = new EventEmitter<ITool>();
    @Output() readonly setToolRemove = new EventEmitter<ITool>();

    toolsList: ITool[] = [];
    toolListAdded: ITool[] = [];
    toolListAddedText = '';

    selectedTool: ITool;
    toolEditable: ITool;

    listOpened = true;
    selectedListOpened = false;
    toolHovered: number;

    ToolsType: any = ToolsType;

    attrTextChangeObserver: any;
    attrLatitudeChangeObserver: any;
    attrLongitudeChangeObserver: any;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private cd: ChangeDetectorRef,
    ) { }



    ngOnInit() {
        this.observeTools();
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();

        if (!!this.attrTextChangeObserver) {
            this.attrTextChangeObserver.unsubscribe();
        }
        if (!!this.attrLatitudeChangeObserver) {
            this.attrLatitudeChangeObserver.unsubscribe();
        }
        if (!!this.attrLongitudeChangeObserver) {
            this.attrLongitudeChangeObserver.unsubscribe();
        }
    }

    trackByType(index, item) {
        return item.type;
    }

    private observeTools() {

        this.toolsTypes
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((tools: ITool[]) => {
                this.toolsList = tools;
                this.cd.markForCheck();
            });

        this.toolsTypeSelected.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((toolType: ITool) => {
                this.selectedTool = toolType;
                this.cd.markForCheck();
            });

        this.tools
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((tools: ITool[]) => {
                this.toolListAdded = tools;
                this.toolListAddedText = (this.toolListAdded.length > 0) ? ` (${this.toolListAdded.length})` : null;
                this.cd.markForCheck();
            });

        this.toolMapEditable.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((tool) => {
                this.toolEditable = tool;
                this.cd.markForCheck();
            });

    }

    toggleList() {
        this.listOpened = !this.listOpened;
    }

    toggleSelected() {
        this.selectedListOpened = !this.selectedListOpened;
    }

    showActionTool(tool: ITool) {
        this.toolHovered = tool.attributes.idMapTool;
    }

    hideActionTool() {
        this.toolHovered = null;
    }


    selectTooleditable(tool: ITool) {
        this.setToolEditable.emit(tool);
    }

    editTool() {
        this.setToolEdit.emit(this.toolEditable);
    }

    private setTool(tool: ITool) {
        if (
            ((!!tool && !!tool.type) &&
                ((!this.selectedTool) || (!!this.selectedTool && this.selectedTool.type !== tool.type)))
        ) {
            this.selectedTool = tool;
        } else {
            this.selectedTool = null;
        }
    }

    selectTool(tool: ITool) {
        this.setTool(tool);
        this.selectToolType.emit(this.selectedTool);
    }

    delTool(tool?: ITool) {
        if (!!tool) {
            this.setToolRemove.emit(tool);
        } else {
            this.setToolRemove.emit(this.toolEditable);
        }
        this.selectTooleditable(null);
    }

    changeToolAttribute(attr: string, value: any) {
        this.toolEditable.attributes[attr] = value;
        this.editTool();
    }

    changeToolAttributeText(attr: string, value: any) {
        if (!this.attrTextChangeObserver) {
            new Observable((observer) => {
                this.attrTextChangeObserver = observer;
            }).pipe(debounceTime(environment.debounceModel)) // wait 300ms after the last event before emitting last event
                .pipe(distinctUntilChanged()) // only emit if value is different from previous value
                .subscribe(() => {
                    this.changeToolAttribute(attr, this.toolEditable.attributes[attr]);
                });
        }
        this.attrTextChangeObserver.next(value);
    }

    onLatitudeChange() {

        if (!this.attrLatitudeChangeObserver) {
            new Observable((observer) => {
                this.attrLatitudeChangeObserver = observer;
            }).pipe(debounceTime(environment.debounceModel)) // wait 300ms after the last event before emitting last event
                .pipe(distinctUntilChanged()) // only emit if value is different from previous value
                .subscribe(() => {
                    this.toolEditable.attributes.center.latitude = this.toolEditable.attributes.points[0].latitude;
                    this.setToolEdit.emit(this.toolEditable);
                });
        }
        this.attrLatitudeChangeObserver.next(this.toolEditable.attributes.points[0].latitude);
    }

    onLongitudeChange() {

        if (!this.attrLongitudeChangeObserver) {
            new Observable((observer) => {
                this.attrLongitudeChangeObserver = observer;
            }).pipe(debounceTime(environment.debounceModel)) // wait 300ms after the last event before emitting last event
                .pipe(distinctUntilChanged()) // only emit if value is different from previous value
                .subscribe(() => {
                    this.toolEditable.attributes.center.longitude = this.toolEditable.attributes.points[0].longitude;
                    this.setToolEdit.emit(this.toolEditable);
                });
        }
        this.attrLongitudeChangeObserver.next(this.toolEditable.attributes.points[0].longitude);
    }


}
