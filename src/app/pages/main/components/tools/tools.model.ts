import { IPointLocation, IMeasurement } from '../map/map.model';

export enum ToolsType {
    LineString = 'LineString',
    Polygon = 'Polygon',
    MultiLineString = 'MultiLineString',
    MultiPolygon = 'MultiPolygon',
    Square = 'Square',
    Rectangle = 'Rectangle',
    Circle = 'Circle',
    Ellipse = 'Ellipse',
    Point = 'Point'
}

export interface IAttributes {
    color: string;
    transparency: number;
    width: number;
    idMapTool?: number;
    title?: string;
    observations?: string;
    points?: IPointLocation[];
    radius?: number;
    center?: IPointLocation;
    majorRadius?: number;
    minorRadius?: number;
    rotate?: number;
}

export interface ITool {
    type: ToolsType;
    icon?: string;
    svgIcon?: string;
    attributes?: IAttributes;
}

interface IMapCoordinates {
    x: number;
    y: number;
}

export interface IToolDialog {
    tool: ITool;
    mapCoordinates: IMapCoordinates;
    measurement: IMeasurement;
}
