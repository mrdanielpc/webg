import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IToolDialog, ToolsType, ITool } from '../tools/tools.model';
import { IMeasurement } from '../map/map.model';

@Component({
    selector: 'app-tools-dialog',
    templateUrl: './tools-dialog.component.html',
    styleUrls: ['./tools-dialog.component.scss']
})
export class ToolsDialogComponent {

    draggable = true;
    useHandle = true;
    isCollapsed = false;

    @Input() tool: IToolDialog;
    @Input() measurements: IMeasurement;

    @Output() readonly closeToolDialog = new EventEmitter<ITool>();


    ToolsType: any = ToolsType;
    constructor() {
    }

    close() {
        this.closeToolDialog.emit(this.tool.tool);
    }

    toggleCollapse() {
        this.isCollapsed = !this.isCollapsed;
    }


}
