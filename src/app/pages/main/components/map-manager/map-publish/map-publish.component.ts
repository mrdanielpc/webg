import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
    MatSnackBar,
    MatSnackBarVerticalPosition,
    MatSnackBarHorizontalPosition
} from '@angular/material/snack-bar';
import {
    FormGroup,
    FormControl
} from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';

import { UserService } from '@app/core/user.service';
import { IUser } from '@app/core/user.model';
import { environment } from '@environment/environment';
import { MapManagerService } from '../map-manager.service';
import { IRoleGroup, IMapRolesGroups } from '../map-manager.model';

@Component({
    selector: 'app-map-publish',
    templateUrl: './map-publish.component.html',
    styleUrls: ['./map-publish.component.scss']
})
export class MapPublishComponent implements OnInit {

    roles: IRoleGroup[] = [];
    groups: IRoleGroup[] = [];

    checkboxGroupRoles = new FormGroup({});
    checkboxGroupGroups = new FormGroup({});

    allowModifiedTarget = false;

    constructor(
        private mapManagerService: MapManagerService,
        public dialogRef: MatDialogRef<MapPublishComponent>,
        @Inject(MAT_DIALOG_DATA) public param: any,
        private snackBar: MatSnackBar,
        private translateService: TranslateService,
        private userService: UserService
    ) {
        const promises: Promise<IRoleGroup[]>[] = [];
        this.userService.getIdentity()
            .then((user: IUser) => {
                this.allowModifiedTarget = (user.id === param.userId);
            });
        promises.push(this.mapManagerService.getRoles().toPromise());
        promises.push(this.mapManagerService.getGroups().toPromise());

        const promisesData: Promise<IMapRolesGroups[]>[] = [];
        promisesData.push(this.mapManagerService.getRolesByMap(param.idMap).toPromise());
        promisesData.push(this.mapManagerService.getGroupsByMap(param.idMap).toPromise());

        Promise.all([...promises])
            .then((resultMaster) => {
                this.roles = resultMaster[0];
                this.groups = resultMaster[1];

                this.roles.forEach((roleList) => {
                    this.checkboxGroupRoles.addControl(roleList.id.toString(), new FormControl());
                });

                this.groups.forEach((groupList) => {
                    this.checkboxGroupGroups.addControl(groupList.id.toString(), new FormControl());
                });

                Promise.all([...promisesData])
                    .then((resultMap) => {

                        const rolesApplied = resultMap[0];
                        const groupsApplied = resultMap[1];

                        this.roles.forEach((roleList) => {
                            const roleInserted = rolesApplied.find((role) => {
                                return (role.wgRoleId === roleList.id);
                            });
                            if (roleInserted) {
                                roleList.objMapRoleGroup = roleInserted;
                                this.checkboxGroupRoles.controls[roleList.id].setValue(true);
                            } else {
                                this.checkboxGroupRoles.controls[roleList.id].setValue(false);
                            }

                        });

                        this.groups.forEach((groupList) => {
                            const groupInserted = groupsApplied.find((group) => {
                                return (group.wgGroupId === groupList.id);
                            });
                            if (groupInserted) {
                                groupList.objMapRoleGroup = groupInserted;
                                this.checkboxGroupGroups.controls[groupList.id].setValue(true);
                            } else {
                                this.checkboxGroupGroups.controls[groupList.id].setValue(false);
                            }

                        });

                    })
                    .catch(() => {
                        this.snackBar.open(this.translateService.instant('map.published.error.map'), null, {
                            duration: environment.toast.duration,
                            verticalPosition: <MatSnackBarVerticalPosition>environment.toast.verticalPosition,
                            horizontalPosition: <MatSnackBarHorizontalPosition>environment.toast.horizontalPosition
                        });
                    });

            })
            .catch(() => {
                this.snackBar.open(this.translateService.instant('map.published.error.master'), null, {
                    duration: environment.toast.duration,
                    verticalPosition: <MatSnackBarVerticalPosition>environment.toast.verticalPosition,
                    horizontalPosition: <MatSnackBarHorizontalPosition>environment.toast.horizontalPosition
                });
            });
    }

    ngOnInit() {
    }

    trackById(index, item) {
        return item.id;
    }

    updateTargetRoles(event, mapRoleGroup: IRoleGroup) {
        if (event.checked) {
            this.mapManagerService.addRole(this.param.idMap, mapRoleGroup.id)
                .subscribe((data) => {
                    this.roles.forEach((role) => {
                        if (role.id === mapRoleGroup.id) {
                            role.objMapRoleGroup = data;
                        }
                    });
                });
        } else {
            if (!!mapRoleGroup.objMapRoleGroup) {
                this.mapManagerService.deleteRole(mapRoleGroup.objMapRoleGroup.id).subscribe();
            }
        }
    }

    updateTargetGroups(event, mapRoleGroup: IRoleGroup) {
        if (event.checked) {
            this.mapManagerService.addGroup(this.param.idMap, mapRoleGroup.id)
                .subscribe((data) => {
                    this.groups.forEach((group) => {
                        if (group.id === mapRoleGroup.id) {
                            group.objMapRoleGroup = data;
                        }
                    });
                });
        } else {
            if (!!mapRoleGroup.objMapRoleGroup) {
                this.mapManagerService.deleteGroup(mapRoleGroup.objMapRoleGroup.id).subscribe();
            }
        }
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
