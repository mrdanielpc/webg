import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ISaveMapData } from '@app/pages/main/main/main.model';

@Component({
    selector: 'app-map-save',
    templateUrl: './map-save.component.html',
    styleUrls: ['./map-save.component.scss']
})
export class MapSaveComponent implements OnInit {

    mapForm: FormGroup;
    model: ISaveMapData;

    constructor(
        private formBuilder: FormBuilder,
        public dialogRef: MatDialogRef<MapSaveComponent>,
        @Inject(MAT_DIALOG_DATA) public data: ISaveMapData,
    ) {
        this.model = data;
        this.mapForm = this.formBuilder.group({
            title: ['', Validators.required],
            observations: ['']
        });

    }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
