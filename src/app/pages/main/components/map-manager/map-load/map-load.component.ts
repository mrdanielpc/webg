import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { DateAdapter, MAT_DATE_LOCALE, MAT_DATE_FORMATS } from '@angular/material/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { MomentDateAdapter, MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { TranslateService } from '@ngx-translate/core';

import { IMap } from '../map-manager.model';
import { MapManagerService } from '../map-manager.service';
import { IField, IFilter } from '@app/components/filter-table/filter-table.model';

@Component({
    selector: 'app-map-load',
    templateUrl: './map-load.component.html',
    styleUrls: ['./map-load.component.scss'],
    providers: [
        { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
        { provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS }
    ],
})
export class MapLoadComponent implements OnInit {

    @ViewChild(MatSort, { static: true }) sort: MatSort;

    displayedColumns = ['id', 'title', 'userLogin', 'observations', 'created_at', 'modified_at'];
    dataSource: MatTableDataSource<IMap>;
    fieldsSearch: IField[] = [];

    selectedValue: string;

    constructor(
        private mapManagerService: MapManagerService,
        public dialogRef: MatDialogRef<MapLoadComponent>,
        private translateService: TranslateService,
        @Inject(MAT_DIALOG_DATA) public idMap: number,
    ) {


        this.fieldsSearch = [
            {
                value: 'id',
                viewValue: this.translateService.instant('map.fields.id'),
                defaultFilter: false
            },
            {
                value: 'title',
                viewValue: this.translateService.instant('map.fields.title'),
                defaultFilter: false
            },
            {
                value: 'userLogin',
                viewValue: this.translateService.instant('map.fields.userLogin'),
                defaultFilter: false
            },
            {
                value: 'observations',
                viewValue: this.translateService.instant('map.fields.observations'),
                defaultFilter: false
            }
        ];
    }

    ngOnInit() {
        this.dataSource = new MatTableDataSource([]);
        this.dataSource.sort = this.sort;
        this.search();
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    closeDialog(id: number) {
        this.dialogRef.close(id);
    }

    private changeFilterField(valueSearch: string) {
        if (!!valueSearch) {
            this.dataSource.filterPredicate = function (data, value: string): boolean {
                return (!!data[valueSearch] && data[valueSearch].toString().toLocaleLowerCase().includes(value));
            };
        } else {
            this.dataSource.filterPredicate = function (data, value: string): boolean {
                let containsValue = false;
                Object.keys(data).forEach((item) => {
                    if (!!data[item]) {
                        if (data[item].toString().trim().toLocaleLowerCase().includes(value)) {
                            containsValue = true;
                        }
                    } else {
                        if (!containsValue) {
                            containsValue = (data[item] === value);
                        }
                    }
                });
                return containsValue;
            };
        }
    }

    applyFilter(data: IFilter) {
        this.selectedValue = data.field;
        this.changeFilterField(data.value);
        this.dataSource.filter = data.field.trim().toLowerCase();
    }

    search() {
        this.mapManagerService.searchMaps()
            .subscribe((maps) => {
                // this.dataSource = new MatTableDataSource(maps || []);
                this.dataSource.data = maps;
                // this.dataSource.sort = this.sort;
                this.changeFilterField(this.selectedValue);
            });
    }

}
