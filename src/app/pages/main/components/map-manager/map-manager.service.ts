import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environment/environment';
import { IMap, IRoleGroup, IMapRolesGroups } from './map-manager.model';

@Injectable({
    providedIn: 'root'
})
export class MapManagerService {

    constructor(
        private http: HttpClient,
    ) {
    }

    createMap(map: IMap) {
        const params = map;
        return this.http.post(environment.apiUrl + 'wg-maps', params);
    }

    getMap(idMap: number) {
        return this.http.get(environment.apiUrl + 'wg-maps/' + idMap);
    }

    updateMap(map: IMap) {
        const params = map;
        return this.http.put(environment.apiUrl + 'wg-maps', params);
    }

    deleteMap(idMap: number) {
        return this.http.delete(environment.apiUrl + 'wg-maps/' + idMap);
    }

    searchMaps(): Observable<IMap[]> {
        return this.http.get<IMap[]>(environment.apiUrl + 'wg-maps');
    }

    getRoles(): Observable<IRoleGroup[]> {
        return this.http.get<IRoleGroup[]>(environment.apiUrl + 'wg-roles');
    }

    getGroups(): Observable<IRoleGroup[]> {
        return this.http.get<IRoleGroup[]>(environment.apiUrl + 'wg-groups');
    }

    /* #region TARGET PUBLISH */

    getRolesByMap(idMap): Observable<IMapRolesGroups[]> {
        // http://localhost:4200/api/wg-maps/1002/groups
        return this.http.get<IMapRolesGroups[]>(`${environment.apiUrl}wg-maps/${idMap}/roles`);
    }

    getGroupsByMap(idMap): Observable<IMapRolesGroups[]> {
        return this.http.get<IMapRolesGroups[]>(`${environment.apiUrl}wg-maps/${idMap}/groups`);
    }

    addRole(idMap: number, role: number): Observable<IMapRolesGroups> {
        return this.http.post<IMapRolesGroups>(`${environment.apiUrl}wg-map-roles`, {
            wgMapId: idMap,
            typeAccess: 0,
            wgRoleId: role
        });
    }

    addGroup(idMap: number, group: number): Observable<IMapRolesGroups> {
        return this.http.post<IMapRolesGroups>(`${environment.apiUrl}wg-map-groups`, {
            wgMapId: idMap,
            typeAccess: 0,
            wgGroupId: group
        });
    }

    deleteRole(idMapGroup): Observable<boolean> {
        return this.http.delete<boolean>(`${environment.apiUrl}wg-map-roles/${idMapGroup}`, {});
    }

    deleteGroup(idMapGroup: number) {
        return this.http.delete<boolean>(`${environment.apiUrl}wg-map-groups/${idMapGroup}`, {});
    }
    /* #endregion */

}
