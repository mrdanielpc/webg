import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Observable } from 'rxjs';
import { ILayerGeoServer, IWorkspace } from '@app/pages/main/main/main.model';
import { MainService } from '@app/pages/main/main/main.service';

@Component({
    selector: 'app-map-publish-layer',
    templateUrl: './map-publish-layer.component.html',
    styleUrls: ['./map-publish-layer.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapPublishLayerComponent implements OnInit {

    mapForm: FormGroup;
    model: ILayerGeoServer;
    workspaces$: Observable<IWorkspace[]>;

    constructor(
        private formBuilder: FormBuilder,
        public dialogRef: MatDialogRef<MapPublishLayerComponent>,
        @Inject(MAT_DIALOG_DATA) public data: ILayerGeoServer,
        private mainService: MainService,
    ) {
        this.model = data;
        this.mapForm = this.formBuilder.group({
            title: [this.model.title, Validators.required],
            observations: [this.model.observations, Validators.required],
            workspace: [this.model.workspace, Validators.required]
        });
    }

    ngOnInit() {
        this.workspaces$ = this.mainService.getWorkspaces();
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}
