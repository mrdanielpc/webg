import { EnumProjection, EnumBaseLayer } from '../map/map.model';
import { IMediaList } from '@app/components/media-list/media.model';

export interface IMap {
    observations: string;
    title: string;
    geoJson: string;
    layerJson: string;
    wgFolderId: number;
    center: string;
    zoom: number;
    tilt: number;
    heading: number;
    projection: EnumProjection;
    baseLayer: EnumBaseLayer | string;
    created_at?: any;
    mediaList?: IMediaList[] | string;
    id?: number;
    modified_at?: any;
    userId?: string;
    userLogin?: string;
    wgMapOriginId?: number;
    roles?: IRoleGroup[];
    groups?: IRoleGroup[];
    publishedAsLayer?: boolean;
}

export interface IFolder {
    createdAt: any;
    id: number;
    modifiedAt: any;
    observations: string;
    title: string;
    userId: string;
    userLogin: string;
    wgFolderParentId: number;
    wgFolders: IFolder[];
    wgMaps: IMap[];
}

export interface IRoleGroup {
    id: number;
    title: string;
    name: string;
    objMapRoleGroup?: IMapRolesGroups;
}

export interface IRoleGroupMerged {
    roles: IRoleGroup[];
    groups: IRoleGroup[];
}

export interface IMapRolesGroups {
    created_at: any;
    id: number;
    modified_at: any;
    typeAccess: number;
    wgMapId: number;
    wgRoleId?: number;
    wgGroupId?: number;
}
