import { Component, OnInit, ChangeDetectionStrategy, Input, Output, OnDestroy, EventEmitter, Inject } from '@angular/core';
import { FeatureCollection } from 'geojson';
import { Subject } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
    selector: 'app-featureinfo-dialog',
    templateUrl: './featureinfo-dialog.component.html',
    styleUrls: ['./featureinfo-dialog.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class FeatureinfoDialogComponent implements OnInit, OnDestroy {

    features: FeatureCollection;
    param = { count: null };

    @Input() featureCollection$: Subject<FeatureCollection>;
    @Output() readonly  close = new EventEmitter<FeatureCollection>();

    constructor(
        public dialogRef: MatDialogRef<FeatureinfoDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: FeatureCollection,
    ) {
        // console.log(data);
        this.param.count = data.features.length;
        this.features = data;
    }

    ngOnInit() {
    }

    ngOnDestroy() {

    }

    closeButton() {
        this.dialogRef.close();
    }

}
