import {
    Component,
    OnInit,
    OnDestroy,
    ViewChild,
    AfterViewInit,
    ChangeDetectionStrategy,
    RendererFactory2,
    Input,
    Output,
    EventEmitter,
    ChangeDetectorRef
} from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import {
    MatSnackBar,
    MatSnackBarVerticalPosition,
    MatSnackBarHorizontalPosition
} from '@angular/material/snack-bar';
import {
    takeUntil,
    debounceTime,
    distinctUntilChanged,
    take
} from 'rxjs/operators';
import { FeatureCollection } from 'geojson';
import * as WorldWind from 'src/vendor/worldwind.js';

import { environment } from '@environment/environment';
import { LayerNode, IServer } from '@app/pages/main/components/layers/layers.model';
import { IMediaResource } from '@app/components/media-list/media.model';
import { IApiEnvironment } from '@app/core/api-environment.model';
import { SvsEventManager } from '@app/core/handlers/eventmanager.service';
import { IMarker, IMapMarker, IMarkerDialog } from '../markers/markers.model';
import { ITool, IToolDialog, ToolsType } from '../tools/tools.model';
import { IMapNavigation, IFeatureInfoParams } from '../../main/main.model';
import {
    EnumMapState,
    EnumProjection,
    EnumBaseLayer,
    IMApCreateParams,
    IPointLocation,
    ILocation
} from './map.model';
import { MapService } from './map.service';
import { MainService } from '../../main/main.service';

@Component({
    selector: 'app-map',
    templateUrl: './map.component.html',
    styleUrls: ['./map.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MapComponent implements OnInit, AfterViewInit, OnDestroy {

    @ViewChild(WorldWind.WorldWindow, { static: false }) scene: WorldWind.WorldWindow;

    // layers
    @Input() layersAdded: Subject<LayerNode[]>;
    // Relación servidor-xml de los capabilities
    @Input() serverList: Subject<IServer[]>;

    @Input() markerTypeSelected: Subject<IMarker>;
    @Input() markerEditable: Subject<IMarker>;
    @Input() markers: Subject<IMapMarker[]>;

    // markers
    @Output() readonly setMarkerEditable = new EventEmitter<IMapMarker>();
    @Output() readonly setMarkerAdded = new EventEmitter<IMapMarker>();
    @Output() readonly setMarkerMoved = new EventEmitter<IMapMarker>();
    @Output() readonly showMarkerDialog = new EventEmitter<IMarkerDialog[]>();

    // tools
    @Input() toolTypeSelected: Subject<ITool>;
    @Input() toolsMap: Subject<ITool[]>;
    @Input() toolEditable: Subject<ITool>;
    @Input() city: Subject<ILocation>;

    // Dialogs
    @Input() toolDialog: Subject<IToolDialog[]>;
    @Input() markerDialog: Subject<IMarkerDialog[]>;

    // Media
    @Input() mediaResources: Subject<IMediaResource[]>;
    @Input() mediaResourceSelected$: Subject<IMediaResource>;
    @Output() readonly showMediaResource = new EventEmitter<IMediaResource>();

    // Map
    @Input() moveToLocation: Subject<ILocation>;
    @Input() timeDiff: Subject<number>;
    @Output() readonly showFeatureInfo = new EventEmitter<FeatureCollection>();
    @Output() readonly setNavigation = new EventEmitter<IMapNavigation>();
    // @Output() readonly  showFeatureInfo = new EventEmitter<FeatureCollection>();

    @Output() readonly showToolDialog = new EventEmitter<IToolDialog[]>();
    @Output() readonly toolMapAdded = new EventEmitter<ITool>();
    @Output() readonly setToolEditable = new EventEmitter<ITool>();
    @Output() readonly setToolEdited = new EventEmitter<ITool>();

    @Input() config: Subject<IApiEnvironment>;

    servers: IServer[] = [];
    layersListAdded: LayerNode[] = [];

    toolType: ITool;
    markerType: IMarker;
    markerMapEditable: IMapMarker;
    markerDialogs: IMarkerDialog[] = [];
    markersAdded: IMapMarker[] = [];

    toolMapEditable: ITool;
    toolDialogs: IToolDialog[] = [];

    isLoadedMap: boolean;
    map: WorldWind;
    mapEditableMode: EnumMapState;

    mediaResourcesAdded: IMediaResource[] = [];

    // Flag que se usa para saber si estamos moviendo y/o creando algo (regular tool)
    private isDragging = false;
    // flag para controlar eventos drag cuando tenemos un marker seleccionado, que no se genere un nuevo marcador.
    private isDraggingMap = false;
    // flag que se utiliza para saber si tenemos que ejecutar el "click" desde el mousedown y no desde el handleClick
    private isClicked = false;

    // Variable donde se guarda el resultado del timeout que se usa para saber si es click o dobleclick
    private tapped: any = -1;
    // Es el elemento seleccionado (tool)
    private pickedItem = null;
    private pickedLayerRenderableItem = null;
    private pickedLayerPoints = null;
    private pickedLayerMiddlePoints = null;
    private pickedLayerMarkerSelected = null;
    private pickedLayerMarker = null;

    private fakeLine = null;
    private initialPoint = null;
    private fakeLineLayer: any[] = [];

    private regularPointSource: ILocation;
    private layerFakeRectangle = null;
    private layerSquare = null;
    private layerCircle = null;
    private layerEllipse = null;

    private layerToolEditableSelected = null;

    startX: number;
    startY: number;

    // Esto se hace para almacenar la x y la y mientras se mueve un surface ( polígono o línea)
    surfaceX: number;
    surfaceY: number;

    DOUBLE_TAP_INTERVAL = 200;

    attrWheelObserver: any;

    private unsubscribe$ = new Subject<void>();

    anyTimeLayer = false;
    terrainTime = new BehaviorSubject<string>(null);

    getMap() {
        return this.map;
    }

    checkReady(): boolean {
        return !!document.getElementById('scene');
    }

    configureProjection(projection: EnumProjection) {
        if (!!this.map) {
            this.mapService.configureProjection(this.map, projection);
        }
    }

    configureBaseLayer(baseLayer: EnumBaseLayer) {
        if (!!this.map) {
            this.mapService.configureBaseLayer(this.map, baseLayer);
        }
    }

    resetMap() {
        this.mapService.initializeMapNavigator(this.map, {});
    }

    createMap(configMap: IMApCreateParams): Promise<any> {
        this.resetListeners();
        return new Promise<any>((resolve) => {
            this.rootRenderer.whenRenderingDone().then(() => {
                if (!this.isLoadedMap) {
                    this.isLoadedMap = true;
                    this.config
                        .pipe(take(1))
                        .subscribe((config: IApiEnvironment) => {
                            configMap.elevationModel = config.elevationModel;
                            this.map = this.mapService.loadMap(configMap);

                            this.observeLoadLayers();

                            this.observeMarkerTypeSelected();
                            this.observeMakers();
                            this.observeMarkerEditable();

                            this.observeToolSelected();
                            this.observeToolAdded();
                            this.observeToolMapEditable();
                            this.observeCity();

                            // this.observeMediaList();

                            this.observeToolDialog();
                            this.observeMarkerDialog();

                            this.observeMediaResources();
                            this.observeMoveToLocation();
                            this.observeTimeDiff();
                            resolve();
                        });
                }
            });
        });
    }

    removeMap() {
        this.isLoadedMap = false;
        this.map = null;
        this.resetListeners();
    }

    ngAfterViewInit() {
    }

    constructor(
        private mapService: MapService,
        private rootRenderer: RendererFactory2,
        private snackBar: MatSnackBar,
        private mainService: MainService,
        private eventManager: SvsEventManager,
        private cd: ChangeDetectorRef,
    ) {
    }

    ngOnInit() {

    }

    ngOnDestroy() {
        this.resetListeners();
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        if (!!this.attrWheelObserver) {
            this.attrWheelObserver.unsubscribe();
        }
    }

    private manageTime = (event) => {
        const time = this.mapService.processTimeLayer(this.map, event);
        if (!!time) {
            this.terrainTime.next(time);
        }
    }

    private mainListeners() {
        this.map.addEventListener('wheel', this.onWheelEvent, false);
        this.map.addEventListener('pointerup', this.onRightButtonUp, false);
    }

    // Eliminamos todos los listeners, pero mantenemos el de la rueda del ratón (wheel)
    private resetListeners() {
        if (!!this.map) {

            // MAIN LISTENERS
            this.map.removeEventListener('wheel', this.onWheelEvent);
            this.map.removeEventListener('pointerup', this.onRightButtonUp);

            this.mainListeners();

            // como no sabemos el estado anterior, eliminamos todos los listeners
            // y worldwind se encarga de gestionarlos correctamente si no existiera
            this.map.removeEventListener('click', this.handleClickSelectToolMarker);
            this.map.removeEventListener('click', this.handleClickAddToolMarker);

            this.map.removeEventListener('pointerdown', this.handleClickAddRegularTool);
            this.map.removeEventListener('pointermove', this.handleClickAddRegularTool);
            this.map.removeEventListener('pointerup', this.handleClickAddRegularTool);
            this.map.removeEventListener('pointerout', this.handleClickAddRegularTool);
            // Listen for mouse
            this.map.removeEventListener('mousedown', this.handleClickAddRegularTool);
            this.map.removeEventListener('mousemove', this.handleClickAddRegularTool);
            this.map.removeEventListener('mouseup', this.handleClickAddRegularTool);
            this.map.removeEventListener('mouseout', this.handleClickAddRegularTool);
            // Listen for touch
            this.map.removeEventListener('touchstart', this.handleClickAddRegularTool);
            this.map.removeEventListener('touchmove', this.handleClickAddRegularTool);
            this.map.removeEventListener('touchend', this.handleClickAddRegularTool);

            this.map.removeEventListener('pointerdown', this.handlePick);
            this.map.removeEventListener('pointermove', this.handlePick);
            this.map.removeEventListener('pointerup', this.handlePick);
            this.map.removeEventListener('pointerout', this.handlePick);
            // Listen for mouse
            this.map.removeEventListener('mousedown', this.handlePick);
            this.map.removeEventListener('mousemove', this.handlePick);
            this.map.removeEventListener('mouseup', this.handlePick);
            this.map.removeEventListener('mouseout', this.handlePick);
            // Listen for touch
            this.map.removeEventListener('touchstart', this.handlePick);
            this.map.removeEventListener('touchmove', this.handlePick);
            this.map.removeEventListener('touchend', this.handlePick);

            // Listeners cuando se generan polígonos irregulares:
            this.map.removeEventListener('pointerdown', this.handleClickAddIRegularToolMarker);
            this.map.removeEventListener('pointermove', this.handleClickAddIRegularToolMarker);
            this.map.removeEventListener('pointerup', this.handleClickAddIRegularToolMarker);
            this.map.removeEventListener('pointerout', this.handleClickAddIRegularToolMarker);
            this.map.removeEventListener('mousedown', this.handleClickAddIRegularToolMarker);
            this.map.removeEventListener('mousemove', this.handleClickAddIRegularToolMarker);
            this.map.removeEventListener('mouseup', this.handleClickAddIRegularToolMarker);
            this.map.removeEventListener('mouseout', this.handleClickAddIRegularToolMarker);
            this.map.removeEventListener('touchstart', this.handleClickAddIRegularToolMarker);
            this.map.removeEventListener('touchmove', this.handleClickAddIRegularToolMarker);
            this.map.removeEventListener('touchend', this.handleClickAddIRegularToolMarker);

        }
    }

    toggleMapEditableMode(mode: EnumMapState) {
        this.mapEditableMode = mode;
        if (!!this.map) {
            switch (mode) {
                case EnumMapState.NONE:
                    // this.removeAllListeners();
                    break;
                case EnumMapState.SELECT_TOOL_MARKER:
                    this.resetListeners();
                    this.map.addEventListener('click', this.handleClickSelectToolMarker, false);
                    break;
                case EnumMapState.MOVE_TOOL_MARKER:
                    this.resetListeners();

                    this.map.addEventListener('click', this.handleClickSelectToolMarker, false);
                    this.map.addEventListener('pointerdown', this.handlePick, false);
                    this.map.addEventListener('pointermove', this.handlePick, false);
                    this.map.addEventListener('pointerup', this.handlePick, false);
                    this.map.addEventListener('pointerout', this.handlePick, false);
                    // Listen for mouse
                    this.map.addEventListener('mousedown', this.handlePick, false);
                    this.map.addEventListener('mousemove', this.handlePick, false);
                    this.map.addEventListener('mouseup', this.handlePick, false);
                    this.map.addEventListener('mouseout', this.handlePick, false);
                    // Listen for touch
                    this.map.addEventListener('touchstart', this.handlePick, false);
                    this.map.addEventListener('touchmove', this.handlePick, false);
                    this.map.addEventListener('touchend', this.handlePick, false);
                    break;
                case EnumMapState.ADD_TOOL_IRREGULAR_MARKER:
                    this.resetListeners();
                    // this.map.addEventListener('click', this.handleClickAddToolMarker, false);

                    // this.map.addEventListener('click', this.handleClickSelectToolMarker, false);

                    this.map.addEventListener('pointerdown', this.handleClickAddIRegularToolMarker, false);
                    this.map.addEventListener('pointermove', this.handleClickAddIRegularToolMarker, false);
                    this.map.addEventListener('pointerup', this.handleClickAddIRegularToolMarker, false);
                    this.map.addEventListener('pointerout', this.handleClickAddIRegularToolMarker, false);
                    // Listen for mouse
                    // Listen for mouse down to select an item
                    this.map.addEventListener('mousedown', this.handleClickAddIRegularToolMarker, false);
                    this.map.addEventListener('mousemove', this.handleClickAddIRegularToolMarker, false);
                    this.map.addEventListener('mouseup', this.handleClickAddIRegularToolMarker, false);
                    this.map.addEventListener('mouseout', this.handleClickAddIRegularToolMarker, false);
                    // Listen for touch
                    this.map.addEventListener('touchstart', this.handleClickAddIRegularToolMarker, false);
                    this.map.addEventListener('touchmove', this.handleClickAddIRegularToolMarker, false);
                    this.map.addEventListener('touchend', this.handleClickAddIRegularToolMarker, false);

                    break;
                case EnumMapState.ADD_TOOL_REGULAR:
                    this.resetListeners();
                    this.map.addEventListener('pointerdown', this.handleClickAddRegularTool, false);
                    this.map.addEventListener('pointermove', this.handleClickAddRegularTool, false);
                    this.map.addEventListener('pointerup', this.handleClickAddRegularTool, false);
                    this.map.addEventListener('pointerout', this.handleClickAddRegularTool, false);
                    // Listen for mouse
                    // Listen for mouse down to select an item
                    this.map.addEventListener('mousedown', this.handleClickAddRegularTool, false);
                    this.map.addEventListener('mousemove', this.handleClickAddRegularTool, false);
                    this.map.addEventListener('mouseup', this.handleClickAddRegularTool, false);
                    this.map.addEventListener('mouseout', this.handleClickAddRegularTool, false);
                    // Listen for touch
                    this.map.addEventListener('touchstart', this.handleClickAddRegularTool, false);
                    this.map.addEventListener('touchmove', this.handleClickAddRegularTool, false);
                    this.map.addEventListener('touchend', this.handleClickAddRegularTool, false);
                    break;
                default:
                    console.error('map.component.ts - toggleMapEditableMode - mode not exists');
                    break;
            }
        }
    }

    /* #region helper functions for mouse events methods */
    private moveMarker(terrainObject, layer?: any) {

        // this.pickedItem.userObject.position =
        //     new WorldWind.Position(
        //         terrainObject.position.latitude,
        //         terrainObject.position.longitude,
        //         terrainObject.position.altitude);
        this.pickedItem.userObject.position =
            new WorldWind.Position(
                terrainObject.position.latitude,
                terrainObject.position.longitude,
                +this.pickedItem.userObject.myMarker.location.altitude
            );
        this.mapService.movePlacemark(this.map, this.pickedItem.userObject.position, layer);
    }

    private moveSurface(layerRenderable, actualX, actualY, layerPoints, layerMiddlePoints) {

        if (!this.surfaceX || !this.surfaceY) {
            this.surfaceX = this.startX;
            this.surfaceY = this.startY;
        }
        // Si es el primer movimiento, hay que coger el punto inicial
        const x = this.surfaceX || this.startX;
        const y = this.surfaceY || this.startY;
        this.mapService.moveSurface(
            this.map,
            layerRenderable,
            layerRenderable.myTool.attributes.idMapTool,
            x,
            y,
            actualX,
            actualY,
            layerPoints,
            layerMiddlePoints
        );

        this.surfaceX = actualX;
        this.surfaceY = actualY;
    }

    private createIrregularTool(pickList) {
        const newMapTool = this.mapService.processTool(this.map, pickList, this.toolType);
        if (!!newMapTool) {
            this.toolMapAdded.next(newMapTool);
            switch (this.toolType.type) {
                case ToolsType.MultiLineString:
                case ToolsType.LineString:
                case ToolsType.Polygon:
                case ToolsType.MultiPolygon:
                    this.initialPoint = null;
                    this.fakeLineLayer = [];
                    break;
            }
        } else {
            switch (this.toolType.type) {
                case ToolsType.MultiLineString:
                case ToolsType.LineString:
                case ToolsType.Polygon:
                case ToolsType.MultiPolygon:
                    this.initialPoint = pickList.terrainObject().position;
                    this.fakeLine = this.mapService.createFakeLine(this.map, this.initialPoint);
                    break;
            }
        }
    }

    private createRegularTool(pickList) {
        const position = pickList.terrainObject().position;
        switch (true) {
            case (this.toolType.type === ToolsType.Square):
            case (this.toolType.type === ToolsType.Rectangle):
            case (this.toolType.type === ToolsType.Ellipse):
                this.regularPointSource = position;
                this.mapService.addSquarePoint(this.map, position);
                const fullBound = this.mapService.getRectangularBoundsFromPoints(this.regularPointSource, this.regularPointSource);
                this.layerFakeRectangle = this.mapService.createLayerRectangle(
                    this.map,
                    fullBound.bounds,
                    (this.toolType.type === ToolsType.Rectangle)
                );
                // Si es un rectángulo, el propio fake se convertirá en uno real al finalizar
                if (this.toolType.type === ToolsType.Square) {
                    // this.layerSquare = this.mapService.createLayerSquare(this.map, this.regularPointSource);
                    // Al ser un polígono, tengo que enviar al menos 3 puntos
                    this.layerSquare = this.mapService.createLayerSquare(
                        this.map,
                        [
                            this.regularPointSource,
                            this.regularPointSource,
                            this.regularPointSource,
                            this.regularPointSource
                        ]
                    );
                } else if (this.toolType.type === ToolsType.Ellipse) {
                    this.layerEllipse = this.mapService.createLayerEllipse(this.map, this.regularPointSource);
                }
                break;
            case (this.toolType.type === ToolsType.Circle):
                this.regularPointSource = position;
                this.mapService.addSquarePoint(this.map, position);
                this.layerCircle = this.mapService.createLayerCircle(this.map, this.regularPointSource, 1);
                break;
            default:
                break;
        }
    }

    private selectLayerForDrag() {
        this.map.layers.forEach((layer) => {
            if (layer.displayName === environment.constants.map.TOOL_LAYER) {
                layer.renderables.forEach((renderable) => {
                    if (
                        !!renderable.myTool &&
                        !!this.pickedItem.userObject.myTool &&
                        renderable.myTool.attributes.idMapTool === this.pickedItem.userObject.myTool.attributes.idMapTool
                    ) {
                        this.pickedLayerRenderableItem = renderable;
                    }
                });
            } else if (layer.displayName === environment.constants.map.TOOL_POINT) {
                this.pickedLayerPoints = layer;
            } else if (layer.displayName === environment.constants.map.TOOL_MIDDLE_POINT) {
                this.pickedLayerMiddlePoints = layer;
            } else if (layer.displayName === environment.constants.map.PLACEMARKSELECTED) {
                this.pickedLayerMarkerSelected = layer;
            } else if (layer.displayName === environment.constants.map.PLACEMARK) {
                this.pickedLayerMarker = layer;
            }
        });
    }

    private isDraggingAnyObject(): boolean {
        // Objetos que se pueden mover:
        // a) Marcadores
        // b) Tools (Polígonos / líneas )
        // c) Puntos de las Tools (Puntos reales, o puntos intermedios)
        return (
            !!this.pickedItem.userObject.myMarker ||
            !!this.pickedItem.userObject.myTool ||
            !!this.pickedItem.userObject.myPoint
        );
    }

    private updateRectangle(layer, zeroPoint, secondPoint) {
        const fullBoundRectangle = this.mapService.getRectangularBoundsFromPoints(
            zeroPoint,
            secondPoint
        );
        const squareWidth = this.mapService.getDistanceBetweenPointsInMeters(
            this.map,
            fullBoundRectangle.bounds[0],
            fullBoundRectangle.bounds[1]
        );
        const squareHeight = this.mapService.getDistanceBetweenPointsInMeters(
            this.map,
            fullBoundRectangle.bounds[0],
            fullBoundRectangle.bounds[3]
        );

        const rectangleCenter = new WorldWind.Location(0, 0, 0);
        WorldWind.Location.interpolateAlongPath(
            WorldWind.GREAT_CIRCLE,
            .5,
            fullBoundRectangle.bounds[0],
            fullBoundRectangle.bounds[2],
            rectangleCenter
        );
        rectangleCenter.altitude = rectangleCenter.altitude || 0;
        const rotateAngleRectangle = 0;
        this.mapService.updateRectangle(
            layer,
            rectangleCenter,
            squareWidth,
            squareHeight,
            rotateAngleRectangle
        );
    }

    private getOppositePoint() {
        const pointPickedIdx = this.pickedItem.userObject.myVerticeIdx;
        let verticeIdx = 0;
        switch (pointPickedIdx) {
            case 0:
                verticeIdx = 2;
                break;
            case 1:
                verticeIdx = 3;
                break;
            case 2:
                verticeIdx = 0;
                break;
            case 3:
                verticeIdx = 1;
                break;
        }
        const oppositePoint = this.pickedItem.parentLayer.renderables.find((position) => {
            return (position.myVerticeIdx === verticeIdx);
        });
        return oppositePoint;
    }

    private dragObject(pickList, actualX, actualY) {
        switch (true) {
            case (!!this.pickedItem.userObject.myMarker):
                const terrainObject = pickList.terrainObject();
                if (terrainObject) {
                    this.moveMarker(terrainObject);
                }
                break;
            case (!!this.pickedItem.userObject.myTool):
                this.moveSurface(
                    this.pickedLayerRenderableItem,
                    actualX,
                    actualY,
                    this.pickedLayerPoints,
                    this.pickedLayerMiddlePoints
                );
                break;
            case (!!this.pickedItem.userObject.myPoint):
                // Si estamos haciendo dragging desde un punto,
                // puede que sea por estar dibujando un polígono regular
                if (!!this.toolType && !!this.regularPointSource) {
                    const actualPoint = this.mapService.getSurfacePolygonPosition(this.map, actualX, actualY);
                    switch (true) {
                        case (this.toolType.type === ToolsType.Circle):
                            const radius = this.mapService.getDistanceBetweenPointsInMeters(
                                this.map,
                                this.regularPointSource,
                                actualPoint
                            );
                            this.layerCircle.renderables[0].myCircle.attributes.radius = radius;
                            this.mapService.updateCircle(
                                this.map,
                                this.layerCircle,
                                this.layerCircle.renderables[0].myCircle.attributes
                            );
                            break;
                        case (this.toolType.type === ToolsType.Square):
                            this.updateRectangle(this.layerFakeRectangle, this.regularPointSource, actualPoint);

                            // Dibujamos el cuadrado temporal
                            const squareBounds = this.mapService.getBoundsRealSquare(
                                this.map,
                                this.regularPointSource,
                                actualPoint
                            );
                            this.layerSquare.renderables[0].mySquare.attributes.points = squareBounds;
                            this.mapService.updateSquare(
                                this.map,
                                this.layerSquare,
                                this.layerSquare.renderables[0].mySquare.attributes
                            );
                            break;
                        case (this.toolType.type === ToolsType.Rectangle):
                            this.updateRectangle(this.layerFakeRectangle, this.regularPointSource, actualPoint);
                            break;
                        case (this.toolType.type === ToolsType.Ellipse):
                            this.updateRectangle(this.layerFakeRectangle, this.regularPointSource, actualPoint);

                            const fullBoundEllipse = this.mapService.getRectangularBoundsFromPoints(
                                this.regularPointSource,
                                actualPoint
                            );
                            const width = this.mapService.getDistanceBetweenPointsInMeters(
                                this.map,
                                fullBoundEllipse.bounds[0],
                                fullBoundEllipse.bounds[1]
                            ) / 2;
                            const height = this.mapService.getDistanceBetweenPointsInMeters(
                                this.map,
                                fullBoundEllipse.bounds[0],
                                fullBoundEllipse.bounds[3]
                            ) / 2;
                            const center = new WorldWind.Location(0, 0, 0);
                            WorldWind.Location.interpolateAlongPath(
                                WorldWind.GREAT_CIRCLE,
                                .5,
                                fullBoundEllipse.bounds[0],
                                fullBoundEllipse.bounds[2],
                                center
                            );
                            const rotateAngle = 0;
                            this.mapService.updateEllipse(
                                this.map,
                                this.layerEllipse,
                                center,
                                width,
                                height,
                                rotateAngle
                            );
                            break;
                        default:
                            break;
                    }
                } else {
                    // Estamos moviendo un punto de un polígono...
                    const newPosition = this.mapService.getSurfacePolygonPosition(this.map, actualX, actualY);
                    // Si estamos moviendo un punto medio, es que vamos a añadir otro punto más
                    // ( p.ej: en un polígono de 4 puntos pasar a tener 5 )
                    if (this.pickedItem.parentLayer.displayName === environment.constants.map.TOOL_MIDDLE_POINT) {
                        let idxToAddPoint = this.pickedItem.userObject.layer.renderables.findIndex((render) => {
                            return (render.myPoint.idPoint === this.pickedItem.userObject.myPoint.idPoint);
                        });
                        idxToAddPoint = (idxToAddPoint !== -1 && !!idxToAddPoint) ? idxToAddPoint + 1 : 1;
                        const firstPart = this.toolMapEditable.attributes.points.slice(0, idxToAddPoint);
                        const element = new WorldWind.Location(
                            newPosition.latitude,
                            newPosition.longitude,
                            newPosition.altitude
                        );
                        element.idPoint = this.pickedItem.userObject.myPoint.idPoint;
                        const finalPart = this.toolMapEditable.attributes.points.slice(
                            idxToAddPoint,
                            this.toolMapEditable.attributes.points.length);
                        const arrPoints = [...firstPart, element, ...finalPart];
                        this.toolMapEditable.attributes.points = arrPoints;
                        this.pickedItem.parentLayer.displayName = environment.constants.map.TOOL_POINT;
                    } else {
                        switch (true) {
                            case (this.toolMapEditable.type === ToolsType.Square):
                                const oppositeSquarePoint = this.getOppositePoint();
                                const squareBounds = this.mapService.getBoundsRealSquare(
                                    this.map,
                                    // this.toolMapEditable.attributes.points[0],
                                    oppositeSquarePoint.position,
                                    newPosition
                                );
                                this.toolMapEditable.attributes.points.map((point, idx) => {
                                    point.latitude = squareBounds[idx].latitude;
                                    point.longitude = squareBounds[idx].longitude;
                                    point.altitude = squareBounds[idx].altitude;
                                });
                                const _attr =
                                    this.layerToolEditableSelected.renderables[0].mySquare ||
                                    this.layerToolEditableSelected.renderables[0].myTool;
                                this.mapService.updateSquare(
                                    this.map,
                                    this.layerToolEditableSelected,
                                    _attr.attributes
                                );
                                break;
                            case (this.toolMapEditable.type === ToolsType.Rectangle):
                                const oppositePoint = this.getOppositePoint();
                                this.updateRectangle(this.layerToolEditableSelected, oppositePoint.position, newPosition);
                                // Borramos los vértices
                                this.pickedItem.parentLayer.renderables.forEach((renderable) => {
                                    renderable.enabled = false;
                                });
                                break;
                            case (this.toolMapEditable.type === ToolsType.Polygon):
                            case (this.toolMapEditable.type === ToolsType.LineString):
                                this.toolMapEditable.attributes.points.map((pointItem: IPointLocation) => {
                                    if (pointItem.idPoint === this.pickedItem.userObject.myPoint.idPoint) {
                                        pointItem.latitude = newPosition.latitude;
                                        pointItem.longitude = newPosition.longitude;
                                        pointItem.altitude = newPosition.altitude;
                                    }
                                });
                                this.mapService.movePoint(
                                    this.map,
                                    this.toolMapEditable
                                );
                                break;
                            default:
                                console.error('map.component.dragObject() - movingpoint - ' + this.toolMapEditable.type);
                                break;
                        }
                    }
                }

                break;
            default:
                console.error('map.component.dragObject() - mainSwitch - ' + this.pickedItem.userObject);
                break;
        }
    }

    private doHover(pickList) {
        if (!!pickList.objects) {
            if (pickList.objects.length > 1) {
                pickList.objects.forEach((object) => {
                    if (object.userObject instanceof WorldWind.Placemark) {
                        this.mapService.highlightMarker(this.map, object.userObject);
                    }
                });
            }
        }
    }
    /* #endregion helper functions for mouse events methods*/

    /* #region Mouse Events */

    // private handleMouseDownCreateRegularTool(x, y, eventType, button) {

    //     if (button === 0 || eventType === 'touchmove') {
    //         this.isClicked = true;

    //         if (!!this.toolType) {
    //             const pickList = this.map.pick(this.map.canvasCoordinates(x, y));
    //             if (
    //                 pickList.hasNonTerrainObjects()
    //             ) {
    //                 this.pickedItem = pickList.topPickedObject();

    //                 this.startX = x;
    //                 this.startY = y;
    //             } else {
    //                 this.createRegularTool(pickList);
    //             }

    //         }
    //     }
    // }

    // private handleMouseDown(x, y, eventType, button) {

    //     if (button === 0 || eventType === 'touchmove') {
    //         this.isClicked = true;
    //         const pickList = this.map.pick(this.map.canvasCoordinates(x, y));
    //         if (
    //             pickList.hasNonTerrainObjects()
    //         ) {
    //             this.pickedItem = pickList.topPickedObject();
    //             if (!!this.pickedItem.userObject.myTool || !!this.pickedItem.userObject.myMarker) {
    //                 this.selectLayerForDrag();
    //             }
    //             // this.startX = x;
    //             // this.startY = y;
    //         }

    //     }
    // }

    private prepareLayers() {
        if (!!this.pickedItem && (!!this.pickedItem.userObject.myTool || !!this.pickedItem.userObject.myMarker)) {
            this.selectLayerForDrag();
        }
    }

    private handleMouseDown(x, y, eventType, button) {

        if (button === 0 || eventType === 'touchmove') {

            if (!this.isClicked) {
                const pickList = this.map.pick(this.map.canvasCoordinates(x, y));

                if (!!this.toolType) {
                    this.createRegularTool(pickList);
                    const newPickList = this.map.pick(this.map.canvasCoordinates(x, y));
                    this.pickedItem = newPickList.topPickedObject();
                    this.prepareLayers();
                } else {
                    this.pickedItem = pickList.topPickedObject();
                    this.prepareLayers();
                }

            }
            if (this.pickedItem) {
                // if (this.isDragging) {
                if (!!this.pickedItem.userObject.myMarker) {
                    // Sólo emitimos el evento si el marcador ha cambiado.
                    if (
                        (!this.markerMapEditable) ||
                        (this.pickedItem.userObject.myMarker.idMapMarker !== this.markerMapEditable.idMapMarker)
                    ) {
                        this.pickedItem.userObject.myMarker.location = this.pickedItem.userObject.position;
                        this.setMarkerEditable.emit(this.pickedItem.userObject.myMarker);
                    }
                }
                if (!!this.pickedItem.userObject.myTool) {
                    // Sólo emitimos el evento si el marcador ha cambiado.
                    if (
                        (!this.toolMapEditable) ||
                        (this.pickedItem.userObject.myTool.attributes.idMapTool !== this.toolMapEditable.attributes.idMapTool)
                    ) {
                        this.setToolEditable.emit(this.pickedItem.userObject.myTool);
                    }
                }
            }

        }
        this.isClicked = true;
        this.startX = x;
        this.startY = y;
    }

    private handleMouseMoveIrregularTool(actualX, actualY) {
        if (!!this.initialPoint) {
            const pickList = this.map.pick(this.map.canvasCoordinates(actualX, actualY));
            const actualPoint = pickList.terrainObject().position;
            this.fakeLineLayer[0].boundaries = [...this.initialPoint, ...actualPoint];

            if ((this.toolType.type === ToolsType.Polygon) || (this.toolType.type === ToolsType.MultiPolygon)) {
                if (this.mapService.getPolygonPoints().length > 2) {
                    this.fakeLineLayer[1].boundaries = [...actualPoint, this.mapService.getPolygonPoints()[0]];
                } else {
                    // Como tenemos dos líneas, hasta que tengamos más de 2 puntos
                    // tenemos que "engañar" y que parezca sólo una hasta que se "desdoble"
                    this.fakeLineLayer[1].boundaries = [...this.initialPoint, ...actualPoint];
                }
            }

            this.mapService.forceRedraw(this.map);
        }

        if (
            this.isClicked
            &&
            (!!this.startX && !!this.startY)
            &&
            (
                Math.abs(this.startX - actualX) > environment.map.pixelsToConsiderIsDragging
                || Math.abs(this.startY - actualY) > environment.map.pixelsToConsiderIsDragging
            )
        ) {
            this.isDraggingMap = true;
        }
    }

    private handleMouseMove(actualX, actualY, eventType, button) {

        if (button === 0 || eventType === 'touchmove') {
            if (this.pickedItem) {
                if (this.isDraggingAnyObject()) {
                    if (!this.isDragging &&
                        (
                            Math.abs(this.startX - actualX) > environment.map.pixelsToConsiderIsDragging
                            ||
                            Math.abs(this.startY - actualY) > environment.map.pixelsToConsiderIsDragging
                        )) {

                        // Aquí se "limpia" el dibujo seleccionado, para que en el momento
                        // en el que empezamos a mover algo, no queden los cuadrados blancos
                        // de los vértices
                        this.mapService.cancelSelectedTool(this.map, this.toolMapEditable);

                        this.isDragging = true;
                    }
                    if (this.isDragging) {
                        const pickList = this.map.pick(this.map.canvasCoordinates(actualX, actualY));
                        this.dragObject(pickList, actualX, actualY);
                    }

                }
            } else {
                if (
                    this.isClicked
                    &&
                    (
                        Math.abs(this.startX - actualX) > environment.map.pixelsToConsiderIsDragging
                        || Math.abs(this.startY - actualY) > environment.map.pixelsToConsiderIsDragging
                    )
                ) {
                    this.isDraggingMap = true;
                } {
                    // debugger;
                    const pickList = this.map.pick(this.map.canvasCoordinates(actualX, actualY));
                    if (pickList.hasNonTerrainObjects()) {
                        this.doHover(pickList);
                    }
                }
            }
        }

    }

    private resetMouseUpData() {
        this.mapService.removeSquarePoint(this.map);
        this.mapService.removeSquarePointCenter(this.map);
        this.map.removeLayer(this.layerFakeRectangle);
        this.regularPointSource = null;
        this.layerFakeRectangle = null;
        this.layerCircle = null;
        this.layerEllipse = null;
        this.layerSquare = null;

        this.isClicked = false;
        this.isDragging = false;
        this.isDraggingMap = false;

        this.pickedLayerRenderableItem = null;
        this.pickedLayerPoints = null;
        this.pickedLayerMiddlePoints = null;
        this.pickedLayerMarkerSelected = null;
        this.pickedLayerMarker = null;

        this.fakeLine = null;
        this.initialPoint = null;
        this.fakeLineLayer = [];

        this.pickedItem = null;
        this.surfaceX = null;
        this.surfaceY = null;
    }

    private handleMouseUpIrregularToolMarker(x, y) {
        if (!this.isDraggingMap && this.isClicked) {
            const pickList = this.map.pick(this.map.canvasCoordinates(x, y));
            if (!!this.toolType && !!this.toolType.type) {
                this.createIrregularTool(pickList);
                switch (this.toolType.type) {
                    case ToolsType.MultiLineString:
                    case ToolsType.LineString:
                    case ToolsType.Polygon:
                    case ToolsType.MultiPolygon:
                        if (!!this.initialPoint) {
                            const actualPoint = pickList.terrainObject().position;
                            this.fakeLineLayer.push(this.mapService.drawSurfacePolyline(
                                this.map,
                                [...this.initialPoint, ...actualPoint],
                                this.toolType,
                                false,
                                true)
                            );
                            // Si es un polígono tenemos que generar 2 líneas fake
                            if ((this.toolType.type === ToolsType.Polygon) || (this.toolType.type === ToolsType.MultiPolygon)) {
                                // const actualPoint = pickList.terrainObject().position;
                                this.fakeLineLayer.push(this.mapService.drawSurfacePolyline(
                                    this.map,
                                    [...this.initialPoint, ...actualPoint],
                                    this.toolType,
                                    false,
                                    true)
                                );
                            }
                        }
                        break;
                    default:
                        break;
                }

            } else {
                if (!!this.markerType && !!this.markerType.id) {
                    const newMapMarker = this.mapService.processMarker(pickList, this.markerType);
                    this.mapService.forceRedraw(this.map);
                    this.setMarkerAdded.emit(newMapMarker);
                }
            }
        }
        this.startX = null;
        this.startY = null;
        this.isClicked = false;
        this.isDraggingMap = false;
    }

    private handleMouseUpCreateRegularTool() {
        // Si estamos dibujando circulo, elipse, cuadrado o rectángulo...
        if (!!this.regularPointSource) {
            switch (true) {
                case (this.toolType.type === ToolsType.Square):
                    const points = this.layerSquare.renderables[0].mySquare.attributes.points;
                    if (
                        !!this.layerSquare.renderables[0].mySquare
                        &&
                        (points[0] !== points[1])
                    ) {
                        const squareTool = this.mapService.transformSquareToTool(this.layerSquare);
                        this.toolMapAdded.next(squareTool);
                    }
                    break;
                case (this.toolType.type === ToolsType.Rectangle):
                    if (!!this.layerFakeRectangle.renderables[0].myRectangle) {
                        const newTool = this.mapService.transformRectangleToTool(this.layerFakeRectangle);
                        if (!!newTool) {
                            this.toolMapAdded.next(newTool);
                        }
                    }
                    break;
                case (this.toolType.type === ToolsType.Circle):
                    if (!!this.layerCircle.renderables[0].myCircle) {
                        const circleTool = this.mapService.transformCircleToTool(this.layerCircle);
                        this.toolMapAdded.next(circleTool);
                    }
                    break;
                case (this.toolType.type === ToolsType.Ellipse):
                    if (this.layerEllipse.renderables[0].myEllipse) {
                        const ellipseTool = this.mapService.transformEllipseToTool(this.layerEllipse);
                        this.toolMapAdded.next(ellipseTool);
                    }
                    break;
                default:
                    break;
            }

        }

        this.resetMouseUpData();
    }

    private handleMouseUp() {

        if (this.pickedItem) {
            if (this.isDragging) {
                if (!!this.pickedItem.userObject.myMarker) {
                    this.pickedItem.userObject.myMarker.location = this.pickedItem.userObject.position;
                    this.setMarkerEditable.emit(this.pickedItem.userObject.myMarker);
                    this.setMarkerMoved.emit(this.pickedItem.userObject);
                }
                if (!!this.pickedItem.userObject.myTool) {
                    this.setToolEdited.emit(this.pickedItem.userObject.myTool);
                }
                // Puede que sea un punto,pero que venga de dibujar el rectángulo haciendo dragging
                if (!!this.pickedItem.userObject.myPoint &&
                    this.pickedItem.parentLayer.displayName !== environment.constants.map.ORIGINAL_POLYGON_POINT) {
                    this.setToolEdited.emit(this.toolMapEditable);
                }
            }
        }

        this.resetMouseUpData();

    }

    private getPickListAndCoordinatesMouseEvent(o) {
        let eventType,
            x, y;
        const button = o.button;
        let isTouchDevice = false;

        // Alias PointerEvent event types to mouse and touch event types
        if (o.type === 'pointerdown' && o.pointerType === 'mouse') {
            eventType = 'mousedown';
        } else if (o.type === 'pointermove' && o.pointerType === 'mouse') {
            eventType = 'mousemove';
        } else if (o.type === 'pointerout' && o.pointerType === 'mouse') {
            eventType = 'mouseout';
        } else if (o.type === 'pointerup' && o.pointerType === 'mouse') {
            eventType = 'mouseup';
        } else if (o.type === 'pointerdown' && o.pointerType === 'touch') {
            eventType = 'touchstart';
        } else if (o.type === 'pointermove' && o.pointerType === 'touch') {
            eventType = 'touchmove';
        } else if (o.type === 'pointercancel' && o.pointerType === 'touch') {
            eventType = 'touchcancel';
        } else if (o.type === 'pointerup' && o.pointerType === 'touch') {
            eventType = 'touchend';
        } else {
            eventType = o.type;
        }

        if (eventType.substring(0, 5) === 'touch') {
            isTouchDevice = true;
            if (o.touches && o.touches.length > 0) {
                x = o.touches[0].clientX;
                y = o.touches[0].clientY;
            } else {
                x = o.clientX;
                y = o.clientY;
            }
        } else {
            if (isTouchDevice) {
                return;
            }
            x = o.clientX;
            y = o.clientY;
        }

        // pickList = this.map.pick(this.map.canvasCoordinates(x, y));
        return {
            // pickList: pickList,
            x: x,
            y: y,
            button: button,
            eventType: eventType
        };
    }

    private handleClickAddIRegularToolMarker = (o) => {
        const {
            x,
            y,
            eventType
        } = this.getPickListAndCoordinatesMouseEvent(o);

        switch (eventType) {
            case 'mousedown':
            case 'touchstart':
                // this.handleMouseDownCreateRegularTool(x, y, eventType, button);
                // this.handleMouseDown(x, y, eventType, button);
                this.handleClickAddToolMarker(o);
                break;
            case 'mousemove':
            case 'touchmove':
                this.handleMouseMoveIrregularTool(x, y);
                break;
            case 'mouseup':
            case 'touchend':
                this.handleMouseUpIrregularToolMarker(x, y);
                break;
        }

        // Prevent pan/drag operations on the globe when we're dragging an object.
        if (this.isDragging) {
            o.preventDefault();
            o.stopImmediatePropagation();
        }
    }

    private handleClickAddRegularTool = (o) => {
        const {
            x,
            y,
            button,
            eventType
        } = this.getPickListAndCoordinatesMouseEvent(o);

        switch (eventType) {
            case 'mousedown':
            case 'touchstart':
                // this.handleMouseDownCreateRegularTool(x, y, eventType, button);
                this.handleMouseDown(x, y, eventType, button);
                break;
            case 'mousemove':
            case 'touchmove':
                this.handleMouseMove(x, y, eventType, button);
                break;
            case 'mouseup':
            case 'mouseout':
            case 'touchend':
            case 'touchcancel':
                // this.handleMouseUp();
                this.handleMouseUpCreateRegularTool();
                break;
        }

        // Prevent pan/drag operations on the globe when we're dragging an object.
        if (this.isDragging) {
            o.preventDefault();
            o.stopImmediatePropagation();
        }
    }

    private handlePick = (o) => {

        const {
            x,
            y,
            button,
            eventType
        } = this.getPickListAndCoordinatesMouseEvent(o);

        switch (eventType) {
            case 'mousedown':
            case 'touchstart':
                this.handleMouseDown(x, y, eventType, button);
                break;
            case 'mousemove':
            case 'touchmove':
                this.handleMouseMove(x, y, eventType, button);
                break;
            case 'mouseup':
            case 'mouseout':
            case 'touchend':
            case 'touchcancel':
                this.handleMouseUp();
                break;
        }
        // Prevent pan/drag operations on the globe when we're dragging an object.
        if (this.isDragging) {
            o.preventDefault();
            o.stopImmediatePropagation();
        }
    }

    private anyActionSelected(): boolean {
        let result: boolean;
        result = (!!this.toolType && !!this.toolType.type);
        if (!result) {
            result = (!!this.markerType && !!this.markerType.id);
        }
        return result;
    }

    private handleClickAddToolMarker = (recognizer) => {
        if (this.anyActionSelected()) {
            const x = recognizer.clientX,
                y = recognizer.clientY;
            this.startX = x;
            this.startY = y;
            this.isClicked = true;
        }
    }

    private getFeatureInfo(
        sector: WorldWind.Sector,
        layers: string[],
        queryLayers: string[],
        width: number,
        height: number,
        x: number,
        y: number
    ) {

        const params: IFeatureInfoParams = {
            bBox: sector,
            layers: layers,
            queryLayers: queryLayers,
            width: width,
            height: height,
            x: x,
            y: y
        };
        this.mainService.getFeatureInfo(params)
            .subscribe((response) => {
                this.showFeatureInfo.emit(response);
            }, (err) => {
            });
    }

    // Se usa desde un that de un timeout
    private mainClick(recognizer) {
        const x = recognizer.clientX,
            y = recognizer.clientY;

        const pick = this.map.canvasCoordinates(x, y);
        const pickList = this.map.pick(pick);

        pickList.objects.forEach(element => {
            switch (true) {
                case element.userObject instanceof WorldWind.Placemark:
                    if (!!element.userObject.myMarker) {
                        const markerMap: IMapMarker = {
                            ...element.userObject.myMarker,
                            idMapMarker: element.userObject.myId,
                            mapObservations: element.userObject.myMarker.mapObservations,
                            mapTitle: element.userObject.myMarker.mapTitle
                        };
                        this.setMarkerEditable.emit(markerMap);
                    } else if (!!element.userObject.myMediaResource) {
                        const realPosition = this.mapService.getHorizontalCoord(document.getElementById('scene'), 600);
                        element.userObject.myMediaResource.mapCoordinates = realPosition;
                        this.showMediaResource.emit(element.userObject.myMediaResource);
                    }
                    break;
                // WorldWind.Compass tiene que ir antes de Renderable,
                // ya que el compass es a su vez renderable, pero no los renderables son compas
                case element.userObject instanceof WorldWind.Compass:
                    this.mapService.clearCompass(this.map);
                    const navigation = this.getNavigationAttributes();
                    this.setNavigation.emit(navigation);
                    this.changeLayerTimezone(0);
                    break;
                case element.userObject instanceof WorldWind.Renderable:
                    if (element.userObject.layer.displayName === environment.constants.map.TOOL_LAYER) {
                        const toolSelected: ITool = element.userObject.layer.renderables[0].myTool;
                        // this.toolMapEditable = toolSelected;
                        this.setToolEditable.emit(toolSelected);
                    }
                    break;
                // case element.userObject instanceof WorldWind.Terrain:

                //     break;
                default:

                    break;
            }

        });


    }

    private doubleClick(recognizer) {
        const x = recognizer.clientX,
            y = recognizer.clientY;
        const width = this.map.canvas.width;
        const height = this.map.canvas.height;
        let sector: WorldWind.Sector;
        let positionClick;
        let mustGetFeatureInfo = false;
        let anyLayerSelected = false; // Se usa este flag para que, aún haciendo click
        // en una capa que contenga Terrain (como todas), si contiene tools o markers,
        // no se visualice el FeatureInfo
        const pickList = this.map.pick(this.map.canvasCoordinates(x, y));
        pickList.objects.forEach(element => {
            switch (true) {
                case element.userObject instanceof WorldWind.SurfacePolyline:
                case element.userObject instanceof WorldWind.SurfacePolygon:
                case element.userObject instanceof WorldWind.SurfaceRectangle:
                case element.userObject instanceof WorldWind.SurfaceEllipse:
                case element.userObject instanceof WorldWind.SurfaceCircle:
                    anyLayerSelected = true;
                    const existsToolDialog = this.toolDialogs.find((toolDialog) => {
                        return (toolDialog.tool.attributes.idMapTool === element.userObject.myTool.idMapTool);
                    });
                    if (!existsToolDialog) {
                        const realPosition = this.mapService.getMousePos(document.getElementById('scene'), x, y);
                        const measurement = this.mapService.getMeasurements(this.map, element);
                        if (measurement) {
                            this.toolDialogs.push({
                                tool: <ITool>element.userObject.myTool,
                                mapCoordinates: realPosition,
                                measurement: measurement
                            });
                            this.showToolDialog.emit(this.toolDialogs);
                        }
                    }
                    break;
                case element.userObject instanceof WorldWind.Placemark:
                    anyLayerSelected = true;
                    if (!!element.userObject.myMarker) {
                        const existsMarkerDialog = this.markerDialogs.find((marker) => {
                            return (marker.marker.idMapMarker === element.userObject.myMarker.idMapMarker);
                        });
                        if (!existsMarkerDialog) {
                            const realPosition = this.mapService.getMousePos(document.getElementById('scene'), x, y);
                            this.markerDialogs.push({
                                marker: <IMapMarker>element.userObject.myMarker,
                                mapCoordinates: realPosition
                            });
                            this.showMarkerDialog.emit(this.markerDialogs);
                        }
                    }
                    break;
                case element.userObject instanceof WorldWind.Terrain:
                    mustGetFeatureInfo = true;
                    positionClick = pickList.terrainObject().position;
                    sector = pickList.terrainObject().userObject.sector;
                    break;
                default:
                    break;
            }
        });
        // if (!anyLayerSelected && mustGetFeatureInfo && this.mapEditableMode === EnumMapState.SELECT_TOOL_MARKER) {
        if (mustGetFeatureInfo && this.mapEditableMode === EnumMapState.SELECT_TOOL_MARKER) {
            const layersDenom: string[] = [];
            let bbox;
            this.layersListAdded.forEach((layer) => {
                if (layer.enabled !== false && layer.item.queryable === true) {
                    layersDenom.push(layer.denom);
                }
            });
            if (layersDenom.length > 0 && positionClick) {
                const center = this.mapService.middlePoint(
                    sector.maxLatitude,
                    sector.maxLongitude,
                    sector.minLatitude,
                    sector.minLongitude
                );

                const latDiff = positionClick.latitude - center.latitude;
                const lngDiff = positionClick.longitude - center.longitude;

                const latMinDiff = sector.minLatitude + latDiff;
                const latMaxDiff = sector.maxLatitude + latDiff;
                const lngMinDiff = sector.minLongitude + lngDiff;
                const lngMaxDiff = sector.maxLongitude + lngDiff;

                bbox = [
                    { latitude: (latMinDiff), longitude: (lngMinDiff) },
                    { latitude: (latMaxDiff), longitude: (lngMaxDiff) }
                ];

                this.getFeatureInfo(bbox, layersDenom, layersDenom, width, height, x, y);
            }
        }
    }

    private onWheelEvent = (recognizer) => {
        const navigator = this.map.navigator;
        const zoom = +navigator.range.toFixed(0);
        if (!this.attrWheelObserver) {
            new Observable((observer) => {
                this.attrWheelObserver = observer;
            }).pipe(debounceTime(environment.debounceModel))
                .pipe(distinctUntilChanged())
                .subscribe((newZoom: number) => {
                    const navigation = this.getNavigationAttributes();
                    this.setNavigation.emit({
                        ...navigation,
                        zoom: newZoom
                    });
                    if ((this.markersAdded.length > 0) || (this.mediaResourcesAdded.length > 0)) {
                        this.mapService.updateMarkerSize(this.map, newZoom);
                    }
                });
        }
        this.attrWheelObserver.next(zoom);
    }

    private changeLayerTimezone(tilt) {
        const timeZoneLayer = this.map.layers.find((layer) => {
            return (layer.displayName === environment.constants.map.LAYER_TIME_ZONE);
        });
        if (timeZoneLayer) {
            timeZoneLayer.enabled = (tilt < 50);
            this.mapService.forceRedraw(this.map);
        }
    }

    private onRightButtonUp = (recognizer) => {
        if (recognizer.button === 2) {
            const navigation = this.getNavigationAttributes();
            this.setNavigation.emit(navigation);
            this.changeLayerTimezone(navigation.tilt);
        }
    }

    private handleClickSelectToolMarker = (recognizer) => {
        if (!!this.isDraggingMap) {
            this.isClicked = false;
            this.isDraggingMap = false;
        } else {
            if (this.tapped === -1) {
                this.tapped = setTimeout(function (recognizerParam, that) {
                    that.tapped = -1;
                    that.mainClick(recognizerParam, recognizer);
                }, this.DOUBLE_TAP_INTERVAL, recognizer, this);
            } else {
                clearTimeout(this.tapped);
                this.tapped = -1;
                this.doubleClick(recognizer);
            }

            this.isClicked = false;
            this.isDraggingMap = false;

        }

    }
    /* #endregion Mouse Events*/

    getZoom(): number {
        const navigator = this.map.navigator;
        const zoom = +navigator.range.toFixed(0);
        return zoom;
    }

    getNavigationAttributes(): IMapNavigation {
        const navigator = this.map.navigator;
        const centerPoint = navigator.lookAtLocation;
        const geoCenter = `
            {
                "type": "Point",
                "coordinates": [
                    ${centerPoint.latitude},${centerPoint.longitude}
                ]
            }`;
        const zoom = +navigator.range.toFixed(0);
        const heading = navigator.heading;
        const tilt = navigator.tilt;
        const navigation: IMapNavigation = {
            geoCenter: geoCenter,
            zoom: zoom,
            heading: heading,
            tilt: tilt,
        };
        // return { geoCenter, zoom, heading, tilt };
        return navigation;
    }

    /* #region observers  */
    private observeLoadLayers() {
        this.layersAdded.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((layers) => {
                this.layersListAdded = layers;
                if (layers.length > 0) {
                    try {
                        this.mapService.createLayers(this.map, layers, this.servers);

                    } catch (error) {
                        this.snackBar.open(error.message, null, {
                            duration: environment.toast.duration,
                            verticalPosition: <MatSnackBarVerticalPosition>environment.toast.verticalPosition,
                            horizontalPosition: <MatSnackBarHorizontalPosition>environment.toast.horizontalPosition,
                            panelClass: 'style-error'
                        });
                    }

                } else {
                    this.mapService.removeAllLayers(this.map);
                }
                const _anyTimeLayer = this.mapService.checkAnyTimeLayer(this.map);
                if (_anyTimeLayer !== this.anyTimeLayer) {
                    this.anyTimeLayer = _anyTimeLayer;
                    if (this.anyTimeLayer) {
                        const SHAPE_FILE = './assets/timezoneLayer/ne_05deg_time_zones_clipped.shp';
                        this.mapService.addShapeFileLayer(this.map, SHAPE_FILE);
                        this.map.addEventListener('mousemove', this.manageTime, false);
                    } else {
                        this.mapService.removeLayerTimeZoneLayer(this.map);
                        this.map.removeEventListener('mousemove', this.manageTime);
                    }
                }
                this.cd.markForCheck();
            });




        this.serverList.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((server: IServer[]) => {
                this.servers = server;
            });
    }

    private observeMarkerTypeSelected() {
        this.markerTypeSelected.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((marker) => {
                this.markerType = marker;
                if (marker) {
                    this.mapService.processDeselectMarkerMap(this.map);
                }
            });
    }

    private observeMakers() {
        this.markers.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((markerMaps: IMapMarker[]) => {
                this.markersAdded = markerMaps;
                if (markerMaps.length > 0) {
                    const zoom = this.getZoom();
                    this.mapService.drawMultiPlacemark(this.map, markerMaps, zoom);
                } else {
                    this.mapService.removeAllPlacemark(this.map);
                }
                this.mapService.forceRedraw(this.map);
            });
    }

    private observeMarkerEditable() {
        this.markerEditable.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((marker: IMapMarker) => {
                if (!!marker && !!marker.idMapMarker) {
                    if ((!this.markerMapEditable) || (this.markerMapEditable.idMapMarker !== marker.idMapMarker)) {
                        const zoom = this.getNavigationAttributes().zoom;
                        this.mapService.processMarkerMapByMarker(this.map, marker, zoom);
                        if (marker.moveToLocation) {
                            this.map.goTo(new WorldWind.Location(marker.location.latitude, marker.location.longitude));
                        }
                        this.mapService.forceRedraw(this.map);
                    }
                } else {
                    this.mapService.processDeselectMarkerMap(this.map);
                }
                this.markerMapEditable = marker;
                this.mapService.forceRedraw(this.map);
            });
    }

    private observeToolSelected() {
        this.toolTypeSelected.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((toolSelected: ITool) => {
                this.toolType = toolSelected;
            });
    }

    private observeToolAdded() {
        this.toolsMap.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((tools: ITool[]) => {
                this.mapService.loadTools(this.map, tools);
                this.mapService.forceRedraw(this.map);
            });
    }

    // private observeMediaList() {
    //     this.mediaResources.asObservable()
    //         .pipe(takeUntil(this.unsubscribe$))
    //         .subscribe((media: IMediaResource[]) => {

    //             // this.map.goTo(new WorldWind.Location(location.latitude, location.longitude));
    //         });
    // }

    private observeToolMapEditable() {
        this.toolEditable.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((tool: ITool) => {
                this.mapService.cancelSelectedTool(this.map, this.toolMapEditable);
                this.toolMapEditable = tool;
                // if (!tool) {
                //     this.mapService.cancelSelectedTool(this.map, this.toolType);
                // } else {
                if (!!tool) {
                    // Esto se hace porque al mover un punto del rectángulo
                    // tenemos que saber el layer y no calcularlo en el mouseMove cada vez que se mueva
                    this.layerToolEditableSelected = this.mapService.processToolEditable(this.map, tool);
                }
            });
    }

    private observeToolDialog() {
        this.toolDialog.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((toolDialogs: IToolDialog[]) => {
                this.toolDialogs = toolDialogs;
            });
    }

    private observeMarkerDialog() {
        this.markerDialog.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((markerDialogs: IMarkerDialog[]) => {
                this.markerDialogs = markerDialogs;
            });
    }

    private observeCity() {
        this.city.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((location: ILocation) => {
                this.map.goTo(new WorldWind.Location(location.latitude, location.longitude));
            });
    }

    private observeMoveToLocation() {
        this.moveToLocation.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((location: ILocation) => {
                this.map.goTo(new WorldWind.Location(location.latitude, location.longitude));
            });
    }

    private observeTimeDiff() {
        this.eventManager.subscribe('timeDiff', (param: any) => {
            const timeDiff = +param.content;
            const newTime = this.mapService.setTimeDiff(this.map, timeDiff);
            const terrainTime = this.mapService.getFormatTime(newTime, this.map.actualTz).toLocaleString();
            this.terrainTime.next(terrainTime);
        });
    }

    private observeMediaResources() {
        this.mediaResources.asObservable()
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((mediaResources: IMediaResource[]) => {
                this.mediaResourcesAdded = mediaResources;
                const navigator = this.map.navigator;
                const zoom = +navigator.range.toFixed(0);
                this.mapService.processMedia(this.map, mediaResources, zoom);
            });
    }

    finishToolAction(tool: ITool) {
        const newMapTool = this.mapService.finishSelection(this.map, tool);
        this.toolMapAdded.next(newMapTool);
        switch (this.toolType.type) {
            case ToolsType.MultiLineString:
            case ToolsType.LineString:
            case ToolsType.Polygon:
            case ToolsType.MultiPolygon:
                this.initialPoint = null;
                this.fakeLineLayer = [];
                break;
        }
        this.startX = null;
        this.startY = null;
        this.isClicked = false;
        this.isDraggingMap = false;
        this.mapService.forceRedraw(this.map);
    }

    canChangeTool(): boolean {
        return this.mapService.canChangeTool();
    }

    /* #endregion */
}
