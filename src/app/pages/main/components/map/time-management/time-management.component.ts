import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    OnDestroy,
    Input,
    ChangeDetectorRef
} from '@angular/core';
import { SvsEventManager } from '@app/core/handlers/eventmanager.service';
import { Subject } from 'rxjs';
import { filter, distinctUntilChanged, takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-time-management',
    templateUrl: './time-management.component.html',
    styleUrls: ['./time-management.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class TimeManagementComponent implements OnInit, OnDestroy {

    @Input() terrainTime: Subject<string>;

    actualTime: string;

    private unsubscribe$ = new Subject<void>();

    constructor(
        private eventManager: SvsEventManager,
        private cd: ChangeDetectorRef,
    ) { }

    ngOnInit() {
        this.terrainTime
            .pipe(distinctUntilChanged())
            .pipe(takeUntil(this.unsubscribe$))
            .pipe(filter((data) => {
                return (!!data);
            }))
            .subscribe((data) => {
                // console.log(data);
                this.actualTime = data;
                this.cd.detectChanges();
            });
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
    }

    private broadcastTimeDiff(timeDiff: number) {
        this.eventManager.broadcast({
            name: 'timeDiff',
            content: timeDiff
        });
    }

    addHour() {
        this.broadcastTimeDiff(1);
    }

    addDay() {
        this.broadcastTimeDiff(24);
    }

    resetTime() {
        this.broadcastTimeDiff(0);
    }

    subtractHour() {
        this.broadcastTimeDiff(-1);
    }

    subtractDay() {
        this.broadcastTimeDiff(-24);
    }

}
