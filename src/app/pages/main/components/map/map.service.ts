import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as WorldWind from 'src/vendor/worldwind.js';
import { environment } from '@environment/environment';
import * as moment from 'moment';

import {
    IPointLocation,
    IMeasurement,
    ILocation,
    ICuadrantal,
    EnumCoordinateLatitude,
    EnumCoordinateLongitude,
    EnumCuadrantalAngles,
    IBounds,
    EnumProjection,
    IMApCreateParams,
    EnumBaseLayer
} from './map.model';

import { LayerNode, IServer } from '@app/pages/main/components/layers/layers.model';
import { ITool, ToolsType, IAttributes } from '../tools/tools.model';
import { IMapMarker, IMarker } from '../markers/markers.model';
import { IMediaResource } from '@app/components/media-list/media.model';

@Injectable({
    providedIn: 'root'
})

export class MapService {


    geoJSON: GeoJSON.GeoJSON[] = [];
    arrPoints: IPointLocation[] = [];
    timeRedraw: any;
    flatGlobe: any;

    starFieldLayer: any;
    // terrainTime: string;

    constructor(
        private translateService: TranslateService,
    ) {
    }

    private hexToRgb = (hex: string) => {
        // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
        const shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
        hex = hex.replace(shorthandRegex, function (m, r, g, b) {
            return r + r + g + g + b + b;
        });

        const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
        return result ? {
            r: parseInt(result[1], 16),
            g: parseInt(result[2], 16),
            b: parseInt(result[3], 16)
        } : null;
    }

    loadMap(configMap: IMApCreateParams): WorldWind {
        WorldWind.configuration.baseUrl = environment.map.config.baseUrl;
        // gpuCacheSize: A Number indicating the size in bytes to allocate from GPU memory for
        // resources such as textures, GLSL programs and buffer objects. Default is 250e6 (250 MB).
        WorldWind.configuration.gpuCacheSize = environment.map.config.gpuCacheSize;
        // layerRetrievalQueueSize: The number of concurrent tile requests allowed per layer. The default is 16.</li>
        // coverageRetrievalQueueSize: The number of concurrent tile requests allowed per elevation coverage. The default is 16.</li>
        WorldWind.configuration.layerRetrievalQueueSize = environment.map.config.layerRetrievalQueueSize;
        WorldWind.configuration.coverageRetrievalQueueSize = environment.map.config.coverageRetrievalQueueSize;

        const wwd = new WorldWind.WorldWindow('scene', null, configMap);
        // wwd.navigator.lookAtLocation.latitude = !!configMap.center ? configMap.center.latitude : environment.map.location.lat;
        // wwd.navigator.lookAtLocation.longitude = !!configMap.center ? configMap.center.longitude : environment.map.location.lng;
        // wwd.navigator.range = configMap.zoom || environment.map.range;
        // wwd.navigator.heading = configMap.heading || 0;
        // wwd.navigator.tilt = configMap.tilt || 0;

        this.initializeMapNavigator(wwd, configMap);

        if (!!configMap.projection) {
            this.configureProjection(wwd, configMap.projection);
        }

        if (!!configMap.baseLayer) {
            this.configureBaseLayer(wwd, configMap.baseLayer);
        } else {
            this.configureBaseLayer(wwd, <EnumBaseLayer>environment.map.baseLayer);
        }

        wwd.addLayer(new WorldWind.CompassLayer());
        wwd.addLayer(new WorldWind.CoordinatesDisplayLayer(wwd));
        wwd.addLayer(new WorldWind.ViewControlsLayer(wwd));

        this.starFieldLayer = new WorldWind.StarFieldLayer();
        wwd.addLayer(this.starFieldLayer);

        this.starFieldLayer.time = new Date();

        const highlightController = new WorldWind.HighlightController(wwd);

        this.forceRedraw(wwd);
        return wwd;
    }

    initializeMapNavigator(map: WorldWind, configMap) {
        map.navigator.lookAtLocation.latitude = !!configMap.center ? configMap.center.latitude : environment.map.location.lat;
        map.navigator.lookAtLocation.longitude = !!configMap.center ? configMap.center.longitude : environment.map.location.lng;
        map.navigator.range = configMap.zoom || environment.map.range;
        map.navigator.heading = configMap.heading || 0;
        map.navigator.tilt = configMap.tilt || 0;
    }

    configureBaseLayer(map: WorldWind, baseLayer: EnumBaseLayer) {

        const layerTexts = Object.values(EnumBaseLayer);

        const layerSelected = Object.entries(EnumBaseLayer)
            .find((layer) => {
                return ((layer[0] === baseLayer) || (layer[1] === baseLayer));
            });
        let previousIdx = -1;
        map.layers.map((item, idx) => {
            const index = layerTexts.findIndex((text) => {
                return (text === map.layers[idx].displayName);
            });
            if (index >= 0) {
                previousIdx = idx;
            }
        });

        for (let i = 0; i < map.layers.length; i++) {
            if (layerTexts.indexOf(map.layers[i].displayName) !== -1) {
                map.removeLayer(map.layers[i]);
                i--;
            }
        }

        const allLayers = map.layers;
        map.layers = [
            new WorldWind[layerSelected[0]](),
            ...allLayers
        ];

        this.forceRedraw(map);
    }

    configureProjection(map: WorldWind, projectionName: EnumProjection) {
        if (projectionName === environment.constants.map.projections['3D']) {
            map.globe = new WorldWind.Globe(new WorldWind.EarthElevationModel());

            if (map.globe !== map.globe) {
                map.globe = map.globe;
            }
        } else {
            if (!this.flatGlobe) {
                this.flatGlobe = new WorldWind.Globe2D();
            }
            if (projectionName === environment.constants.map.projections.EQUIRECTANGULAR) {
                this.flatGlobe.projection = new WorldWind.ProjectionEquirectangular();
            } else if (projectionName === environment.constants.map.projections.MERCATOR) {
                this.flatGlobe.projection = new WorldWind.ProjectionMercator();
            } else if (projectionName === environment.constants.map.projections.NORTH_POLAR) {
                this.flatGlobe.projection = new WorldWind.ProjectionPolarEquidistant(environment.constants.map.projections.NORTH);
            } else if (projectionName === environment.constants.map.projections.SOUTH_POLAR) {
                this.flatGlobe.projection = new WorldWind.ProjectionPolarEquidistant(environment.constants.map.projections.SOUTH);
            } else if (projectionName === environment.constants.map.projections.NORTH_UPS) {
                this.flatGlobe.projection = new WorldWind.ProjectionUPS(environment.constants.map.projections.NORTH);
            } else if (projectionName === environment.constants.map.projections.SOUTH_UPS) {
                this.flatGlobe.projection = new WorldWind.ProjectionUPS(environment.constants.map.projections.SOUTH);
            } else if (projectionName === environment.constants.map.projections.NORTH_GNOMONIC) {
                this.flatGlobe.projection = new WorldWind.ProjectionGnomonic(environment.constants.map.projections.NORTH);
            } else if (projectionName === environment.constants.map.projections.SOUTH_GNOMONIC) {
                this.flatGlobe.projection = new WorldWind.ProjectionGnomonic(environment.constants.map.projections.SOUTH);
            }

            if (map.globe !== this.flatGlobe) {
                map.globe = this.flatGlobe;
            }
        }
        this.forceRedraw(map);
    }

    forceRedraw(map: WorldWind) {
        if (!this.timeRedraw) {
            this.timeRedraw = setTimeout(() => {

                const time = new Date();
                this.starFieldLayer.time = time;

                map.redraw();
                clearTimeout(this.timeRedraw);
                this.timeRedraw = null;
            }, environment.map.config.timeToRedraw);
        }
    }

    removeSquarePoint(map: WorldWind) {
        map.layers.map((item, idx) => {
            if (item.displayName === environment.constants.map.ORIGINAL_POLYGON_POINT) {
                map.removeLayer(item);
            }
        });
    }

    private _createSquarePoint(point: IPointLocation) {
        const canvas = document.createElement('canvas');
        const ctx2d = canvas.getContext('2d');
        canvas.setAttribute('id', 'square');
        canvas.width = environment.map.squareSize;
        canvas.height = environment.map.squareSize;
        ctx2d.rect(0, 0, environment.map.squareSize, environment.map.squareSize);
        ctx2d.fillStyle = 'white';
        ctx2d.fill();
        const placemarkAttributes = new WorldWind.PlacemarkAttributes(null);
        placemarkAttributes.imageSource = new WorldWind.ImageSource(canvas);
        placemarkAttributes.imageOffset = new WorldWind.Offset(WorldWind.OFFSET_FRACTION, 0.5, WorldWind.OFFSET_FRACTION, 0.5);
        placemarkAttributes.imageScale = 1;
        const highlightAttributes = new WorldWind.PlacemarkAttributes(placemarkAttributes);
        highlightAttributes.imageScale = environment.map.squareHighlight;
        const placemarkPosition = new WorldWind.Position(point.latitude, point.longitude);
        const placemark = new WorldWind.Placemark(placemarkPosition, false, placemarkAttributes);
        placemark.altitudeMode = WorldWind.RELATIVE_TO_GROUND;
        placemark.highlightAttributes = highlightAttributes;
        placemark.myPoint = point;
        return placemark;
    }

    addSquarePointCenter(map: WorldWind, point: IPointLocation) {
        const placemark = this._createSquarePoint(point);
        const placemarkLayer = new WorldWind.RenderableLayer(environment.constants.map.REGULAR_CENTER_POINT);
        placemarkLayer.addRenderable(placemark);
        map.addLayer(placemarkLayer);
    }

    removeSquarePointCenter(map: WorldWind) {
        map.layers.map((item, idx) => {
            if (item.displayName === environment.constants.map.REGULAR_CENTER_POINT) {
                map.removeLayer(item);
            }
        });
    }

    addSquarePoint(map: WorldWind, point: IPointLocation) {
        const placemark = this._createSquarePoint(point);
        const placemarkLayer = new WorldWind.RenderableLayer(environment.constants.map.ORIGINAL_POLYGON_POINT);
        placemarkLayer.addRenderable(placemark);
        map.addLayer(placemarkLayer);
    }

    private getLayerToolPoints(map: WorldWind, middlePoints = false) {
        let layerPoint;
        const displayName: string = middlePoints ? environment.constants.map.TOOL_MIDDLE_POINT : environment.constants.map.TOOL_POINT;
        map.layers.forEach((layer) => {
            if (layer.displayName === displayName) {
                layerPoint = layer;
            }
        });
        if (!layerPoint) {
            layerPoint = new WorldWind.RenderableLayer(displayName);
        }
        return layerPoint;
    }

    addSquarePoints(map: WorldWind, tool: ITool, mainLayer) {

        let points = tool.attributes.points;
        if (
            (tool.type === ToolsType.Rectangle)
            ||
            (tool.type === ToolsType.Square)
        ) {
            points = [...mainLayer.renderables[0]._boundaries];
        }

        const placemarkLayer = this.getLayerToolPoints(map);
        let fakeArrPoints: IPointLocation[] = [];
        const realMiddlePoint: IPointLocation[] = [];
        points.forEach((point, idx: number) => {

            const placemark = this._createSquarePoint(point);
            if (
                (tool.type === ToolsType.Rectangle)
                ||
                (tool.type === ToolsType.Square)
            ) {
                // formatBoundaries devuelve los puntos empezando desde abajo a la izquierda en sentido contrario a las agujas del reloj
                placemark.myVerticeIdx = 3 - idx;
            }
            placemarkLayer.addRenderable(placemark);
            if (fakeArrPoints.length === 0) {
                fakeArrPoints.push(point);
            } else {
                if (fakeArrPoints.length === 1) {
                    fakeArrPoints.push(point);
                    if ((tool.type === ToolsType.Polygon) || (tool.type === ToolsType.LineString)) {
                        const fakePoint = this.middlePoint(
                            fakeArrPoints[0].latitude,
                            fakeArrPoints[0].longitude,
                            fakeArrPoints[1].latitude,
                            fakeArrPoints[1].longitude
                        );
                        fakePoint.idPoint = new Date().getTime() + idx;
                        realMiddlePoint.push(fakePoint);
                    }
                    // Nos quedamos con el segundo punto,
                    // para que en el siguiente ciclo de código
                    // se tome como punto de partida.
                    fakeArrPoints = [];
                    fakeArrPoints.push(point);
                }
            }
        });

        if ((tool.type === ToolsType.Polygon) || (tool.type === ToolsType.LineString)) {
            if (tool.type === ToolsType.Polygon) {
                // Con esto dibujamos el punto medio que hay entre el primer punto y el último.
                const lastPoint = this.middlePoint(
                    points[points.length - 1].latitude,
                    points[points.length - 1].longitude,
                    points[0].latitude,
                    points[0].longitude
                );
                lastPoint.idPoint = new Date().getTime();
                realMiddlePoint.push(lastPoint);
            }

            const placemarkFakePoints = this.getLayerToolPoints(map, true);
            realMiddlePoint.forEach((middlePoint) => {
                const placemark = this._createSquarePoint(middlePoint);
                placemarkFakePoints.opacity = .5;
                placemarkFakePoints.addRenderable(placemark);
            });
            map.addLayer(placemarkFakePoints);

        }

        map.addLayer(placemarkLayer);
        this.forceRedraw(map);
    }

    addFakeLine(map: WorldWind, arrPoints: IPointLocation[]) {
        this.drawSurfacePolyline(map, [arrPoints[0], arrPoints[arrPoints.length - 1]], null, false, true);
    }

    removeFakeLine(map: WorldWind) {
        const fakeLines = [];
        map.layers.forEach((layer, idx) => {
            if (layer.displayName === environment.constants.map.FAKE_LINE) {
                // map.layers.splice(idx, 1);
                fakeLines.push(layer);
            }
        });
        fakeLines.forEach((line) => {
            map.removeLayer(line);
        });
    }

    removeLines(map: WorldWind) {
        map.layers.map((item) => {
            if (item.displayName === environment.constants.map.TOOL_LAYER) {
                const idxToRemove = [];
                item.renderables.forEach((renderable, idx) => {
                    // if (renderable instanceof WorldWind.SurfacePolyline) {
                    if (renderable instanceof WorldWind.SurfacePolyline &&
                        renderable.source !== environment.constants.map.SHAPE_FROM_LINE) {
                        idxToRemove.push(idx);
                    }
                });
                const copyRenderables = [];
                item.renderables.forEach((render, index) => {
                    if (idxToRemove.indexOf(index) === -1) {
                        copyRenderables.push(render);
                    }
                });
                item.renderables = copyRenderables;
            }
        });
    }

    private getToolLayer(map: WorldWind): WorldWind.RenderableLayer {
        let toolLayer;
        if (!!map && map.layers) {
            map.layers.forEach((layerMap) => {
                if (layerMap.displayName === environment.constants.map.TOOL_LAYER) {
                    toolLayer = layerMap;
                }
            });
        }

        if (!toolLayer) {
            toolLayer = new WorldWind.RenderableLayer(environment.constants.map.TOOL_LAYER);
            map.addLayer(toolLayer);
        }

        return toolLayer;
    }

    removeAllPlacemark(map: WorldWind) {
        const placemarkRemoved: any[] = [];
        if (!!map && map.layers) {

            map.layers.forEach((layerMap) => {
                if (layerMap.displayName === environment.constants.map.PLACEMARK) {
                    placemarkRemoved.push(layerMap);
                }
            });

            const count = placemarkRemoved.length;
            for (let i = 0; i < count; i++) {
                map.removeLayer(placemarkRemoved[i]);
            }
        }
    }

    private getMarker(map: WorldWind, marker: IMapMarker): IMapMarker {
        return map.layers.find((layer) => {
            if (!!layer.renderables) {
                return layer.renderables.find((renderable) => {
                    return (renderable.myId === marker.idMapMarker);
                });
            } else {
                return null;
            }
        });
    }

    existsMarker(map: WorldWind, marker: IMapMarker): boolean {
        const result = this.getMarker(map, marker);
        return (!!result);
    }

    private updatePlaceMark(map: WorldWind, marker: IMapMarker) {
        const markerToEdit = this.getMarker(map, marker);
        markerToEdit.mapObservations = marker.mapObservations;
        markerToEdit.mapTitle = marker.mapTitle;

        const renderables = this.getPlacemark(map);
        renderables.forEach((renderable, idx) => {
            if (renderable.myId === marker.idMapMarker) {
                if ((renderable.position.latitude !== marker.location.latitude)
                    || (renderable.position.longitude !== marker.location.longitude)
                    || (renderable.position.altitude !== marker.location.altitude)) {

                    renderable.position =
                        new WorldWind.Position(
                            marker.location.latitude,
                            marker.location.longitude,
                            marker.location.altitude);

                    // Si actualizamos la posición, puede que exista el layer del markerselected,
                    // y tambien habría que modificarlo ahí.
                    this.movePlacemark(map, renderable.position);
                }
            }
        });

    }

    private getPlacemark(map: WorldWind) {
        let result;
        if (!!map && map.layers) {
            map.layers.forEach((layerMap) => {
                if (layerMap.displayName === environment.constants.map.PLACEMARK) {
                    result = layerMap.renderables;
                }
            });
        }
        return result;
    }

    drawMultiPlacemark(map: WorldWind, markers: IMapMarker[], zoom: number) {
        const renderables = this.getPlacemark(map);
        if ((!!renderables) && (markers.length < renderables.length)) {
            const layerToRemove: any[] = [];
            let shouldDeselect = false;
            renderables.forEach((renderable, idx) => {
                const markerFound = markers.find((marker) => {
                    return (renderable.myId === marker.idMapMarker);
                });
                if (!markerFound) {
                    shouldDeselect = true;
                    layerToRemove.push(idx);
                }
            });
            for (let idx = 0; idx < layerToRemove.length; idx++) {
                renderables.splice(layerToRemove[idx], 1);
            }
            if (shouldDeselect) {
                this.processDeselectMarkerMap(map);
            }
        } else {
            markers.forEach((newMarker) => {
                if (!this.existsMarker(map, newMarker)) {
                    this.drawPlacemark(map, newMarker, zoom);
                } else {
                    this.updatePlaceMark(map, newMarker);
                }
            });
        }
        // this.updateMarkerSize(map, zoom);
        // this.forceRedraw(map);
    }

    private getPlacemarkLayer(map: WorldWind): WorldWind.RenderableLayer {
        let placemarkLayer;
        if (!!map && map.layers) {
            map.layers.forEach((layerMap) => {
                if (layerMap.displayName === environment.constants.map.PLACEMARK) {
                    placemarkLayer = layerMap;
                }
            });
        }
        return placemarkLayer;
    }

    getPlacemarkSelected(map: WorldWind): WorldWind.RenderableLayer {
        let placemarkLayer;
        if (!!map && map.layers) {
            map.layers.forEach((layerMap) => {
                if (layerMap.displayName === environment.constants.map.PLACEMARK) {
                    placemarkLayer = layerMap.renderables[0];
                }
            });
        }
        return placemarkLayer;
    }

    private createRenderablePlacemark(marker: IMapMarker, zoom: number): WorldWind.Placemark {
        const placemarkAttributes = new WorldWind.PlacemarkAttributes(null);
        placemarkAttributes.imageSource = marker.iconUrl;
        placemarkAttributes.imageOffset = new WorldWind.Offset(
            WorldWind.OFFSET_FRACTION, marker.offsetX,
            WorldWind.OFFSET_FRACTION, marker.offsetY);
        placemarkAttributes.imageScale = this.getZoomScale(zoom);

        // const highlightAttributes = new WorldWind.PlacemarkAttributes(placemarkAttributes);
        // highlightAttributes.imageScale = 1.2;

        placemarkAttributes.labelAttributes.offset = new WorldWind.Offset(
            WorldWind.OFFSET_FRACTION, 0.5,
            WorldWind.OFFSET_FRACTION, 1.0);
        placemarkAttributes.labelAttributes.color = WorldWind.Color.YELLOW;

        placemarkAttributes.drawLeaderLine = true;

        const placemarkPosition = new WorldWind.Position(
            marker.location.latitude,
            marker.location.longitude,
            +(marker.location.altitude || environment.map.markerAltitude)
        );
        const placemark = new WorldWind.Placemark(placemarkPosition, false, placemarkAttributes);
        // placemark.highlightAttributes = highlightAttributes;

        // ABSOLUTE
        // RELATIVE_TO_GROUND
        // CLAMP_TO_GROUND
        placemark.altitudeMode = WorldWind.RELATIVE_TO_GROUND;
        placemark.myId = marker.idMapMarker;
        placemark.myMarker = marker;
        return placemark;
    }

    highlightMarker(map: WorldWind, userObject?: any) {
        if (!!userObject && !!userObject.myMarker) {
            map.layers.forEach((layer) => {
                if (!!layer.renderables) {
                    layer.renderables.forEach((renderable) => {
                        renderable.highlighted = false;
                        renderable.label = null;
                    });
                }
            });

            if (!!userObject) {
                userObject.highlighted = true;
                userObject.label = userObject.myMarker.mapTitle;
            }
        }
    }

    drawPlacemark(map: WorldWind, marker: IMapMarker, zoom: number) {
        const placemark = this.createRenderablePlacemark(marker, zoom);
        let placemarkLayer = this.getPlacemarkLayer(map);
        if (!placemarkLayer) {
            placemarkLayer = new WorldWind.RenderableLayer(environment.constants.map.PLACEMARK);
            placemarkLayer.addRenderable(placemark);
            map.addLayer(placemarkLayer);
        } else {
            placemarkLayer.addRenderable(placemark);
        }

    }

    movePlacemark(map: WorldWind, position: WorldWind.Position, layer?: any) {
        if (!layer) {
            if (!!map && map.layers) {
                map.layers.forEach((layerMap) => {
                    if (layerMap.displayName === environment.constants.map.PLACEMARKSELECTED) {
                        layerMap.renderables.forEach((renderable) => {
                            renderable.position = position;
                        });
                    }
                });
            }
        } else {
            layer.renderables.forEach((renderable) => {
                renderable.position = position;
            });
        }

        this.forceRedraw(map);
    }

    getSurfacePolygonPosition(map: WorldWind, startX: number, startY: number) {
        let position;
        map.pick(map.canvasCoordinates(startX, startY)).objects.forEach((obj) => {
            if (obj.userObject instanceof WorldWind.Terrain) {
                position = obj.position;
            }

        });
        return position;
    }

    moveSurface(
        map: WorldWind, layerRenderable, idMapTool: number,
        originX: number, originY: number, finalX: number, finalY: number,
        layerPoints,
        layerMiddlePoints) {

        const originalPosition: IPointLocation = this.getSurfacePolygonPosition(map, originX, originY);
        const finalPosition: IPointLocation = this.getSurfacePolygonPosition(map, finalX, finalY);
        const additionX = (originalPosition.latitude - finalPosition.latitude);
        const additionY = (originalPosition.longitude - finalPosition.longitude);

        layerRenderable.myTool.attributes.points.forEach((point: IPointLocation) => {
            point.latitude = point.latitude - additionX;
            point.longitude = point.longitude - additionY;
        });

        this.removeLayerToolById(map, idMapTool);
        map.removeLayer(layerPoints);
        map.removeLayer(layerMiddlePoints);

        switch (layerRenderable.myTool.type) {
            case ToolsType.Circle:
                this.drawSurfaceCircle(map, layerRenderable.myTool);
                break;
            case ToolsType.Ellipse:
                this.drawSurfaceEllipse(map, layerRenderable.myTool);
                break;
            case ToolsType.Rectangle:
                this.drawSurfaceRectangle(map, layerRenderable.myTool);
                break;
            case ToolsType.Square:
            case ToolsType.Polygon:
            case ToolsType.MultiPolygon:
                this.drawSurfacePolygon(map, layerRenderable.myTool);
                break;
            case ToolsType.LineString:
            case ToolsType.MultiLineString:
                this.drawSurfacePolyline(map, layerRenderable.myTool.attributes.points, layerRenderable.myTool, true, false);
                break;
            default:
                break;
        }
        this.forceRedraw(map);
    }

    addPointToSurfacePolygon(
        map: WorldWind,
        toolEditable: ITool,
        point: IPointLocation,
        actualX: number,
        actualY: number,
        actualZ: number,
        idxToAddPoint: number
    ) {

        const firstPart = toolEditable.attributes.points.slice(0, idxToAddPoint);
        const element = point;
        const finalPart = toolEditable.attributes.points.slice(idxToAddPoint, toolEditable.attributes.points.length);
        const arrPoints = [...firstPart, element, ...finalPart];
        toolEditable.attributes.points = arrPoints;
    }

    movePoint(map: WorldWind, toolEditable: ITool) {

        const layerToCreatePolygon = this.getLayerToolById(map, toolEditable.attributes.idMapTool);
        map.removeLayer(layerToCreatePolygon);
        this.cancelSelectedTool(map, toolEditable);
        this.removeFakeLine(map);
        switch (true) {
            case (toolEditable.type === ToolsType.MultiLineString):
            case (toolEditable.type === ToolsType.LineString):
                this.drawSurfacePolyline(map, toolEditable.attributes.points, toolEditable, true, false);
                break;
            case (toolEditable.type === ToolsType.MultiPolygon):
            case (toolEditable.type === ToolsType.Polygon):
                this.drawSurfacePolygon(map, toolEditable);
                break;
            default:
                console.error('[map.service.movePoint() - ' + toolEditable.type);
                break;
        }

    }

    private getOpacity(transparency: number): number {
        return (transparency <= 0 ? 1 : (1 - (transparency / 100)));
    }

    private createOneLayer(map: WorldWind, layer: LayerNode, servers: IServer[]) {
        const xml = servers.find((server: IServer) => {
            return (server.url === layer.item.gisServerUrl);
        }).xml;
        const realXml = (new DOMParser()).parseFromString(xml, 'text/xml');
        const wms = new WorldWind.WmsCapabilities(realXml);
        const wmsLayerCapabilities = wms.getNamedLayer(layer.item.name);
        if (!!wmsLayerCapabilities) {
            const wmsConfig = WorldWind.WmsLayer.formLayerConfiguration(wmsLayerCapabilities);
            wmsConfig.title = layer.item.name;

            let wmsLayer: WorldWind.WmsLayer | WorldWind.WmsTimeDimensionedLayer;
            // let timeSequence;

            if (wmsConfig.timeSequences &&
                (wmsConfig.timeSequences[wmsConfig.timeSequences.length - 1] instanceof WorldWind.PeriodicTimeSequence)) {

                // timeSequence = wmsConfig.timeSequences[wmsConfig.timeSequences.length - 1];
                wmsLayer = new WorldWind.WmsTimeDimensionedLayer(wmsConfig);
                // wmsLayer.time = timeSequence.startTime;
                // wmsLayer.timeSequence = timeSequence;
                wmsLayer.time = new Date();
            } else if (wmsConfig.timeSequences &&
                (wmsConfig.timeSequences[wmsConfig.timeSequences.length - 1] instanceof Date)) {

                // timeSequence = wmsConfig.timeSequences[wmsConfig.timeSequences.length - 1];
                wmsLayer = new WorldWind.WmsTimeDimensionedLayer(wmsConfig);
                // wmsLayer.time = wmsConfig.timeSequences[0];
                // wmsLayer.timeSequence = timeSequence;
                wmsLayer.time = new Date();
            } else {
                wmsLayer = new WorldWind.WmsLayer(wmsConfig);
            }
            wmsLayer.opacity = this.getOpacity(layer.item.transparency);
            wmsLayer.id = layer.item.idLayer;
            wmsLayer.enabled = (layer.enabled !== false) ? true : false;
            wmsLayer.type = 'WEBGIS';
            map.addLayer(wmsLayer);
        } else {
            console.error(`Layer ${layer.item.name} not found.`);
            throw new Error(`Layer ${layer.item.name} not found.`);
        }

    }

    private removeLayers(map) {
        const layersRemoved: any[] = [];
        if (!!map && map.layers) {
            map.layers.forEach((layerMap) => {
                if (layerMap.type === 'WEBGIS') {
                    layersRemoved.push(layerMap);
                }
            });
            const count = layersRemoved.length;
            for (let i = 0; i < count; i++) {
                map.removeLayer(layersRemoved[i]);
            }
        }
    }

    checkAnyTimeLayer(map: WorldWind): boolean {
        // WmsTimeDimensionedLayer
        return map.layers.filter((layer) => {
            return layer instanceof WorldWind.WmsTimeDimensionedLayer;
        }).length > 0;
    }

    createLayers(map: WorldWind, layers: LayerNode[], servers: IServer[]) {
        this.removeLayers(map);
        const newLayers = [...layers];
        newLayers.reverse().forEach((layer) => {
            try {
                this.createOneLayer(map, layer, servers);
            } catch (error) {
                throw new Error(error);
            }
        });
        this.forceRedraw(map);
    }

    removeAllLayers(map: WorldWind) {
        this.removeLayers(map);
    }

    clearCompass(map: WorldWind) {
        map.navigator.heading = 0;
        map.navigator.tilt = 0;
        this.forceRedraw(map);
    }
    private processPolygon(map: WorldWind, point: IPointLocation) {
        this.arrPoints.push(point);
        // Si es el primer click, no hacemos nada.
        if (this.arrPoints.length === 1) {
            this.addSquarePoint(map, point);
        } else {
            // const originPoint = this.arrPoints[this.arrPoints.length - 1];
            this.drawSurfacePolyline(map, this.arrPoints, null, false, false);
        }

        // Si ya tenemos más de 3 puntos ( polígono )
        // Generamos una última línea más transparente para "cerrarlo"
        if (this.arrPoints.length > 2) {
            // this.removeFakeLine(map);
            // this.addFakeLine(map, this.arrPoints);
        }
    }

    processMedia(map: WorldWind, mediaResources: IMediaResource[], zoom: number): any {
        let layerMedia = map.layers.find((layer) => {
            return layer.displayName === environment.constants.map.LAYER_MEDIA;
        });
        if (layerMedia) {
            layerMedia.removeAllRenderables();
        } else {
            layerMedia = new WorldWind.RenderableLayer(environment.constants.map.LAYER_MEDIA);
            map.addLayer(layerMedia);
        }
        const idsAdded: {
            idResource: string,
            idMediaList: number
        }[] = [];

        mediaResources.forEach((mediaResource: IMediaResource) => {
            if (mediaResource.visible) {
                const exists = idsAdded.find((ids) => {
                    return (ids.idMediaList === mediaResource.idMediaList && ids.idResource === mediaResource.id);
                });

                if (!exists) {
                    // if (idsAdded.indexOf(mediaResource.id) === -1) {
                    const urlImg = `assets/images/pushpins/media-${mediaResource.isVideo ? `movie2.png` : `picture4.png`}`;
                    const marker = this.createGenericPlacemark(
                        urlImg,
                        0, 0,
                        mediaResource.latitude,
                        mediaResource.longitude,
                        WorldWind.Color.WHITE,
                        zoom);
                    marker.myMediaResource = mediaResource;
                    idsAdded.push({
                        idResource: mediaResource.id,
                        idMediaList: mediaResource.idMediaList,
                    });
                    layerMedia.addRenderable(marker);
                }
            }

        });
        this.forceRedraw(map);
    }

    private createGenericPlacemark(imageSource: string,
        offsetX: number, offsetY: number, latitude: number,
        longitude: number, color: WorldWind.Color, zoom: number): WorldWind.Placemark {
        const placemarkAttributes = new WorldWind.PlacemarkAttributes(null);
        placemarkAttributes.imageSource = imageSource;
        placemarkAttributes.imageOffset = new WorldWind.Offset(
            WorldWind.OFFSET_FRACTION, offsetX,
            WorldWind.OFFSET_FRACTION, offsetY);
        placemarkAttributes.imageScale = this.getZoomScale(zoom);

        // const highlightAttributes = new WorldWind.PlacemarkAttributes(placemarkAttributes);
        // highlightAttributes.imageScale = 1.2;

        placemarkAttributes.labelAttributes.offset = new WorldWind.Offset(
            WorldWind.OFFSET_FRACTION, 0.5,
            WorldWind.OFFSET_FRACTION, 1.0);
        placemarkAttributes.labelAttributes.color = color; // WorldWind.Color.YELLOW;

        placemarkAttributes.drawLeaderLine = true;

        const placemarkPosition = new WorldWind.Position(
            latitude,
            longitude,
            +(environment.map.markerAltitude)
        );
        const placemark = new WorldWind.Placemark(placemarkPosition, false, placemarkAttributes);
        // placemark.highlightAttributes = highlightAttributes;

        // ABSOLUTE
        // RELATIVE_TO_GROUND
        // CLAMP_TO_GROUND
        placemark.altitudeMode = WorldWind.RELATIVE_TO_GROUND;
        // placemark.myId = marker.idMapMarker;
        // placemark.myMarker = marker;
        return placemark;
    }

    getPolygonPoints() {
        return this.arrPoints;
    }

    drawSurfacePolyline(
        map: WorldWind,
        arrPoints: IPointLocation[],
        tool?: ITool,
        forceFinish: boolean = false,
        isFakeLine: boolean = false
    ): WorldWind.SurfacePolyline {
        if (forceFinish === true && isFakeLine === true) {
            throw new Error('Bad params. forceFinish & isFakeLine can not be true');
        }
        let color;
        let transparency;
        let width;
        if (!!tool && !!tool.type) {
            color = this.hexToRgb(tool.attributes.color);
            transparency = tool.attributes.transparency;
            width = tool.attributes.width;
        } else {
            color = this.hexToRgb(environment.tools.attributes.color);
            transparency = environment.tools.attributes.transparency;
            width = environment.tools.attributes.width;
        }
        const outlineColor = new WorldWind.Color(
            (((color.r * 100) / 255) / 100),
            (((color.g * 100) / 255) / 100),
            (((color.b * 100) / 255) / 100),
            transparency);
        const attributes = new WorldWind.ShapeAttributes(null);
        attributes.outlineColor = outlineColor;
        attributes.outlineWidth = width;
        const wwPoints: any[] = [];
        arrPoints.map((point) => {
            const _idPoint = point.idPoint;
            point = new WorldWind.Location(point.latitude, point.longitude, point.altitude);
            point.idPoint = _idPoint;
            wwPoints.push(point);
        });
        const polyline = new WorldWind.SurfacePolyline(wwPoints, attributes);

        // Esto se usa para que, cuando hemos dibujado ya una Polyline entera
        // cuando dibujemos la siguiente, no borre ese surfacepolyline
        if (forceFinish) {
            polyline.source = environment.constants.map.SHAPE_FROM_LINE;
            polyline.myTool = tool;
            // Al terminar, borramos la capa que se crea mientras no hemos terminado
            // const tmpToolLayer = this.getToolLayer(map);
            // map.removeLayer(tmpToolLayer);
            // const toolLayer = new WorldWind.RenderableLayer(environment.constants.map.TOOL_LAYER);
            const toolLayer = this.getLayerToolById(map, tool.attributes.idMapTool);
            toolLayer.addRenderable(polyline);
            map.addLayer(toolLayer);
        } else {
            if (isFakeLine) {
                const fakeLayer = new WorldWind.RenderableLayer(environment.constants.map.FAKE_LINE);
                fakeLayer.addRenderable(polyline);
                map.addLayer(fakeLayer);
            } else {
                const toolLayer = this.getToolLayer(map);
                toolLayer.addRenderable(polyline);
            }
        }
        return polyline;

    }

    private createAttributes(attr: IAttributes) {
        const color = this.hexToRgb(attr.color);

        const outlineColor = new WorldWind.Color(
            (((color.r * 100) / 255) / 100),
            (((color.g * 100) / 255) / 100),
            (((color.b * 100) / 255) / 100),
            (attr.transparency));

        const percentInterior = (environment.tools.attributes.interiorTransparency * 100);
        let interiorTransparency = (((attr.transparency * 100) * percentInterior / 100) / 100);

        if (interiorTransparency < 0) {
            interiorTransparency = 0;
        } else if (interiorTransparency > 1) {
            interiorTransparency = 1;
        }

        const interiorColor = new WorldWind.Color(
            (((color.r * 100) / 255) / 100),
            (((color.g * 100) / 255) / 100),
            (((color.b * 100) / 255) / 100),
            interiorTransparency);

        const attributes = new WorldWind.ShapeAttributes(null);
        attributes.outlineColor = outlineColor;
        attributes.interiorColor = interiorColor;
        attributes.outlineWidth = attr.width;
        return attributes;
    }

    drawSurfacePolygon(map: WorldWind, tool: ITool) {
        const wwPoints: any[] = [];
        tool.attributes.points.map((point) => {
            const _idPoint = point.idPoint;
            point = new WorldWind.Location(point.latitude, point.longitude, point.altitude);
            point.idPoint = _idPoint;
            wwPoints.push(point);
        });

        const attributes = this.createAttributes(tool.attributes);
        const polygon = new WorldWind.SurfacePolygon(wwPoints, attributes);
        const toolLayer = new WorldWind.RenderableLayer(environment.constants.map.TOOL_LAYER);
        polygon.myTool = tool;
        toolLayer.addRenderable(polygon);
        map.addLayer(toolLayer);
    }

    drawSurfaceRectangle(map: WorldWind, tool: ITool) {
        const attributes = this.createAttributes(tool.attributes);
        const rectangle = new WorldWind.SurfaceRectangle(
            tool.attributes.center,
            tool.attributes.majorRadius,
            tool.attributes.minorRadius,
            tool.attributes.rotate || 0,
            attributes
        );
        const toolLayer = new WorldWind.RenderableLayer(environment.constants.map.TOOL_LAYER);
        rectangle.myTool = tool;
        toolLayer.addRenderable(rectangle);
        map.addLayer(toolLayer);
    }

    drawSurfaceEllipse(map: WorldWind, tool: ITool) {

        const attributes = this.createAttributes(tool.attributes);
        const ellipse = new WorldWind.SurfaceEllipse(
            tool.attributes.points[0],
            tool.attributes.majorRadius,
            tool.attributes.minorRadius,
            tool.attributes.rotate || 0,
            attributes
        );
        const toolLayer = new WorldWind.RenderableLayer(environment.constants.map.TOOL_LAYER);
        ellipse.myTool = tool;
        toolLayer.addRenderable(ellipse);
        map.addLayer(toolLayer);

    }

    drawSurfaceCircle(map: WorldWind, tool: ITool) {

        const attributes = this.createAttributes(tool.attributes);
        const circle = new WorldWind.SurfaceCircle(tool.attributes.points[0], tool.attributes.radius, attributes);
        const toolLayer = new WorldWind.RenderableLayer(environment.constants.map.TOOL_LAYER);
        circle.myTool = tool;
        toolLayer.addRenderable(circle);
        map.addLayer(toolLayer);

    }

    createFakeLine(map: WorldWind, newPoint: IPointLocation) {
        return null;
    }

    private processLine(map: WorldWind, newPoint: IPointLocation) {
        this.arrPoints.push(newPoint);
        if (this.arrPoints.length === 1) {
            this.addSquarePoint(map, newPoint);
        } else {
            this.drawSurfacePolyline(map, this.arrPoints);
            this.removeSquarePoint(map);
            this.addSquarePoint(map, newPoint);
        }
    }

    private isFinished(pickList): boolean {
        return (
            pickList.objects.length > 0 &&
            pickList.objects[0].userObject instanceof WorldWind.Placemark &&
            pickList.objects[0].parentLayer.displayName === environment.constants.map.ORIGINAL_POLYGON_POINT
        );
    }

    canChangeTool(): boolean {
        return (this.arrPoints.length === 0);
    }

    finishSelection(map: WorldWind, tool: ITool): ITool {
        let toolResponse: ITool;
        if (this.arrPoints.length > 0) {
            toolResponse = {
                type: tool.type,
                attributes: {
                    color: tool.attributes.color,
                    idMapTool: new Date().getTime(),
                    observations: null,
                    title: this.translateService.instant('tools.types.' + tool.type),
                    transparency: tool.attributes.transparency,
                    width: tool.attributes.width,
                    points: this.arrPoints
                }
            };
            this.removeFakeLine(map);
            this.removeSquarePoint(map);
            this.removeLines(map);
            this.arrPoints = [];
        }
        return toolResponse;
    }

    getLayerToolById(map: WorldWind, idMapTool: number) {
        let result;
        map.layers.forEach((layer, idx) => {
            if (layer.displayName === environment.constants.map.TOOL_LAYER) {
                layer.renderables.forEach((renderable) => {
                    if (!!renderable.myTool && renderable.myTool.attributes.idMapTool === idMapTool) {
                        result = map.layers[idx];
                    }
                });
            }
        });
        if (!result) {
            result = new WorldWind.RenderableLayer(environment.constants.map.TOOL_LAYER);
        }
        return result;
    }

    removeLayerToolById(map: WorldWind, idMapTool: number) {
        map.layers.forEach((layer, idx) => {
            if (layer.displayName === environment.constants.map.TOOL_LAYER) {
                layer.renderables.forEach((renderable) => {
                    if (!!renderable.myTool && renderable.myTool.attributes.idMapTool === idMapTool) {
                        map.removeLayer(map.layers[idx]);
                    }
                });
            }
        });
    }

    private existsSurfaceTool(map: WorldWind, idMapTool: number): boolean {
        let exists = false;
        map.layers.forEach((layer) => {
            if (layer.displayName === environment.constants.map.TOOL_LAYER) {
                layer.renderables.forEach((renderable) => {
                    if (!!renderable.myTool && renderable.myTool.attributes.idMapTool === idMapTool) {
                        exists = true;
                    }
                });
            }
        });
        return exists;
    }

    private getLayerTools(map: WorldWind): number {
        let result;
        const toolLayers = map.layers.filter((layer) => {
            return (
                (layer.displayName === environment.constants.map.TOOL_LAYER)
                &&
                (!!layer.renderables && layer.renderables.length > 0)
            );
            // return (layer.displayName === environment.constants.map.TOOL_LAYER);
        });
        result = !!toolLayers ? toolLayers.length : 0;
        return result;
    }

    private updateSurface(map: WorldWind, tool: ITool) {
        map.layers.forEach((layer, idx) => {
            if (layer.displayName === environment.constants.map.TOOL_LAYER) {
                layer.renderables.forEach((renderable) => {
                    if (renderable.myTool.attributes.idMapTool === tool.attributes.idMapTool) {
                        map.removeLayer(map.layers[idx]);
                        switch (renderable.myTool.type) {
                            case ToolsType.Rectangle:
                                this.drawSurfaceRectangle(map, tool);
                                break;
                            case ToolsType.Square:
                            case ToolsType.Polygon:
                            case ToolsType.MultiPolygon:
                                this.drawSurfacePolygon(map, tool);
                                break;
                            case ToolsType.MultiLineString:
                            case ToolsType.LineString:
                                this.drawSurfacePolyline(map, tool.attributes.points, tool, true, false);
                                break;
                            case ToolsType.Circle:
                                this.drawSurfaceCircle(map, tool);
                                break;
                            case ToolsType.Ellipse:
                                this.drawSurfaceEllipse(map, tool);
                                break;
                            default:
                                console.error('[map.service.updateSurface() - ' + tool.type);
                                break;
                        }
                    }
                });
            }
        });
    }

    loadTools(map: WorldWind, tools: ITool[]) {

        const layerTools = this.getLayerTools(map);
        if (tools.length < layerTools) {
            // Quiere decir que se ha eliminado 1 dibujo.
            const idMapToolArray: number[] = [];
            tools.map((item) => {
                idMapToolArray.push(item.attributes.idMapTool);
            });
            const layerToRemove = [];
            map.layers.forEach((layer, idx) => {
                if (layer.displayName === environment.constants.map.TOOL_LAYER) {
                    if (!!layer.renderables) {
                        layer.renderables.forEach((renderable) => {
                            if (renderable.myTool) {
                                if (idMapToolArray.indexOf(renderable.myTool.attributes.idMapTool) === -1) {
                                    layerToRemove.push(map.layers[idx]);
                                }
                            }
                        });
                    }
                }
            });
            const count = layerToRemove.length;
            for (let j = 0; j < count; j++) {
                map.removeLayer(layerToRemove[j]);
            }
        } else {
            tools.forEach((tool: ITool) => {
                switch (tool.type) {
                    // IRREGULAR TOOLS
                    case ToolsType.Polygon:
                        if (this.existsSurfaceTool(map, tool.attributes.idMapTool)) {
                            this.updateSurface(map, tool);
                        } else {
                            this.drawSurfacePolygon(map, tool);
                        }
                        break;
                    case ToolsType.LineString:
                        if (this.existsSurfaceTool(map, tool.attributes.idMapTool)) {
                            this.updateSurface(map, tool);
                        } else {
                            this.drawSurfacePolyline(map, tool.attributes.points, tool, true);
                        }
                        break;
                    // REGULAR TOOLS
                    case ToolsType.Circle:
                        // const middPoints = this.getLayerToolPoints(map, true);
                        // const layerPoints = this.getLayerToolPoints(map, false);
                        // map.removeLayer(middPoints);
                        // map.removeLayer(layerPoints);
                        // this.removeSquarePoint(map);
                        this.addSquarePointCenter(map, tool.attributes.points[0]);
                        if (this.existsSurfaceTool(map, tool.attributes.idMapTool)) {
                            this.updateSurface(map, tool);
                        } else {
                            this.drawSurfaceCircle(map, tool);
                        }
                        break;
                    case ToolsType.Ellipse:
                        if (this.existsSurfaceTool(map, tool.attributes.idMapTool)) {
                            this.updateSurface(map, tool);
                        } else {
                            this.drawSurfaceEllipse(map, tool);
                        }
                        break;
                    case ToolsType.Rectangle:
                        if (this.existsSurfaceTool(map, tool.attributes.idMapTool)) {
                            this.updateSurface(map, tool);
                        } else {
                            this.drawSurfaceRectangle(map, tool);
                        }
                        break;
                    case ToolsType.Square:
                        if (this.existsSurfaceTool(map, tool.attributes.idMapTool)) {
                            this.updateSurface(map, tool);
                        } else {
                            this.drawSurfacePolygon(map, tool);
                        }
                        break;
                    default:
                        console.error('[map.service.loadTools() - ' + tool.type);
                        break;
                }
            });
        }

        // this.forceRedraw(map);
    }

    processToolEditable(map: WorldWind, tool: ITool) {
        let layerTool = null;
        map.layers.forEach((layer) => {
            if (layer.displayName === environment.constants.map.TOOL_LAYER) {
                let isLayerToolEditable;
                if (!!tool && !!tool.type) {
                    isLayerToolEditable = layer.renderables.find((renderable) => {
                        return (renderable.myTool.attributes.idMapTool === tool.attributes.idMapTool);
                    });
                    if (!isLayerToolEditable) {
                        layer.opacity = environment.map.disabledOpacity;
                    } else {
                        layer.opacity = 1;
                        layerTool = layer;
                    }
                }
            }
        });
        this.forceRedraw(map);

        if (!!layerTool) {
            // TODO: buscar evento en worldwind que se dispare cuando boundariesArePrepared
            // esté a true. Si no se hace así, puede ser que al añadir los puntos de un rectángulo
            // no esté preparado para haber cargado los valores de los boundaries, y falle.
            if (layerTool.renderables[0].boundariesArePrepared !== true) {
                const myTimeout = setTimeout(() => {
                    if (layerTool.renderables[0].boundariesArePrepared === true) {
                        this.addSquarePoints(map, layerTool.renderables[0].myTool, layerTool);
                        clearTimeout(myTimeout);
                    }
                }, 200);
            } else {
                this.addSquarePoints(map, layerTool.renderables[0].myTool, layerTool);
            }

        }

        return layerTool;
    }

    cancelSelectedTool(map: WorldWind, tool: ITool) {
        this.finishSelection(map, tool);
        // Eliminamos las dos capas de puntos (Los puntos originales, y los puntos medios)
        const layerPoints = this.getLayerToolPoints(map, false);
        const layerMiddlePoints = this.getLayerToolPoints(map, true);
        map.removeLayer(layerPoints);
        map.removeLayer(layerMiddlePoints);
        map.layers.forEach((layer) => {
            if (layer.displayName === environment.constants.map.TOOL_LAYER) {
                layer.opacity = 1;
            }
        });
    }

    processTool(map: WorldWind, pickList: any, tool: ITool): ITool {
        const point = pickList.terrainObject().position;
        point.idPoint = new Date().getTime();
        if (this.isFinished(pickList)) {
            return this.finishSelection(map, tool);
        } else {
            switch (tool.type) {
                case ToolsType.Polygon:
                    this.processPolygon(map, point);
                    break;
                case ToolsType.LineString:
                    this.processLine(map, point);
                    break;
                default:
                    console.error('[MAP.draw] - not implemented');
                    break;
            }
        }

        this.forceRedraw(map);
    }

    private getPointFromPickList(pickList): IPointLocation {
        let point: IPointLocation;
        point = {
            latitude: pickList.terrainObject().position.latitude,
            longitude: pickList.terrainObject().position.longitude,
            altitude: pickList.terrainObject().position.altitude
        };
        return point;
    }

    processMarker(pickList: any, markerSelected: IMarker): IMapMarker {
        const point = this.getPointFromPickList(pickList);
        point.altitude = environment.map.markerAltitude;
        const newMapMarker: IMapMarker = {
            ...markerSelected,
            mapObservations: markerSelected.observations,
            mapTitle: markerSelected.title,
            idMapMarker: new Date().getTime(),
            location: point,
            moveToLocation: false
        };
        return newMapMarker;
    }

    private preparePlacemarkLayers(map: WorldWind): WorldWind.RenderableLayer {
        const placemarkLayer = this.getPlacemarkLayer(map);
        if (placemarkLayer) {
            placemarkLayer.opacity = environment.map.disabledOpacity;
        }

        map.layers.forEach((layer, idx) => {
            if (layer.displayName === environment.constants.map.PLACEMARK) {
                map.layers.splice(idx, 1);
            }
        });

        map.layers.forEach((layerMap, idx) => {
            if (layerMap.displayName === environment.constants.map.PLACEMARKSELECTED) {
                map.layers.splice(idx, 1);
            }
        });
        return placemarkLayer;
    }

    processMarkerMapByMarker(map: WorldWind, marker: IMapMarker, zoom: number) {
        const placemark = this.createRenderablePlacemark(marker, zoom);
        const placemarkLayer = this.preparePlacemarkLayers(map);
        const placemarkLayerSelected = new WorldWind.RenderableLayer(environment.constants.map.PLACEMARKSELECTED);
        placemarkLayerSelected.addRenderable(placemark);
        map.addLayer(placemarkLayerSelected);
        map.addLayer(placemarkLayer);
        // this.forceRedraw(map);
        return marker;
    }

    processMarkerMapSelected(map: WorldWind, element: any): IMapMarker {
        // const placemarkLayer = this.preparePlacemarkLayers(map);

        const markerMap: IMapMarker = {
            ...element.userObject.myMarker,
            idMapMarker: element.userObject.myId,
            mapObservations: element.userObject.myMarker.mapObservations,
            mapTitle: element.userObject.myMarker.mapTitle
        };

        const placemarkLayerSelected = new WorldWind.RenderableLayer(environment.constants.map.PLACEMARKSELECTED);
        placemarkLayerSelected.addRenderable(element.userObject);
        map.addLayer(placemarkLayerSelected);
        // map.addLayer(placemarkLayer);
        return markerMap;
    }

    processDeselectMarkerMap(map: WorldWind) {

        const placemarkLayer = this.getPlacemarkLayer(map);

        if (placemarkLayer) {
            placemarkLayer.opacity = 1;
        }
        if (!!map && !!map.layers) {
            map.layers.forEach((layerMap, idx) => {
                if (layerMap.displayName === environment.constants.map.PLACEMARKSELECTED) {
                    map.layers.splice(idx, 1);
                }
            });
        }

    }

    getHorizontalCoord(canvas, modalWidth = 600) {
        const rect = canvas.getBoundingClientRect();
        // El 600 es el ancho por defecto de la pantalla
        return {
            x: ((rect.width - modalWidth) / 2),
            y: 0
        };
    }

    getMousePos(canvas, clientX, clientY) {
        const rect = canvas.getBoundingClientRect();
        return {
            x: (clientX - rect.left) / (rect.right - rect.left) * canvas.width,
            y: (clientY - rect.top) / (rect.bottom - rect.top) * canvas.height
        };
    }

    private toRad = (number) => {
        return number * Math.PI / 180;
    }

    private toDeg = (number) => {
        return number * (180 / Math.PI);
    }

    middlePoint = (originLatitude, originLongitude, targetLatitude, targetLongitude): IPointLocation => {


        const coordsArray = [
            { latitude: originLatitude, longitude: originLongitude },
            { latitude: targetLatitude, longitude: targetLongitude }
        ];

        // let coordsArray = coords;
        // if (typeof coords === 'object' && !(coords instanceof Array)) {
        //     coordsArray = [];

        //     for (const key in coords) {
        //         coordsArray.push(this.coords(coords[key]));
        //     }
        // }

        // if (!coordsArray.length) {
        //     return false;
        // }

        let X = 0.0;
        let Y = 0.0;
        let Z = 0.0;
        let lat, lon, hyp;

        coordsArray.forEach((coord) => {
            // lat = this.latitude(coord).toRad();
            // lon = this.longitude(coord).toRad();
            lat = this.toRad(coord.latitude);
            lon = this.toRad(coord.longitude);
            X += Math.cos(lat) * Math.cos(lon);
            Y += Math.cos(lat) * Math.sin(lon);
            Z += Math.sin(lat);
        });

        const nb_coords = coordsArray.length;
        X = X / nb_coords;
        Y = Y / nb_coords;
        Z = Z / nb_coords;

        lon = Math.atan2(Y, X);
        hyp = Math.sqrt(X * X + Y * Y);
        lat = Math.atan2(Z, hyp);

        return {
            latitude: +(this.toDeg(lat)).toFixed(6),
            longitude: +(this.toDeg(lon)).toFixed(6),
        };
    }


    middlePointSimple = (originLatitude, originLongitude, targetLatitude, targetLongitude): IPointLocation => {

        // -- Longitude difference
        // const dLng = this.toRad((targetLongitude - originLongitude));

        // // -- Convert to radians
        // originLatitude = this.toRad(originLatitude);
        // targetLatitude = this.toRad(targetLatitude);
        // originLongitude = this.toRad(originLongitude);

        // const bX = Math.cos(targetLatitude) * Math.cos(dLng);
        // const bY = Math.cos(targetLatitude) * Math.sin(dLng);
        // const lat3 = Math.atan2(
        //     Math.sin(originLatitude) + Math.sin(targetLatitude),
        //     Math.sqrt((Math.cos(originLatitude) + bX) * (Math.cos(originLatitude) + bX) + bY * bY)
        // );
        // const lng3 = originLongitude + Math.atan2(bY, Math.cos(originLatitude) + bX);

        // // -- Return result
        // // return [this.toDeg(lng3), this.toDeg(lat3)];
        // return {
        //     latitude: this.toDeg(lat3),
        //     longitude: this.toDeg(lng3)
        // };



        return {
            latitude: (originLatitude + targetLatitude) / 2,
            longitude: (originLongitude + targetLongitude) / 2
        };
    }

    private isNumber = (num) => {
        return !isNaN(num) && num !== null && !Array.isArray(num);
    }

    private containsNumber = (coordinates) => {
        if (coordinates.length > 1 && this.isNumber(coordinates[0]) && this.isNumber(coordinates[1])) {
            return true;
        }

        if (Array.isArray(coordinates[0]) && coordinates[0].length) {
            return this.containsNumber(coordinates[0]);
        }
        throw new Error('coordinates must only contain numbers');
    }

    getCoords = (obj) => {
        if (!obj) {
            throw new Error('obj is required');
        }
        let coordinates;

        // Array of numbers
        if (obj.length) {
            coordinates = obj;
            // Geometry Object
        } else if (obj.coordinates) {
            coordinates = obj.coordinates;
            // Feature
        } else if (obj.geometry && obj.geometry.coordinates) {
            coordinates = obj.geometry.coordinates;
        }
        // Checks if coordinates contains a number
        if (obj) {
            // this.containsNumber(coordinates);
            const coord = [
                obj.longitude,
                obj.latitude
            ];

            return coord;
        }
        throw new Error('No valid coordinates');
    }

    sortPointsByLatLng$1 = (arrPoints, flip) => {
        const pointsByLatitude = {};
        // divide points by rows with the same latitude
        for (let i = 0; i < arrPoints.length; i++) {
            // callback(geojson.features[i], i);
            const lat = this.getCoords(arrPoints[i])[1];
            if (!pointsByLatitude[lat]) {
                pointsByLatitude[lat] = [];
            }
            pointsByLatitude[lat].push(arrPoints[i]);
        }
        // sort points (with the same latitude) by longitude
        const orderedRowsByLatitude = Object.keys(pointsByLatitude).map((lat) => {
            const row = pointsByLatitude[lat];
            const rowOrderedByLongitude = row.sort((a, b) => {
                return this.getCoords(a)[0] - this.getCoords(b)[0];
            });
            return rowOrderedByLongitude;
        });

        // sort rows (of points with the same latitude) by latitude
        const pointMatrix = orderedRowsByLatitude.sort((a, b) => {
            if (flip) {
                return this.getCoords(a[0])[1] - this.getCoords(b[0])[1];
            } else {
                return this.getCoords(b[0])[1] - this.getCoords(a[0])[1];
            }
        });
        const result = [];
        pointMatrix.forEach((point) => {
            result.push(point[0]);
        });
        return result;
    }

    // new WorldWind.Position(41.8267, -98.7686, 0),
    // new WorldWind.Position(32.6658, -99.6049, 0),
    // new WorldWind.Position(34.1708, -111.0846, 0),
    // new WorldWind.Position(42.7502, -111.0705, 0),
    // new WorldWind.Position(41.8267, -98.7686, 0)

    getMeasurements(map: WorldWind, surface): IMeasurement {
        if (surface.userObject.myTool) {
            const lengthMeasurer = new WorldWind.LengthMeasurer(map);
            const areaMeasurer = new WorldWind.AreaMeasurer(map);
            // const pathPositions =  surface.userObject.boundaries || surface.userObject._boundaries;
            let pathPositions: WorldWind.Position[] = [];
            switch (true) {
                case (surface.userObject.myTool.type === ToolsType.Ellipse):
                case (surface.userObject.myTool.type === ToolsType.Circle):
                case (surface.userObject.myTool.type === ToolsType.Rectangle):
                case (surface.userObject.myTool.type === ToolsType.Square):
                    pathPositions = surface.userObject.boundaries || surface.userObject._boundaries;
                    break;
                case (surface.userObject.myTool.type === ToolsType.Polygon):
                case (surface.userObject.myTool.type === ToolsType.LineString):
                    surface.userObject.myTool.attributes.points.forEach((point) => {
                        pathPositions.push(new WorldWind.Position(point.latitude, point.longitude, point.altitude));
                    });
                    break;

                default:
                    break;
            }

            // surface.renderables...
            const distance = lengthMeasurer.getLength(pathPositions, false, WorldWind.GREAT_CIRCLE);
            const terrainDistance = lengthMeasurer.getLength(pathPositions, true, WorldWind.GREAT_CIRCLE);
            const geographicDistance = lengthMeasurer.getGeographicDistance(pathPositions, WorldWind.GREAT_CIRCLE);
            const area = areaMeasurer.getArea(pathPositions, false, WorldWind.GREAT_CIRCLE);
            const terrainArea = areaMeasurer.getArea(pathPositions, true, WorldWind.GREAT_CIRCLE);

            // Display the results.
            const result: IMeasurement = {
                geoDist: +(geographicDistance / 1e3).toFixed(3),
                dist: +(distance / 1e3).toFixed(3),
                terrainDist: +(terrainDistance / 1e3).toFixed(3),
                projectedArea: +(area / 1e6).toFixed(3),
                terrainArea: +(terrainArea / 1e6).toFixed(3)
            };
            return result;
        }
    }

    getDistanceBetweenPointsInMeters(map: WorldWind, sourcePoint: WorldWind.Location, targetPoint: WorldWind.Location): number {
        const lengthMeasurer = new WorldWind.LengthMeasurer(map);
        const pathPositions = [sourcePoint, targetPoint];
        const distance = lengthMeasurer.getLength(pathPositions, false, WorldWind.GREAT_CIRCLE);
        return distance;
    }

    createCoord(map, coord, bearing, distance) {
        const
            radius = map.globe.equatorialRadius, // meters
            δ = Number(distance) / radius, // angular distance in radians
            θ = this.toRad(Number(bearing));

        const φ1 = this.toRad(coord[1]),
            λ1 = this.toRad(coord[0]);

        const φ2 = Math.asin(Math.sin(φ1) * Math.cos(δ) +
            Math.cos(φ1) * Math.sin(δ) * Math.cos(θ));

        let λ2 = λ1 + Math.atan2(Math.sin(θ) * Math.sin(δ) * Math.cos(φ1),
            Math.cos(δ) - Math.sin(φ1) * Math.sin(φ2));
        // normalise to -180..+180°
        λ2 = (λ2 + 3 * Math.PI) % (2 * Math.PI) - Math.PI;

        return [this.toDeg(λ2), this.toDeg(φ2)];
    }

    // Operaciones CUADRANTAL
    // private getMillesFromKm(km: number) {
    //     return km * environment.constants.miles;
    // }
    // private round(vaue, decimals) {
    //     return Math.round(vaue * Math.pow(10, decimals)) / Math.pow(10, decimals);
    // }
    // FIN Operaciones CUADRANTAL

    getBoundsRealSquare(map: WorldWind, sourcePosition: WorldWind.Position, targetPosition: WorldWind.Position): WorldWind.Location {
        // const fullBound: IBounds = this.getRectangularBoundsFromPoints(sourcePosition, targetPosition);
        // const squareBase = this.getMillesFromKm(
        // this.getDistanceBetweenPointsInMeters(map, fullBound.bounds[0], fullBound.bounds[2]) / 1e3);
        // const distance = Math.hypot(squareBase, squareBase);
        // const nLat1 = fullBound.bounds[1].latitude;
        // const nLong1 = fullBound.bounds[1].longitude;
        // const direction = fullBound.cuadrantal.angle;
        // const nDifLatM = this.round(distance * Math.cos(this.toRad(direction)), 1);
        // const nDifLat = nDifLatM / 60;
        // const nLat2 = nLat1 + nDifLat;

        // const apartamiento = this.round(distance * Math.sin(this.toRad(direction)), 1);
        // const nPaso = nLat1 + nLat2;
        // const nLatMedia = nPaso / 2;
        // const nDifLongM = this.round(apartamiento / Math.cos(this.toRad(nLatMedia)), 1);
        // const nDifLong = nDifLongM / 60;
        // const nLong2 = nLong1 + nDifLong;

        // const resultPoint: ILocation = {
        //     latitude: nLat2,
        //     longitude: nLong2
        // };

        // const boundResult: WorldWind.Location[] = [];
        // switch (true) {
        //     case (fullBound.cuadrantal.sector === 1):

        //         break;
        //     case (fullBound.cuadrantal.sector === 2):
        //         boundResult.push(fullBound.bounds[0]);
        //         boundResult.push(fullBound.bounds[1]);
        //         boundResult.push(new WorldWind.Location(resultPoint.latitude, fullBound.bounds[2].longitude, 0));
        //         boundResult.push(new WorldWind.Location(resultPoint.latitude, resultPoint.longitude, resultPoint.altitude));
        //         break;
        //     case (fullBound.cuadrantal.sector === 3):

        //         break;
        //     case (fullBound.cuadrantal.sector === 4):

        //         break;
        //     default:
        //         break;
        // }

        const fullBound: IBounds = this.getRectangularBoundsFromPoints(sourcePosition, targetPosition);
        const squareBase = this.getDistanceBetweenPointsInMeters(map, fullBound.bounds[0], fullBound.bounds[2]);
        const equatorialRadius = map.globe.equatorialRadius;
        const θ = this.toDeg(squareBase / equatorialRadius);
        const latThirdPoint = (fullBound.bounds[0].latitude - θ);
        const thirdPoint = new WorldWind.Location(latThirdPoint, fullBound.bounds[0].longitude);
        const secondPoint = new WorldWind.Location(latThirdPoint, fullBound.bounds[1].longitude);
        const boundResult: WorldWind.Location[] = [];
        boundResult.push(fullBound.bounds[0]);
        boundResult.push(fullBound.bounds[1]);
        boundResult.push(secondPoint);
        boundResult.push(thirdPoint);
        return boundResult;
    }

    getRectangularBoundsFromPoints(sourcePosition: WorldWind.Position, targetPosition: WorldWind.Position): IBounds {

        let result: IBounds;
        // Generamos los 4 puntos en base al primer y último click.
        const bounds = [];
        // Generamos el cuadrantal correcto
        const cuadrantal: ICuadrantal = {
            axisLatitude: (sourcePosition.latitude < targetPosition.latitude) ? EnumCoordinateLatitude.N : EnumCoordinateLatitude.S,
            axisLongitude: (sourcePosition.longitude < targetPosition.longitude) ? EnumCoordinateLongitude.E : EnumCoordinateLongitude.W
        };

        if (cuadrantal.axisLatitude === EnumCoordinateLatitude.N) {
            if (cuadrantal.axisLongitude === EnumCoordinateLongitude.E) {
                cuadrantal.angle = EnumCuadrantalAngles.first;
                cuadrantal.sector = 1;
                bounds.push(new WorldWind.Position(targetPosition.latitude, sourcePosition.longitude, 0));
                bounds.push(new WorldWind.Position(targetPosition.latitude, targetPosition.longitude, 0));
                bounds.push(new WorldWind.Position(sourcePosition.latitude, targetPosition.longitude, 0));
                bounds.push(new WorldWind.Position(sourcePosition.latitude, sourcePosition.longitude, sourcePosition.altitude || 0));
            } else {
                cuadrantal.angle = EnumCuadrantalAngles.fourth;
                cuadrantal.sector = 4;
                bounds.push(new WorldWind.Position(targetPosition.latitude, targetPosition.longitude, targetPosition.altitude || 0));
                bounds.push(new WorldWind.Position(targetPosition.latitude, sourcePosition.longitude, 0));
                bounds.push(new WorldWind.Position(sourcePosition.latitude, sourcePosition.longitude, sourcePosition.altitude || 0));
                bounds.push(new WorldWind.Position(sourcePosition.latitude, targetPosition.longitude, 0));
            }
        } else {
            if (cuadrantal.axisLongitude === EnumCoordinateLongitude.E) {
                cuadrantal.angle = EnumCuadrantalAngles.second;
                cuadrantal.sector = 2;
                bounds.push(new WorldWind.Position(sourcePosition.latitude, sourcePosition.longitude, sourcePosition.altitude || 0));
                bounds.push(new WorldWind.Position(sourcePosition.latitude, targetPosition.longitude, 0));
                bounds.push(new WorldWind.Position(targetPosition.latitude, targetPosition.longitude, targetPosition.altitude || 0));
                bounds.push(new WorldWind.Position(targetPosition.latitude, sourcePosition.longitude, 0));
            } else {
                cuadrantal.angle = EnumCuadrantalAngles.third;
                cuadrantal.sector = 3;
                bounds.push(new WorldWind.Position(sourcePosition.latitude, targetPosition.longitude, 0));
                bounds.push(new WorldWind.Position(sourcePosition.latitude, sourcePosition.longitude, sourcePosition.altitude || 0));
                bounds.push(new WorldWind.Position(targetPosition.latitude, sourcePosition.longitude, 0));
                bounds.push(new WorldWind.Position(targetPosition.latitude, targetPosition.longitude, targetPosition.altitude || 0));
            }
        }
        result = {
            bounds: bounds,
            cuadrantal: cuadrantal
        };
        return result;
    }

    // getSizeFromPoints(map: WorldWind, sourcePosition: WorldWind.Position, targetPosition: WorldWind.Position): ISize {
    //     // Points ordered in a clockwise direction
    //     const bounds = this.getBoundsFromPoints(sourcePosition, targetPosition);
    //     console.log(bounds);
    //     const size: ISize = {
    //         width: this.getDistanceBetweenPointsInMeters(map, bounds[0], bounds[1]),
    //         height: this.getDistanceBetweenPointsInMeters(map, bounds[0], bounds[3])
    //     };
    //     return size;
    // }

    // createRegularTool(map: WorldWind, points: ILocation[], type: ToolsType) {
    //     const toolRegular = new WorldWind.RenderableLayer(environment.constants.map.TOOL_LAYER);
    //     const idMapTool = new Date().getTime();
    //     const newTool: ITool = {
    //         type: type,
    //         attributes: {
    //             idMapTool: idMapTool,
    //             color: environment.tools.attributes.color,
    //             observations: null,
    //             title: this.translateService.instant('tools.types.' + type),
    //             transparency: environment.tools.attributes.transparency,
    //             width: environment.tools.attributes.width,
    //         }
    //     };
    //     const attr = this.createAttributes(newTool.attributes);
    //     const polygon = new WorldWind.SurfacePolygon(points, attr);
    //     polygon.myTool = newTool;
    //     toolRegular.addRenderable(polygon);
    //     map.addLayer(toolRegular);
    //     let toolRegularLayer;
    //     map.layers.forEach((layer) => {
    //         if (layer.displayName === environment.constants.map.TOOL_LAYER) {
    //             layer.renderables.forEach((renderable) => {
    //                 if (renderable.myTool.attributes.idMapTool === idMapTool) {
    //                     toolRegularLayer = layer;
    //                 }
    //                 console.log(renderable);
    //             });

    //         }
    //     });
    //     return toolRegularLayer;
    // }

    createLayerCircle(map: WorldWind, center: WorldWind.Location, radius: number) {
        const toolFakeCircle = new WorldWind.RenderableLayer(environment.constants.map.LAYER_CIRCLE);
        const fakeTool: ITool = {
            type: ToolsType.Circle,
            attributes: {
                color: environment.tools.attributes.color,
                observations: null,
                title: this.translateService.instant('tools.types.' + ToolsType.Circle),
                transparency: environment.tools.attributes.transparency,
                width: environment.tools.attributes.width,
                radius: 0,
                points: [center]
            }
        };
        const attr = this.createAttributes(fakeTool.attributes);
        const cirlce = new WorldWind.SurfaceCircle(center, radius, attr);
        cirlce.myCircle = fakeTool;
        toolFakeCircle.addRenderable(cirlce);
        map.addLayer(toolFakeCircle);
        let toolCircleLayer;
        map.layers.forEach((layerMap) => {
            if (layerMap.displayName === environment.constants.map.LAYER_CIRCLE) {
                toolCircleLayer = layerMap;
            }
        });
        return toolCircleLayer;
    }


    createLayerEllipse(map: WorldWind, point: WorldWind.Location) {
        const toolFakeEllipse = new WorldWind.RenderableLayer(environment.constants.map.LAYER_ELLIPSE);
        const fakeTool: ITool = {
            type: ToolsType.Ellipse,
            attributes: {
                color: environment.tools.attributes.color,
                observations: null,
                title: this.translateService.instant('tools.types.' + ToolsType.Ellipse),
                transparency: environment.tools.attributes.transparency,
                width: environment.tools.attributes.width,
                radius: 0,
                points: [point]
            }
        };
        const attr = this.createAttributes(fakeTool.attributes);
        const ellipse = new WorldWind.SurfaceEllipse(point, 1, 1, 0, attr);
        ellipse.myEllipse = fakeTool;
        toolFakeEllipse.addRenderable(ellipse);
        map.addLayer(toolFakeEllipse);
        let toolEllipseLayer;
        map.layers.forEach((layerMap) => {
            if (layerMap.displayName === environment.constants.map.LAYER_ELLIPSE) {
                toolEllipseLayer = layerMap;
            }
        });
        return toolEllipseLayer;
    }

    createLayerSquare(map: WorldWind, points: ILocation[]) {
        // const toolFakeSquare = new WorldWind.RenderableLayer(environment.constants.map.FAKE_SQUARE);
        // const fakeTool: ITool = {
        //     type: ToolsType.Square,
        //     attributes: {
        //         color: environment.tools.attributes.color,
        //         observations: null,
        //         title: this.translateService.instant('tools.types.' + ToolsType.Square),
        //         transparency: environment.tools.attributes.transparency,
        //         width: environment.tools.attributes.width,
        //         points: [center],
        //         squareCenter: center,
        //         squareSideSize: 1
        //     }
        // };
        // const attr = this.createAttributes(fakeTool.attributes);
        // const square = new WorldWind.SurfaceRectangle(center, 1, 1, 0, attr);
        // square.mySquare = fakeTool;
        // toolFakeSquare.addRenderable(square);
        // map.addLayer(toolFakeSquare);
        // let toolSquareLayer;
        // map.layers.forEach((layerMap) => {
        //     if (layerMap.displayName === environment.constants.map.FAKE_SQUARE) {
        //         toolSquareLayer = layerMap;
        //     }
        // });
        // return toolSquareLayer;
        const toolSquare = new WorldWind.RenderableLayer(environment.constants.map.LAYER_SQUARE);
        const fakeTool: ITool = {
            type: ToolsType.Square,
            attributes: {
                color: environment.tools.attributes.color,
                observations: null,
                title: this.translateService.instant('tools.types.Square'),
                transparency: environment.tools.attributes.transparency,
                width: environment.tools.attributes.width,
                points: points
            }
        };
        const attr = this.createAttributes(fakeTool.attributes);
        const polygon = new WorldWind.SurfacePolygon(points, attr);
        polygon.mySquare = fakeTool;
        toolSquare.addRenderable(polygon);
        map.addLayer(toolSquare);
        let toolSquareLayer;
        map.layers.forEach((layerMap) => {
            if (layerMap.displayName === environment.constants.map.LAYER_SQUARE) {
                toolSquareLayer = layerMap;
            }
        });
        return toolSquareLayer;
    }

    createLayerRectangle(map: WorldWind, points: ILocation[], isRealRectangle: boolean) {
        const toolFakeRectangle = new WorldWind.RenderableLayer(environment.constants.map.FAKE_RECTANGLE);
        const fakeTool: ITool = {
            type: ToolsType.Rectangle,
            attributes: {
                color: environment.tools.attributes.color,
                observations: null,
                title: this.translateService.instant('tools.types.Rectangle'),
                transparency: (isRealRectangle !== true) ? .3 : environment.tools.attributes.transparency,
                width: environment.tools.attributes.width,
            }
        };
        const attr = this.createAttributes(fakeTool.attributes);
        // const polygon = new WorldWind.SurfacePolygon(points, attr);
        const polygon = new WorldWind.SurfaceRectangle(
            new WorldWind.Location(points[0].latitude, points[0].longitude, points[0].altitude),
            1,
            1,
            0,
            attr
        );
        polygon.myRectangle = fakeTool;
        toolFakeRectangle.addRenderable(polygon);
        map.addLayer(toolFakeRectangle);
        let toolRectangleLayer;
        map.layers.forEach((layerMap) => {
            if (layerMap.displayName === environment.constants.map.FAKE_RECTANGLE) {
                toolRectangleLayer = layerMap;
            }
        });
        return toolRectangleLayer;
    }

    transformSquareToTool(layerSquare): ITool {
        layerSquare.displayName = environment.constants.map.TOOL_LAYER;
        layerSquare.renderables[0].myTool = layerSquare.renderables[0].mySquare;
        layerSquare.renderables[0].mySquare = undefined;
        const newTool: ITool = layerSquare.renderables[0].myTool;
        newTool.attributes.idMapTool = new Date().getTime();
        newTool.attributes.points.map((point, idx) => {
            point = new WorldWind.Location(point.latitude, point.longitude, (point.altitude || 0));
            point.idPoint = new Date().getTime() + idx;
        });
        return newTool;
    }

    transformRectangleToTool(layerFakeRectangle): ITool {
        if (layerFakeRectangle.renderables[0].myRectangle.attributes.points) {
            layerFakeRectangle.displayName = environment.constants.map.TOOL_LAYER;
            layerFakeRectangle.renderables[0].myTool = layerFakeRectangle.renderables[0].myRectangle;
            layerFakeRectangle.renderables[0].myRectangle = undefined;
            const newTool: ITool = layerFakeRectangle.renderables[0].myTool;
            newTool.attributes.transparency = environment.tools.attributes.transparency;
            newTool.attributes.idMapTool = new Date().getTime();
            newTool.attributes.points.map((point, idx) => {
                point.idPoint = new Date().getTime() + idx;
            });
            return newTool;
        }
    }

    transformCircleToTool(layerCircle): ITool {
        // if ( layerCircle.renderables[0].myCircle.attributes.points){

        // }
        layerCircle.displayName = environment.constants.map.TOOL_LAYER;
        layerCircle.renderables[0].myTool = layerCircle.renderables[0].myCircle;
        layerCircle.renderables[0].myCircle = undefined;
        const circleTool: ITool = layerCircle.renderables[0].myTool;
        circleTool.attributes.idMapTool = new Date().getTime();
        circleTool.attributes.points.map((point, idx) => {
            point.idPoint = new Date().getTime() + idx;
        });
        return circleTool;
    }

    transformEllipseToTool(layerEllipse): ITool {
        layerEllipse.displayName = environment.constants.map.TOOL_LAYER;
        layerEllipse.renderables[0].myTool = layerEllipse.renderables[0].myEllipse;
        layerEllipse.renderables[0].myEllipse = undefined;
        const ellipseTool: ITool = layerEllipse.renderables[0].myTool;
        ellipseTool.attributes.idMapTool = new Date().getTime();
        // circleTool.attributes.points.map((point, idx) => {
        //     point.idPoint = new Date().getTime() + idx;
        // });
        return ellipseTool;
    }

    updateCircle(map, layerCircle, attributes: IAttributes) {
        // const attr = this.createAttributes(attributes);
        // Cuando lo estamos creando, trabajamos con myCircle,
        // pero luego es myTool, como el resto de heramientas
        const targetAttr = !!layerCircle.renderables[0].myCircle ? layerCircle.renderables[0].myCircle : layerCircle.renderables[0].myTool;
        targetAttr.attributes.radius = attributes.radius;
        targetAttr.attributes.points = [attributes.points[0]];
        layerCircle.renderables[0].radius = attributes.radius;
        layerCircle.renderables[0].center = attributes.points[0];

        // this.forceRedraw(map);
    }

    // updateSquare(map: WorldWind, layerSquare, attributes: IAttributes, center: ILocation) {
    updateSquare(map: WorldWind, layerSquare, attributes: IAttributes) {
        const objSquare = layerSquare.renderables[0].mySquare || layerSquare.renderables[0].myTool;
        objSquare.attributes.points = attributes.points;
        layerSquare.renderables[0].boundaries = attributes.points;
        // layerSquare.renderables[0].mySquare.attributes.squareCenter = center;
        // layerSquare.renderables[0].mySquare.attributes.width = attributes.squareSideSize;
        // layerSquare.renderables[0].mySquare.attributes.height = attributes.squareSideSize;

        // layerSquare.renderables[0].center = center;
        // layerSquare.renderables[0].width = attributes.squareSideSize;
        // layerSquare.renderables[0].height = attributes.squareSideSize;
        // this.forceRedraw(map);
    }

    // updateFakeRectangle(map, layerFakeRectangle, points: ILocation[]) {
    //     layerFakeRectangle.renderables[0].myRectangle.attributes.points = points;
    //     layerFakeRectangle.renderables[0].boundaries = points;
    //     this.forceRedraw(map);
    // }

    updateRectangle(
        layerRectangle,
        center: WorldWind.Location,
        widht: number,
        height: number,
        rotate: number
    ) {
        const objToUpdate = layerRectangle.renderables[0].myRectangle || layerRectangle.renderables[0].myTool;
        objToUpdate.attributes.center = center;
        objToUpdate.attributes.points = [center];
        objToUpdate.attributes.majorRadius = widht;
        objToUpdate.attributes.minorRadius = height;
        objToUpdate.attributes.rotate = rotate;

        layerRectangle.renderables[0].center = center;
        layerRectangle.renderables[0].width = widht;
        layerRectangle.renderables[0].height = height;
        layerRectangle.renderables[0].heading = rotate;
        // this.forceRedraw(map);
    }

    updateEllipse(
        map: WorldWind,
        layerEllipse,
        center: WorldWind.Location,
        majorRadius: number,
        minorRadius: number,
        rotate: number
    ) {
        layerEllipse.renderables[0].myEllipse.attributes.center = center;
        layerEllipse.renderables[0].myEllipse.attributes.points = [center];
        layerEllipse.renderables[0].myEllipse.attributes.majorRadius = majorRadius;
        layerEllipse.renderables[0].myEllipse.attributes.minorRadius = minorRadius;
        layerEllipse.renderables[0].myEllipse.attributes.rotate = rotate;

        layerEllipse.renderables[0].center = center;
        layerEllipse.renderables[0].majorRadius = majorRadius;
        layerEllipse.renderables[0].minorRadius = minorRadius;
        layerEllipse.renderables[0].heading = rotate;
        // this.forceRedraw(map);
    }

    updateLayerRegular(map: WorldWind, layerRegular, points: ILocation[]) {
        layerRegular.renderables[0].myTool.attributes.points = points;
        layerRegular.renderables[0].boundaries = points;
    }

    private getZoomScale(actualZoom: number) {
        const levels = environment.map.zoomRange;
        const range = levels.find((level) => {
            return (level.zoom > actualZoom);
        });
        return !!range ? range.size : levels[levels.length - 1].size;
    }

    updateMarkerSize(map: WorldWind, zoom: number) {
        map.layers.forEach((layerMap) => {
            if (
                (layerMap.displayName === environment.constants.map.PLACEMARK)
                || (layerMap.displayName === environment.constants.map.PLACEMARKSELECTED)
                || (layerMap.displayName === environment.constants.map.LAYER_MEDIA)
            ) {
                // placemarkRemoved.push(layerMap);
                // console.log(layerMap.renderables);
                const imageScale = this.getZoomScale(zoom);
                // console.log(`cambiamos el atributo imageScale a: ${imageScale}`);
                layerMap.renderables.map((layer) => {
                    layer.attributes.imageScale = imageScale;
                });
                // layerMap.renderables[0].attributes.imageScale = imageScale;
            }
        });
        this.forceRedraw(map);
    }

    private shapeConfCallback = function (attributes, record) {
        const configuration: any = {};
        configuration.name = attributes.values.name || attributes.values.Name || attributes.values.NAME;
        configuration.attributes = new WorldWind.ShapeAttributes(null);
        // Fill the polygon with a random pastel color.
        configuration.attributes.interiorColor = new WorldWind.Color(
            0.375 + 0.5 * Math.random(),
            0.375 + 0.5 * Math.random(),
            0.375 + 0.5 * Math.random(),
            environment.map.transparencyTimeZoneLayer);   // We must draw the interior to enable picking, but make it transparent

        // Disabled drawing the outlines to improve rendering/picking performance
        configuration.attributes.drawOutline = false;
        // Make the DBaseRecord and Layer available to picked objects
        configuration.userProperties = { record: attributes, layer: record.shapefile.layer };

        return configuration;
    };

    addShapeFileLayer(map: WorldWind, shapeFileUrl: WorldWind.Shapefile) {
        const layer = new WorldWind.RenderableLayer(environment.constants.map.LAYER_TIME_ZONE);
        const shapefile = new WorldWind.Shapefile(shapeFileUrl);
        shapefile.load(null, this.shapeConfCallback, layer);
        map.addLayer(layer);
    }

    removeLayerTimeZoneLayer(map: WorldWind) {
        const timeZoneLayer = map.layers.filter((layer) => {
            return (layer.displayName === environment.constants.map.LAYER_TIME_ZONE);
        })[0];
        if (timeZoneLayer) {
            map.removeLayer(timeZoneLayer);
        }
    }

    setTimeDiff(map: WorldWind, timeDiff: number): number {
        let newTime: number;
        map.layers.forEach((layer) => {
            if (layer instanceof WorldWind.WmsTimeDimensionedLayer) {
                if (timeDiff === 0) {
                    layer.time = null;
                    map.actualTime = null;
                } else {
                    const newTimeDiff = moment(layer.time || new Date()).add(timeDiff, 'h');
                    layer.time = new Date(+newTimeDiff.format('x'));
                    map.actualTime = +newTimeDiff.format('x');
                }
                newTime = layer.time;
            }
        });
        return newTime;
    }

    handleUIEvent(event) {
        let eventType,
            clientX,
            clientY;
        if (event.type.indexOf('pointer') !== -1) {
            eventType = event.pointerType; // possible values are 'mouse', 'pen' and 'touch'
        } else if (event.type.indexOf('mouse') !== -1) {
            eventType = 'mouse';
        } else if (event.type.indexOf('touch') !== -1) {
            eventType = 'touch';
        }

        if (event.type.indexOf('leave') !== -1) {
            clientX = null; // clear the event coordinates when a pointer leaves the canvas
            clientY = null;
        } else {
            clientX = event.clientX;
            clientY = event.clientY;
        }
        return {
            eventType: eventType,
            clientX: clientX,
            clientY: clientY,
        };
        // this.wwd.redraw();
    }

    processTimeLayer(map: WorldWind, event: any) {
        const obj = this.handleUIEvent(event);
        let pickPoint;

        const canvasCenter = new WorldWind.Vec2(map.canvas.width / 2, map.canvas.height / 2),
            canvasMouseOver = map.canvasCoordinates(obj.clientX, obj.clientY);

        let time,
            pickList;

        if ((obj.eventType === 'mouse' || obj.eventType === 'pen') && obj.clientX && obj.clientY) {
            // pickPoint = map.canvasCoordinates(obj.clientX, obj.clientY);
            pickPoint = canvasMouseOver;
            if (pickPoint[0] >= 0 && pickPoint[0] < map.canvas.width &&
                pickPoint[1] >= 0 && pickPoint[1] < map.canvas.height) {
                pickList = map.pick(canvasMouseOver);
            }
        } else if (obj.eventType === 'touch') {
            pickPoint = canvasCenter;
            pickList = map.pick(canvasCenter);
        }

        let i = 0,
            len = 0,
            pickedObject,
            userObject,
            record;

        if (!!pickList && pickList.hasNonTerrainObjects()) {
            for (i = 0, len = pickList.objects.length; i < len; i++) {
                pickedObject = pickList.objects[i];
                if (pickedObject.isTerrain) {
                    continue;
                }
                userObject = pickedObject.userObject;
                if (userObject.userProperties) {
                    record = userObject.userProperties.record;
                    if (record) {
                        const timeModified = map.actualTime;
                        map.actualTz = record.values.zone;
                        time = this.getFormatTime(timeModified, map.actualTz);
                        break;
                    }
                }
            }
        }

        return time ? time : null;
    }

    getFormatTime(timeModified: number, tz: number): string {
        let tzDiff;
        let timeToShow;
        if (timeModified) {
            tzDiff = tz * 60 + new Date(timeModified).stdTimezoneOffset();
            timeToShow = new Date(timeModified).getTime();
        } else {
            tzDiff = tz * 60 + new Date().stdTimezoneOffset();
            timeToShow = new Date().getTime();
        }
        return new Date(timeToShow + tzDiff * 60 * 1000).toLocaleString();
    }

}
