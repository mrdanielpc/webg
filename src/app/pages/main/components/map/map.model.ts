export interface ILocation {
    latitude: number;
    longitude: number;
    altitude?: number;
}

export interface IPointLocation extends ILocation {
    idPoint?: number;
}

export interface IMapTool {
    name: string;
    selected: boolean;
}

export interface IMeasurement {
    geoDist: number;
    dist: number;
    terrainDist: number;
    projectedArea: number;
    terrainArea: number;
}

export interface ISize {
    width: number;
    height: number;
}

export enum EnumCoordinateLatitude {
    N = 'N',
    S = 'S'
}

export enum EnumCoordinateLongitude {
    W = 'W',
    E = 'E'
}

enum EnumSectorCuadrantal {
    first = 1,
    second = 2,
    third = 3,
    fourth = 4
}

export interface IBounds {
    bounds: IPointLocation[];
    cuadrantal: ICuadrantal;
}

export interface ICuadrantal {
    // originalDirection: number;
    angle?: EnumCuadrantalAngles;
    sector?: EnumSectorCuadrantal;
    axisLatitude: EnumCoordinateLatitude;
    axisLongitude: EnumCoordinateLongitude;
}

export enum EnumCuadrantalAngles {
    first = 45,
    second = 225,
    third = 135,
    fourth = 315
}

export enum EnumMapState {
    NONE = 0,
    SELECT_TOOL_MARKER = 1,
    ADD_TOOL_IRREGULAR_MARKER = 2,
    ADD_TOOL_REGULAR = 3,
    MOVE_TOOL_MARKER = 4
}

export enum EnumMapOwner {
    MINE = 0,
    OTHER = 1
}

export enum EnumProjection {
    '3D' = '3D',
    EQUIRECTANGULAR = 'Equirectangular',
    MERCATOR = 'Mercator',
    NORTH_POLAR = 'North Polar',
    SOUTH_POLAR = 'South Polar',
    NORTH_UPS = 'North UPS',
    SOUTH_UPS = 'South UPS',
    NORTH_GNOMONIC = 'North Gnomonic',
    SOUTH_GNOMONIC = 'South Gnomonic',
}

export enum EnumBaseLayer {
    'BMNGLayer' = 'Blue Marble',
    'BMNGLandsatLayer' = 'Blue Marble & Landsat',
    // 'BingAerialLayer' = 'Bing Aerial',
    // 'BingAerialWithLabelsLayer' = 'Bing Aerial with Labels',
    // 'BingRoadsLayer' = 'Bing Roads',
    'OpenStreetMapImageLayer' = 'Open Street Map',
    'WebgisAerialLayer' = 'Aerial',
    'WebgisAerialWithLabelsLayer' = 'Aerial with Labels',
    'WebgisRoadsLayer' = 'Roads',
}

export interface IMApCreateParams {
    center?: ILocation;
    zoom?: number;
    heading?: number;
    tilt?: number;
    projection?: EnumProjection;
    baseLayer?: EnumBaseLayer;
    userToken?: string | null;
    elevationModel?: {
        url: string | null;
        layer: string | null;
        version: string | null;
    };
}

export interface ITime {
    time: string;
}
