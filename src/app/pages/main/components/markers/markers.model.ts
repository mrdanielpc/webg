export interface IPosition {
    latitude: number;
    longitude: number;
    altitude?: number;
}

export interface IMapCoordinates {
    x: number;
    y: number;
}

export interface IMarker {
    id: number;
    iconUrl: string;
    observations: string;
    title: string;
    offsetX: number;
    offsetY: number;
}

export interface IMapMarker extends IMarker {
    idMapMarker: number;
    mapTitle: string;
    mapObservations: string;
    location: IPosition;
    moveToLocation: boolean;
}

export interface IMarkerDialog {
    marker: IMapMarker;
    mapCoordinates: IMapCoordinates;
}
