import { Component, OnInit, Input, Output, EventEmitter, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil, filter } from 'rxjs/operators';
import { environment } from '@environment/environment';
import { IMapMarker, IMarker } from './markers.model';
@Component({
    selector: 'app-markers',
    templateUrl: './markers.component.html',
    styleUrls: ['./markers.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MarkersComponent implements OnInit, OnDestroy {

    @Input() markersTypes: Subject<IMarker[]>;
    @Input() markersTypeSelected: Subject<IMarker>;
    @Input() markers: BehaviorSubject<IMapMarker[]>;
    @Input() markerMapEditable: Subject<IMapMarker>;

    @Output() readonly setMarkerType = new EventEmitter<IMarker>();
    @Output() readonly setMarkerEdit = new EventEmitter<IMapMarker>();
    @Output() readonly setMarkerRemove = new EventEmitter<IMapMarker>();
    @Output() readonly setMarkerEditable = new EventEmitter<IMapMarker>();

    markersList: IMarker[] = [];
    markersAdded: IMapMarker[] = [];

    selectedMarkerType: IMarker;
    markerEditable: IMapMarker;

    searchChangeObserver: any;

    listOpened = true;
    selectedListOpened = false;

    markerMapHovered: number;

    attrTextChangeObserver: any;
    private unsubscribe$ = new Subject<void>();
    searchDone = false;

    constructor(
        private cd: ChangeDetectorRef,
    ) {
    }

    ngOnInit() {
        this.markersTypes
            .pipe(takeUntil(this.unsubscribe$))
            .pipe((filter((data) => {
                return !!data;
            })))
            .subscribe((markersList: IMapMarker[]) => {
                this.markersList = markersList;
                this.searchDone = true;
                this.cd.markForCheck();
            });
        this.markersTypeSelected
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((marker: IMarker) => {
                this.selectedMarkerType = marker;
                this.cd.markForCheck();
            });
        this.markerMapEditable
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((markerMapSelected: IMapMarker) => {
                this.markerEditable = markerMapSelected;
                this.cd.markForCheck();
            });
        this.markers
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((markersAdded: IMapMarker[]) => {
                this.markersAdded = markersAdded;
                this.cd.markForCheck();
            });
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        if (!!this.searchChangeObserver) {
            this.searchChangeObserver.unsubscribe();
        }
        if (!!this.attrTextChangeObserver) {
            this.attrTextChangeObserver.unsubscribe();
        }
    }

    trackById(index, item) {
        return item.id;
    }

    trackByIdMapMarker(index, item) {
        return item.idMapMarker;
    }

    toggleList() {
        this.listOpened = !this.listOpened;
    }

    toggleSelected() {
        this.selectedListOpened = !this.selectedListOpened;
    }

    selectMarker(marker: IMarker) {
        if (
            ((!!marker && !!marker.id) &&
                ((!this.selectedMarkerType) || (!!this.selectedMarkerType && this.selectedMarkerType.id !== marker.id)))
        ) {
            this.selectedMarkerType = marker;
        } else {
            this.selectedMarkerType = null;
        }
        this.setMarkerType.emit(this.selectedMarkerType);
    }

    private moveMarker() {
        this.setMarkerEdit.emit(this.markerEditable);
    }


    editMarker() {
        this.setMarkerEdit.emit(this.markerEditable);
    }

    delMarker(marker: IMapMarker) {
        if (!!marker) {
            // Si eliminamos el marker que está seleccionado, lo desmarcamos para que se oculte el panel de atributos
            if (!!this.markerEditable && !!marker && this.markerEditable.idMapMarker === marker.idMapMarker) {
                this.setMarkerEditable.emit(null);
            }
            this.setMarkerRemove.emit(marker);

        } else {
            this.setMarkerRemove.emit(this.markerEditable);
            this.setMarkerEditable.emit(null);
        }

    }

    changeMArkerAttribute(attr: string, value: any) {
        this.markerEditable[attr] = value;
        this.editMarker();
    }

    changeMarkerAttributeText(attr: string, value: any) {
        if (!this.attrTextChangeObserver) {
            new Observable((observer) => {
                this.attrTextChangeObserver = observer;
            }).pipe(debounceTime(environment.debounceModel)) // wait 300ms after the last event before emitting last event
                .pipe(distinctUntilChanged()) // only emit if value is different from previous value
                .subscribe(() => {
                    this.changeMArkerAttribute(attr, this.markerEditable[attr]);
                });
        }
        this.attrTextChangeObserver.next(value);
    }

    onPositionChange(searchValue: string) {

        if (!this.searchChangeObserver) {
            new Observable((observer) => {
                this.searchChangeObserver = observer;
            }).pipe(debounceTime(environment.debounceModel)) // wait 300ms after the last event before emitting last event
                .pipe(distinctUntilChanged()) // only emit if value is different from previous value
                .subscribe(() => {
                    this.moveMarker();
                });
        }
        this.searchChangeObserver.next(searchValue);
    }

    goToMarker(marker: IMapMarker) {
        if ((!!this.markerEditable && this.markerEditable.idMapMarker !== marker.idMapMarker)
            || (!this.markerEditable)) {
            this.setMarkerEditable.emit(marker);
        }
    }

    showActionMarker(marker: IMapMarker) {
        this.markerMapHovered = marker.idMapMarker;
    }

    hideActionMarker() {
        this.markerMapHovered = null;
    }

}
