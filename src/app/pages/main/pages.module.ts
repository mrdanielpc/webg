import { NgModule } from '@angular/core';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { MatTreeModule } from '@angular/material/tree';
import { MatSliderModule } from '@angular/material/slider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatSortModule } from '@angular/material/sort';
import { DragDropModule } from '@angular/cdk/drag-drop';

import { ComponentsModule } from '@app/components/components.module';
import { MainComponent } from '@app/pages/main/main/main.component';
import { PagesRoutingModule } from './pages.routes';
import { LayersComponent } from './components/layers/layers.component';
import { ToolsComponent } from './components/tools/tools.component';
import { MapComponent } from './components/map/map.component';
import { MarkersComponent } from './components/markers/markers.component';
import { MarkerDialogComponent } from './components/marker-dialog/marker-dialog.component';
import { ToolsDialogComponent } from './components/tools-dialog/tools-dialog.component';
import { MapLoadComponent } from './components/map-manager/map-load/map-load.component';
import { MapPublishComponent } from './components/map-manager/map-publish/map-publish.component';
import { MapSaveComponent } from './components/map-manager/map-save/map-save.component';
import { MediaComponent } from './components/media/media.component';
import { MediaDialogComponent } from './components/media-dialog/media-dialog.component';
import { FeatureinfoDialogComponent } from './components/featureinfo-dialog/featureinfo-dialog.component';
import { MapPublishLayerComponent } from './components/map-manager/map-publish-layer/map-publish-layer.component';
import { TimeManagementComponent } from './components/map/time-management/time-management.component';
import { WgLightboxComponent } from './components/wg-lightbox/wg-lightbox.component';

const MATERIAL_PAGES_MODULES = [
    MatTooltipModule,
    MatListModule,
    MatIconModule,
    MatDividerModule,
    MatTreeModule,
    MatSliderModule,
    MatFormFieldModule,
    MatCardModule,
    MatDialogModule,
    MatSelectModule,
    MatTableModule,
    MatCheckboxModule,
    MatInputModule,
    MatButtonModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatSortModule,
];

@NgModule({
    imports: [
        PagesRoutingModule,
        ComponentsModule,
        MATERIAL_PAGES_MODULES,
        DragDropModule,
    ],
    exports: [
        ComponentsModule,
        MATERIAL_PAGES_MODULES,
        DragDropModule,
    ],
    declarations: [
        LayersComponent,
        ToolsComponent,
        MapComponent,
        MarkersComponent,
        MarkerDialogComponent,
        ToolsDialogComponent,
        MainComponent,
        MapLoadComponent,
        MapPublishComponent,
        MapSaveComponent,
        MediaComponent,
        MediaDialogComponent,
        FeatureinfoDialogComponent,
        MapPublishLayerComponent,
        TimeManagementComponent,
        WgLightboxComponent,
    ],
    entryComponents: [
        LayersComponent,
        ToolsComponent,
        MapComponent,
        MarkersComponent,
        MarkerDialogComponent,
        ToolsDialogComponent,
        MainComponent,
        MapLoadComponent,
        MapPublishComponent,
        MediaComponent,
        MapSaveComponent,
        MapPublishLayerComponent,
        FeatureinfoDialogComponent,
        TimeManagementComponent,
        WgLightboxComponent,
    ],
    providers: [

    ]
})

export class PagesModule { }

