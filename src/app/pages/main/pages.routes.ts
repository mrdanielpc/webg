import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from '@app/pages/main/main/main.component';

const routes: Routes = [
    {
        path: '',
        component: MainComponent,
        data: {
            showNavbarMenu: true,
            showNavbarSelectIcon: true,
            showNavbarSelectBaseLayerIcon: true,
            showSelectSearchCity: true,
            pageTitle: 'main.index'
        }
    },
    {
        path: 'map/:id',
        component: MainComponent,
        data: {
            showNavbarMenu: true,
            showNavbarSelectIcon: true,
            showNavbarSelectBaseLayerIcon: true,
            showSelectSearchCity: true,
            pageTitle: 'main.index'
        }
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes),
    ],
    exports: [
        RouterModule,
    ]
})
export class PagesRoutingModule { }
