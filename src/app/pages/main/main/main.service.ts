import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as WorldWind from 'src/vendor/worldwind.js';
import { FeatureCollection } from 'geojson';
import { filter, tap, map } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import {
    MatSnackBar,
    MatSnackBarVerticalPosition,
    MatSnackBarHorizontalPosition
} from '@angular/material/snack-bar';

import { environment } from '@environment/environment';

import { ApiEnvironmentService } from '@app/core/api-environment.service';
import { IFeatureInfoParams, ILayerGeoServer, IWorkspace } from './main.model';
import { IPanel } from '@app/pages/admin/panel/panel.model';
import { ITool, ToolsType } from '../components/tools/tools.model';
import { IMapMarker } from '../components/markers/markers.model';

@Injectable({
    providedIn: 'root'
})
export class MainService {

    FIELD_TYPE = 'type';
    FIELD_COORDINATES = 'coordinates';
    FIELD_GEOMETRY = 'geometry';
    FIELD_PROPERTIES = 'properties';
    FIELD_FEATURES = 'features';
    TYPE_FEATURE = 'Feature';
    TYPE_FEATURE_COLLECTION = 'FeatureCollection';

    wsObserver: any;

    constructor(
        private http: HttpClient,
        private apiEnvironment: ApiEnvironmentService,
        private translateService: TranslateService,
        private snackBar: MatSnackBar,
    ) {
    }

    updateAccountPanel(panel: IPanel): Observable<IPanel> {
        return this.http.put<IPanel>(`${environment.apiUrl}account/panel`, panel);
    }

    serializeGeoJSON(mapWW: WorldWind): WorldWind.GeoJSON {
        const renderables: any[] = [];
        mapWW.layers.forEach((layer) => {
            switch (true) {
                case (layer.displayName === environment.constants.map.PLACEMARK):
                case (layer.displayName === environment.constants.map.TOOL_LAYER):
                    layer.renderables.forEach((renderable) => {
                        renderables.push(renderable);
                    });
                    break;
                default:
                    break;
            }
        });
        const exportedGeoJSON = this.exportRenderables(renderables);
        return exportedGeoJSON || '{}';
    }

    private generateFeature(renderable) {
        const text = `{
            "${this.FIELD_TYPE}" : "${this.TYPE_FEATURE}",
            "${this.FIELD_PROPERTIES}": ${JSON.stringify(renderable.myTool || renderable.myMarker)},
            "${this.FIELD_GEOMETRY}": ${WorldWind.GeoJSONExporter.exportRenderable(renderable)}
        }`;
        return text;
    }

    private exportRenderables(renderables) {
        if (renderables.length === 0) {
            return '{"type":"FeatureCollection","features":[]}';
        }
        const featureCollection =
            `{
                "${this.FIELD_TYPE}" : "${this.TYPE_FEATURE_COLLECTION}",
                "${this.FIELD_FEATURES}" : [
                    ${
            renderables.map((renderable) => {
                return this.generateFeature(renderable);
            })}
        ]}`;
        return featureCollection;
    }

    deserializeGeoJSON(geoJSON: WorldWind.GeoJSON) {
        const tools: ITool[] = [];
        const markers: IMapMarker[] = [];
        if (!!geoJSON.features) {
            geoJSON.features.forEach((features) => {
                if (features.geometry.type === ToolsType.Point) {
                    markers.push(features.properties);
                } else {
                    tools.push(features.properties);
                }
            });
        } else {
            if (geoJSON.type === ToolsType.Point) {
                markers.push(geoJSON.properties);
            } else {
                tools.push(geoJSON.properties);
            }

        }

        return {
            tools: tools,
            markers: markers
        };
    }

    standardEncoding(v: string): string {
        return v
            .replace('@', '%40')
            .replace(':', '%3A')
            // .replace('$', '%24')
            .replace(',', '%2C')
            // .replace(';', '%3B')
            // .replace('+', '%2B')
            // .replace('=', '%3D')
            // .replace('?', '%3F')
            .replace('/', '%2F');
    }

    replaceAll(text: string): string {
        return text.split(',').join('%2C');
    }

    private arrayToQueryString(paramsArray: []): string {
        let text = '';
        for (let idx = 0; idx < paramsArray.length; idx++) {
            const key = Object.keys(paramsArray[idx])[0];
            const value = Object.values(paramsArray[idx])[0];
            text += `${idx === 0 ? '?' : '&'}${key}=${value}`;
        }
        return text;
    }

    getFeatureInfo(paramsFeatureInfo: IFeatureInfoParams): Observable<FeatureCollection> {
        const bbox = paramsFeatureInfo.bBox;
        const layers = paramsFeatureInfo.queryLayers.map(lay => this.standardEncoding(lay)).join(',');
        const paramsArray = [
            { 'SERVICE': 'WMS' },
            { 'VERSION': '1.1.1' },
            { 'REQUEST': 'GetFeatureInfo' },
            { 'BBOX': this.replaceAll(`${bbox[0].longitude},${bbox[0].latitude},${bbox[1].longitude},${bbox[1].latitude}`) },
            { 'FORMAT': this.standardEncoding('application/json') },
            { 'TRANSPARENT': true },
            { 'QUERY_LAYERS': layers },
            { 'LAYERS': layers },
            { 'exceptions': this.standardEncoding('application/vnd.ogc.se_inimage') },
            { 'INFO_FORMAT': this.standardEncoding('application/json') },
            { 'FEATURE_COUNT': 50 },
            { 'X': 500 },
            { 'Y': 500 },
            { 'SRS': this.standardEncoding('EPSG:4326') },
        ];
        const URL_BASE = this.apiEnvironment.getConfig().geoserver.url;

        let URL = `${URL_BASE}/wms`;
        let q = this.arrayToQueryString(<[]>paramsArray);
        // for (let idx = 0; idx < paramsArray.length; idx++) {
        //     const key = Object.keys(paramsArray[idx])[0];
        //     const value = Object.values(paramsArray[idx])[0];
        //     q += `${idx === 0 ? '?' : '&'}${key}=${value}`;
        // }
        q += `&STYLES=&WIDTH=1001&HEIGHT=1001`;
        URL += q;
        // return this.http.get<FeatureCollection>(URL, { params: params });
        return this.http.get<FeatureCollection>(URL)
            .pipe(tap((featureCollection: FeatureCollection) => {
                if (featureCollection.features.length === 0) {
                    this.snackBar.open(this.translateService.instant('webgis.http.204'), null, {
                        duration: environment.toast.duration,
                        verticalPosition: <MatSnackBarVerticalPosition>environment.toast.verticalPosition,
                        horizontalPosition: <MatSnackBarHorizontalPosition>environment.toast.horizontalPosition
                    });
                }
            }))
            .pipe(filter((featureCollection: FeatureCollection) => {
                return (featureCollection.features.length > 0);
            }))
            .pipe(tap((featureCollection: FeatureCollection) => {
                featureCollection.features.map((feature) => {
                    const _feature = feature;
                    let allProps;
                    allProps = feature.properties;
                    if (!!feature.properties.properties) {
                        allProps = {
                            ...allProps,
                            ...JSON.parse(feature.properties.properties).attributes,
                            'type': JSON.parse(feature.properties.properties).type
                        };
                        delete allProps.properties;
                    }
                    const keys = Object.keys(allProps);
                    keys.forEach((key) => {
                        const value = allProps[key];
                        if (!(!!value || value === 0)) {
                            allProps[key] = '----';
                        }
                    });
                    delete allProps['bbox'];
                    _feature.properties = allProps;
                    return _feature;
                });
                return featureCollection;
            }));
    }

    saveLayerGeoServer(layerGeoServer: ILayerGeoServer) {
        return this.http.post(`${environment.apiUrl}map-as-layers`, layerGeoServer);
    }

    getWorkspaces(): Observable<IWorkspace[]> {
        return this.http.get<IWorkspace[]>(`/geoserver/rest/workspaces.json`)
            // return this.http.get<IWorkspace[]>(`./app/workspaces.json`)
            .pipe(map((response: any) => {
                return response.workspaces.workspace;
            }));
    }

}
