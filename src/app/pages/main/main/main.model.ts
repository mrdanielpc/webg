
export enum EnumMapActions {
    NEW,
    LOAD,
    SAVE,
    SAVE_AS,
    EDIT,
    RESET,
    DELETE,
    PUBLISH,
    EXPORT_AS_JSON,
    PUBLISH_AS_LAYER,
}

export interface ISaveMapData {
    title: string;
    observations?: string;
}

export interface ILayerGeoServer {
    mapId: number;
    title: string;
    workspace: string;
    observations?: string;
}

export interface IWorkspace {
    name: string;
}

// {
// 	"place_id": "187220933",
// 	"licence": "Data © OpenStreetMap contributors, ODbL 1.0. https:\/\/www.openstreetmap.org\/copyright",
// 	"osm_type": "relation",
// 	"osm_id": "7444",
// 	"boundingbox": ["48.8155755", "48.902156", "2.224122", "2.4697602"],
// 	"lat": "48.8566101",
// 	"lon": "2.3514992",
// 	"display_name": "París, Isla de Francia, Francia metropolitana, Francia",
// 	"class": "place",
// 	"type": "city",
// 	"importance": 0.31723364983048,
// 	"icon": "http:\/\/ip-10-98-168-68.mq-us-east-1.ec2.aolcloud.net\/nominatim\/images\/mapicons\/poi_place_city.p.20.png"
// }

export interface IMapQuestAPI {
    place_id: string;
    licence: string;
    osm_type: string;
    osm_id: string;
    boundingbox: string[];
    lat: string;
    lon: string;
    display_name: string[];
    class: string;
    type: string;
    importance: number;
    icon: string;
}

interface IBbox {
    longitude: number;
    latitude: number;
}

export interface IFeatureInfoParams {
    queryLayers: string[];
    layers: string[];
    bBox: IBbox[];
    width: number;
    height: number;
    x: number;
    y: number;
}

export interface ILegendGraphicParams {
    // Style of layer for which to produce legend graphic. If not present, the default style is selected.
    // The style may be any valid style available for a layer, including non-SLD internally-defined styles
    style?: string;
    // Feature type for which to produce the legend graphic. This is not needed if the layer has only a single feature type.
    featureType?: string;
    // Rule of style to produce legend graphic for, if applicable.
    // In the case that a style has multiple rules but no specific rule is selected,
    // then the map server is obligated to produce a graphic that is representative of all of the rules of the style
    rule?: string;
    // this parameter may assist the server in selecting a more appropriate representative
    // graphic by eliminating internal rules that are out-of-scope.
    // Specifying the scale will also make the symbolizers using Unit Of Measure resize
    // according to the specified scale
    scale?: string;
    // This gives the MIME type of the file format in which to return the legend graphic.
    // Allowed values are the same as for the FORMAT= parameter of the WMS GetMap request
    format: string;

    width: number;
    height: number;
}

interface ILegend {
    layerName: string;
    title: string;
    rules: any[]; // https://docs.geoserver.org/stable/en/user/services/wms/get_legend_graphic/index.html#json-output-format
}

export interface ILegendGraphic {
    legend: ILegend[];
}

export interface IMapNavigation {
    geoCenter: string;
    zoom: number;
    heading: number;
    tilt: number;
}

export interface IAlbum {
    src: string;
    caption?: string;
    thumb: string;
}

export interface IAlbumFullData extends IAlbum {
    id: string;
}

export interface IGallery {
    index: number;
    images: IAlbumFullData[];
    title: string;
}
