import {
    Component,
    OnInit,
    OnDestroy,
    ViewChild,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    HostListener,
    AfterViewInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { BehaviorSubject, Subscription, Subject, forkJoin, Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { environment } from '@environment/environment';

import { UserService } from '@app/core/user.service';
import { Base64Service } from '@app/core/base64.service';

import {
    LoadMarkers,
    AddMapMarker,
    RemoveMapMarker,
    EditMapMarker,
    CleanMarkers,
    AddMapMarkers,
} from '@app/redux/markers/markers.actions';
import {
    LoadTools,
    AddTool,
    RemoveTool,
    EditTool,
    CleanTools,
    AddTools
} from '@app/redux/tools/tools.action';
import { WebgisTitleService } from '@app/core/language/language.helper';
import { SvsEventManager } from '@app/core/handlers/eventmanager.service';

import {
    SetDirty,
    SetPristine,
    CleanMap,
    SetMarkerSelected,
    SetToolSelected,
    SetToolTypeSelected,
    SetMArkerTypeSelected,
    SetMapOwnerState,
    SetMapEditable,
    ClearSelected,
    SetMapPublishedAsLayer,
    SetBaseLayer,
    SetProjection
} from '@app/redux/map/map.actions';
import { WebGisState } from '@app/redux/global.reducer';
import { ApiService } from '@app/core/api.service';
import {
    LoadLayerList,
    AddLayers,
    RemoveLayer,
    ChangeTransparency,
    ReorderLayers,
    ShowLayer,
    HideLayer,
    CleanLayers,
    LoadLayersAdded,
    AddLayerList,
} from '@app/redux/layers/layers.actions';

import { MapState } from '@app/redux/map/map.reducer';
import { IUser } from '@app/core/user.model';
import { MatSnackBar, MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { take, filter, takeUntil } from 'rxjs/operators';

import { LayerNode, IServer, IMainLayer } from '../components/layers/layers.model';
import { ConfirmDialogComponent } from '@app/components/confirm/confirm.component';
import { LoadPanels, ClosePanels, TogglePanel } from '@app/redux/panels/panel.actions';
import { IMediaList, IMediaResource, IUserRequest, IMediaInfo } from '../../../components/media-list/media.model';
import {
    ShowMediaResource,
    AddMediaInfo,
    AddResource,
    SetMediaListSelected,
    RemoveMediaList,
    ClearUser,
    ToggleMediaVisibility,
    CleanMedia,
    LoadMediaList
} from '@app/redux/media/media.actions';
import { FeatureCollection } from 'geojson';
import { FeatureinfoDialogComponent } from '../components/featureinfo-dialog/featureinfo-dialog.component';
import { IApiEnvironment } from '@app/core/api-environment.model';
import { MapComponent } from '../components/map/map.component';
import { IPanel, PanelSide, PanelType } from '@app/pages/admin/panel/panel.model';
import { IMarker, IMapMarker, IMarkerDialog } from '../components/markers/markers.model';
import { ITool, IToolDialog } from '../components/tools/tools.model';
import { IMap } from '../components/map-manager/map-manager.model';
import { EnumProjection, EnumBaseLayer, EnumMapOwner, EnumMapState, ILocation } from '../components/map/map.model';
import { MainService } from './main.service';
import { MapManagerService } from '../components/map-manager/map-manager.service';
import { MediaService } from '../components/media/media.service';
import { EnumMapActions, ILayerGeoServer, ISaveMapData, IMapQuestAPI, IMapNavigation } from './main.model';
import { MapPublishComponent } from '../components/map-manager/map-publish/map-publish.component';
import { MapPublishLayerComponent } from '../components/map-manager/map-publish-layer/map-publish-layer.component';
import { MapSaveComponent } from '../components/map-manager/map-save/map-save.component';
import { MapLoadComponent } from '../components/map-manager/map-load/map-load.component';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';

@Component({
    selector: 'app-main',
    templateUrl: './main.component.html',
    styleUrls: ['./main.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainComponent implements OnInit, OnDestroy, AfterViewInit {

    @ViewChild(MapComponent, { static: true }) mapCmp: MapComponent;

    panelListRight: IPanel[] = [];
    panelListLeft: IPanel[] = [];
    side: PanelSide;
    PANELS_DRAG_TYPES = ['panels'];

    // No sé el motivo, pero sólo se recibe el evento en layers.component.ts si es BehaviorSubject
    layerTypes$ = new BehaviorSubject<LayerNode[]>(null);
    serverList$ = new BehaviorSubject<IServer[]>([]);

    layersSelected$ = new BehaviorSubject<LayerNode[]>([]);
    layersAdded: LayerNode[] = [];

    markersTypeSelected$ = new Subject<IMarker>();

    markersTypes$ = new BehaviorSubject<IMarker[]>(null);
    // markersTypes: IMarker[] = [];
    markerEditable$ = new Subject<IMarker>();

    markers$ = new BehaviorSubject<IMapMarker[]>([]);
    markerDialogs: IMarkerDialog[] = [];
    servers: IServer[] = [];

    mediaList$ = new BehaviorSubject<IMediaList[]>([]);
    mediaResources$ = new Subject<IMediaResource[]>();
    mediaResourcesSelected$ = new Subject<IMediaResource>();
    featureInfoSelected$ = new Subject<FeatureCollection>();

    moveToLocation$ = new Subject<ILocation>();
    timeDiff$ = new Subject<number>();

    _mediaList: IMediaList[] = [];

    toolsTypes$ = new BehaviorSubject<ITool[]>([]);
    toolTypeSelected$ = new Subject<ITool>();
    city$ = new Subject<ILocation>();

    toolsAdded$ = new BehaviorSubject<ITool[]>([]);
    toolEditable$ = new Subject<ITool>();
    oldToolType: ITool;

    toolDialogs: IToolDialog[] = [];

    toolDialogs$ = new Subject<IToolDialog[]>();
    markerDialogs$ = new Subject<IMarkerDialog[]>();

    private unsubscribe$ = new Subject<void>();
    evtMapAction: Subscription;
    mediaResourceSelected: IMediaResource;

    model: IMap;
    isDirtyMap = false;
    isEditableMap = false;
    actualProjection: EnumProjection;
    actualBaseLayer: EnumBaseLayer;

    actualMapState: MapState;

    userReq$ = new BehaviorSubject<IUserRequest>(null);
    private userReq: IUserRequest;

    mediaListSelected$ = new Subject<IMediaList>();

    config$: Observable<IApiEnvironment>;
    PanelType: any = PanelType;
    showFakeLeft = false;
    showFakeRight = false;
    readonly C_ELLIPSE = 'ellipse';

    constructor(
        private api: ApiService,
        private store: Store<WebGisState>,
        private translateService: TranslateService,
        private titleService: WebgisTitleService,
        private snackBar: MatSnackBar,
        private userService: UserService,
        private base64Service: Base64Service,
        private route: ActivatedRoute,
        private mainService: MainService,
        private eventManager: SvsEventManager,
        private router: Router,
        private dialog: MatDialog,
        private mapManagerService: MapManagerService,
        private cd: ChangeDetectorRef,
        private mediaService: MediaService,
    ) {
        this.config$ = this.store.select('webgis', 'user', 'config')
            .pipe(filter((result) => {
                return !!result;
            }))
            .pipe(take(1));
    }

    trackById(index, item) {
        return item.id;
    }

    trackByMarker(index, item) {
        return item.marker.id;
    }

    trackByTool(index, item) {
        return item.tool.type;
    }

    @HostListener('document:keydown.escape', ['$event'])
    onKeydownHandler(evt: KeyboardEvent) {
        this.clearDataSelected();
        this.closeAllDialogs();
        this.closeResourceDialog();
    }

    private setOwnerState(map) {
        this.userService.getIdentity().then((user: IUser) => {
            // this.accessToken = user.accessToken;
            const ownerState: EnumMapOwner = (!!map && map.userId === user.id || !map) ? EnumMapOwner.MINE : EnumMapOwner.OTHER;
            this.store.dispatch(new SetMapOwnerState(ownerState));
        });
    }

    private loadMap(idMap: number) {
        if (this.isDirtyMap || (!!this.model && !!this.model.id)) {
            // if (this.isDirtyMap || (!!idMap)) {
            this.clean();
        }
        this.mapManagerService.getMap(idMap).toPromise()
            .then((map: IMap) => {
                this.model = map;
                this.setOwnerState(this.model);
                this.titleService.updateTitle(map.title);
                this.setMapPristine();
                let center: ILocation;
                if (!!map.center) {
                    center = {
                        latitude: JSON.parse(map.center).coordinates[0],
                        longitude: JSON.parse(map.center).coordinates[1],
                    };
                }
                const baseLayer: EnumBaseLayer = <EnumBaseLayer>(map.baseLayer || environment.map.baseLayer);
                const projection: EnumProjection = <EnumProjection>(map.projection || EnumProjection['3D']);
                this.mapCmp.removeMap();
                this.mapCmp.createMap({
                    center: center,
                    zoom: map.zoom,
                    heading: map.heading,
                    tilt: map.tilt,
                    baseLayer: baseLayer,
                    projection: projection,
                    userToken: this.userService.getToken()
                }).then(() => {
                    this.store.dispatch(new SetBaseLayer(baseLayer));
                    this.store.dispatch(new SetProjection(projection));
                    this.store.dispatch(new SetMapEditable(EnumMapState.SELECT_TOOL_MARKER));
                    if (!!map.geoJson) {
                        const { tools, markers } = this.mainService.deserializeGeoJSON(JSON.parse(map.geoJson));
                        this.store.dispatch(new AddTools(tools));
                        this.store.dispatch(new AddMapMarkers(markers));
                    }
                    if (!!map.mediaList) {
                        map.mediaList = <IMediaList[]>JSON.parse(<string>map.mediaList);
                        this.store.dispatch(new LoadMediaList(map.mediaList));
                    }
                    const layers = !!map.layerJson ? JSON.parse(map.layerJson) : [];
                    this.store.dispatch(new LoadLayersAdded(layers));
                });
            })
            .catch((err) => {
                this.router.navigate(['']);
            });
    }

    private setMapDirty() {
        this.store.dispatch(new SetDirty());
    }

    private setMapPristine() {
        this.store.dispatch(new SetPristine());
    }

    ngOnInit() {
        this.evtMapAction = this.eventManager.subscribe('mapAction', (data: any) => {
            switch (true) {
                case (data.content.action === EnumMapActions.NEW):
                    this.newMap();
                    break;
                case (data.content.action === EnumMapActions.LOAD):
                    this.searchMap();
                    break;
                case (data.content.action === EnumMapActions.EDIT):
                    this.editMap();
                    break;
                case (data.content.action === EnumMapActions.SAVE):
                    this.saveMap(false, false);
                    break;
                case (data.content.action === EnumMapActions.SAVE_AS):
                    this.saveMap(true, false);
                    break;
                case (data.content.action === EnumMapActions.RESET):
                    this.resetMap();
                    break;
                case (data.content.action === EnumMapActions.DELETE):
                    this.deleteMap();
                    break;
                case (data.content.action === EnumMapActions.PUBLISH):
                    this.publishMap();
                    break;
                case (data.content.action === EnumMapActions.EXPORT_AS_JSON):
                    this.exportGeoJSON();
                    break;
                case (data.content.action === EnumMapActions.PUBLISH_AS_LAYER):
                    this.exportAsLayer();
                    break;
                default:
                    console.error('main.component.ts - mapAction not defined');
                    break;
            }
        });
        this.api.getPanels();
        this.loadData();
        this.prepareMap();
    }

    ngAfterViewInit() {


    }

    private prepareMap() {
        this.route.params
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((params) => {
                if (!!params.id) {
                    const idMap = +params.id;
                    this.loadMap(idMap);
                } else {
                    this.titleService.addKeyToTitle('map.untitled');
                    this.mapCmp.createMap({
                        userToken: this.userService.getToken()
                    })
                        .then(() => {
                            this.store.dispatch(new SetMapEditable(EnumMapState.SELECT_TOOL_MARKER));
                        });
                    this.setOwnerState(null);
                }
            });
    }

    ngOnDestroy() {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        if (!!this.evtMapAction) {
            this.evtMapAction.unsubscribe();
        }
        this.clean();
        this.store.dispatch(new SetMapEditable(EnumMapState.NONE));
    }

    /* #region mapActions */
    private newMap() {
        if (this.isDirtyMap) {
            const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                data: {
                    title: this.translateService.instant('map.newmaptitle'),
                    question: this.translateService.instant('map.newmap'),
                    positive: 'map.loadnewmap'
                }
            });
            dialogRef.afterClosed()
                .pipe(take(1))
                .subscribe((result) => {
                    // Si se cierra el diálogo desde el botón Escape, no se
                    if (!!result) {
                        const _result = JSON.parse(result);
                        if (_result === true) {
                            this.router.navigate(['']);
                        }
                    }
                });
        } else {
            this.router.navigate(['']);
        }

    }

    private publishMap() {
        if (!!this.model && !!this.model.id) {
            this.dialog.open(MapPublishComponent, {
                width: '700px',
                data: {
                    userId: this.model.userId,
                    idMap: this.model.id
                }
            });

        } else {
            this.dialog.open(MapPublishComponent, {
                width: '700px',
                data: {
                    publishTarget: {
                        roles: this.model.roles,
                        groups: this.model.groups
                    }
                }
            });
        }
    }

    private downloadObjectAsJSON = (exportObj, exportName) => {
        const dataStr = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(exportObj, null, 2));
        const downloadAnchorNode = document.createElement('a');
        downloadAnchorNode.setAttribute('href', dataStr);
        downloadAnchorNode.setAttribute('download', exportName + '.json');
        document.body.appendChild(downloadAnchorNode); // required for firefox
        downloadAnchorNode.click();
        downloadAnchorNode.remove();
    }

    public exportGeoJSON() {
        const map = this.mapCmp.getMap();
        const geoJSON = this.mainService.serializeGeoJSON(map);
        if (!!geoJSON) {
            const titlte = !!this.model ? this.model.title : moment().format('L LTS');
            this.downloadObjectAsJSON(JSON.parse(geoJSON), titlte);
        } else {
            this.snackBar.open(this.translateService.instant('map.asgeojson.noinfo'), null, {
                duration: environment.toast.duration,
                verticalPosition: <MatSnackBarVerticalPosition>environment.toast.verticalPosition,
                horizontalPosition: <MatSnackBarHorizontalPosition>environment.toast.horizontalPosition
            });
        }
    }

    private saveAsLayer() {
        const params: ILayerGeoServer = {
            title: this.model.title,
            observations: this.model.observations,
            mapId: this.model.id,
            workspace: null
        };
        const dialogRef = this.dialog.open(MapPublishLayerComponent, {
            width: '700px',
            data: params
        });
        dialogRef.afterClosed()
            .pipe(take(1))
            .subscribe((layerGeoServer: ILayerGeoServer) => {
                if (layerGeoServer) {
                    this.mainService.saveLayerGeoServer(layerGeoServer)
                        .pipe(take(1))
                        .subscribe((mainLayer: IMainLayer) => {
                            console.log('AddLayerList');
                            this.store.dispatch(new AddLayerList({ layer: mainLayer, workSpace: layerGeoServer.workspace }));
                            // this.store.dispatch(new AddLayerList(mainLayer));
                            this.store.dispatch(new SetMapPublishedAsLayer());
                        });
                }
            });
    }

    private exportAsLayer() {
        if (!this.isDirtyMap && !!this.model && !this.model.geoJson) {
            this.snackBar.open(this.translateService.instant('map.aslayer.noinfo'), null, {
                duration: environment.toast.duration,
                verticalPosition: <MatSnackBarVerticalPosition>environment.toast.verticalPosition,
                horizontalPosition: <MatSnackBarHorizontalPosition>environment.toast.horizontalPosition
            });
        } else {
            if (this.isDirtyMap || (!this.model)) {
                const mustCreateNewMap = !!this.model ? false : true;
                this.saveMap(mustCreateNewMap, true)
                    .then((response) => {
                        if (response) {
                            this.exportAsLayer();
                        }
                    });
            } else {
                this.saveAsLayer();
            }
        }
    }

    private deleteMap() {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: this.translateService.instant('map.delete'),
                question: this.translateService.instant('map.deleteask'),
                positive: 'map.delete'
            }
        });

        dialogRef.afterClosed()
            .pipe(take(1))
            .subscribe((result: ISaveMapData) => {
                if (result) {
                    this.mapManagerService.deleteMap(this.model.id)
                        .pipe(take(1))
                        .subscribe(() => {
                            this.router.navigate(['']);
                        });
                }
            });

    }

    private editMap() {
        const dialogRef = this.dialog.open(MapSaveComponent, {
            data: <ISaveMapData>{
                title: !!this.model ? this.model.title : this.translateService.instant('map.untitled'),
                observations: !!this.model ? this.model.observations : '',
                positive: 'map.save'
            }
        });

        dialogRef.afterClosed()
            .pipe(take(1))
            .subscribe((result: ISaveMapData) => {
                if (result) {
                    this.model.title = result.title;
                    this.model.observations = result.observations;
                    this.mapManagerService.updateMap(this.model).toPromise()
                        .then((resultMap: IMap) => {
                            this.model.title = result.title;
                            this.model.observations = result.observations;
                            this.titleService.updateTitle(this.model.title);
                            this.snackBar.open(this.translateService.instant('map.edited'), null, {
                                duration: environment.toast.duration,
                                verticalPosition: <MatSnackBarVerticalPosition>environment.toast.verticalPosition,
                                horizontalPosition: <MatSnackBarHorizontalPosition>environment.toast.horizontalPosition
                            });
                        })
                        .catch((err) => {
                            console.error(err);
                        });

                }
            });

    }

    private searchMap() {
        const dialogRef = this.dialog.open(MapLoadComponent, {
            width: '700px',
            data: {
                idMap: -1
            }
        });

        dialogRef.afterClosed()
            .pipe(take(1))
            .subscribe((idMap: number) => {
                if (idMap > 0) {
                    if (!!this.model && !!this.model.id && this.model.id === idMap) {
                        this.resetMap();
                    } else {
                        if (this.isDirtyMap) {
                            const replaceDialogRef = this.dialog.open(ConfirmDialogComponent, {
                                data: {
                                    title: this.translateService.instant('map.loadmap'),
                                    question: this.translateService.instant('map.loadmapask'),
                                    positive: 'map.loadmap'
                                }
                            });
                            replaceDialogRef.afterClosed()
                                .pipe(take(1))
                                .subscribe((resultReplace) => {
                                    // Si se cierra el diálogo desde el botón Escape, no se
                                    if (!!resultReplace) {
                                        const _result = JSON.parse(resultReplace);
                                        if (_result === true) {
                                            this.router.navigate(['map', idMap]);
                                        }
                                    }
                                });
                        } else {
                            this.router.navigate(['map', idMap]);
                        }
                    }

                }
            });
    }

    private getBaseLayerKey(baseLayerText: string): string {
        const layerKey = Object.keys(EnumBaseLayer)
            .find((key) => {
                return (EnumBaseLayer[key] === baseLayerText);
            });
        return layerKey;

    }

    private createMapObject(mapPublishedAsLayer: boolean): IMap {
        const { geoCenter, zoom, heading, tilt } = this.mapCmp.getNavigationAttributes();
        const geoJSON = this.mainService.serializeGeoJSON(this.mapCmp.getMap());
        const layerJson = JSON.stringify(this.layersAdded);
        const newMediaList = JSON.parse(JSON.stringify(this._mediaList));
        const mediaList = newMediaList ? newMediaList.map((media: any) => {
            delete media['resources'];
            return media;
        }) : null;

        const newMap: IMap = {
            ...this.model,
            geoJson: geoJSON,
            layerJson: layerJson,
            wgFolderId: 0,
            center: geoCenter,
            zoom: zoom,
            tilt: tilt,
            heading: heading,
            projection: this.actualProjection,
            mediaList: mediaList && JSON.stringify(mediaList),
            baseLayer: this.actualBaseLayer || this.getBaseLayerKey(environment.map.baseLayer),
            publishedAsLayer: mapPublishedAsLayer
        };
        return newMap;
    }

    private createNewMap(mapPublishedAsLayer: boolean): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            const dialogRef = this.dialog.open(MapSaveComponent, {
                data: <ISaveMapData>{
                    title: !!this.model ? this.model.title : this.translateService.instant('map.untitled'),
                    observations: !!this.model ? this.model.observations : '',
                    positive: 'map.save'
                }
            });

            dialogRef.afterClosed()
                .pipe(take(1))
                .subscribe((result: ISaveMapData) => {
                    if (result) {
                        const newMap: IMap = this.createMapObject(mapPublishedAsLayer);
                        newMap.title = result.title;
                        newMap.observations = result.observations;
                        delete newMap['id'];
                        this.mapManagerService.createMap(newMap)
                            .pipe(take(1))
                            .subscribe((resultMap: IMap) => {
                                this.model = resultMap;
                                this.setMapPristine();
                                this.router.navigate(['map', resultMap.id]);
                                resolve(true);
                            }, reject);
                    } else {
                        resolve(false);
                    }
                });
        });
    }

    private saveMap(forceNewMap, mapPublishedAsLayer): Promise<boolean> {
        return new Promise<boolean>((resolve, reject) => {
            if (!!this.model && !!this.model.id) {
                if (forceNewMap) {
                    this.createNewMap(mapPublishedAsLayer)
                        .then(() => {
                            resolve(true);
                        })
                        .catch(() => {
                            reject();
                        });
                } else {
                    if (mapPublishedAsLayer) {
                        const replaceDialogRef = this.dialog.open(ConfirmDialogComponent, {
                            data: {
                                title: this.translateService.instant('map.update.title'),
                                question: this.translateService.instant('map.update.question'),
                                positive: 'map.save'
                            }
                        });
                        replaceDialogRef.afterClosed()
                            .pipe(take(1))
                            .subscribe((resultReplace) => {
                                if (!!resultReplace) {
                                    const newMap = this.createMapObject(mapPublishedAsLayer);
                                    this.mapManagerService.updateMap(newMap)
                                        .pipe(take(1))
                                        .subscribe((resultMap: IMap) => {
                                            this.model = resultMap;
                                            resolve(true);
                                            this.setMapPristine();
                                        }, reject);
                                } else {
                                    resolve(false);
                                }

                            });
                    } else {
                        const newMap = this.createMapObject(mapPublishedAsLayer);
                        this.mapManagerService.updateMap(newMap)
                            .pipe(take(1))
                            .subscribe((resultMap: IMap) => {
                                this.model = resultMap;
                                resolve(true);
                                this.setMapPristine();
                            }, reject);
                    }

                }

            } else {
                this.createNewMap(mapPublishedAsLayer)
                    .then((result) => {
                        resolve(result);
                    })
                    .catch(() => {
                        reject();
                    });
            }
        });
    }

    private resetMap() {

        if (this.isDirtyMap) {
            const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                data: {
                    title: this.translateService.instant('map.reloadmap'),
                    question: this.translateService.instant('map.reloadmapask'),
                    positive: 'map.reset'
                }
            });
            dialogRef.afterClosed()
                .pipe(take(1))
                .subscribe((result) => {
                    // Si se cierra el diálogo desde el botón Escape, no se
                    if (!!result) {
                        if (!!this.model && !!this.model.id) {
                            this.loadMap(this.model.id);
                        } else {
                            this.router.navigate(['dummy'], { skipLocationChange: true });
                        }
                    }
                });
        }

    }
    /* #endregion */

    /* #region DRAG & PANEL */

    private loadLayersData() {
        this.layerTypes$
            .pipe(take(1))
            .subscribe((data) => {
                if (!data || data.length === 0) {
                    this.api.getLayersList()
                        .pipe(take(1))
                        .subscribe((layerList) => {
                            this.store.dispatch(new LoadLayerList(layerList));
                        });
                }
            });
    }

    private loadMarkersData() {
        this.markersTypes$
            .pipe(take(1))
            .subscribe((data) => {
                if (!data || data.length === 0) {
                    this.api.getMakers()
                        .pipe(take(1))
                        .subscribe((markers: IMarker[]) => {
                            this.store.dispatch(new LoadMarkers(markers));
                        });
                }
            });

    }

    private loadToolsData() {
        this.toolsTypes$
            .pipe(take(1))
            .subscribe((tool) => {
                if (!tool || tool.length === 0) {
                    this.api.getTool()
                        .pipe(take(1))
                        .subscribe((tools: ITool[]) => {
                            this.store.dispatch(new LoadTools(tools));
                        });
                }
            });
    }

    setFixed(panel: IPanel) {

        switch (panel.title) {
            case PanelType.LAYER:
                this.loadLayersData();
                break;
            case PanelType.MARKER:
                this.loadMarkersData();
                break;
            case PanelType.TOOL:
                this.loadToolsData();
                break;
            default:
                break;
        }

        panel.isFixed = !panel.isFixed;
        this.store.dispatch(new TogglePanel(panel));
    }

    onDragStart(event) {
        if (this.panelListLeft.length === 0) {
            this.showFakeLeft = true;
        }
        if (this.panelListRight.length === 0) {
            this.showFakeRight = true;
        }
    }

    onDragEnd(event) {
        this.showFakeLeft = false;
        this.showFakeRight = false;
    }

    drop(event: CdkDragDrop<string[]>) {
        if (event.previousContainer === event.container) {
            moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
            if (event.previousIndex !== event.currentIndex) {
                this.updateAllPanels(true);
            }
        } else {
            transferArrayItem(event.previousContainer.data,
                event.container.data,
                event.previousIndex,
                event.currentIndex);
            this.updateAllPanels(true);
        }
    }

    private updateAllPanels(mustClean: boolean = false) {
        const observArr = [];
        this.panelListLeft.forEach((panel: IPanel, idx) => {
            panel.order = idx;
            panel.side = PanelSide.LEFT;
            observArr.push(this.mainService.updateAccountPanel(panel));
        });

        this.panelListRight.forEach((panel: IPanel, idx) => {
            panel.order = idx;
            panel.side = PanelSide.RIGHT;
            observArr.push(this.mainService.updateAccountPanel(panel));
        });

        forkJoin(observArr)
            .subscribe((response: IPanel[]) => {
                this.store.dispatch(new LoadPanels(response));
                if (mustClean) {
                    this.clearDataSelected();
                }
            });
    }

    /* #endregion */

    /* #region Load & Observe data */

    private clearDataSelected() {
        this.store.dispatch(new ClearSelected());
        this.store.dispatch(new ClosePanels());
    }

    private closeAllDialogs() {
        this.toolDialogs = [];
        this.toolDialogs$.next([]);
        this.markerDialogs = [];
        this.markerDialogs$.next([]);
    }

    private clean() {

        this.store.dispatch(new CleanLayers());
        this.store.dispatch(new CleanMap());
        this.store.dispatch(new CleanMarkers());
        this.store.dispatch(new CleanTools());
        this.store.dispatch(new CleanMedia());
    }

    private loadData() {

        this.observePanels();
        this.loadLayers();
        // layers
        this.loadLayersSelected();
        // markers
        this.loadMarkers();
        this.loadMarkerSelected();
        this.loadMarkerMaps();
        // tools
        this.observeTools();
        // map
        this.observeMap();
        this.observeCity();
        this.observeProjection();
        this.observeBaseLayer();
        // media
        // this.loadMedia();
        this.observeUserRequestMultimedia();
        this.observeMediaResourceSelected();
        // this.observeFeatureInfoSelected();
        this.observeMediaList();
        this.observeMediaResources();
        this.observeMediaListSelected();
    }

    private dispatchToolType(toolType: ITool) {
        this.selectToolType(toolType);
        this.toolTypeSelected$.next({ ...toolType });
        this.actualMapState.toolTypeSelected = toolType;
        this.oldToolType = null;
    }

    private checkActivePanel(mapState: MapState) {
        if (!!this.panelListLeft && !!this.panelListRight) {
            [...this.panelListLeft, ...this.panelListRight].forEach((panel) => {
                if (panel.title === PanelType.TOOL) {
                    panel.typeSelected = (!!mapState.toolTypeSelected && !!mapState.toolTypeSelected.type);
                    if (panel.typeSelected) {
                        panel.typeSelectedIcon = mapState.toolTypeSelected.icon || mapState.toolTypeSelected.svgIcon;
                    }
                } else if (panel.title === PanelType.MARKER) {
                    panel.typeSelected = (!!mapState.markerTypeSelected && !!mapState.markerTypeSelected.id);
                    if (panel.typeSelected) {
                        panel.typeSelectedUrl = mapState.markerTypeSelected.iconUrl;
                    }
                }
            });
        }
    }

    private observeMap() {

        this.store.select('webgis', 'map', 'timeDiff')
            .subscribe((timeDiff: number) => {
                this.timeDiff$.next(timeDiff);
            });

        this.store.select('webgis', 'map')
            .subscribe((newState: MapState) => {
                if (!!this.actualMapState) {
                    if (newState.isDirty !== this.actualMapState.isDirty) {
                        this.isDirtyMap = newState.isDirty;
                    }
                    if (newState.editableMode !== this.actualMapState.editableMode) {
                        // console.log('editableMode changed to: ' + newState.editableMode);
                        this.mapCmp.toggleMapEditableMode(newState.editableMode);
                    }
                    if (newState.markerSelected !== this.actualMapState.markerSelected) {
                        // console.log('markerSelected changed!');
                        this.markerEditable$.next(newState.markerSelected);
                    }
                    if (newState.markerTypeSelected !== this.actualMapState.markerTypeSelected) {
                        // console.log('markerTypeSelected changed!');
                        this.markersTypeSelected$.next(newState.markerTypeSelected);
                    }
                    if (newState.toolSelected !== this.actualMapState.toolSelected) {
                        // console.log('toolSelected changed!');
                        this.toolEditable$.next(newState.toolSelected);
                    }
                    if (newState.toolTypeSelected !== this.actualMapState.toolTypeSelected) {
                        // console.log('toolTypeSelected changed!');
                        if (
                            (!this.oldToolType) ||
                            (!newState.toolTypeSelected) ||
                            (this.oldToolType.type !== newState.toolTypeSelected.type)
                        ) {
                            if (this.mapCmp.canChangeTool()) {
                                this.toolTypeSelected$.next({ ...newState.toolTypeSelected });
                            } else {
                                this.oldToolType = this.actualMapState.toolTypeSelected;
                                this.actualMapState.toolTypeSelected = null;
                                const toolText = {
                                    tool: this.translateService.instant('tools.types.' + this.oldToolType.type)
                                };
                                const dialogRef = this.dialog.open(ConfirmDialogComponent, {
                                    data: {
                                        title: this.translateService.instant('tools.question.title', toolText),
                                        question: this.translateService.instant('tools.question.desc', toolText),
                                        positive: 'tools.endtool'
                                    }
                                });
                                dialogRef.afterClosed()
                                    .pipe(take(1))
                                    .subscribe((result) => {
                                        // Si se cierra el diálogo desde el botón Escape, no se
                                        if (!!result) {
                                            const _result = JSON.parse(result);
                                            if (_result === true) {
                                                this.mapCmp.finishToolAction(this.oldToolType);
                                                this.dispatchToolType(newState.toolTypeSelected);

                                            }
                                        } else {
                                            this.dispatchToolType(this.oldToolType);
                                        }
                                    });
                            }
                        }
                    }

                    this.checkActivePanel(newState);

                }

                switch (true) {
                    // case (newState.editableMode === EnumMapState.NONE):
                    case (newState.editableMode === EnumMapState.SELECT_TOOL_MARKER):
                        this.setCursor('auto');
                        break;
                    case (newState.editableMode === EnumMapState.MOVE_TOOL_MARKER):
                        this.setCursor('grab');
                        break;
                    case (
                        (newState.editableMode !== EnumMapState.MOVE_TOOL_MARKER) &&
                        ((!!newState.markerTypeSelected) || (!!newState.toolTypeSelected))
                    ):
                        this.setCursor('crosshair');
                        break;
                    default:
                        break;
                }

                this.actualMapState = newState;
            });
    }

    private observeCity() {
        this.store.select('webgis', 'map', 'searchCity')
            .pipe(filter((city) => {
                return !!city;
            }))
            .subscribe((city: IMapQuestAPI) => {
                const location: ILocation = {
                    latitude: +city.lat,
                    longitude: +city.lon
                };
                this.city$.next(location);
            });
    }

    private observeUserRequestMultimedia() {
        this.store.select('webgis', 'media', 'userReq')
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((user: IUserRequest) => {
                this.userReq = user;
                this.userReq$.next(user);
            });
    }

    private observeMediaList() {
        this.store.select('webgis', 'media', 'mediaList')
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((mediaList: IMediaList[]) => {
                this.mediaList$.next(mediaList);
                this._mediaList = mediaList;
                this.cd.markForCheck();
            });
    }

    private observeMediaListSelected() {
        this.store.select('webgis', 'media', 'mediaListSelected')
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((mediaList: IMediaList) => {
                this.mediaListSelected$.next(mediaList);
            });
    }

    private observeProjection() {
        this.store.select('webgis', 'map', 'projection')
            .pipe(filter((data) => {
                return !!data;
            }))
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((projection: EnumProjection) => {
                this.actualProjection = projection;
                if (this.model) {
                    this.model.projection = projection;
                }
                this.mapCmp.configureProjection(projection);
            });
    }

    private observeBaseLayer() {
        this.store.select('webgis', 'map', 'baseLayer')
            .pipe(filter((data) => {
                return !!data;
            }))
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((baseLayer: EnumBaseLayer) => {
                this.actualBaseLayer = baseLayer;
                if (this.model) {
                    this.model.baseLayer = baseLayer;
                }
                this.mapCmp.configureBaseLayer(baseLayer);
            });
    }

    private observeTools() {
        this.store.select('webgis', 'tools', 'toolsType')
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((tools: ITool[]) => {
                this.toolsTypes$.next(tools);
            });

        this.store.select('webgis', 'tools', 'tools')
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((tools: ITool[]) => {
                this.toolsAdded$.next(tools);
            });

    }

    private observePanels() {

        this.store.select('webgis', 'panels', 'panels')
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((panels: IPanel[]) => {
                if (!!panels && panels.length > 0) {
                    const pnlLeft: IPanel[] = [];
                    const pnlRight: IPanel[] = [];
                    panels.forEach((panel) => {
                        switch (panel.title) {
                            case PanelType.LAYER:
                                panel.icon = 'layers';
                                break;
                            case PanelType.TOOL:
                                panel.icon = 'gesture';
                                break;
                            case PanelType.MARKER:
                                panel.icon = 'add_location';
                                break;
                            case PanelType.MEDIA:
                                panel.icon = 'collections';
                                break;
                            default:
                                break;
                        }
                        if (panel.side === PanelSide.LEFT) {
                            pnlLeft.push(panel);
                        } else {
                            pnlRight.push(panel);
                        }
                    });
                    this.panelListLeft = [...pnlLeft];
                    this.panelListRight = [...pnlRight];
                    this.cd.markForCheck();
                }
            });
    }

    private loadMarkers() {
        // this.obsMarkersMarkersTypes =
        this.store.select('webgis', 'markers', 'markersTypes')
            .pipe(takeUntil(this.unsubscribe$))
            .pipe(filter((data) => {
                return !!data;
            }))
            .subscribe((markersList: IMarker[]) => {
                // let markersLoaded = false;
                // this.markersTypes = markersList;
                this.markersTypes$.next(markersList);
                // if (!!markersList && markersList.length > 0) {
                //     this.panelListLeft.forEach((panel) => {
                //         if (panel.title === PanelType.MARKER) {
                //             markersLoaded = true;
                //             panel.data = [...markersList];
                //         }
                //     });
                //     if (!markersLoaded) {
                //         this.panelListRight.forEach((panel) => {
                //             if (panel.title === PanelType.MARKER) {
                //                 panel.data = [...markersList];
                //             }
                //         });
                //     }
                // }
            });
    }

    private observeMediaResources() {
        // loadMediaResources
        this.store.select('webgis', 'media', 'mediaResources')
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((mediaResources: IMediaResource[]) => {
                this.mediaResources$.next(mediaResources);
            });
    }

    private observeMediaResourceSelected() {
        this.store.select('webgis', 'media', 'mediaResourceSelected')
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((mediaResource: IMediaResource) => {
                this.mediaResourceSelected = mediaResource;
                this.mediaResourcesSelected$.next(mediaResource);
                this.cd.markForCheck();
            });
    }

    // private observeFeatureInfoSelected() {
    //     this.store.select('webgis', 'media', 'featureInfoSelected')
    //         .pipe(takeUntil(this.unsubscribe$))
    //         .subscribe((feature: FeatureCollection) => {
    //             // this.mediaResourceSelected = feature;
    //             this.featureInfoSelected$.next(feature);
    //             this.cd.markForCheck();
    //         });
    // }

    closeResourceDialog() {
        this.store.dispatch(new ShowMediaResource(null));
    }

    private loadLayersSelected() {
        this.store.select('webgis', 'layers', 'layersAdded')
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((layersSelected: LayerNode[]) => {
                this.layersAdded = layersSelected;
                this.addLayers(layersSelected, true);
            });
    }

    private loadMarkerMaps() {
        this.store.select('webgis', 'markers', 'markers')
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((markerMaps: IMapMarker[]) => {
                this.markers$.next(markerMaps);
            });
    }

    private loadMarkerSelected() {
        this.store.select('webgis', 'markers', 'markerEditable')
            .pipe(takeUntil(this.unsubscribe$))
            .subscribe((marker: IMarker) => {
                this.markerEditable$.next(marker);
            });
    }

    private loadLayers() {

        this.store.select('webgis', 'layers', 'layersList')
            .pipe(takeUntil(this.unsubscribe$))
            .pipe(filter((data) => {
                return !!data;
            }))
            .subscribe((layerNodes: LayerNode[]) => {
                this.layerTypes$.next([...layerNodes]);
            });
    }
    /* #endregion */

    private getUrls(nodes): Promise<string[]> {
        return new Promise((resolve, reject) => {
            const urlsCapabilities: string[] = [];
            const urlsTokens: string[] = [];
            nodes.forEach((node) => {
                const serverFound = this.servers.find((server) => {
                    return (server.url === node.item.gisServerUrl);
                });
                if (!!serverFound && !!serverFound.xml) {
                    // node.xml = serverFound.xml;
                } else {
                    const myUrl = new URL(node.item.gisServerUrl);
                    const urlSave = myUrl.protocol + '//' +
                        myUrl.hostname + ':' +
                        myUrl.port +
                        myUrl.pathname;
                    const token = this.userService.getToken();
                    if (!!token) {
                        const tokenDecoded = token.split('.')[1];
                        const payLoadToken = JSON.parse(this.base64Service.decode(tokenDecoded));
                        if (new Date(payLoadToken.exp * 1000) > new Date()) {
                            sessionStorage.setItem(urlSave, token);
                        } else {
                            urlsTokens.push(urlSave);
                        }
                    }
                    urlsCapabilities.push(node.item.gisServerUrl);
                }
            });
            // Si tenemos que actualizar el token para coger los capabilities...
            if (urlsTokens.length > 0) {
                this.userService.getIdentity(true)
                    .then((user) => {
                        urlsTokens.forEach((url) => {
                            sessionStorage.setItem(url, user.accessToken);
                        });
                        resolve(urlsCapabilities);
                    });
            } else {
                resolve(urlsCapabilities);
            }

        });
    }

    addLayers(nodes: LayerNode[], avoidDispatch = false) {
        const urlsArray: Promise<string>[] = [];
        this.getUrls(nodes)
            .then((urls) => {
                const uniqueUrl = [...new Set(urls)];
                uniqueUrl.forEach((url) => {
                    urlsArray.push(this.api.getServerXml(url).toPromise());
                });
                Promise.all(urlsArray)
                    .then((xmlArr) => {
                        xmlArr.forEach((xml, idx) => {
                            this.servers.push({
                                url: uniqueUrl[idx],
                                xml: xml
                            });
                        });

                        this.serverList$.next(this.servers);
                        if (!avoidDispatch) {
                            this.servers.forEach((server: IServer) => {
                                nodes.forEach((node: LayerNode) => {
                                    if (node.item.gisServerUrl === server.url) {
                                        // const realXml = (new DOMParser()).parseFromString(server.xml, 'text/xml');
                                        // const wms = new WorldWind.WmsCapabilities(realXml);
                                        // const wmsLayerCapabilities = wms.getNamedLayer(layer.item.name);
                                    }
                                });

                            });

                            this.store.dispatch(new AddLayers(nodes));
                            this.setMapDirty();
                        } else {
                            this.layersSelected$.next(nodes);
                        }
                    });
            });

    }

    layerOrdered(nodes: LayerNode[]) {
        this.store.dispatch(new ReorderLayers(nodes));
        this.setMapDirty();
    }

    toggleVisibility(node: LayerNode) {
        if (node.enabled) {
            this.store.dispatch(new ShowLayer(node));
        } else {
            this.store.dispatch(new HideLayer(node));
        }
        this.setMapDirty();
    }

    toggleMediaVisibility(media: IMediaList) {
        this.store.dispatch(new ToggleMediaVisibility(media));
    }

    setMediaListSelected(media: IMediaList) {
        this.store.dispatch(new SetMediaListSelected(media));
        this.setMapDirty();
    }

    // addResources(mediaResources: IMediaResource[]) {
    //     this.store.dispatch(new AddResource(mediaResources));
    // }

    removeMedia(media: IMediaList) {
        this.store.dispatch(new RemoveMediaList(media));
        this.setMapDirty();
    }

    clearUser() {
        this.store.dispatch(new ClearUser());
    }

    addResources(mediaResources: IMediaResource[]) {
        this.store.dispatch(new AddResource(mediaResources));
    }

    removeLayer(node: LayerNode) {
        this.store.dispatch(new RemoveLayer(node));
        this.setMapDirty();
    }

    changeTransparency(node: LayerNode) {
        this.store.dispatch(new ChangeTransparency(node));
        this.setMapDirty();
    }

    addMarkerMap(marker: IMapMarker) {
        this.store.dispatch(new AddMapMarker(marker));
        this.setMapDirty();
    }

    addTool(tool: ITool) {
        this.store.dispatch(new AddTool(tool));
        this.setMapDirty();
    }

    setToolEditable(tool: ITool) {
        this.store.dispatch(new SetToolSelected(tool));
    }

    moveToLocation(location: ILocation) {
        this.moveToLocation$.next(location);
    }

    showMediaResource(mediaResource: IMediaResource) {
        if (!!mediaResource.mediaInfo) {
            this.store.dispatch(new ShowMediaResource(mediaResource));
        } else {
            this.mediaService.getResourceInfo(this.userReq, mediaResource)
                .pipe(take(1))
                .subscribe((mediaParameters: IMediaInfo[]) => {
                    mediaResource.mediaInfo = mediaParameters;
                    this.store.dispatch(new AddMediaInfo({ ...mediaResource }));
                });
        }
    }

    updateLocation(userObject: any) {
        // const newPosition = userObject.position;
        // const marker: IMapMarker = userObject.myMarker;
        // marker.location = {
        //     latitude: newPosition.latitude,
        //     longitude: newPosition.longitude,
        // };
        // this.store.dispatch(new EditMapMarker(marker));
        this.setMapDirty();
    }

    closeMarkerDialog(marker: IMapMarker) {
        this.markerDialogs.forEach((markerDialog, idx) => {
            if (markerDialog.marker.idMapMarker === marker.idMapMarker) {
                this.markerDialogs.splice(idx, 1);
            }
        });
    }

    closeToolDialog(tool: ITool) {
        this.toolDialogs.forEach((toolDialog, idx) => {
            if (toolDialog.tool.attributes.idMapTool === tool.attributes.idMapTool) {
                this.toolDialogs.splice(idx, 1);
            }
        });
    }

    setMarkerEditable(markerMap: IMapMarker, moveToLocation: boolean) {
        if (!!markerMap) {
            markerMap.moveToLocation = moveToLocation;
        }
        this.store.dispatch(new SetMarkerSelected(markerMap));
    }

    editMarker(marker: IMapMarker) {
        this.closeMarkerDialog(marker);
        this.store.dispatch(new EditMapMarker(marker));
        this.setMapDirty();
    }

    setMarkerType(markerType: IMarker) {
        this.store.dispatch(new SetMArkerTypeSelected(markerType));
    }

    removeMarker(marker: IMapMarker) {
        this.store.dispatch(new RemoveMapMarker(marker));
        this.setMapDirty();
    }

    createMarkerDialog(dialogs: IMarkerDialog[]) {
        this.markerDialogs = dialogs;
    }

    createToolDialog(tools: IToolDialog[]) {
        this.toolDialogs = tools;
    }

    selectToolType(tool: ITool) {
        this.store.dispatch(new SetToolTypeSelected(tool));
    }

    removeTool(tool: ITool) {
        this.store.dispatch(new RemoveTool(tool));
        this.setMapDirty();
    }

    editTool(tool: ITool) {
        this.store.dispatch(new EditTool(tool));
        this.store.dispatch(new SetToolSelected(tool));
        this.setMapDirty();
    }

    private setCursor(cursor: string) {
        const map = <HTMLElement>document.getElementsByTagName('app-map')[0];
        if (!!map) {
            map.style.cursor = cursor;
        }
    }

    showFeatureInfo(feature: FeatureCollection) {
        const dialogRef = this.dialog.open(FeatureinfoDialogComponent, {
            data: feature,
        });
        dialogRef.afterClosed()
            .pipe(take(1))
            .subscribe((result) => {
                console.log(result);
            });
    }

    setNavigation(navigation: IMapNavigation) {
        // this.store.dispatch(new s)
        this.setMapDirty();
    }

}
