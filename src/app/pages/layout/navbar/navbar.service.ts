import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { filter, take } from 'rxjs/operators';

import { IApiEnvironment } from '@app/core/api-environment.model';
import { WebGisState } from '@app/redux/global.reducer';
import { IMapQuestAPI } from '@app/pages/main/main/main.model';

@Injectable({
    providedIn: 'root'
})
export class NavbarService {

    config: IApiEnvironment;

    constructor(
        private http: HttpClient,
        private store: Store<WebGisState>,
    ) {
        this.store.select('webgis', 'user', 'config')
            .pipe(filter((result) => {
                return !!result;
            }))
            .pipe(take(1))
            .subscribe((config: IApiEnvironment) => {
                this.config = config;
            });
    }

    searchCity(city: string): Observable<IMapQuestAPI[]> {
        return this.http.get<IMapQuestAPI[]>(
            `${this.config.mapquest.apiUrl}${city}?format=json&key=${this.config.mapquest.consumerKey}`
        );
    }

}
