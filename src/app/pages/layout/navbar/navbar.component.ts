import { Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef, Input } from '@angular/core';
import { Router, NavigationEnd, ActivatedRouteSnapshot } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';

import { WebGisState } from '@app/redux/global.reducer';
import { SvsEventManager } from '@app/core/handlers/eventmanager.service';

import { WebgisTitleService } from '@app/core/language/language.helper';
import { NavbarService } from './navbar.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { debounceTime, switchMap } from 'rxjs/operators';
import { environment } from '@environment/environment';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { SearchCity, SetProjection, SetBaseLayer } from '@app/redux/map/map.actions';
import { MatSelectChange } from '@angular/material/select';
import { EnumMapActions, IMapQuestAPI } from '@app/pages/main/main/main.model';
import { EnumMapOwner, EnumProjection, EnumBaseLayer } from '@app/pages/main/components/map/map.model';

export interface IAction {
    name: string;
    icon: string;
    action: EnumMapActions;
    enabled: boolean;
    visible: boolean;
}

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavbarComponent implements OnInit, OnDestroy {

    actions: IAction[] = [
        {
            action: EnumMapActions.NEW,
            icon: 'library_add',
            name: 'new',
            enabled: true,
            visible: true
        },
        {
            action: EnumMapActions.LOAD,
            icon: 'open_in_browser',
            name: 'load',
            enabled: true,
            visible: true
        },
        {
            action: EnumMapActions.EDIT,
            icon: 'edit',
            name: 'edit',
            enabled: true,
            visible: true
        },
        {
            action: EnumMapActions.SAVE,
            icon: 'save',
            name: 'save',
            enabled: true,
            visible: true
        },
        {
            action: EnumMapActions.SAVE_AS,
            icon: 'save_alt',
            name: 'saveas',
            enabled: false,
            visible: true
        },
        {
            action: EnumMapActions.RESET,
            icon: 'autorenew',
            name: 'reset',
            enabled: true,
            visible: true
        },
        {
            action: EnumMapActions.DELETE,
            icon: 'delete_forever',
            name: 'delete',
            enabled: true,
            visible: true
        },
        {
            action: EnumMapActions.PUBLISH,
            icon: 'group_add',
            name: 'publish',
            enabled: true,
            visible: true
        },
        {
            action: EnumMapActions.EXPORT_AS_JSON,
            icon: 'save_alt',
            name: 'exportjson',
            enabled: true,
            visible: true
        },
        {
            action: EnumMapActions.PUBLISH_AS_LAYER,
            icon: 'layers',
            name: 'saveaslayer',
            enabled: true,
            visible: true
        },
    ];

    showMenu = false;
    showSelectIcon = false;
    showSelectBaseLayerIcon = false;
    showSelectSearchCity = false;
    showMapDirty = false;

    unSavedTooltip: string = null;

    actualMapOwner: EnumMapOwner;

    obsMapIsDirty: Subscription;
    obsMapOwner: Subscription;
    obsMapProjection: Subscription;
    obsMapBaseLayer: Subscription;
    obsMapPublishedAsLayer: Subscription;

    searchForm: FormGroup;
    isLoading = false;
    cities: Observable<IMapQuestAPI[]>;
    searchText: string;

    projections = [];
    projectionByDefault;
    baseLayerByDefault;
    baseLayers: { value: string, name: string }[] = [];
    title: string;

    constructor(
        private store: Store<WebGisState>,
        private cd: ChangeDetectorRef,
        private eventManager: SvsEventManager,
        private router: Router,
        private translateService: TranslateService,
        private titleService: WebgisTitleService,
        private navbarService: NavbarService,
        private fb: FormBuilder,
    ) {

        this.eventManager.subscribe('titleChanged', (param) => {
            this.title = param.content;
        });

        this.projections = [
            {
                value: EnumProjection['3D'],
                text: this.translateService.instant('map.projection.3D')
            },
            {
                value: EnumProjection.EQUIRECTANGULAR,
                text: this.translateService.instant('map.projection.EQUIRECTANGULAR')
            },
            {
                value: EnumProjection.MERCATOR,
                text: this.translateService.instant('map.projection.MERCATOR')
            },
            {
                value: EnumProjection.NORTH_GNOMONIC,
                text: this.translateService.instant('map.projection.NORTH_POLAR')
            },
            {
                value: EnumProjection.NORTH_POLAR,
                text: this.translateService.instant('map.projection.SOUTH_POLAR')
            },
            {
                value: EnumProjection.NORTH_UPS,
                text: this.translateService.instant('map.projection.NORTH_UPS')
            },
            {
                value: EnumProjection.SOUTH_GNOMONIC,
                text: this.translateService.instant('map.projection.SOUTH_UPS')
            },
            {
                value: EnumProjection.SOUTH_POLAR,
                text: this.translateService.instant('map.projection.NORTH_GNOMONIC')
            },
            {
                value: EnumProjection.SOUTH_UPS,
                text: this.translateService.instant('map.projection.SOUTH_GNOMONIC')
            }
        ];

        this.baseLayerByDefault = Object.keys(EnumBaseLayer)
            .find((key) => {
                return EnumBaseLayer[key] === environment.map.baseLayer;
            });

        this.baseLayers = Object.keys(EnumBaseLayer)
            .map((key) => {
                return ({
                    value: key,
                    name: EnumBaseLayer[key]
                });
            });

        this.projectionByDefault = this.projections[0].value;

        this.titleService.updateOnRouting();

        const url: string = this.router.routerState.snapshot.url;
        this.processActionsRouting(url);

        this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                this.processActionsRouting(this.router.routerState.snapshot.url);
            }
        });
        this.searchForm = this.fb.group({
            userInput: null
        });

        this.cities = this.searchForm
            .get('userInput')
            .valueChanges
            .pipe(
                debounceTime(environment.debounceModel),
                switchMap(value => {
                    if (!!value && !value.place_id) {
                        return this.navbarService.searchCity(value);
                    } else {
                        return [];
                    }
                })
            );
    }

    trackById(index, item) {
        return item.action;
    }

    trackByValue(index, item) {
        return item.value;
    }

    displayFn(city: IMapQuestAPI) {
        if (city) {
            return city.display_name;
        }
    }

    resetInput() {
        const input = this.searchForm.controls['userInput'];
        input.setValue(null);
        input.updateValueAndValidity({ onlySelf: false, emitEvent: true });
    }

    citySelected(optSelected: MatAutocompleteSelectedEvent) {
        const city = <IMapQuestAPI>optSelected.option.value;
        this.store.dispatch(new SearchCity(city));
    }

    ngOnInit() {
        this.observeMapState();
    }

    ngOnDestroy() {
        this.obsMapIsDirty.unsubscribe();
        this.obsMapOwner.unsubscribe();
        this.obsMapProjection.unsubscribe();
        this.obsMapPublishedAsLayer.unsubscribe();
        this.obsMapBaseLayer.unsubscribe();
    }

    private observeMapState() {

        this.obsMapPublishedAsLayer = this.store.select('webgis', 'map', 'publishedAsLayer')
            .subscribe((publishedAsLayer: boolean) => {
                this.actions.find((action) => {
                    return (action.action === EnumMapActions.PUBLISH_AS_LAYER);
                }).enabled = !publishedAsLayer;
            });

        this.obsMapProjection = this.store.select('webgis', 'map', 'projection')
            .subscribe((mapProjection: EnumProjection) => {
                this.projectionByDefault = mapProjection;
            });

        this.obsMapBaseLayer = this.store.select('webgis', 'map', 'baseLayer')
            .subscribe((baseLayer: EnumBaseLayer) => {
                this.baseLayerByDefault = baseLayer;
            });

        this.obsMapIsDirty = this.store.select('webgis', 'map', 'isDirty')
            .subscribe((isDirty: boolean) => {
                this.showMapDirty = isDirty;
                this.actions.find((action) => {
                    return (action.action === EnumMapActions.RESET);
                }).enabled = isDirty;
                this.unSavedTooltip = this.showMapDirty ? this.translateService.instant('map.dirtyadvice') : null;
                this.cd.markForCheck();
            });

        this.obsMapOwner = this.store.select('webgis', 'map', 'mapOwnerState')
            .subscribe((owner: EnumMapOwner) => {
                this.actualMapOwner = owner;
                this.processActionsOwner(this.actualMapOwner);
                this.cd.markForCheck();
            });
    }

    private getRouteObject(routeSnapshot: ActivatedRouteSnapshot): any {
        let data = !!(routeSnapshot.data.pageTitle || routeSnapshot.data.showNavbarMenu) ? routeSnapshot : null;
        if (routeSnapshot.firstChild) {
            data = this.getRouteObject(routeSnapshot.firstChild) || data;
        }
        return data;
    }

    private processActionsOwner(mapOwner: EnumMapOwner) {
        if (mapOwner === EnumMapOwner.MINE) {
            this.actions.find((action) => {
                return (action.action === EnumMapActions.PUBLISH);
            }).visible = true;
            this.actions.find((action) => {
                return (action.action === EnumMapActions.SAVE);
            }).visible = true;
            this.actions.find((action) => {
                return (action.action === EnumMapActions.EDIT);
            }).visible = true;
            this.actions.find((action) => {
                return (action.action === EnumMapActions.DELETE);
            }).visible = true;
            this.actions.find((action) => {
                return (action.action === EnumMapActions.RESET);
            }).visible = true;
        } else {
            this.actions.find((action) => {
                return (action.action === EnumMapActions.PUBLISH);
            }).visible = false;
            this.actions.find((action) => {
                return (action.action === EnumMapActions.SAVE);
            }).visible = false;
            this.actions.find((action) => {
                return (action.action === EnumMapActions.EDIT);
            }).visible = false;
            this.actions.find((action) => {
                return (action.action === EnumMapActions.DELETE);
            }).visible = false;
            this.actions.find((action) => {
                return (action.action === EnumMapActions.RESET);
            }).visible = false;
        }
    }

    private processActionsRouting(url: string): void {
        const route = this.getRouteObject(this.router.routerState.snapshot.root);
        if (!!route && !!route.data) {
            this.showMenu = !!route.data.showNavbarMenu;
            this.showSelectIcon = !!route.data.showNavbarSelectIcon;
            this.showSelectBaseLayerIcon = !!route.data.showNavbarSelectBaseLayerIcon;
            this.showSelectSearchCity = !!route.data.showSelectSearchCity;

            this.cd.markForCheck();
            switch (true) {
                // Cargamos el mapa nuevo
                case (!route.url[0] && url === '/'):
                    this.actions.find((action) => {
                        return (action.action === EnumMapActions.NEW);
                    }).enabled = false;
                    this.actions.find((action) => {
                        return (action.action === EnumMapActions.EDIT);
                    }).enabled = false;
                    this.actions.find((action) => {
                        return (action.action === EnumMapActions.DELETE);
                    }).enabled = false;
                    this.actions.find((action) => {
                        return (action.action === EnumMapActions.PUBLISH);
                    }).enabled = false;
                    this.actions.find((action) => {
                        return (action.action === EnumMapActions.SAVE_AS);
                    }).enabled = false;
                    break;
                // Cargamos un mapa concreto
                case (!!route.url[0] && route.url[0].path === 'map'):
                    this.actions.find((action) => {
                        return (action.action === EnumMapActions.NEW);
                    }).enabled = true;
                    this.actions.find((action) => {
                        return (action.action === EnumMapActions.EDIT);
                    }).enabled = true;
                    this.actions.find((action) => {
                        return (action.action === EnumMapActions.DELETE);
                    }).enabled = true;
                    this.actions.find((action) => {
                        return (action.action === EnumMapActions.PUBLISH);
                    }).enabled = true;
                    this.actions.find((action) => {
                        return (action.action === EnumMapActions.SAVE_AS);
                    }).enabled = true;
                    break;
                default:
                    break;
            }
        }

    }

    openMenu() {
        this.eventManager.broadcast({
            name: 'openSideNav'
        });
    }

    dispatchMapBaseLayer(baseLayer: MatSelectChange) {
        this.store.dispatch(new SetBaseLayer(baseLayer.value));
    }

    dispatchMapProjection(projection: MatSelectChange) {
        this.store.dispatch(new SetProjection(projection.value));
    }

    dispatchMapAction(action: EnumMapActions) {
        this.eventManager.broadcast({
            name: 'mapAction',
            content: {
                action: action
            }
        });
    }

}
