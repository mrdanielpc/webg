import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { SvsEventManager } from '@app/core/handlers/eventmanager.service';
import { Store, State } from '@ngrx/store';
import { WebGisState } from '@app/redux/global.reducer';
import { ApiEnvironmentService } from '@app/core/api-environment.service';
import { IUser } from '@app/core/user.model';
import { take, filter } from 'rxjs/operators';
import { IApiEnvironment } from '@app/core/api-environment.model';
import { LoadAPIConfig } from '@app/redux/user/user.actions';
import { timer } from 'rxjs';
import { environment } from '@environment/environment';
import { UserService } from '@app/core/user.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

    @ViewChild('fnMenu', { static: false }) menu: MatSidenav;
    showProgressBar = false;
    counter: number;
    config: IApiEnvironment;

    constructor(
        private eventManager: SvsEventManager,
        private apiEnv: ApiEnvironmentService,
        private store: Store<WebGisState>,
        private userService: UserService,
    ) {
        this.counter = 0;
        eventManager.subscribe('xhrStart', (response: any) => {
            this.counter++;
            if (this.counter > 0) {
                setTimeout(() => {
                    this.showProgressBar = true;
                });
            }
        });
        eventManager.subscribe('xhrStop', (response: any) => {
            this.counter--;
            if (this.counter === 0) {
                this.showProgressBar = false;
            }
        });

        // No hace falta hacer un unsubscribe ya que este componente
        // está vivo mientras la aplicación esté abierta
        // y aún cambiando de módulos (lazy routing) solo se crea una vez
        if (environment.mustRefreshToken) {
            timer(0, environment.forceRefreshToken)
                .subscribe((val) => {
                    this.userService.getServerProfileInfo();
                });
        }

        this.store.select('webgis', 'user', 'user')
            .pipe(filter((result) => {
                return !!result;
            }))
            .pipe(take(1))
            .subscribe((user: IUser) => {
                if (!!user) {
                    this.apiEnv.searchAPIEnvironment()
                        .then((config) => {
                            this.config = { ...config };
                            this.store.dispatch(new LoadAPIConfig(config));
                        });
                }
            });
    }

    open() {
        this.menu.open();
    }

    close() {
        this.menu.close();
    }

    ngOnInit() {
        this.eventManager.subscribe('openSideNav', (data: any) => {
            this.open();
        });
        this.eventManager.subscribe('closeSideNav', (data: any) => {
            this.close();
        });

    }

}
