import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '@environment/environment';
import { HttpClient } from '@angular/common/http';
import { IPanel, IPanelRole, IPanelGroup } from './panel.model';
import { IRole } from '../role/role.model';
import { IGroup } from '../group/group.model';

@Injectable({
    providedIn: 'root'
})
export class PanelService {

    constructor(
        private http: HttpClient,
    ) { }

    get(): Observable<IPanel[]> {
        return this.http.get<IPanel[]>(environment.apiUrl + 'wg-panels');
    }

    update(panel: IPanel): Observable<IPanel> {
        return this.http.put<IPanel>(environment.apiUrl + 'wg-panels', panel);
    }

    getRoles(): Observable<IRole[]> {
        return this.http.get<IRole[]>(environment.apiUrl + 'wg-roles');
    }

    getGroups(): Observable<IGroup[]> {
        return this.http.get<IGroup[]>(environment.apiUrl + 'wg-groups');
    }

    addRole(idPanel: number, role: number): Observable<IPanelRole> {
        return this.http.post<IPanelRole>(`${environment.apiUrl}wg-panel-roles`, {
            idPanel: idPanel,
            idRole: role
        });
    }

    addGroup(idPanel: number, group: number): Observable<IPanelGroup> {
        return this.http.post<IPanelGroup>(`${environment.apiUrl}wg-panel-groups`, {
            idPanel: idPanel,
            idGroup: group
        });
    }

    deleteRole(idPanel: number, idRole: number): Observable<boolean> {
        return this.http.delete<boolean>(`${environment.apiUrl}wg-panel-roles/${idPanel}/${idRole}`, {});
    }

    deleteGroup(idPanel: number, idGroup: number) {
        return this.http.delete<boolean>(`${environment.apiUrl}wg-panel-groups/${idPanel}/${idGroup}`, {});
    }

}
