import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { IPanel } from '../panel.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { WebGisState } from '@app/redux/global.reducer';
import { PanelService } from '../panel.service';
import { EditPanel } from '@app/redux/panels/panel.actions';

@Component({
    selector: 'app-panel-edit',
    templateUrl: './panel-edit.component.html',
    styleUrls: ['./panel-edit.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PanelEditComponent implements OnInit {

    actualPanel: IPanel;
    panelForm: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<PanelEditComponent>,
        @Inject(MAT_DIALOG_DATA) public panel: IPanel,
        private formBuilder: FormBuilder,
        private panelService: PanelService,
        private store: Store<WebGisState>,
    ) {
        this.actualPanel = this.panel;
        this.panelForm = this.formBuilder.group({
            id: ['', Validators.required],
            order: ['', Validators.required],
            title: ['', Validators.required],
            side: ['', Validators.required],
        });
        this.panelForm.controls['id'].disable();
        this.panelForm.controls['title'].disable();
    }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    save() {
        this.panelService.update(this.actualPanel)
            .subscribe((panel: IPanel) => {
                this.store.dispatch(new EditPanel(panel));
                this.dialogRef.close();
            });
    }
}
