import { Component, OnInit, Inject, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';
import {
    FormGroup,
    FormControl
} from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';

import { environment } from '@environment/environment';

import { WebGisState } from '@app/redux/global.reducer';
import { PanelService } from '../panel.service';
import { IPanel } from '../panel.model';
import { EditPanel } from '@app/redux/panels/panel.actions';
import { IRole } from '../../role/role.model';
import { IGroup } from '../../group/group.model';

@Component({
    selector: 'app-panel-permissions',
    templateUrl: './panel-permissions.component.html',
    styleUrls: ['./panel-permissions.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PanelPermissionsComponent implements OnInit {

    roles: IRole[] = [];
    groups: IGroup[] = [];

    checkboxGroupRoles = new FormGroup({});
    checkboxGroupGroups = new FormGroup({});

    allowModifiedTarget = false;

    constructor(
        private panelService: PanelService,
        public dialogRef: MatDialogRef<PanelPermissionsComponent>,
        @Inject(MAT_DIALOG_DATA) public panel: IPanel,
        private snackBar: MatSnackBar,
        private translateService: TranslateService,
        private cd: ChangeDetectorRef,
        private store: Store<WebGisState>,
    ) {
        const promises: Promise<IRole[] | IGroup[]>[] = [];
        promises.push(this.panelService.getRoles().toPromise());
        promises.push(this.panelService.getGroups().toPromise());

        Promise.all([...promises])
            .then((resultMaster) => {
                this.roles = resultMaster[0];
                this.groups = resultMaster[1];

                this.roles.forEach((roleList) => {
                    const roleRetrieved = panel.wgRoles.find((role) => {
                        return (role.id === roleList.id);
                    });
                    this.checkboxGroupRoles.addControl(
                        roleList.id.toString(),
                        new FormControl((!!roleRetrieved && !!roleRetrieved.id))
                    );
                });

                this.groups.forEach((groupList) => {
                    const groupRetrieved = panel.wgGroups.find((group) => {
                        return (group.id === groupList.id);
                    });
                    this.checkboxGroupGroups.addControl(
                        groupList.id.toString(),
                        new FormControl((!!groupRetrieved && !!groupRetrieved.id))
                    );
                });

                this.cd.markForCheck();

            })
            .catch(() => {
                this.snackBar.open(this.translateService.instant('map.published.error.master'), null, {
                    duration: environment.toast.duration,
                    verticalPosition: <MatSnackBarVerticalPosition>environment.toast.verticalPosition,
                    horizontalPosition: <MatSnackBarHorizontalPosition>environment.toast.horizontalPosition
                });
            });
    }

    ngOnInit() {
    }

    trackById(index, item) {
        return item.id;
    }

    updateTargetRoles(event, role: IRole) {
        if (event.checked) {
            this.panelService.addRole(this.panel.id, role.id)
                .subscribe(() => {
                    this.panel.wgRoles.push(role);
                    this.store.dispatch(new EditPanel(this.panel));
                });
        } else {
            this.panelService.deleteRole(this.panel.id, role.id)
                .subscribe(() => {
                    this.panel.wgRoles.forEach((roleItem, idx) => {
                        if (role.id === roleItem.id) {
                            this.panel.wgRoles.splice(idx, 1);
                        }
                    });
                    this.store.dispatch(new EditPanel(this.panel));
                });
        }
    }

    updateTargetGroups(event, group: IGroup) {
        if (event.checked) {
            this.panelService.addGroup(this.panel.id, group.id)
                .subscribe(() => {
                    this.panel.wgGroups.push(group);
                    this.store.dispatch(new EditPanel(this.panel));
                });
        } else {
            this.panelService.deleteGroup(this.panel.id, group.id)
                .subscribe(() => {
                    this.panel.wgGroups.forEach((groupItem, idx) => {
                        if (group.id === groupItem.id) {
                            this.panel.wgGroups.splice(idx, 1);
                        }
                    });
                    this.store.dispatch(new EditPanel(this.panel));
                });
        }
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
