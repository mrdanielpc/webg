import { Component, OnInit, ChangeDetectionStrategy, ViewChild, ChangeDetectorRef, OnDestroy } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { take, filter } from 'rxjs/operators';

import { IField, IFilter } from '@app/components/filter-table/filter-table.model';
import { PanelService } from '../panel.service';
import { WebGisState } from '@app/redux/global.reducer';
import { LoadPanels } from '@app/redux/panels/panel.actions';
import { PanelEditComponent } from '../panel-edit/panel-edit.component';
import { IPanel } from '../panel.model';
import { PanelPermissionsComponent } from '../panel-permissions/panel-permissions.component';

@Component({
    selector: 'app-panel-list',
    templateUrl: './panel-list.component.html',
    styleUrls: ['./panel-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class PanelListComponent implements OnInit, OnDestroy {
    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

    displayedColumns = ['id', 'title', 'side', 'order'];
    dataSource: MatTableDataSource<IPanel>;
    fieldsSearch: IField[] = [];

    selectedValue: string;

    obsPanels: Subscription;
    evtDialog: Subscription;

    constructor(
        private panelService: PanelService,
        private translateService: TranslateService,
        private dialog: MatDialog,
        private store: Store<WebGisState>,
        private cd: ChangeDetectorRef,
    ) {
        this.fieldsSearch = [
            {
                value: 'id',
                viewValue: this.translateService.instant('panel.fields.id'),
                defaultFilter: false
            },
            {
                value: 'title',
                viewValue: this.translateService.instant('panel.fields.title'),
                defaultFilter: false
            },
            {
                value: 'order',
                viewValue: this.translateService.instant('panel.fields.order'),
                defaultFilter: false
            },
            {
                value: 'side',
                viewValue: this.translateService.instant('panel.fields.side'),
                defaultFilter: false
            }
        ];
    }

    ngOnInit() {
        // this.selectedValue = null;
        this.dataSource = new MatTableDataSource();
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.changeFilterField(this.selectedValue);
        this.observePanels();
        this.search();
    }

    ngOnDestroy() {
        if (!!this.evtDialog) {
            this.evtDialog.unsubscribe();
        }
        this.obsPanels.unsubscribe();
        this.store.dispatch(new LoadPanels([]));
    }

    private observePanels() {
        this.obsPanels = this.store.select('webgis', 'panels', 'panels')
            .pipe(filter((result: IPanel[]) => {
                return (result.length > 0);
            }))
            .subscribe((roles) => {
                this.dataSource.data = roles;
                this.cd.markForCheck();
            });
    }

    private search() {
        this.panelService.get()
            .pipe(take(1))
            .subscribe((panels) => {
                this.store.dispatch(new LoadPanels(panels));
            });
    }

    private changeFilterField(valueSearch: string) {
        if (!!valueSearch) {
            this.dataSource.filterPredicate = function (data, value: string): boolean {
                return (!!data[valueSearch] && data[valueSearch].toString().toLocaleLowerCase().includes(value));
            };
        } else {
            this.dataSource.filterPredicate = function (data, value: string): boolean {
                let containsValue = false;
                Object.keys(data).forEach((item) => {
                    if (!!data[item]) {
                        if (data[item].toString().trim().toLocaleLowerCase().includes(value)) {
                            containsValue = true;
                        }
                    } else {
                        if (!containsValue) {
                            containsValue = (data[item] === value);
                        }
                    }
                });
                return containsValue;
            };
        }
    }

    applyFilter(data: IFilter) {
        this.selectedValue = data.field;
        this.changeFilterField(data.value);
        this.dataSource.filter = data.field.trim().toLowerCase();
    }

    edit(panel: IPanel) {
        this.dialog.open(PanelEditComponent, {
            width: '700px',
            data: panel
        });
    }

    managePermissions(panel: IPanel) {
        const dialogRef = this.dialog.open(PanelPermissionsComponent, {
            width: '700px',
            data: panel
        });
    }

}
