import { IGroup } from '../group/group.model';
import { IRole } from '../role/role.model';

export enum PanelType {
    LAYER = 'layer',
    TOOL = 'tool',
    MARKER = 'marker',
    MEDIA = 'media'
}

export enum PanelSide {
    LEFT = 'LEFT',
    RIGHT = 'RIGHT'
}

interface IPanelApi {
    id: number;
    order: number;
    title: PanelType;
    side: PanelSide;
    wgGroups: IGroup[];
    wgRoles: IRole[];
}

export interface IPanel extends IPanelApi {
    isFixed?: boolean;
    icon: string;
    typeSelected: boolean;
    typeSelectedUrl: string;
    typeSelectedIcon?: string;
}

export interface IPanelRole {
    idPanel: number;
    idRole: number;
}

export interface IPanelGroup {
    idPanel: number;
    idGroup: number;
}
