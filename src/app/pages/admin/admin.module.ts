import { NgModule } from '@angular/core';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatOptionModule } from '@angular/material/core';
import { MatTooltipModule } from '@angular/material/tooltip';

import { ComponentsModule } from '@app/components/components.module';
import { GroupAddComponent } from './group/group-add/group-add.component';
import { RoleAddComponent } from './role/role-add/role-add.component';
import { PanelListComponent } from './panel/panel-list/panel-list.component';
import { PanelEditComponent } from './panel/panel-edit/panel-edit.component';
import { MarkerTypeListComponent } from './marker-type/marker-type-list/marker-type-list.component';
import { MarkerTypeEditComponent } from './marker-type/marker-type-edit/marker-type-edit.component';
import { MarkerTypeAddComponent } from './marker-type/marker-type-add/marker-type-add.component';
import { MarkerTypePermissionsComponent } from './marker-type/marker-type-permissions/marker-type-permissions.component';
import { PanelPermissionsComponent } from './panel/panel-permissions/panel-permissions.component';
import { AdminRoutingModule } from './admin.routes';
import { RoleListComponent } from './role/role-list/role-list.component';
import { RoleEditComponent } from './role/role-edit/role-edit.component';
import { GroupListComponent } from './group/group-list/group-list.component';
import { GroupEditComponent } from './group/group-edit/group-edit.component';

const MATERIAL_MODULES = [
    MatSortModule,
    MatPaginatorModule,
    MatTableModule,
    MatSelectModule,
    MatOptionModule,
    MatTooltipModule,
    MatCheckboxModule,
];

@NgModule({
    imports: [
        AdminRoutingModule,
        ComponentsModule,
        MATERIAL_MODULES,
    ],
    exports: [
        ComponentsModule,
        MarkerTypePermissionsComponent,
        PanelPermissionsComponent,
    ],
    declarations: [
        RoleListComponent,
        RoleEditComponent,
        GroupListComponent,
        GroupEditComponent,
        RoleEditComponent,
        GroupAddComponent,
        RoleAddComponent,
        PanelListComponent,
        PanelEditComponent,
        MarkerTypeListComponent,
        MarkerTypeEditComponent,
        MarkerTypeAddComponent,
        MarkerTypePermissionsComponent,
        PanelPermissionsComponent,
    ],
    entryComponents: [
        GroupEditComponent,
        RoleEditComponent,
        GroupAddComponent,
        RoleAddComponent,
        MarkerTypeEditComponent,
        MarkerTypeAddComponent,
        PanelEditComponent,
        MarkerTypePermissionsComponent,
        PanelPermissionsComponent,
    ],
    providers: [

    ]
})

export class AdminModule { }
