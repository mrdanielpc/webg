import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import { RoleService } from '../role.service';
import { WebGisState } from '@app/redux/global.reducer';
import { IRole } from '../role.model';
import { AddRole } from '@app/redux/roles/role.actions';

@Component({
    selector: 'app-role-add',
    templateUrl: './role-add.component.html',
    styleUrls: ['./role-add.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoleAddComponent implements OnInit {

    actualRole: IRole = {
        id: null,
        name: null,
        title: null
    };
    roleForm: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<RoleAddComponent>,
        private formBuilder: FormBuilder,
        private roleService: RoleService,
        private store: Store<WebGisState>,
    ) {
        this.roleForm = this.formBuilder.group({
            title: ['', Validators.required],
            name: ['', Validators.required],
        });

    }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    save() {
        this.roleService.add(this.actualRole)
            .subscribe((group: IRole) => {
                this.store.dispatch(new AddRole(group));
                this.dialogRef.close();
            });
    }

}
