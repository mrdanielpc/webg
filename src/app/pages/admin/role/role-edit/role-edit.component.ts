import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';

import { RoleService } from '../role.service';
import { IRole } from '../role.model';
import { WebGisState } from '@app/redux/global.reducer';
import { EditRole } from '@app/redux/roles/role.actions';

@Component({
    selector: 'app-role-edit',
    templateUrl: './role-edit.component.html',
    styleUrls: ['./role-edit.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoleEditComponent implements OnInit {

    actualRole: IRole;
    roleForm: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<RoleEditComponent>,
        @Inject(MAT_DIALOG_DATA) public role: IRole,
        private formBuilder: FormBuilder,
        private roleService: RoleService,
        private store: Store<WebGisState>,
    ) {
        this.actualRole = this.role;
        this.roleForm = this.formBuilder.group({
            id: [''],
            title: ['', Validators.required],
            name: ['', Validators.required],
        });
        this.roleForm.controls['id'].disable();
    }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    save() {
        this.roleService.update(this.actualRole)
            .subscribe((group: IRole) => {
                this.store.dispatch(new EditRole(group));
                this.dialogRef.close();
            });
    }

}
