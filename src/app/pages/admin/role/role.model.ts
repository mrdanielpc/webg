export interface IRole {
    id: number;
    name: string;
    title: string;
}
