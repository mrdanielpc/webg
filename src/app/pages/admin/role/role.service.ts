import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environment/environment';
import { IRole } from './role.model';

@Injectable({
    providedIn: 'root'
})
export class RoleService {

    constructor(
        private http: HttpClient,
    ) {
    }

    get(): Observable<IRole[]> {
        return this.http.get<IRole[]>(environment.apiUrl + 'wg-roles');
    }

    update(role: IRole): Observable<IRole> {
        return this.http.put<IRole>(environment.apiUrl + 'wg-roles', role);
    }

    add(group: IRole): Observable<IRole> {
        return this.http.post<IRole>(environment.apiUrl + 'wg-roles', group);
    }

    delete(role: IRole): Observable<IRole> {
        return this.http.delete<IRole>(environment.apiUrl + 'wg-roles/' + role.id);
    }

}
