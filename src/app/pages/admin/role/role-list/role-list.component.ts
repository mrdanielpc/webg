import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    OnDestroy,
    ViewChild,
    ChangeDetectorRef
} from '@angular/core';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { IRole } from '../role.model';
import { Subscription } from 'rxjs';
import { LoadRoles, RemoveRole } from '@app/redux/roles/role.actions';
import { filter, take } from 'rxjs/operators';
import { RoleEditComponent } from '../role-edit/role-edit.component';
import { IFilter, IField } from '@app/components/filter-table/filter-table.model';
import { RoleService } from '../role.service';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { WebGisState } from '@app/redux/global.reducer';
import { Store } from '@ngrx/store';
import { RoleAddComponent } from '../role-add/role-add.component';
import { ConfirmDialogComponent } from '@app/components/confirm/confirm.component';

@Component({
    selector: 'app-role-list',
    templateUrl: './role-list.component.html',
    styleUrls: ['./role-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoleListComponent implements OnInit, OnDestroy {

    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

    displayedColumns = ['id', 'title', 'name'];
    dataSource: MatTableDataSource<IRole>;
    fieldsSearch: IField[] = [];

    selectedValue: string;

    obsRoles: Subscription;
    evtDialog: Subscription;

    constructor(
        private roleService: RoleService,
        private translateService: TranslateService,
        private dialog: MatDialog,
        private store: Store<WebGisState>,
        private cd: ChangeDetectorRef,
    ) {
        this.fieldsSearch = [
            {
                value: 'id',
                viewValue: this.translateService.instant('webgisApp.role.fields.id'),
                defaultFilter: false
            },
            {
                value: 'title',
                viewValue: this.translateService.instant('webgisApp.role.fields.title'),
                defaultFilter: false
            },
            {
                value: 'name',
                viewValue: this.translateService.instant('webgisApp.role.fields.name'),
                defaultFilter: false
            }
        ];
    }

    ngOnInit() {
        // this.selectedValue = null;

        this.dataSource = new MatTableDataSource();
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.changeFilterField(this.selectedValue);
        this.observeRoles();
        this.search();
    }

    ngOnDestroy() {
        if (!!this.evtDialog) {
            this.evtDialog.unsubscribe();
        }
        this.obsRoles.unsubscribe();
        this.store.dispatch(new LoadRoles([]));
    }

    private observeRoles() {
        this.obsRoles = this.store.select('webgis', 'roles', 'roles')
            .pipe(filter((result: IRole[]) => {
                return (result.length > 0);
            }))
            .subscribe((roles) => {
                this.dataSource.data = roles;
                this.cd.markForCheck();
            });
    }

    private search() {
        this.roleService.get()
            .pipe(take(1))
            .subscribe((groups) => {
                this.store.dispatch(new LoadRoles(groups));
            });
    }

    private changeFilterField(valueSearch: string) {
        if (!!valueSearch) {
            this.dataSource.filterPredicate = function (data, value: string): boolean {
                return (!!data[valueSearch] && data[valueSearch].toString().toLocaleLowerCase().includes(value));
            };
        } else {
            this.dataSource.filterPredicate = function (data, value: string): boolean {
                let containsValue = false;
                Object.keys(data).forEach((item) => {
                    if (!!data[item]) {
                        if (data[item].toString().trim().toLocaleLowerCase().includes(value)) {
                            containsValue = true;
                        }
                    } else {
                        if (!containsValue) {
                            containsValue = (data[item] === value);
                        }
                    }
                });
                return containsValue;
            };
        }
    }

    applyFilter(data: IFilter) {
        this.selectedValue = data.field;
        this.changeFilterField(data.value);
        this.dataSource.filter = data.field.trim().toLowerCase();
    }

    edit(role: IRole) {
        this.dialog.open(RoleEditComponent, {
            width: '700px',
            data: role
        });
    }

    delete(role: IRole) {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: this.translateService.instant('webgisApp.role.home.removeLabel'),
                question: this.translateService.instant('webgisApp.role.delete.question', { id: role.title }),
                positive: 'entity.action.remove'
            }
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (!!result) {
                const _result = JSON.parse(result);
                if (_result === true) {
                    this.roleService.delete(role)
                        .pipe(take(1))
                        .subscribe(() => {
                            this.store.dispatch(new RemoveRole(role));
                        });
                }
            }
        });
    }

    add() {
        this.dialog.open(RoleAddComponent, {
            width: '700px'
        });
    }

}

