import { Component, OnInit, ChangeDetectionStrategy, Inject, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Store } from '@ngrx/store';

import { IMarkerType } from '../marker-type.model';
import { EditMarkerType } from '@app/redux/marker-types/marker-type.actions';
import { MarkerTypeService } from '../marker-type.service';
import { WebGisState } from '@app/redux/global.reducer';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
    selector: 'app-marker-type-edit',
    templateUrl: './marker-type-edit.component.html',
    styleUrls: ['./marker-type-edit.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MarkerTypeEditComponent implements OnInit {
    actualMarkerType: IMarkerType;
    markerTypeForm: FormGroup;
    newIconSrc: string;

    constructor(
        public dialogRef: MatDialogRef<MarkerTypeEditComponent>,
        @Inject(MAT_DIALOG_DATA) public markerType: IMarkerType,
        private formBuilder: FormBuilder,
        private markerTypeService: MarkerTypeService,
        private store: Store<WebGisState>,
        private sanitizer: DomSanitizer,
        private cd: ChangeDetectorRef,
    ) {
        this.actualMarkerType = { ...this.markerType };
        this.markerTypeForm = this.formBuilder.group({
            id: [this.actualMarkerType.id, Validators.required],
            title: [this.actualMarkerType.title, Validators.required],
            icon: [this.actualMarkerType.icon, Validators.required],
            iconContentType: [this.actualMarkerType.iconContentType, Validators.required],
            observations: [this.actualMarkerType.observations, Validators.required],
            offsetX: [this.actualMarkerType.offsetX, Validators.required],
            offsetY: [this.actualMarkerType.offsetY, Validators.required],
        });
        this.markerTypeForm.controls['id'].disable();

    }

    ngOnInit() {
    }

    onFileChange(event) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);

            reader.onload = (data: any) => {
                const base64 = data.currentTarget.result;
                // const contentType = base64.substring(base64.indexOf(':') + 1, base64.indexOf(';'));
                const contentType = this.markerTypeService.getContentType(base64);

                this.actualMarkerType.icon = base64;
                this.markerTypeForm.controls['icon'].setValue(base64);
                this.newIconSrc = base64;

                this.actualMarkerType.iconContentType = contentType;
                this.markerTypeForm.controls['iconContentType'].setValue(contentType);

                this.cd.markForCheck();
            };
        }
    }

    getIcon(icon: string, contentType: string) {
        return this.sanitizer.bypassSecurityTrustResourceUrl('data:' + contentType + ';base64,' + icon);
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    save() {
        const editedMarker = { ...this.actualMarkerType };
        editedMarker.icon = editedMarker.icon.substring(editedMarker.icon.indexOf(',') + 1);
        this.markerTypeService.update(editedMarker)
            .subscribe((markerType: IMarkerType) => {
                this.store.dispatch(new EditMarkerType(markerType));
                this.dialogRef.close();
            });
    }

}
