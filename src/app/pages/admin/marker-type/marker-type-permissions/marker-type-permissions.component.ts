import { Component, OnInit, Inject, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarVerticalPosition, MatSnackBarHorizontalPosition } from '@angular/material/snack-bar';
import {
    FormGroup,
    FormControl
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { TranslateService } from '@ngx-translate/core';
import { environment } from '@environment/environment';

import { MarkerTypeService } from '../marker-type.service';
import { IMarkerType } from '../marker-type.model';
import { EditMarkerType } from '@app/redux/marker-types/marker-type.actions';
import { WebGisState } from '@app/redux/global.reducer';
import { IRole } from '../../role/role.model';
import { IGroup } from '../../group/group.model';


@Component({
    selector: 'app-marker-type-permissions',
    templateUrl: './marker-type-permissions.component.html',
    styleUrls: ['./marker-type-permissions.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MarkerTypePermissionsComponent implements OnInit {

    roles: IRole[] = [];
    groups: IGroup[] = [];

    checkboxGroupRoles = new FormGroup({});
    checkboxGroupGroups = new FormGroup({});

    allowModifiedTarget = false;

    constructor(
        private markerTypeService: MarkerTypeService,
        public dialogRef: MatDialogRef<MarkerTypePermissionsComponent>,
        @Inject(MAT_DIALOG_DATA) public markerType: IMarkerType,
        private snackBar: MatSnackBar,
        private translateService: TranslateService,
        private cd: ChangeDetectorRef,
        private store: Store<WebGisState>,
    ) {
        const promises: Promise<IRole[] | IGroup[]>[] = [];
        promises.push(this.markerTypeService.getRoles().toPromise());
        promises.push(this.markerTypeService.getGroups().toPromise());

        Promise.all([...promises])
            .then((resultMaster) => {
                this.roles = resultMaster[0];
                this.groups = resultMaster[1];

                this.roles.forEach((roleList) => {
                    const roleRetrieved = markerType.wgRoles.find((role) => {
                        return (role.id === roleList.id);
                    });
                    this.checkboxGroupRoles.addControl(
                        roleList.id.toString(),
                        new FormControl((!!roleRetrieved && !!roleRetrieved.id))
                    );
                });

                this.groups.forEach((groupList) => {
                    const groupRetrieved = markerType.wgGroups.find((group) => {
                        return (group.id === groupList.id);
                    });
                    this.checkboxGroupGroups.addControl(
                        groupList.id.toString(),
                        new FormControl((!!groupRetrieved && !!groupRetrieved.id))
                    );
                });

                this.cd.markForCheck();

            })
            .catch(() => {
                this.snackBar.open(this.translateService.instant('map.published.error.master'), null, {
                    duration: environment.toast.duration,
                    verticalPosition: <MatSnackBarVerticalPosition>environment.toast.verticalPosition,
                    horizontalPosition: <MatSnackBarHorizontalPosition>environment.toast.horizontalPosition
                });
            });
    }

    ngOnInit() {
    }

    trackById(index, item) {
        return item.id;
    }

    updateTargetRoles(event, role: IRole) {
        if (event.checked) {
            this.markerTypeService.addRole(this.markerType.id, role.id)
                .subscribe(() => {
                    this.markerType.wgRoles.push(role);
                    this.store.dispatch(new EditMarkerType(this.markerType));
                });
        } else {
            this.markerTypeService.deleteRole(this.markerType.id, role.id)
                .subscribe(() => {
                    this.markerType.wgRoles.forEach((roleItem, idx) => {
                        if (role.id === roleItem.id) {
                            this.markerType.wgRoles.splice(idx, 1);
                        }
                    });
                    this.store.dispatch(new EditMarkerType(this.markerType));
                });
        }
    }

    updateTargetGroups(event, group: IGroup) {
        if (event.checked) {
            this.markerTypeService.addGroup(this.markerType.id, group.id)
                .subscribe(() => {
                    this.markerType.wgGroups.push(group);
                    this.store.dispatch(new EditMarkerType(this.markerType));
                });
        } else {
            this.markerTypeService.deleteGroup(this.markerType.id, group.id)
                .subscribe(() => {
                    this.markerType.wgGroups.forEach((groupItem, idx) => {
                        if (group.id === groupItem.id) {
                            this.markerType.wgGroups.splice(idx, 1);
                        }
                    });
                    this.store.dispatch(new EditMarkerType(this.markerType));
                });
        }
    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
