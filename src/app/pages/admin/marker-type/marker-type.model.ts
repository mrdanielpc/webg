import { IGroup } from '../group/group.model';
import { IRole } from '../role/role.model';

export interface IMarkerType {
    id: number;
    title: string;
    icon: string;
    iconContentType: string;
    observations: string;
    offsetX: number;
    offsetY: number;

    wgGroups?: IGroup[];
    wgRoles?: IRole[];
}

export interface IMarkerTypeRole {
    idMarkerType: number;
    idRole: number;
}

export interface IMarkerTypeGroup {
    idMarkerType: number;
    idGroup: number;
}
