import {
    Component,
    OnInit,
    ChangeDetectionStrategy,
    ChangeDetectorRef,
    ViewChild,
    ElementRef
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import { DomSanitizer } from '@angular/platform-browser';

import { IMarkerType } from '../marker-type.model';
import { AddMarkerType } from '@app/redux/marker-types/marker-type.actions';
import { MarkerTypeService } from '../marker-type.service';
import { WebGisState } from '@app/redux/global.reducer';

@Component({
    selector: 'app-marker-type-add',
    templateUrl: './marker-type-add.component.html',
    styleUrls: ['./marker-type-add.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MarkerTypeAddComponent implements OnInit {

    @ViewChild('fileInput', { read: ElementRef, static: false }) private fileInput: ElementRef;

    actualMarkerType: IMarkerType = {
        icon: null,
        iconContentType: null,
        id: null,
        observations: null,
        offsetX: 0,
        offsetY: 0,
        title: null,
        wgGroups: [],
        wgRoles: []
    };
    markerTypeForm: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<MarkerTypeAddComponent>,
        private formBuilder: FormBuilder,
        private markerTypeService: MarkerTypeService,
        private store: Store<WebGisState>,
        private sanitizer: DomSanitizer,
        private cd: ChangeDetectorRef,
    ) {
        this.markerTypeForm = this.formBuilder.group({
            icon: ['', Validators.required],
            title: ['', Validators.required],
            iconContentType: ['', Validators.required],
            observations: ['', Validators.required],
            offsetX: ['', Validators.required],
            offsetY: ['', Validators.required],
        });

    }

    ngOnInit() {
    }

    onFileChange(event) {
        const reader = new FileReader();

        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);

            reader.onload = (data: any) => {
                const base64 = data.currentTarget.result;
                const contentType = base64.substring(base64.indexOf(':') + 1, base64.indexOf(';'));

                this.actualMarkerType.icon = base64;
                this.markerTypeForm.controls['icon'].setValue(base64);

                this.actualMarkerType.iconContentType = contentType;
                this.markerTypeForm.controls['iconContentType'].setValue(contentType);

                this.cd.markForCheck();
            };
        }
    }

    getIcon(icon: string, contentType: string) {
        return this.sanitizer.bypassSecurityTrustResourceUrl('data:' + contentType + ';base64,' + icon);
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    save() {
        const newMarker = this.actualMarkerType;
        newMarker.icon = newMarker.icon.substring(newMarker.icon.indexOf(',') + 1);
        this.markerTypeService.add(this.actualMarkerType)
            .subscribe((markerType: IMarkerType) => {
                this.store.dispatch(new AddMarkerType(markerType));
                this.dialogRef.close();
            });
    }


}
