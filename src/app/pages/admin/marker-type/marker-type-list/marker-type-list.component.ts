import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ViewChild, ChangeDetectorRef } from '@angular/core';
import { MarkerTypeEditComponent } from '../marker-type-edit/marker-type-edit.component';
import { IMarkerType } from '../marker-type.model';
import { LoadMarkerTypes, RemoveMarkerType } from '@app/redux/marker-types/marker-type.actions';

import { MarkerTypeAddComponent } from '../marker-type-add/marker-type-add.component';
import { MarkerTypeService } from '../marker-type.service';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { Store } from '@ngrx/store';
import { WebGisState } from '@app/redux/global.reducer';
import { filter, take } from 'rxjs/operators';
import { DomSanitizer } from '@angular/platform-browser';
import { IFilter, IField } from '@app/components/filter-table/filter-table.model';
import { ConfirmDialogComponent } from '@app/components/confirm/confirm.component';
import { MarkerTypePermissionsComponent } from '../marker-type-permissions/marker-type-permissions.component';

@Component({
    selector: 'app-marker-type-list',
    templateUrl: './marker-type-list.component.html',
    styleUrls: ['./marker-type-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class MarkerTypeListComponent implements OnInit, OnDestroy {

    @ViewChild(MatSort, { static: true }) sort: MatSort;
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

    displayedColumns = ['id', 'icon', 'title', 'iconContentType', 'observations', 'offsetX', 'offsetY'];
    dataSource: MatTableDataSource<IMarkerType>;
    fieldsSearch: IField[] = [];

    selectedValue: string;

    obsMarkerTypes: Subscription;
    evtDialog: Subscription;

    constructor(
        private markerTypeService: MarkerTypeService,
        private translateService: TranslateService,
        private dialog: MatDialog,
        private store: Store<WebGisState>,
        private cd: ChangeDetectorRef,
        private sanitizer: DomSanitizer,
    ) {
        this.fieldsSearch = [
            {
                value: 'id',
                viewValue: this.translateService.instant('webgisApp.markerType.fields.id'),
                defaultFilter: false
            },
            {
                value: 'icon',
                viewValue: this.translateService.instant('webgisApp.markerType.fields.icon'),
                defaultFilter: false
            },
            {
                value: 'title',
                viewValue: this.translateService.instant('webgisApp.markerType.fields.title'),
                defaultFilter: false
            },
            {
                value: 'iconContentType',
                viewValue: this.translateService.instant('webgisApp.markerType.fields.iconContentType'),
                defaultFilter: false
            },
            {
                value: 'observations',
                viewValue: this.translateService.instant('webgisApp.markerType.fields.observations'),
                defaultFilter: false
            },
            {
                value: 'offsetX',
                viewValue: this.translateService.instant('webgisApp.markerType.fields.offsetX'),
                defaultFilter: false
            },
            {
                value: 'offsetY',
                viewValue: this.translateService.instant('webgisApp.markerType.fields.offsetY'),
                defaultFilter: false
            }

        ];
    }

    ngOnInit() {
        // this.selectedValue = null;

        this.dataSource = new MatTableDataSource();
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.changeFilterField(this.selectedValue);
        this.observeRoles();
        this.search();
    }

    ngOnDestroy() {
        if (!!this.evtDialog) {
            this.evtDialog.unsubscribe();
        }
        this.obsMarkerTypes.unsubscribe();
        this.store.dispatch(new LoadMarkerTypes([]));
    }

    getIcon(icon: string, contentType: string) {
        return this.sanitizer.bypassSecurityTrustResourceUrl('data:' + contentType + ';base64,' + icon);
    }

    private observeRoles() {
        this.obsMarkerTypes = this.store.select('webgis', 'markerTypes', 'markerTypes')
            .pipe(filter((result: IMarkerType[]) => {
                return (result.length > 0);
            }))
            .subscribe((markerTypes) => {
                // markerTypes.forEach((markerType) => {
                //     const icon: any = this.getIcon(markerType.icon, markerType.iconContentType);
                //     markerType.base64 = icon.changingThisBreaksApplicationSecurity;
                // });
                // const markers = [...markerTypes];
                this.dataSource.data = markerTypes;
                this.cd.markForCheck();
            });
    }

    private search() {
        this.markerTypeService.get()
            .pipe(take(1))
            .subscribe((groups) => {
                this.store.dispatch(new LoadMarkerTypes(groups));
            });
    }

    private changeFilterField(valueSearch: string) {
        if (!!valueSearch) {
            this.dataSource.filterPredicate = function (data, value: string): boolean {
                return (!!data[valueSearch] && data[valueSearch].toString().toLocaleLowerCase().includes(value));
            };
        } else {
            this.dataSource.filterPredicate = function (data, value: string): boolean {
                let containsValue = false;
                Object.keys(data).forEach((item) => {
                    if (!!data[item]) {
                        if (data[item].toString().trim().toLocaleLowerCase().includes(value)) {
                            containsValue = true;
                        }
                    } else {
                        if (!containsValue) {
                            containsValue = (data[item] === value);
                        }
                    }
                });
                return containsValue;
            };
        }
    }

    applyFilter(data: IFilter) {
        this.selectedValue = data.field;
        this.changeFilterField(data.value);
        this.dataSource.filter = data.field.trim().toLowerCase();
    }

    edit(role: IMarkerType) {
        this.dialog.open(MarkerTypeEditComponent, {
            width: '700px',
            data: role
        });
    }

    add() {
        this.dialog.open(MarkerTypeAddComponent, {
            width: '700px'
        });
    }

    delete(role: IMarkerType) {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: this.translateService.instant('webgisApp.markerType.home.removeLabel'),
                question: this.translateService.instant('webgisApp.markerType.delete.question', { id: role.title }),
                positive: 'entity.action.remove'
            }
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (!!result) {
                const _result = JSON.parse(result);
                if (_result === true) {
                    this.markerTypeService.delete(role)
                        .pipe(take(1))
                        .subscribe(() => {
                            this.store.dispatch(new RemoveMarkerType(role));
                        });
                }
            }
        });
    }

    managePermissions(markerType: IMarkerType) {
        const dialogRef = this.dialog.open(MarkerTypePermissionsComponent, {
            width: '700px',
            data: markerType
        });
    }

}
