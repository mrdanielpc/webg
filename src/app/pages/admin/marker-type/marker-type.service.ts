import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environment/environment';
import { IMarkerType, IMarkerTypeRole, IMarkerTypeGroup } from './marker-type.model';
import { IRole } from '../role/role.model';
import { IGroup } from '../group/group.model';

@Injectable({
    providedIn: 'root'
})
export class MarkerTypeService {

    constructor(
        private http: HttpClient,
    ) {
    }

    get(): Observable<IMarkerType[]> {
        return this.http.get<IMarkerType[]>(environment.apiUrl + 'wg-marker-types');
    }

    update(marker: IMarkerType): Observable<IMarkerType> {
        return this.http.put<IMarkerType>(environment.apiUrl + 'wg-marker-types', marker);
    }

    add(marker: IMarkerType): Observable<IMarkerType> {
        return this.http.post<IMarkerType>(environment.apiUrl + 'wg-marker-types', marker);
    }

    delete(marker: IMarkerType): Observable<IMarkerType> {
        return this.http.delete<IMarkerType>(environment.apiUrl + 'wg-marker-types/' + marker.id);
    }

    getContentType(base64: string): string {
        return base64.substring(base64.indexOf(':') + 1, base64.indexOf(';'));
    }

    getRoles(): Observable<IRole[]> {
        return this.http.get<IRole[]>(environment.apiUrl + 'wg-roles');
    }

    getGroups(): Observable<IGroup[]> {
        return this.http.get<IGroup[]>(environment.apiUrl + 'wg-groups');
    }

    addRole(idMarkerType: number, role: number): Observable<IMarkerTypeRole> {
        return this.http.post<IMarkerTypeRole>(`${environment.apiUrl}wg-markertype-roles`, {
            idMarkerType: idMarkerType,
            idRole: role
        });
    }

    addGroup(idMarkerType: number, group: number): Observable<IMarkerTypeGroup> {
        return this.http.post<IMarkerTypeGroup>(`${environment.apiUrl}wg-markertype-groups`, {
            idMarkerType: idMarkerType,
            idGroup: group
        });
    }

    deleteRole(idMarkerType: number, idRole: number): Observable<boolean> {
        return this.http.delete<boolean>(`${environment.apiUrl}wg-markertype-roles/${idMarkerType}/${idRole}`, {});
    }

    deleteGroup(idMarkerType: number, idGroup: number) {
        return this.http.delete<boolean>(`${environment.apiUrl}wg-markertype-groups/${idMarkerType}/${idGroup}`, {});
    }

}
