import { Component, OnInit, ChangeDetectionStrategy, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { IGroup } from '../group.model';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { GroupService } from '../group.service';
import { EditGroup } from '@app/redux/groups/group.actions';
import { WebGisState } from '@app/redux/global.reducer';
import { Store } from '@ngrx/store';

@Component({
    selector: 'app-group-edit',
    templateUrl: './group-edit.component.html',
    styleUrls: ['./group-edit.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupEditComponent implements OnInit {

    actualGroup: IGroup;
    groupForm: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<GroupEditComponent>,
        @Inject(MAT_DIALOG_DATA) private group: IGroup,
        private formBuilder: FormBuilder,
        private groupService: GroupService,
        private store: Store<WebGisState>,
    ) {
        this.actualGroup = this.group;
        this.groupForm = this.formBuilder.group({
            id: [''],
            title: ['', Validators.required],
            name: ['', Validators.required],
        });
        this.groupForm.controls['id'].disable();
    }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    save() {
        this.groupService.update(this.actualGroup)
            .subscribe((group: IGroup) => {
                this.store.dispatch(new EditGroup(group));
                this.dialogRef.close();
            });
    }

}

