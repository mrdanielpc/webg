import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Store } from '@ngrx/store';

import { GroupService } from '../group.service';
import { WebGisState } from '@app/redux/global.reducer';
import { IGroup } from '../group.model';
import { AddGroup } from '@app/redux/groups/group.actions';

@Component({
    selector: 'app-group-add',
    templateUrl: './group-add.component.html',
    styleUrls: ['./group-add.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupAddComponent implements OnInit {

    actualGroup: IGroup = {
        id: null,
        name: null,
        title: null
    };
    groupForm: FormGroup;

    constructor(
        public dialogRef: MatDialogRef<GroupAddComponent>,
        private formBuilder: FormBuilder,
        private groupService: GroupService,
        private store: Store<WebGisState>,
    ) {
        this.groupForm = this.formBuilder.group({
            title: ['', Validators.required],
            name: ['', Validators.required],
        });
    }

    ngOnInit() {
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    save() {
        this.groupService.add(this.actualGroup)
            .subscribe((group: IGroup) => {
                this.store.dispatch(new AddGroup(group));
                this.dialogRef.close();
            });
    }

}
