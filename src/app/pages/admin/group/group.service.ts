import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environment/environment';
import { IGroup } from './group.model';

@Injectable({
    providedIn: 'root'
})
export class GroupService {

    constructor(
        private http: HttpClient,
    ) {
    }

    get(): Observable<IGroup[]> {
        return this.http.get<IGroup[]>(environment.apiUrl + 'wg-groups');
    }

    update(group: IGroup): Observable<IGroup> {
        return this.http.put<IGroup>(environment.apiUrl + 'wg-groups', group);
    }

    add(group: IGroup): Observable<IGroup> {
        return this.http.post<IGroup>(environment.apiUrl + 'wg-groups', group);
    }

    delete(group: IGroup): Observable<IGroup> {
        return this.http.delete<IGroup>(environment.apiUrl + 'wg-groups/' + group.id);
    }
}
