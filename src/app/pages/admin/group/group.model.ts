export interface IGroup {
    id: number;
    name: string;
    title: string;
}
