import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { LoadGroups, RemoveGroup } from '@app/redux/groups/group.actions';
import { IGroup } from '../group.model';
import { filter, take } from 'rxjs/operators';
import { GroupEditComponent } from '../group-edit/group-edit.component';
import { Subscription } from 'rxjs';
import { GroupService } from '../group.service';
import { TranslateService } from '@ngx-translate/core';
import { MatDialog } from '@angular/material/dialog';
import { WebGisState } from '@app/redux/global.reducer';
import { Store } from '@ngrx/store';
import { GroupAddComponent } from '../group-add/group-add.component';
// import { ConfirmDialogComponent } from '@app/core/components/confirm/confirm.component';
import { MatSort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { IFilter, IField } from '@app/components/filter-table/filter-table.model';
import { ConfirmDialogComponent } from '@app/components/confirm/confirm.component';

@Component({
    selector: 'app-group-list',
    templateUrl: './group-list.component.html',
    styleUrls: ['./group-list.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupListComponent implements OnInit, OnDestroy {

    @ViewChild(MatSort, {static: true}) sort: MatSort;
    @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

    displayedColumns = ['id', 'title', 'name'];
    dataSource: MatTableDataSource<IGroup>;
    fieldsSearch: IField[] = [];

    selectedValue: string;

    obsGroups: Subscription;
    evtDialog: Subscription;

    constructor(
        private groupService: GroupService,
        private translateService: TranslateService,
        private dialog: MatDialog,
        private store: Store<WebGisState>,
        private cd: ChangeDetectorRef,
    ) {
        this.fieldsSearch = [
            {
                value: 'id',
                viewValue: this.translateService.instant('webgisApp.group.fields.id'),
                defaultFilter: false
            },
            {
                value: 'title',
                viewValue: this.translateService.instant('webgisApp.group.fields.title'),
                defaultFilter: false
            },
            {
                value: 'name',
                viewValue: this.translateService.instant('webgisApp.group.fields.name'),
                defaultFilter: false
            }
        ];
    }

    ngOnInit() {
        // this.selectedValue = null;
        this.dataSource = new MatTableDataSource();
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.changeFilterField(this.selectedValue);
        this.observeGroups();
        this.search();
    }

    ngOnDestroy() {
        if (!!this.evtDialog) {
            this.evtDialog.unsubscribe();
        }
        this.obsGroups.unsubscribe();
        this.store.dispatch(new LoadGroups([]));
    }

    private observeGroups() {
        this.obsGroups = this.store.select('webgis', 'groups', 'groups')
            .pipe(filter((result: IGroup[]) => {
                return (result.length > 0);
            }))
            .subscribe((groups) => {
                this.dataSource.data = groups;
                this.cd.markForCheck();
            });
    }

    private search() {
        this.groupService.get()
            .pipe(take(1))
            .subscribe((groups) => {
                this.store.dispatch(new LoadGroups(groups));
            });
    }

    private changeFilterField(valueSearch: string) {
        if (!!valueSearch) {
            this.dataSource.filterPredicate = function (data, value: string): boolean {
                return (!!data[valueSearch] && data[valueSearch].toString().toLocaleLowerCase().includes(value));
            };
        } else {
            this.dataSource.filterPredicate = function (data, value: string): boolean {
                let containsValue = false;
                Object.keys(data).forEach((item) => {
                    if (!!data[item]) {
                        if (data[item].toString().trim().toLocaleLowerCase().includes(value)) {
                            containsValue = true;
                        }
                    } else {
                        if (!containsValue) {
                            containsValue = (data[item] === value);
                        }
                    }
                });
                return containsValue;
            };
        }
    }

    applyFilter(data: IFilter) {
        this.selectedValue = data.field;
        this.changeFilterField(data.value);
        this.dataSource.filter = data.field.trim().toLowerCase();
    }

    edit(group: IGroup) {
        this.dialog.open(GroupEditComponent, {
            width: '700px',
            data: group
        });
    }

    add() {
        this.dialog.open(GroupAddComponent, {
            width: '700px'
        });
    }

    delete(group: IGroup) {
        const dialogRef = this.dialog.open(ConfirmDialogComponent, {
            data: {
                title: this.translateService.instant('webgisApp.group.home.removeLabel'),
                question: this.translateService.instant('webgisApp.group.delete.question', { id: group.title }),
                positive: 'entity.action.remove'
            }
        });
        dialogRef.afterClosed().subscribe((result) => {
            if (!!result) {
                const _result = JSON.parse(result);
                if (_result === true) {
                    this.groupService.delete(group)
                        .pipe(take(1))
                        .subscribe(() => {
                            this.store.dispatch(new RemoveGroup(group));
                        });
                }
            }
        });
    }

}
