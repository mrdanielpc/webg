import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PanelListComponent } from './panel/panel-list/panel-list.component';
import { MarkerTypeListComponent } from './marker-type/marker-type-list/marker-type-list.component';
import { RoleListComponent } from './role/role-list/role-list.component';
import { GroupListComponent } from './group/group-list/group-list.component';


const routes: Routes = [
    {
        path: 'role',
        children: [
            {
                path: '',
                redirectTo: 'list',
                pathMatch: 'full'
            },
            {
                path: 'list',
                component: RoleListComponent,
                data: {
                    pageTitle: 'global.menu.admin.main'
                }
            }
        ]
    },
    {
        path: 'group',
        children: [
            {
                path: '',
                redirectTo: 'list',
                pathMatch: 'full'
            },
            {
                path: 'list',
                component: GroupListComponent,
                data: {
                    pageTitle: 'global.menu.admin.main'
                }
            }
        ]
    },
    {
        path: 'panel',
        children: [
            {
                path: '',
                redirectTo: 'list',
                pathMatch: 'full'
            },
            {
                path: 'list',
                component: PanelListComponent,
                data: {
                    pageTitle: 'global.menu.admin.main'
                }
            }
        ]
    },
    {
        path: 'marker-type',
        children: [
            {
                path: '',
                redirectTo: 'list',
                pathMatch: 'full'
            },
            {
                path: 'list',
                component: MarkerTypeListComponent,
                data: {
                    pageTitle: 'global.menu.admin.main'
                }
            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [
        RouterModule,
    ]
})
export class AdminRoutingModule { }
