const gulp = require('gulp');
const merge = require('gulp-merge-json');
const childProcess = require('child_process');

const i18nFolder = './i18n/';
const deployFolder = '../../../target/www';

const languages = [
    'es',
    'gl',
    'en'
];

gulp.task('i18n', () => {
    return new Promise((resolve, reject) => {
        languages.forEach((lng) => {
            gulp.src(i18nFolder + lng + '**/*.json')
                .pipe(merge({
                    fileName: lng + '.json'
                }))
                .pipe(gulp.dest('src/i18n/'));
        });
        resolve();
    });
});

gulp.task('deploy', () => {
    return new Promise((resolve, reject) => {
        gulp.src([
            'dist/webgis/**/*'
        ]).pipe(gulp.dest(deployFolder));
        resolve();
    });
});

gulp.task('install:ww', () => {
    return new Promise((resolve, reject) => {
        childProcess.execSync('npm i', {
            cwd: './worldwind',
            stdio: [0, 1, 2]
        });
        resolve();
    });
});

gulp.task('override:ww', () => {
    return new Promise((resolve, reject) => {
        const sourceFiles = './webgisWorldWind/**';
        const targetFiles = './worldwind/src';
        gulp.src(sourceFiles, { base: './webgisWorldWind' })
            .pipe(gulp.dest(targetFiles))
            .on('error', reject)
            .on('end', resolve);
        resolve();
    });
});

gulp.task('build:ww', () => {
    return new Promise((resolve, reject) => {
        childProcess.execSync('npm run build', {
            cwd: './worldwind',
            stdio: [0, 1, 2]
        });
        resolve();
    });
});

gulp.task('copy:ww', () => {
    return new Promise((resolve, reject) => {
        const sourceFiles = [
            './worldwind/build/dist/worldwind.js',
            './worldwind/build/dist/worldwind.min.js'
        ];
        const targetFiles = './src/vendor';
        gulp.src(sourceFiles)
            .pipe(gulp.dest(targetFiles))
            .on('error', reject)
            .on('end', resolve);
        resolve();
    });
});

gulp.task('refresh:ww', () => {
    return new Promise((resolve, reject) => {
        childProcess.execSync('gulp build:ww && gulp copy:ww', {
            cwd: './worldwind',
            stdio: [0, 1, 2]
        });
        resolve();
    });
});
